Description

    Historically, the BLLIP Parser has be distributed with a version of
    the parser and reranker trained on an "AUX"ified version of the Penn
    Treebank where various verbs were tagged as AUX instead of their
    original taggings.  This model is made from a more traditional
    version of the WSJ Penn Treebank (version 2.0).

Parser

    Trained on WSJ (sections 2-21) and 2,036,950 self-trained sentences from Gigaword.
    The Gigaword sentences were parsed with a WSJ trained parser and reranker.

Reranker

    Trained on WSJ (sections 2-21), spfeatures

-- David McClosky (dmcc@cs.stanford.edu)
   1.29.2014
