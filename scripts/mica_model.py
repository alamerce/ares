#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Script for MICA modeling
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import subprocess
import argparse


#==============================================================================
# Parameters and useful function(s)
#==============================================================================

# Useful directories and references 
WD = "wd"
MICA_DIR = "mica/"
MICA_LIB = "lib"
MICA_WD = MICA_DIR + WD
MICA_EX_PARKING = "parking.def"


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    arg_parser = argparse.ArgumentParser(description="Semantic Transformation")   
    arg_parser.add_argument("input",
                            help="input document for working")
    arg_parser.add_argument("--verbose",
                            default=0,
                            type=int,
                            help="verbose: 0, 1, 2")    
    return arg_parser.parse_args()
    

def prepare_data():
    print("-- Preparation of work data")
    subprocess.run(["cp", "-r", WD, MICA_DIR]) # copying input file(s)
    os.chdir(MICA_DIR) # change directory
   
    
def generate_mica_code(args):
    print("-- Generation of Mica code")
    print("--- Using example parking (" + args.input + ")")
    subprocess.run(["cp", "def/" + MICA_EX_PARKING, WD])
   
    
def model_with_mica_env(args):
    print("-- Modeling with Mica Interactive Environment")
    os.system(MICA_LIB + "/ocaml_mica" +
              " -I " + MICA_LIB +
              " -init " + MICA_LIB + "/mica_init.ml" +
              " < " + args.input)
    os.system("exit 0")
    
    
def ending():
    print("-- Ending of CAMR Parsing")
    os.chdir("..") # change directory
    subprocess.run(["cp", "-r", MICA_WD, "."]) # copying result data    
        

#==============================================================================
# Main process
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    prepare_data()
    generate_mica_code(args)
    model_with_mica_env(args)
    ending()


