#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Prediction script (STOG Parsing)
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import subprocess
import argparse
from os.path import dirname, basename, splitext


#==============================================================================
# Parameters
#==============================================================================

# Useful directories and references 
WD = "wd"
STOG_DIR = "stog/"
STOG_WD = STOG_DIR + WD
CORENLP_DIR = "../corenlp/"
CORENLP_SERVER = "edu.stanford.nlp.pipeline.StanfordCoreNLPServer"
CORENLP_START_SCRIPT = "scripts/start_corenlp_server.sh"
CORENLP_END_SCRIPT = "scripts/stop_corenlp_server.sh"
STOG_ARCHIVE_FILE = "data/ckpt-amr-2.0"
STOG_WEIGHTS_FILE = "data/ckpt-amr-2.0/best.th"
STOG_UTILS_DIR = "data/amr_2.0_utils"
STOG_COMPOUND_FILE = STOG_UTILS_DIR + "/joints.txt"
STOG_AMR_PARSING = "stog.data.dataset_readers.amr_parsing."
STOG_ANNOTATOR = STOG_AMR_PARSING + "preprocess.feature_annotator"
STOG_INPUT_CLEANER = STOG_AMR_PARSING + "preprocess.input_cleaner"
STOG_RECATEGORIZOR = STOG_AMR_PARSING + "preprocess.recategorizer"
STOG_TEXT_ANONYMIZOR = STOG_AMR_PARSING + "preprocess.text_anonymizor"
STOG_SENSE_REMOVER = STOG_AMR_PARSING + "preprocess.sense_remover"
STOG_NODE_RESTORE = STOG_AMR_PARSING + "postprocess.node_restore"
STOG_PREDICT = "stog.commands.predict"


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    print("-- Argument checking")
    arg_parser = argparse.ArgumentParser(description="Semantic Transformation")   
    arg_parser.add_argument("input",
                            help="input document to annotate")
    args = arg_parser.parse_args()
    print("--- input: " + args.input)
    return args
    

def predict(args):
    print("-- Prediction (Main parsing stage)")
    output_file = dirname(args.input) + "/"
    output_file += splitext(basename(args.input))[0]
    output_file += ".pred"
    subprocess.run(
            ["python", "-u", "-m", 
             STOG_PREDICT, 
             "--archive-file", STOG_ARCHIVE_FILE, 
             "--weights-file", STOG_WEIGHTS_FILE, 
             "--input-file", args.input, 
             "--batch-size", str(32), 
             "--use-dataset-reader", 
             "--output-file", output_file, 
             "--silent", 
             "--beam-size", str(5), 
             "--predictor", "STOG"], 
            check=True, 
            universal_newlines=True)     

    
#==============================================================================
# Exécution
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    os.chdir(STOG_DIR) # change directory
    predict(args)