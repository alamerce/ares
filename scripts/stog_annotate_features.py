#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Features annotations script (STOG Parsing)
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import subprocess
import argparse
import time
from os.path import dirname, basename, splitext


#==============================================================================
# Parameters and useful function(s)
#==============================================================================

# Useful directories and references 
WD = "wd"
STOG_DIR = "stog/"
STOG_WD = STOG_DIR + WD
CORENLP_DIR = "../corenlp/"
CORENLP_SERVER = "edu.stanford.nlp.pipeline.StanfordCoreNLPServer"
CORENLP_START_SCRIPT = "scripts/start_corenlp_server.sh"
CORENLP_END_SCRIPT = "scripts/stop_corenlp_server.sh"
STOG_ARCHIVE_FILE = "data/ckpt-amr-2.0"
STOG_WEIGHTS_FILE = "data/ckpt-amr-2.0/best.th"
STOG_UTILS_DIR = "data/amr_2.0_utils"
STOG_COMPOUND_FILE = STOG_UTILS_DIR + "/joints.txt"
STOG_AMR_PARSING = "stog.data.dataset_readers.amr_parsing."
STOG_ANNOTATOR = STOG_AMR_PARSING + "preprocess.feature_annotator"
STOG_INPUT_CLEANER = STOG_AMR_PARSING + "preprocess.input_cleaner"
STOG_RECATEGORIZOR = STOG_AMR_PARSING + "preprocess.recategorizer"
STOG_TEXT_ANONYMIZOR = STOG_AMR_PARSING + "preprocess.text_anonymizor"
STOG_SENSE_REMOVER = STOG_AMR_PARSING + "preprocess.sense_remover"
STOG_NODE_RESTORE = STOG_AMR_PARSING + "postprocess.node_restore"
STOG_PREDICT = "stog.commands.predict"
    
# Default values
DEFAULT_INPUT = "*"
DEFAULT_PARSER = "stog"
DEFAULT_VERBOSE = 1
        
# Function to evaluate the verbose level
def is_verbose_level(verbose_level, expected_level):
    return verbose_level >= expected_level 

# Function to print a text according to verbose level
def verbose_print(args, level, text):
    indent = ""
    for i in range(level):
        indent += "-"
    if level > 0:
        indent += " "
    if is_verbose_level(args.verbose, level):
        print(indent + text)


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    arg_parser = argparse.ArgumentParser(description="Semantic Transformation")   
    arg_parser.add_argument("input",
                            help="input document to annotate")
    arg_parser.add_argument("--verbose",
                            default=DEFAULT_VERBOSE,
                            type=int,
                            help="verbose level: 0, 1, .. , 9")  
    args = arg_parser.parse_args()
    verbose_print(args, 4, "Argument checking")
    verbose_print(args, 5, "input: " + args.input)
    verbose_print(args, 5, "verbose: " + str(args.verbose))
    return args

    
def start_corenlp_server():
    verbose_print(args, 4, "Starting Core NLP server")
    if is_verbose_level(args.verbose, 6):
        subprocess.Popen(
                ["xterm", "-hold", 
                 "-e", "sh", CORENLP_START_SCRIPT, 
                 "&"])
    else:
        subprocess.Popen(
                ["xterm", 
                 "-e", "sh", CORENLP_START_SCRIPT, 
                 "&"])
    time.sleep(5)
    

def annotate_features(work_file):
    verbose_print(args, 4, "Feature annotations for AMR parsing")
    co_value = is_verbose_level(args.verbose, 6)
    subprocess.run(
            ["python", "-u", "-m", 
             STOG_ANNOTATOR, 
             work_file,
             "--compound_file", STOG_COMPOUND_FILE], 
             check=True, 
             capture_output = co_value,
             universal_newlines=True)    
    return work_file + ".features"
        
    
def stop_corenlp_server():
    verbose_print(args, 4, "Stopping Core NLP server")
    if is_verbose_level(args.verbose, 6):
        subprocess.Popen(
                ["xterm", "-hold", 
                 "-e", "sh", CORENLP_END_SCRIPT, 
                 "&"])
    else:
        subprocess.Popen(
                ["xterm", 
                 "-e", "sh", CORENLP_END_SCRIPT, 
                 "&"])
    
    
#==============================================================================
# Exécution
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    p_server = start_corenlp_server()
    os.chdir(STOG_DIR) # change directory
    work_file = args.input
    work_file = annotate_features(work_file)
    output_file = dirname(args.input) + "/"
    output_file += splitext(basename(args.input))[0]
    output_file += ".features"
    os.rename(work_file, output_file)
    os.chdir("..") # change directory
    stop_corenlp_server()