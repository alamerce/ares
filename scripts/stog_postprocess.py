#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Postprocessing script (STOG Parsing)
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import subprocess
import argparse
from os.path import dirname, basename, splitext


#==============================================================================
# Parameters
#==============================================================================

# Useful directories and references 
WD = "wd"
STOG_DIR = "stog/"
STOG_WD = STOG_DIR + WD
CORENLP_DIR = "../corenlp/"
CORENLP_SERVER = "edu.stanford.nlp.pipeline.StanfordCoreNLPServer"
CORENLP_START_SCRIPT = "scripts/start_corenlp_server.sh"
CORENLP_END_SCRIPT = "scripts/stop_corenlp_server.sh"
STOG_ARCHIVE_FILE = "data/ckpt-amr-2.0"
STOG_WEIGHTS_FILE = "data/ckpt-amr-2.0/best.th"
STOG_UTILS_DIR = "data/amr_2.0_utils"
STOG_COMPOUND_FILE = STOG_UTILS_DIR + "/joints.txt"
STOG_AMR_PARSING = "stog.data.dataset_readers.amr_parsing."
STOG_ANNOTATOR = STOG_AMR_PARSING + "preprocess.feature_annotator"
STOG_INPUT_CLEANER = STOG_AMR_PARSING + "preprocess.input_cleaner"
STOG_RECATEGORIZOR = STOG_AMR_PARSING + "preprocess.recategorizer"
STOG_TEXT_ANONYMIZOR = STOG_AMR_PARSING + "preprocess.text_anonymizor"
STOG_SENSE_REMOVER = STOG_AMR_PARSING + "preprocess.sense_remover"
STOG_NODE_RESTORE = STOG_AMR_PARSING + "postprocess.node_restore"
STOG_WIKIFICATION = STOG_AMR_PARSING + "postprocess.wikification"
STOG_EXPANDER = STOG_AMR_PARSING + "postprocess.expander"
STOG_MIN_EXPANDER = STOG_AMR_PARSING + "postprocess.minimal_expander"
STOG_PREDICT = "stog.commands.predict"


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    print("-- Argument checking")
    arg_parser = argparse.ArgumentParser(description="Semantic Transformation")   
    arg_parser.add_argument("input",
                            help="input document to annotate")
    args = arg_parser.parse_args()
    print("--- input: " + args.input)
    return args
    

def frame_lookup(work_file):
    print("-- Frame lookup")
    subprocess.run(
            ["python", "-u", "-m", 
             STOG_NODE_RESTORE, 
             "--amr_files", args.input,
             "--util_dir", STOG_UTILS_DIR], 
            check=True, 
            universal_newlines=True)   
    return work_file + ".frame"
    

def wikification(work_file):
    print("-- Wikification...")
    subprocess.run(
            ["python", "-u", "-m", 
             STOG_WIKIFICATION, 
             "--amr_files", work_file,
             "--util_dir", STOG_UTILS_DIR], 
             #"----dump_spotlight_wiki", "1",
            check=True, 
            universal_newlines=True)   
    return work_file + ".wiki"
    

def expand_nodes(work_file):
    print("-- Expanding nodes")
    subprocess.run(
            ["python", "-u", "-m", 
             STOG_MIN_EXPANDER, #STOG_EXPANDER, 
             "--amr_files", work_file,
             "--util_dir", STOG_UTILS_DIR], 
            check=True, 
            universal_newlines=True)   
    return work_file + ".expand"
    
    
#==============================================================================
# Exécution
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    os.chdir(STOG_DIR) # change directory
    work_file = args.input
    work_file = frame_lookup(work_file)
    #work_file = wikification(work_file)
    work_file = expand_nodes(work_file)
    output_file = dirname(args.input) + "/"
    output_file += splitext(basename(args.input))[0]
    output_file += ".postproc"
    os.rename(work_file, output_file)