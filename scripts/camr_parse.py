#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Script for CAMR parsing
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import subprocess
import argparse


#==============================================================================
# Parameters and useful function(s)
#==============================================================================

# Useful directories and references 
WD = "wd"
CAMR_DIR = "camr/"
CAMR_WD = CAMR_DIR + WD
CAMR_PARSER = "amr_parsing.py"
CAMR_MODEL = "semeval.m"
CAMR_OUT_SUFFIX = ".all.semeval.parsed"

# Function for verbose level
def is_verbose_level(expected_level, verbose_level):
    return expected_level <= verbose_level


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    print("-- Argument checking")
    arg_parser = argparse.ArgumentParser(description="CAMR Parsing")   
    arg_parser.add_argument("input",
                            help="input document for working")
    arg_parser.add_argument("--verbose",
                            default=0,
                            type=int,
                            help="verbose: 0, 1, 2")   
    args = arg_parser.parse_args()
    print("--- input: " + args.input)
    print("--- verbose: " + str(args.verbose))
    return args
    

def prepare_data():
    print("-- Preparation of work data")
    subprocess.run(["cp", "-r", WD, CAMR_DIR]) # copying input file(s)
    os.chdir(CAMR_DIR) # change directory
      
    
def preprocessing(args, capture_output_value):
    print("-- Preprocessing for AMR parsing")
    subprocess.run(
            ["python2", CAMR_PARSER, 
             "-m", "preprocess", 
             args.input], 
            check=True, 
            capture_output=capture_output_value,
            universal_newlines=True)
   
    
def parsing(args, capture_output_value):
    print("-- Parsing with a pre-trained model (" + CAMR_MODEL + ")")
    subprocess.run(
            ["python2", CAMR_PARSER, 
             "-m", "parse", 
             "--model", CAMR_MODEL,
             args.input], 
            check=True, 
            capture_output=capture_output_value,
            universal_newlines=True)
  
    
def ending():
    print("-- Ending of CAMR Parsing")
    os.chdir("..") # change directory
    subprocess.run(["cp", "-r", CAMR_WD, "."])
    

#==============================================================================
# Main process
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    capture_output_value = not(is_verbose_level(2, args.verbose))
    prepare_data()
    preprocessing(args, capture_output_value)
    parsing(args, capture_output_value)
    ending()


