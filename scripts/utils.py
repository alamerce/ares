#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - STOG ending script (STOG Parsing)
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import subprocess


#==============================================================================
# Parameters
#==============================================================================

# Format of AMR division
AMRDIV_FORMAT = ["# ::id", "# ::snt", "# ::out"]   
    
#==============================================================================
# Useful functions
#==============================================================================

def copy_file(f_in, f_out):
    """ Copy file f_in to file f_out, sentence by sentence.
    """
    sentences_list = list()
    with open(f_in, "r") as reading_file: # r = read
        for sentence in reading_file.readlines():
            sentences_list.append(sentence)
    with open(f_out, "w") as writing_file: # w = write
        for sentence in sentences_list: # for each sentence
            writing_file.write(sentence)
            
            
def is_sentence_empty(sentence):
    """ True if the sentence is empty.
    """
    return ((sentence == "") | (sentence == "\n"))
            
            
def remove_empty_sentences(work_file):
    """ Remove empty sentences on work_file.
    """
    sentences_list = list()
    with open(work_file, "r") as reading_file: # r = read
        for sentence in reading_file.readlines():
            if not is_sentence_empty(sentence):
                sentences_list.append(sentence)
    with open(work_file, "w") as writing_file: # w = write
        for sentence in sentences_list: # for each sentence
                writing_file.write(sentence)


def is_comment(sentence):
    """ True if the sentence is a comment.
    """
    return sentence.startswith("#")


def is_amrdiv_format(sentence):
    """ True if the sentence corresponds to amr div format.
    """
    result = False
    for ref in AMRDIV_FORMAT:
        if sentence.startswith(ref):
            result = True
    return result


def convert_to_amrep_format(work_file):
    """ Convert the work file according to amrep format.
    """
    sentences_list = list()
    with open(work_file, "r") as reading_file: # r = read
        for sentence in reading_file.readlines():
            if is_comment(sentence):
                if is_amrdiv_format(sentence):
                    sentences_list.append(sentence)
            else:
                sentences_list.append(sentence)
    with open(work_file, "w") as writing_file: # w = write
        for sentence in sentences_list: # for each sentence
                writing_file.write(sentence)
    