#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Script for semantic transformation
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import subprocess
import argparse


#==============================================================================
# Parameters and useful function(s)
#==============================================================================

# Useful directories and references 
WD = "wd"
ATP_DIR = "atp/"
ATP_WD = ATP_DIR + WD
ATP_JAR = "build/libs/atp-1.0-SNAPSHOT.jar"

# Function for verbose level
def is_verbose_level(expected_level, verbose_level):
    return expected_level <= verbose_level


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    print("-- Argument checking")
    arg_parser = argparse.ArgumentParser(description="Semantic Transduction (transform)")   
    arg_parser.add_argument("input",
                            help="input data")
    arg_parser.add_argument("--verbose",
                            default=0,
                            type=int,
                            help="verbose: 0, 1, 2")
    args = arg_parser.parse_args()
    print("--- input: " + args.input)
    print("--- verbose: " + str(args.verbose))
    return args
    

def prepare_data():
    print("-- Preparation of work data")
    subprocess.run(["cp", "-r", WD, ATP_DIR]) # copying input file(s)
    os.chdir(ATP_DIR) # change directory
   
    
def transform(args, capture_output_value):
    print("-- Abstract Transduction Processing")
    subprocess.run(
            ["java", "-jar", 
             ATP_JAR, 
             args.input], 
            check=True, 
            capture_output=capture_output_value,
            universal_newlines=True) 
  
    
def ending():
    print("-- Ending: ATP")
    os.chdir("..") # change directory
    subprocess.run(["cp", "-r", ATP_WD, "."]) # copying result data
        

#==============================================================================
# Main process
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    capture_output_value = not(is_verbose_level(2, args.verbose))
    prepare_data()
    transform(args, capture_output_value)
    ending()


