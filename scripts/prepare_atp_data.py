#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# convert_text_to_stog
#------------------------------------------------------------------------------
# Script to convert sentence files to stog format. The conversion operates from
# an input directory containing the files to convert, and creates the converted
# files in an output directory.
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import argparse
import glob
from os.path import basename, splitext


#==============================================================================
# Parameters
#==============================================================================

# Data directories and files
AMR_SUFFIX = ".amrep"
DEFAULT_DATA_DIR = "wd/"
DEFAULT_INPUT_FILE = DEFAULT_DATA_DIR + "test" + AMR_SUFFIX


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    print("-- Argument checking")
    arg_parser = argparse.ArgumentParser(description="Semantic Transformation")   
    arg_parser.add_argument("input",
                            help="input document to convert")
    arg_parser.add_argument("input_dir",
                            help="directory with files to convert")
    arg_parser.add_argument("output_dir",
                            help="directory for output file") 
    args = arg_parser.parse_args()
    print("--- input: " + args.input)
    print("--- input dir: " + args.input_dir)
    print("--- output dir: " + args.output_dir)
    return args

    
def target_data(args):
    print("-- Data targeting")
    if args.input == "*":
        input_filepaths = args.input_dir + "*" + AMR_SUFFIX
        amr_output_file = args.output_dir + "input" + AMR_SUFFIX
    else:
        input_filepaths = args.input_dir + args.input + "*" + AMR_SUFFIX
        amr_output_file = args.output_dir + args.input + AMR_SUFFIX
    print("--- input filepaths: " + input_filepaths)
    print("--- output file in amr format: " + amr_output_file)
    return (input_filepaths, amr_output_file)
    

def read_input_files(filepaths):
    print("-- Reading input files to recover a list of sentences")
    sentences_list = list()
    for element in filepaths:
        filename = splitext(basename(element))[0]    
        corpus = filename
        input_file = args.input_dir + filename + AMR_SUFFIX
        print("--- Reading sentence file: " + filename)
        with open(input_file, "r") as reading_file: # r = read
            for sentence in reading_file.readlines():
                sentences_list.append((corpus, sentence))
    return sentences_list
   
    
def write_text_files(sentences_list, text_output_file):    
    print("--- Writing output file in amr format")
    with open(text_output_file, "a") as writing_file: # a = append
        for (_, sentence) in sentences_list: # for each sentence
            writing_file.write(sentence)

        
    
#==============================================================================
# Exécution
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    (input_filepaths, amr_output_file) = target_data(args)
    filepaths = glob.glob(input_filepaths)
    sentences_list = read_input_files(filepaths)
    write_text_files(sentences_list, amr_output_file)