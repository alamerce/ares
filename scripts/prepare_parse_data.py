#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# convert_text_to_stog
#------------------------------------------------------------------------------
# Script to convert sentence files to stog format. The conversion operates from
# an input directory containing the files to convert, and creates the converted
# files in an output directory.
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import argparse
import glob
from os.path import basename, splitext
import utils


#==============================================================================
# Parameters
#==============================================================================

# Data references
#CORPUS = "parking"
SECTION = "prep data"

# Data directories and files
TEXT_SUFFIX = ".txt"
PREP_SUFFIX = ".prep" 
TEMP_SUFFIX = ".temp" 
DEFAULT_DATA_DIR = "wd/"
DEFAULT_INPUT_FILE = DEFAULT_DATA_DIR + "test.txt"
DEFAULT_OUTPUT_FILE = DEFAULT_DATA_DIR + "test" + PREP_SUFFIX

# Text definitions
COMMENTS_TEXT = "# "
RELEASE_TEXT = "AMR release"
CORPUS_TEXT = "corpus: "
SECTION_TEXT = "section: "
AMR_NUM_TEXT = "number of AMRs: "
GEN_DATE_TEXT = "generated on"
DEFAULT_DATE_1 = "Tue Feb 19, 2020 at 12:00:00"
DEFAULT_DATE_2 = "2020-02-19 12:00:00.0"
ID_TEXT = "::id "
DATE_TEXT = "::date "
ANNOTATOR_TEXT = "::annotator "
DEFAULT_ANNOTATOR = "Annotator"
PREFERRED_TEXT = "::preferred "
EMPTY_LINE = "\n"
SENTENCE_TEXT = "::snt "
AMR_DUMMY_TEXT = "(d / dummy)"
SAVE_DATE_TEXT = "::save-date "


#==============================================================================
# Functions to define generated text lines
#==============================================================================

def heading(corpus, section, amrs_number):
    line = COMMENTS_TEXT
    line += RELEASE_TEXT + "; "
    line += CORPUS_TEXT + corpus + "; "
    line += SECTION_TEXT + section + "; "
    line += AMR_NUM_TEXT + str(amrs_number) + " "
    line += "(" + GEN_DATE_TEXT + DEFAULT_DATE_1 + ")"
    line += "\n"
    return line

def identifier_line(id_name):
    line = COMMENTS_TEXT
    line += ID_TEXT + id_name + " "
    #line += DATE_TEXT + DEFAULT_DATE_2 + " "
    #line += ANNOTATOR_TEXT + DEFAULT_ANNOTATOR + " "
    #line += PREFERRED_TEXT
    return line + "\n"
    
def sentence_line(sentence):
    line = COMMENTS_TEXT   
    line += SENTENCE_TEXT
    line += sentence
    return line

def save_date_line():
    line = COMMENTS_TEXT
    line += SAVE_DATE_TEXT + DEFAULT_DATE_2
    return line + "\n"

def amr_dummy_line():
    line = AMR_DUMMY_TEXT
    return line + "\n"


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments():
    print("-- Argument checking")
    arg_parser = argparse.ArgumentParser(description="Semantic Transformation")   
    arg_parser.add_argument("input",
                            help="input document to convert")
    arg_parser.add_argument("input_dir",
                            help="directory with files to convert")
    arg_parser.add_argument("output_dir",
                            help="directory for output file") 
    args = arg_parser.parse_args()
    print("--- input: " + args.input)
    print("--- input dir: " + args.input_dir)
    print("--- output dir: " + args.output_dir)
    return args

    
def target_data(args):
    print("-- Data targeting")
    if args.input == "*":
        input_filepaths = args.input_dir + "*" + TEXT_SUFFIX
        text_output_file = args.output_dir + "input" + TEXT_SUFFIX
        prep_output_file = args.output_dir + "input" + PREP_SUFFIX
    else:
        input_filepaths = args.input_dir + args.input + "*" + TEXT_SUFFIX
        text_output_file = args.output_dir + args.input + TEXT_SUFFIX
        prep_output_file = args.output_dir + args.input + PREP_SUFFIX
    print("--- input filepaths: " + input_filepaths)
    print("--- output file in text format: " + text_output_file)
    print("--- output file in prep format: " + prep_output_file)
    return (input_filepaths, text_output_file, prep_output_file)
    

def read_input_files(filepaths):
    print("-- Reading input files to recover a list of sentences")
    sentences_list = list()
    for element in filepaths:
        filename = splitext(basename(element))[0]    
        corpus = filename
        input_file = args.input_dir + filename + TEXT_SUFFIX
        print("--- Reading sentence file: " + filename)
        with open(input_file, "r") as reading_file: # r = read
            for sentence in reading_file.readlines():
                if not utils.is_sentence_empty(sentence):
                    sentences_list.append((corpus, sentence))
    return sentences_list
   
    
def write_text_files(sentences_list, text_output_file):    
    print("--- Writing output file in text format")
    with open(text_output_file, "a") as writing_file: # a = append
        for (_, sentence) in sentences_list: # for each sentence
            writing_file.write(sentence)


def convert_sentences(sentences_list, prep_output_file):
    print("--- Conversion of sentences and writing output file in prep format")
    with open(prep_output_file, "w") as writing_file: # a = append
        #amrs_number = len(sentences_list)
        corpus = splitext(basename(prep_output_file))[0]    
        #writing_file.write(heading(corpus, SECTION, amrs_number))
        #writing_file.write(EMPTY_LINE)
        sentences_list = sorted(sentences_list, 
                                key=lambda sentences: sentences[0])
        sentence_num = 0
        current_corpus = "-"
        for (corpus, sentence) in sentences_list: # for each sentence
            if current_corpus != corpus:
                sentence_num = 0
            current_corpus = corpus
            sentence_id_name = current_corpus + "_" + str(sentence_num)
            writing_file.write(identifier_line(sentence_id_name))
            writing_file.write(sentence_line(sentence))
            writing_file.write(save_date_line())
            writing_file.write(amr_dummy_line())
            writing_file.write(EMPTY_LINE)
            sentence_num += 1
        
    
#==============================================================================
# Exécution
#==============================================================================

if __name__ == '__main__':
    args = control_arguments()
    (input_filepaths, text_output_file, prep_output_file) = target_data(args)
    filepaths = glob.glob(input_filepaths)
    sentences_list = read_input_files(filepaths)
    write_text_files(sentences_list, text_output_file)
    convert_sentences(sentences_list, prep_output_file)