#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Script for cleaning work data
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import os
import shutil


#==============================================================================
# Parameters and useful function(s)
#==============================================================================

# Useful directories and references 
WD = "wd"
CAMR_DIR = "camr/"
CAMR_WD = CAMR_DIR + WD
CAMR_PARSER = "amr_parsing.py"
CAMR_MODEL = "semeval.m"
CAMR_OUT_SUFFIX = ".all.semeval.parsed"
ATP_DIR = "atp/"
ATP_WD = ATP_DIR + WD
ATP_JAR = "build/libs/atp-1.0-SNAPSHOT.jar"
MICA_DIR = "mica/"
MICA_LIB = "lib"
MICA_WD = MICA_DIR + WD
MICA_EX_PARKING = "parking.def"
STOG_DIR = "stog/"
STOG_WD = STOG_DIR + WD


#==============================================================================
# Script steps
#==============================================================================
    
def clean_working_directories():
    print("-- Cleaning working directories")
    shutil.rmtree(WD, ignore_errors=True)
    shutil.rmtree(CAMR_WD, ignore_errors=True)
    shutil.rmtree(ATP_WD, ignore_errors=True)
    shutil.rmtree(MICA_WD, ignore_errors=True)
    shutil.rmtree(STOG_WD, ignore_errors=True)
    os.mkdir(WD)
    os.mkdir(CAMR_WD)
    os.mkdir(ATP_WD)
    os.mkdir(MICA_WD)
    os.mkdir(STOG_WD)
    

#==============================================================================
# Main process
#==============================================================================

if __name__ == '__main__':
    clean_working_directories()


