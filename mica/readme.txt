 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************

Mica is an Ocaml library implementing the Modal Interface theory
published in:

    Jean-Baptiste Raclet, Eric Badouel, Albert Benveniste, Beno�t
    Caillaud, Axel Legay, Roberto Passerone: A Modal Interface Theory
    for Component-based Design. Fundam. Inform. 108(1-2): 119-149
    (2011)

In Mica, systems and interfaces are represented by extension. However,
a careful design of the state and event heap enables the definition,
composition and analysis of reasonably large systems and interfaces
(~10^6 states). The heap stores states and events in a hashing table
and ensures structural equality (there is no duplication). Therefore
complex data-structures for states and events induce a very low
overhead, as checking equality is done in constant time.

Thanks to the Inter module and the mica interactive environment, users
can define complex systems and interfaces using Ocaml syntax. It is
even possible to define parameterized components as Ocaml functions.

Mica is available as an open-source distribution, under the CeCILL-C
Free Software Licence Agreement, version 1. Users agree to comply with
the terms of the licence contained in the following document:

    http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html

This software can be downloaded at: http://www.irisa.fr/s4/download/mica/

This distribution contains several folders:

     - src    		   Source code
     - doc		   Documentation
     - examples		   Examples

It compiles, runs and has been tested on the following platforms:

    - Linux
    - MacOS X version 10.4 or later

It should compile and run on the following platforms. However, this
has not been tested:

    - Solaris, OpenBSD, FreeBSD
    - Windows NT/2000/XP/Vista/7 with Cygwin

This software requires the following tools:

    - The GraphViz graph mapping toolbox (dot command)
    - gmake (only for Darwin and most likely Solaris)
    - Ocaml (version 3.12 or later, compiles on earlier versions but
      has not been thoroughly tested)

To compile this software, change to folder src and follow instructions
in the readme.txt file located there.

Examples can be found in folder examples. Follow instructions in file
readme.txt located there.

Documentation is minimal :

    - Documentation of the Inter module and of the mica interactive
      environment, that should be used to define, compose and analyze
      systems and interfaces.
