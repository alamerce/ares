(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * A rather silly proof that some Mersene numbers are not prime
 *
 * $Id: ex_mersene.ml 470 2011-11-30 10:02:39Z bcaillau $
 *
 *)

(* run with mica *)

let n = 3000;;

let sieve k =
  let a = Array.make (k+1) true
  and l = ref [] in
    begin
      a.(0) <- false;
      a.(1) <- false;
      for i = 2 to k do
	let j = ref (i+i) in
	  while (!j) <= k do
	    a.(!j) <- false;
	    j := (!j)+i
	  done
      done;
      for i = k downto 0 do
	if a.(i) then l := i::(!l)
      done;
      !l
    end;;
  
let primes = sieve n;;

let power p q =
  let r = ref 1 in
    begin
      for i=1 to q do
	r := (!r)*p
      done;
      !r
    end;;

let rec mers k =
  function
      [] -> []
    | i :: l ->
	let m = (power 2 i)-1 in
	  if m > k
	  then []
	  else m :: (mers k l);;
 
let mersenes = mers n primes;;  

let succ = ident "succ";;
let prime = ident "prime";;
let mersene = ident "mersene";;

let g_prime =
  new_signature ();
  input succ;
  output prime;
  define_signature ();;

let c_prime =
  new_interface g_prime;
  init (int 0);
  for i = 0 to n-1 do
    must (int i) succ (int (i+1))
  done;
  List.iter
    (fun i -> must (int i) prime (int i))
    primes;
  define_interface ();;

let g_mersene =
  new_signature ();
  input succ;
  output mersene;
  define_signature ();;

let c_mersene =
  new_interface g_mersene;
  init (int 0);
  for i = 0 to n-1 do
    must (int i) succ (int (i+1))
  done;
  List.iter
    (fun i -> must (int i) mersene (int i))
    mersenes;
  define_interface ();;
 
let g_imply =
  new_signature ();
  input succ;
  output mersene;
  output prime;
  define_signature ();;

let q0 = ident "q0";;
let q1 = ident "q1";;
let q2 = ident "q2";;

let tt = ident "tt";;

let c_imply =
  new_interface g_imply;
  init q0;
  may q0 succ q0;
  may q0 prime tt;
  may q0 mersene q1;
  must q1 prime tt;
  may q1 mersene tt;
  may q1 succ tt;
  may tt prime tt;
  may tt mersene tt;
  may tt succ tt;
  define_interface ();;

print_interface c_imply;;

let c_conj = conjunction c_prime c_mersene;;

is_consistent c_conj;;

refines c_conj c_imply;; (* fails on 2047 = 2^11-1 *)

print_statistics ();;
