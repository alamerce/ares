(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Testing the performance of the minimization algorithm
 *
 * $Id: ex_minimal.ml 473 2011-12-05 12:40:10Z bcaillau $
 *
 * Run with mica
 *
 *)

(* Measuring CPU time *)

let t = ref 0.

let start () = t := Sys.time ();;

let stop () = (Sys.time ()) -. (!t);;

(* Interface definition *)

start ();;

let a = ident "a";;
let b = ident "b";;
let c = ident "c";;
let d = ident "d";;

let state i j = tuple [| (int i) ; (int j) |];;

let generic p q r m =
  let n = p*q*r*m in
    begin
      new_interface
	(new_signature ();
	 output a;
	 output b;
	 output c;
	 output d;
	 define_signature ());
      init (state 0 0);
      for i=0 to n-2 do
	for j=0 to m-1 do
	  must (state i j) d (state (i+1) j)
	done
      done;
      for j=0 to m-1 do
	must (state (n-1) j) d (state 0 j)
      done;
      for i=0 to n-1 do
	for j=0 to m-1 do
	  let x = state i j
	  and y = state i ((j+1) mod m) in
	    begin
	      (if (i mod p) = 0 then may x a y);
	      (if (i mod q) = 0 then may x b y);
	      (if (i mod r) = 0 then may x c y)
	    end
	done
      done;
      define_interface ()
    end;;

let s = generic 3 5 7 5;;

info_interface s;;

stop ();;

start ();;
    
let s' = minimize s;;

stop ();;

info_interface s';;

start ();;

refines s s';;

refines s' s;;

stop ();;

print_statistics ();;
