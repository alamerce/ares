(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Example of architecture synthesis using the projection and quotient
 * operators.
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, INRIA, 2012
 *
 * Run with mica under emacs with the Tuareg mode
 *
 * $Id$
 *
 *)

(*** Utilities ***)

(* function composition operator *)

let ($$) = fun f g x -> f (g x);;

(* simplified minimal interface *)

let simple = simplify $$ minimize;;

(* terminal max *)

let strong = interface_of_system $$ max_implementation;;

(* simplified minimal system *)

let minimal = min_implementation $$ simple $$ interface_of_system;;

(* Trivial interface *)

let tt =
  new_interface (new_signature (); define_signature ());
  init (int 0);
  define_interface ();;

(* Conjunction of a list of interfaces *)

let fold_op f =
  function
      [] -> tt
    | c0::l -> List.fold_left f c0 l;;

let fold_conjunction = fold_op conjunction;;

let fold_product = fold_op product;;

let fold_parallel =
  function
      [] -> invalid_arg "fold_parallel: empty list"
    | i0::l -> List.fold_left parallel i0 l;;

let collate l = simple (fold_conjunction l);;

let gather l =  simple (fold_product l);;

let map_collate f l =
  collate (List.map f l);;

(* Sub-sampler *)

let sampler n i o =
  let s =
    new_signature ();
    input i;
    output o;
    define_signature ()
  in
    new_interface s;
    init (int 0);
    for j=0 to n-1 do
      must (int j) i (int (j+1))
    done;
    must (int n) o (int 0);
    define_interface ();;

(* Architecture *)

let arch n m i o =
  let e =
    Array.init
      (m+1)
      (fun j ->
	if j <= 0
	then i
	else
	  if j >= m
	  then o
	else tuple [| (ident "e"); (int j) |]) in
  let c =
    Array.init
      m
      (fun j -> fun () -> sampler n e.(j) e.(j+1)) in
  let r = ref (c.(0) ()) in
    begin
      for i=1 to m-1 do
	r := product (!r) (c.(i) ())
      done;
      !r
    end;;

let arch24 = arch 10 5 (ident "in") (ident "out");;

let arch52 = arch 5 2 (ident "in") (ident "out");;

info_interface arch24;;
info_interface arch52;;

display_interface arch52 "arch52";;

let abs24 =
  simplify
    (abstract
	[
	  (ident "in");
	  (ident "out")
	]
	arch24);;

info_interface abs24;;

let abs52 =
  simplify
    (abstract
	[
	  (ident "in");
	  (ident "out")
	]
	arch52);;

info_interface abs52;;

display_interface abs52 "abs52";;

assert (refines arch24 abs24);;
assert (refines arch52 abs52);;

print_statistics ();;
