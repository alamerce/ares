(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, July 2012.                            *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Very simple architecture synthesis example using the projection and
 * quotient operators.
 *
 * $Id$
 *
 * Run with Mica
 *
 *)

(*** Utilities ***)

(* function composition operator *)

let ($$) = fun f g x -> f (g x);;

(* simplified minimal interface *)

let simple = simplify $$ minimize;;

(* simplified minimal system *)

let minimal = min_implementation $$ simple $$ interface_of_system;;

(*** States and events ***)

let a = ident "a"
and b = ident "b";;

let q0 = int 0
and q1 = int 1
and q2 = int 2
and q3 = int 3;;

let s1 =
  new_signature ();
  output a;
  output b;
  define_signature ();;

let c1 =
  new_interface s1;
  init q0;
  must q0 a q1;
  must q0 b q2;
  may q1 b q3;
  may q2 a q3;
  define_interface ();;

display_interface c1 "c1";;

let t1 =
  new_signature ();
  output a;
  define_signature ();;

let d1 = (* simple *) (project t1 c1);;

display_interface d1 "d1";;

let c2 = simple (quotient c1 d1);;

assert (refines (product d1 c2) c1);;

is_complete c2;;

display_interface c2 "c2";;

let t2 =
  new_signature ();
  output b;
  input a;
  define_signature ();;

let d2 = (* simple *) (project t2 c2);;

display_interface d2 "d2";;

let c3 = simple (quotient c2 d2);;

info_interface c3;;

assert (refines (product d2 c3) c2);;

is_complete c3;; (* We are done *)

(* c1 has been decomposed into the product of d1 by d2 *)

assert (refines  (product d1 d2) c1);;

(* Independent implementations *)

let m1 = minimal (min_implementation d1)
and m2 = minimal (min_implementation d2);;

display_system m1 "m1";;
display_system m2 "m2";;

let m = parallel m1 m2;;

assert (satisfies m c1);;

