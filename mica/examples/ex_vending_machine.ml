(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * The classic vending machine revisited
 *
 * $Id: ex_vending_machine.ml 487 2012-04-06 07:39:12Z bcaillau $
 *
 * Run with mica
 *
 *)

(* Trivial interface *)

let tt =
  new_interface (new_signature (); define_signature ());
  init (int 0);
  define_interface ();;

(* Conjunction of a list of interfaces *)

let fold_conjunction =
  function
      [] -> tt
    | c0::l -> List.fold_left conjunction c0 l;;

let collate l = simplify (minimize (fold_conjunction l));;

let fold_collate f l =
  collate (List.map f l);;

(* non-zero iteration of a regular expression*)

let plus x = concat x (star x);;

(* Sum iterator *)

let fold_sum f =
  function
      [] -> empty ()
    | x::l ->
	List.fold_right
	  (fun x e -> sum e (f x))
	  l (f x);;
    
(* Drinks *)

let drinks = [ ident "coffee"; ident "tea"; ident "chocolate" (* ; ident "cappuccino"; ident "cafe_latte" *) ];;

let drinks_but d = List.filter (fun d' -> d' != d) drinks;;

(* Events *)

let req d = tuple [| ident "req"; d |];;

let del d = tuple [| ident "del"; d |];;

let coin = ident "coin";;
let cancel = ident "cancel";;

(* Regular expression defining several states of the machine *)

let standby =
  star
    (concat
       (star
	  (sum
	     (prefix coin (epsilon ()))
	     (fold_sum (fun d -> prefix (req d) (epsilon ())) drinks)))
       (sum
	  (prefix cancel (epsilon ()))
	  (fold_sum (fun d -> (prefix (del d) (epsilon ()))) drinks)));;
	  
print_expression standby;;

let coin_inserted =
  concat
    standby
    (plus (prefix coin (epsilon ())));;

print_expression coin_inserted;;

let request_made =
  concat
    coin_inserted
    (plus
       (fold_sum (fun d -> prefix (req d) (epsilon ())) drinks));;

print_expression request_made;;

let standby_or_coin_inserted =
  sum
    standby
    coin_inserted;;

print_expression standby_or_coin_inserted;;

let standby_or_request_made =
  sum
    standby
    request_made;;

print_expression standby_or_request_made;;

let coin_inserted_or_request_made =
  sum
    coin_inserted
    request_made;;

print_expression coin_inserted_or_request_made;;

(* Requirements *)

(* Viewpoint 1: nominal case where no drink is ordered *)

(* Signature of viewpoint 1 *)

let g1 =
  new_signature ();
  input coin;
  input cancel;
  define_signature ();;

(* R11 *)

let req11 =
  collate
    [
      after_must g1 (epsilon ()) coin; (* initially, a coin can be inserted *)
      (* after_cannot g1 (epsilon ()) cancel; (* initially, the cancel button is disabled *) *)
      (* every_cannot g1 [| cancel ; cancel |]; (* cancel cannot be pressed twice in a row *) *)
      every_must g1 [| cancel; coin |]; (* after a cancellation, a coin can be inserted *)
      every_must g1 [| coin; cancel |] (* after inserting a coin, cancel can be pressed *)
    ];;

(* Viewpoint 1 requirement *)

let vp1 = collate [ req11 ];;

info_interface vp1;;

display_interface vp1 "VP1";;    

(* Viewpoint 2: nominal use-case for one drink *)

(* Signature of viewpoint 2 *)

let g2 d =
  new_signature ();
  input coin;
  input cancel;
  input (req d);
  output (del d);
  define_signature ();;

let gg2 =
  new_signature ();
  input coin;
  input cancel;
  List.iter
    (fun d ->
       begin
	 input (req d);
	 output (del d)
       end)
    drinks;
  define_signature ();;

(* R21 *)

let req21 d =
  collate
    [
      every_must gg2 [| (req d) ; (del d) |]; (* after pressing request, we must get drink *)
      every_must gg2 [| (del d) ; coin |]; (* after obtaining drink, we must be allowed to insert a coin *)
      every_must gg2 [| coin ; (req d) |] (* after inserting a coin, we must be allowed to order drink *)
    ];;

let r21 = fold_collate req21 drinks;;

info_interface r21;;

display_interface r21 "R21";;

(* R22 *)

let req22 d =
  collate
    [
      after_cannot gg2 standby_or_coin_inserted (del d); (* drink cannot be obtained in standby or once a coin has been inserted *)
      (* after_cannot gg2 standby_or_request_made (req d); *) (* drink cannot be requested in standby mode or once a request has been made *)
      (* after_cannot gg2 coin_inserted_or_request_made coin *) (* a coin can not be inserted after a coin has been inserted or a request has been made *)
    ];;

let r22 = fold_collate req22 drinks;;

info_interface r22;;

display_interface r22 "R22";;

(* R23: after a request for a given drink we can not get something else *)

let req23 d =
  fold_collate
    (fun d' -> every_cannot gg2 [| req d; del d' |])
    (drinks_but d);;

display_interface (req23 (ident "tea")) "R23";;

(* Global requirement for viewpoint 2 *)

let vp2 =
  fold_collate
    (fun d -> collate [ req21 d; req22 d; req23 d ])
    drinks;;

info_interface vp2;;

display_interface vp2 "VP2";;

(* Cross-viewpoint global requirement *)

let global = collate [ vp1; vp2 ];;

info_interface global;;

display_interface global "Global";;
