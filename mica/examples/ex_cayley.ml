(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Define a large Cayley graph ~ 10^6 transitions
 *
 * $Id: ex_cayley.ml 473 2011-12-05 12:40:10Z bcaillau $
 *
 *)

(* run with mica *)

let n = 1000;; (* 1.2 million transitions, requires about 2.7Gb memory *)

let plus i = tuple [| (ident "plus"); (int i) |]

let minus i = tuple [| (ident "minus"); (int i) |]

let equal i = tuple [| (ident "equal"); (int i) |]

let t = ref 0.

let start () = t := Sys.time ();;

let stop () = (Sys.time ()) -. (!t);;

start ();;

let cayley_s =
  new_system
    (new_signature ();
     for i=0 to n-1 do
       input (plus i);
       input (minus i);
       output (equal i)
     done;
     define_signature ());
  init (int 0);
  for i=0 to n-1 do
    trans (int i) (equal i) (int i);
    for j=0 to n-1 do
      let k = j-i in
	if k > 0
	then trans (int i) (plus k) (int j)
	else
	  if k <0
	  then trans (int i) (minus (-k)) (int j)
    done
  done;
  define_system ();;

stop ();;

start ();;

let cayley_i =
  new_interface
    (new_signature ();
     for i=0 to n-1 do
       input (plus i);
       input (minus i);
       output (equal i)
     done;
     define_signature ());
  init (int 0);
  for i=0 to n-1 do
    must (int i) (equal i) (int i);
    for j=0 to n-1 do
      let k = j-i in
	if k > 0
	then must (int i) (plus k) (int j)
	else
	  if k <0
	  then must (int i) (minus (-k)) (int j)
    done
  done;
  define_interface ();;

stop ();;

start ();;

info_interface cayley_i;;

satisfies cayley_s cayley_i;;

print_statistics ();;

stop ();;
