(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Counter example developped in Fundamenta Informaticae paper
 *
 * $Id: ex_paper_fi.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

open Inter;;

new_session ();;

let a = (ident "a");;
let b = (ident "b");;
let c = (ident "c");;

let q0 = (int 0);;
let q1 = (int 1);;
let q2 = (int 2);;

(* Wrong version of the example, as it appeared in the paper. Thanks to Wlater Vogler who pointed out the error *)

let g1 =
  new_signature ();
  output a;
  output b;
  define_signature ();;

print_signature g1;;

let c1 =
  new_interface g1;
  init q0;
  may q0 a q1;
  may q1 b q2;
  define_interface ();;

print_interface c1;;

is_consistent c1;; (* true *)

let i1 =
  new_system g1;
  init q0;
  trans q0 a q1;
  define_system ();;

print_system i1;;

satisfies i1 c1;; (* true *)

let g2 =
  new_signature ();
  input a;
  input b;
  input c;
  define_signature ();;

print_signature g2;;

let c2 =
  new_interface g2;
  init q0;
  may q0 c q1;
  may q1 a q2;
  define_interface ();;

print_interface c2;;

is_consistent c2;; (* true *)

let i2 =
  new_system g2;
  init q0;
  trans q0 c q1;
  trans q1 a q2;
  define_system ();;

print_system i2;;

satisfies i2 c2;; (* true *)

let i = parallel i1 i2;; (* States 0 and 0 are incompatible wrt to event a : this is not what we wanted *)

print_system i;;

let g' =
  new_signature ();
  output a;
  output b;
  input c;
  define_signature ();;

print_signature g';;

(* c' is the pruning according to Nymann et al. *)

let c' =
  new_interface  g';
  init q0;
  define_interface ();;

print_interface c';;

satisfies i c';; (* false : failure in state pair ((0,0),0): event c has modality cannot. *)

(* c'' is the optimistic parallel composition *)

let c'' = parallel_optimistic c1 c2;;

print_interface c'';;

satisfies i c'' (* false : empty specification *);;

(* The example in the paper is flawed. It can be corrected by chaging the orientation of event a. See below *)

(* Correct version of the example *)

let g1 =
  new_signature ();
  input a;
  output b;
  define_signature ();;

print_signature g1;;

let c1 =
  new_interface g1;
  init q0;
  may q0 a q1;
  may q1 b q2;
  define_interface ();;

print_interface c1;;

is_consistent c1;; (* true *)

let i1 =
  new_system g1;
  init q0;
  trans q0 a q1;
  define_system ();;

print_system i1;;

satisfies i1 c1;; (* true *)

let g2 =
  new_signature ();
  output a;
  input b;
  input c;
  define_signature ();;

print_signature g2;;

let c2 =
  new_interface g2;
  init q0;
  may q0 c q1;
  may q1 a q2;
  define_interface ();;

print_interface c2;;

is_consistent c2;; (* true *)

let i2 =
  new_system g2;
  init q0;
  trans q0 c q1;
  trans q1 a q2;
  define_system ();;

print_system i2;;

satisfies i2 c2;; (* true *)

let i = parallel i1 i2;; (* Now there is no incompatibility *)

print_system i;;

let g' =
  new_signature ();
  output a;
  output b;
  input c;
  define_signature ();;

print_signature g';;

(* c' is the pruning according to Nymann et al. *)

let c' =
  new_interface  g';
  init q0;
  define_interface ();;

print_interface c';;

satisfies i c';; (* false : failure in state pair ((0,0),0): event c has modality cannot. *)

(* c'' is the optimistic parallel composition *)

let c'' = parallel_optimistic c1 c2;;

print_interface c'';;

(*
 *
 * Here is the result:
 *
 * {
 *  init state (0,0);
 *  may trans (0,0) .. c .> ();
 *  may trans () .. c .> ();
 *  may trans () .. b .> ();
 *  may trans () .. a .> ();
 * } : << c:? b:! a:! >>
 *
 *)

satisfies i c'' (* true : what we expected *);;
