(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Parking tickets and requirements on the ticket dispenser
 *
 * $Id: ex_ticket.ml 483 2011-12-14 16:56:01Z bcaillau $
 *
 *)

(* run with mica *)

(* tickets have a unique id *)

let ticket i = tuple [| (ident "ticket"); (int i) |];;

(* ticket dispenser state is a tuple (mode, set of dispensed tickets) *)

type mode =
    Listen
  | Emit
  | Test of int
  | Discard of int;;

let mode_state =
  function
      Listen -> ident "listen"
    | Emit -> ident "emit"
    | Test i -> tuple [| (ident "test") ; (int i) |]
    | Discard i -> tuple [| (ident "discard") ; (int i) |];;

let disp_state m l = tuple [| (mode_state m) ; (set (List.map ticket l)) |];;

(* iterator on all states of the dispenser *)

let rec disp_iter f n =
  if n <= 0
  then f []
  else
    disp_iter
      (fun l ->
	 begin
	   f l;
	   f ((n-1)::l);
	   ()
	 end)
      (n-1);;

let ss = ref [];;

let enum_ss n =
  disp_iter
    (fun l -> ss := l::(!ss))
    n;;

enum_ss 4;;

!ss;;

let emit_req = ident "emit_request";;

let emit i = tuple [| (ident "emit") ; (ticket i) |];;

let emit_fail = tuple [| (ident "emit") ; tuple [||] |];;

let discard_req i = tuple [| (ident "discard") ; (ticket i) |];;

let test_req i = tuple [| (ident "test") ; (ticket i) |];;

let resp b = tuple [| (ident "resp") ; (bool b) |]

let generic_dispenser n =
  new_interface
    (new_signature ();
     input emit_req;
     output emit_fail;
     output (resp false);
     output (resp true);
     for i=0 to n-1 do
       output (emit i);
       input (discard_req i);
       input (test_req i)
     done;
     define_signature ());
  init (disp_state Listen []);
  disp_iter
    (fun s ->
       begin
	 must (disp_state Listen s) emit_req (disp_state Emit s);
	 for i=0 to n-1 do
	   must (disp_state Listen s) (discard_req i) (disp_state (Discard i) s);
	   must (disp_state Listen s) (test_req i) (disp_state (Test i) s);
	   if List.mem i s
	   then
	     begin
	       must (disp_state (Discard i) s) (resp true) (disp_state Listen (List.filter (fun j -> i <> j) s));
	       must (disp_state (Test i) s) (resp true) (disp_state Listen s)
	     end
	   else
	     begin
	       must (disp_state (Discard i) s) (resp false) (disp_state Listen s);
	       must (disp_state (Test i) s) (resp false) (disp_state Listen s)
	     end
	 done;
	 let full = ref true in
	   for i=0 to n-1 do
	     if not (List.mem i s)
	     then
	       begin
		 full := false;
		 may (disp_state Emit s) (emit i) (disp_state Listen (i::s))
	       end
	   done;
	   if !full
	   then
	     must (disp_state Emit s) emit_fail (disp_state Listen s)
       end)
    n;
  define_interface ();;

let dispenser2 = simplify (generic_dispenser 2);;

let dispenser4 = simplify (generic_dispenser 4);;

let dispenser6 = simplify (generic_dispenser 6);;

let dispenser8 = simplify (generic_dispenser 8);;

let dispenser10 = simplify (generic_dispenser 10);;

let dispenser12 = simplify (generic_dispenser 12);;

info_interface dispenser2;;

display_interface dispenser2 "dispenser2";;

info_interface dispenser4;;

display_interface dispenser4 "dispenser4";;

info_interface dispenser6;;

info_interface dispenser8;;

info_interface dispenser10;;

info_interface dispenser12;;

print_statistics ();;
