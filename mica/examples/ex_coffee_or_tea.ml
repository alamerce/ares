(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * The classic coffee/tea vending machine
 *
 * $Id: ex_coffee_or_tea.ml 473 2011-12-05 12:40:10Z bcaillau $
 *
 * Run with mica
 *
 *)

(* Trivial interface *)

let tt =
  new_interface (new_signature (); define_signature ());
  init (int 0);
  define_interface ();;

(* conjunction of a list of interfaces *)

let fold_conjunction =
  function
      [] -> tt
    | c0::l -> List.fold_left conjunction c0 l;;

(* conjunction of a set of requirements *)

let conjunction_set rs =
  let l = List.map snd rs in
    simplify
      (minimize
	 (fold_conjunction l));;

(* non-zero iteration of a regular expression*)

let plus x = concat x (star x);;

(* Function [enum_min] enumerates minimal subsets of a given set of interfaces refining a given interface *)
(* Some admissible sets can be missed because weak implication is not complete *)

let rec enum_admi s =
  if is_trivial s
  then
    fun _ -> [[]]
  else
    function
	[] -> []
      | (e,r)::l ->
	  let s' = simplify (minimize (wimply s r)) in
	    if not (refines s' s) (* Have we gained anything? *)
	    then (* Yes *)
	      let k = enum_admi s' l in
	      let k' = List.map (fun x -> (e,r)::x) k in
		(enum_admi s l) @ k'
	    else (* No *)
	      (enum_admi s l);;

let elim_double h =
  List.fold_left
    (fun k t ->
       let m = List.length t in
	 if
	   List.exists
	     (fun t' ->
		let m' = List.length t in
		  (m=m') &&
		    (List.fold_left2
		       (fun b (e,s) (e',s') ->
			  b && (e=e') && (s==s'))
		       true t t'))
	     k
       then k
       else t::k)
    [] h;;
	 
let enum_min s l =
  let k1 = enum_admi s l in
  let k2 = List.map List.rev k1 in
  let k3 = List.flatten (List.map (enum_admi s) k2) in
  elim_double (List.map List.rev k3);;
    
(* Events *)

let coffee_req = ident "coffee_req";;
let tea_req = ident "tea_req";;
let coffee = ident "coffee";;
let tea = ident "tea";;

(* Signatures of the vending machine *)

let g1 =
  new_signature ();
  input coffee_req;
  input tea_req;
  output coffee;
  output tea;
  define_signature ();;

(* Requirements *)

(*
 *
 * R1 : coffee is served when requesting coffee, same thing with tea
 *
 *)

let req1 =
  simplify
    (minimize
	  (fold_conjunction
		[every_must g1 [| coffee_req ; coffee |];
		 every_must g1 [| tea_req ; tea |];
		 every_cannot g1 [| coffee_req ; tea |];
		 every_cannot g1 [| tea_req ; coffee |]]));;

info_interface req1;;

display_interface req1 "R1";;

(* R2 : atmost one beverage is delivered for each order *)

let req2 =
  simplify
    (minimize
       (fold_conjunction
	  [every_cannot g1 [| tea ; tea |];
	   every_cannot g1 [| tea ; coffee |];
	   every_cannot g1 [| coffee ; tea |];
	   every_cannot g1 [| coffee ; coffee |]]));;

info_interface req2;;

display_interface req2 "R2";;

(* Regular expression defining the standby mode of the machine *)

let standby =
  star
    (concat
       (plus (sum (prefix tea_req (epsilon ())) (prefix coffee_req (epsilon ()))))
       (plus (sum (prefix tea (epsilon ())) (prefix coffee (epsilon ())))));;

print_expression standby;;

(* R3: nothing should be delivered before an order is passed *)

let req3 =
  simplify
    (minimize
       (fold_conjunction
	  [
	    after_cannot g1 standby tea;
	    after_cannot g1 standby coffee 
	  ]));;

info_interface req3;;

display_interface req3 "R3";;

(* R4: the machine is receptive to inputs in standby mode *)

let req4 =
  simplify
    (minimize
       (conjunction
	  (after_must g1 standby tea_req)
	  (after_must g1 standby coffee_req)));;

info_interface req4;;

display_interface req4 "R4";;

(* Resulting global requirement *)

let reqs_0 = [ ("R1",req1); ("R2",req2); ("R3",req3); ("R4",req4) ];;

let global_req_0 = conjunction_set reqs_0;;

is_consistent global_req_0;;

info_interface global_req_0;;

display_interface global_req_0 "G0";;

(* Is the requirements set minimal? *)

let min_reqs_0 = enum_min global_req_0 reqs_0;; (* No: R2 is redundent *)

(* The new set of requirements is: *)

let reqs_1 = List.hd min_reqs_0;;

let global_req_1 = conjunction_set reqs_1;;

(*
 *
 * R5 : Now, we want to change the vending machine so that only the first action
 * on the tea_/coffee_req buttons is taken into account.
 *
 *)

let any_req =
  sum
    (prefix tea_req (epsilon ()))
    (prefix coffee_req (epsilon ()));;

let tea_is_req =
  concat
    standby
    (prefix tea_req (star any_req));;

print_expression tea_is_req;;

let coffee_is_req =
  concat
    standby
    (prefix coffee_req (star any_req));;

print_expression coffee_is_req;;

let req5 =
  simplify
    (minimize
       (fold_conjunction
	  [
	    after_must g1 tea_is_req tea;
	    after_must g1 coffee_is_req coffee
	  ]));;

info_interface req5;;

display_interface req5 "R5";;

(* Is R5 consistent with the others? *)

is_consistent (conjunction global_req_1 req5);; (* Yes *)

let reqs_2 = reqs_1 @ [ ("R5",req5) ];;

let global_req_2 = conjunction_set reqs_2;;

info_interface global_req_2;;

display_interface global_req_2 "G2";;

(* Are there any redundent requirement? *)

enum_min global_req_2 reqs_2;; (* No solution *)

(* The reason is that some requirements are conflictual with R5 *)
(* This can be fixed with a non-conflictual (redundent) requirement *)
(* The residual specification tells us what this requirement should be: *)

let res = simplify (minimize (List.fold_left (fun s (_,r) -> wimply s r) global_req_2 reqs_2));;

info_interface res;;

display_interface res "Residual";;
	  
is_trivial res;; (* coffee_req should be disabled after tea has been ordered *) 
  
(* The additional requirement takes care of this *)

let req6 =
  simplify
    (minimize
       (fold_conjunction
	  [
	    after_cannot g1 tea_is_req coffee_req;
	    after_cannot g1 coffee_is_req tea_req
	  ]));;

display_interface req6 "R6";;

refines req6 res;; (* True: R6 refines the residual *)

(* The new requirements set is: *)

let reqs_3 = reqs_2 @ [ ("R6",req6) ];;

let global_req_3 = conjunction_set reqs_3;;

(* We can now enumerate minimal requirements sets *)

let min_reqs_3 = enum_min global_req_3 reqs_3;; (* { R1, R3, R4, R6 } is minimal and admissible *)

(* Remark R5 is redundent. R6 is a non-conflictual requirement replacing R5 *)

(* Our final set of requirements is: *)

let reqs_4 = List.hd min_reqs_3;;
