(* The Parking example *)
let disp = true;;

(* Basic definitions: states, events *)

let q = [| int 0; int 1; int 2; int 3; int 4; int 5; int 6; int 7 |];;

let vs b = tuple [| ident "vehicule_sense"; bool b |];;

let tr = ident "ticket_request";;
let ti = ident "ticket_issue";;
let td = ident "ticket_discard";;
let tg = ident "ticket_grasp";;
let go = ident "gate_open";;
let gc = ident "gate_close";;

(* REQUIREMENTS *)

(* Contract 1: well-behaving cars can enter *)

let assumption_car_behaves_well =
  new_interface
    (new_signature ();
     output (vs true);
     output (vs false);
     input go;
     input gc;
     define_signature ());
  init q.(0);
  must q.(0) (vs false) q.(0);
  must q.(0) (vs true) q.(1);
  must q.(1) go q.(2);
  must q.(2) (vs false) q.(3);
  must q.(3) gc q.(0);
  define_interface ();;

if disp then display_interface assumption_car_behaves_well "assumption_car_behaves_well";;

print_signature (signature_of_interface assumption_car_behaves_well);;

let guarantee_car_can_enter =
  new_interface
    (new_signature ();
     input tr;
     input tg;
     output go;
     define_signature ());
  init q.(0);
  may q.(0) tr q.(1);
  may q.(1) tr q.(1);
  may q.(1) tg q.(2);
  must q.(2) go q.(0);
  may q.(2) tr q.(2);
  define_interface ();;

if disp then display_interface guarantee_car_can_enter "guarantee_car_can_enter";;

print_signature (signature_of_interface  guarantee_car_can_enter);;

let contract_car_can_enters_if_it_behaves_well =
  contract
    guarantee_car_can_enter
    assumption_car_behaves_well;;

info_interface contract_car_can_enters_if_it_behaves_well;;

is_consistent contract_car_can_enters_if_it_behaves_well;;

if disp then 
  display_interface
    contract_car_can_enters_if_it_behaves_well
    "contract_car_can_enters_if_it_behaves_well";;

print_signature (signature_of_interface contract_car_can_enters_if_it_behaves_well);;

(* Contract 2: Use case when the driver does not pickup the ticket *)

let assumption_no_ticket_pickup =
  new_interface
    (new_signature ();
     input ti;
     input td;
     output tg;
     output (vs false);
     output (vs true);
     define_signature ());
  init q.(0);
  must q.(0) (vs false) q.(0);
  must q.(0) (vs true) q.(0);
  must q.(0) td q.(0);
  must q.(0) tg q.(0);
  must q.(0) ti q.(1);
  must q.(1) (vs true) q.(1);
  must q.(1) (vs false) q.(2);
  must q.(2) (vs false) q.(2);
  must q.(2) td q.(0);
  define_interface ();;

let guarantee_ticket_is_discarded =
  new_interface
    (new_signature ();
     output ti;
     output td;
     input (vs false);
     input tg;
     define_signature ());
  init q.(0);
  may q.(0) td q.(0);
  may q.(0) tg q.(0);
  may q.(0) (vs false) q.(0);
  may q.(0) ti q.(1);
  may q.(1) tg q.(1);
  may q.(1) (vs false) q.(2);
  may q.(2) (vs false) q.(2);
  may q.(2) tg q.(2);
  must q.(2) td q.(0);
  define_interface ();;

let contract_ticket_is_discarded_if_not_pickedup =
  contract guarantee_ticket_is_discarded assumption_no_ticket_pickup;;

info_interface contract_ticket_is_discarded_if_not_pickedup;;

is_consistent contract_ticket_is_discarded_if_not_pickedup;;

if disp then display_interface contract_ticket_is_discarded_if_not_pickedup "contract_ticket_is_discarded_if_not_pickedup";;

print_signature (signature_of_interface contract_ticket_is_discarded_if_not_pickedup);;

(* the conjunction of both contracts *)

let requirements =
  conjunction contract_car_can_enters_if_it_behaves_well contract_ticket_is_discarded_if_not_pickedup;;

info_interface requirements;;

is_consistent requirements;; (* check consistency *)

if disp then display_interface requirements "requirements";; (* Congratulations if you can understand this giant interface *)

(* REFINEMENT *)

(* Viewpoint 1: Gate must be receptive to state changes. Stuttering is optional. *)

let sigma_entry_gate_receptive =
  new_signature ();
  input (vs true);
  input (vs false);
  define_signature ();;

let itf_entry_gate_receptive =
  new_interface sigma_entry_gate_receptive;
  init q.(0);
  must q.(0) (vs false) q.(1);
  must q.(0) (vs true) q.(2);
  may q.(1) (vs false) q.(1);
  must q.(1) (vs true) q.(2);
  must q.(2) (vs false) q.(1);
  may q.(2) (vs true) q.(2);
  define_interface ();;

if disp then display_interface itf_entry_gate_receptive "itf_entry_gate_receptive";;

(* Viewpoint 2: contextual receptivity to ticket_request *)

let sigma_entry_gate_request =
  new_signature ();
  input (vs true);
  input (vs false);
  input tr;
  define_signature ();;

let itf_entry_gate_request =
  new_interface sigma_entry_gate_request;
  init q.(0);
  must q.(0) tr q.(0);
  may q.(0) (vs false) q.(1);
  may q.(0) (vs true) q.(2);
  may q.(1) (vs false) q.(1);
  may q.(1) (vs true) q.(2);
  may q.(1) tr q.(1);
  may q.(2) (vs false) q.(1);
  may q.(2) (vs true) q.(2);
  must q.(2) tr q.(3);
  may q.(3) (vs false) q.(1);
  may q.(3) (vs true) q.(3);
  may q.(3) tr q.(3);
  define_interface ();;

if disp then display_interface itf_entry_gate_request "itf_entry_gate_request";;

(* Viewpoint 3: vehicule_sense, ticket, and gate protocol *)

let sigma_entry_gate_ticket =
  new_signature ();
  input (vs true);
  input (vs false);
  input tr;
  output ti;
  output td;
  input tg;
  output go;
  output gc;
  define_signature ();;

let itf_entry_gate_ticket =
  new_interface sigma_entry_gate_ticket;
  init q.(0);
  may q.(0) (vs false) q.(0);
  may q.(0) (vs true) q.(1);
  may q.(0) tr q.(0);
  may q.(1) (vs false) q.(0);
  may q.(1) (vs true) q.(1);
  may q.(1) tr q.(2);
  may q.(2) (vs false) q.(0);
  may q.(2) (vs true) q.(2);
  may q.(2) tr q.(2);
  must q.(2) ti q.(3);
  may q.(3) (vs false) q.(4);
  may q.(3) (vs true) q.(3);
  must q.(3) tg q.(5);
  may q.(3) tr q.(3);
  must q.(4) td q.(0);
  may q.(4) (vs true) q.(3);
  may q.(4) (vs false) q.(4);
  may q.(4) tr q.(4);
  may q.(5) (vs false) q.(0);
  may q.(5) (vs true) q.(5);
  must q.(5) go q.(6);
  may q.(5) tr q.(5);
  may q.(6) (vs false) q.(7);
  may q.(6) (vs true) q.(6);
  may q.(6) tr q.(6);
  must q.(7) gc q.(0);
  may q.(7) (vs false) q.(7);
  may q.(7) (vs true) q.(6);
  may q.(7) tr q.(7);
  define_interface ();;

if disp then display_interface itf_entry_gate_ticket "itf_entry_gate_ticket";;

(* Refinement = conjunction of the three viewpoints *)

let itf_entry_gate =
  conjunction
    (conjunction
       itf_entry_gate_receptive
       itf_entry_gate_request)
    itf_entry_gate_ticket;;

info_interface itf_entry_gate;;

print_signature (signature_of_interface itf_entry_gate);;

is_consistent itf_entry_gate;;

if disp then display_interface itf_entry_gate "itf_entry_gate";;

(* Check that gate specification does not refine the first contract *)

refines itf_entry_gate contract_car_can_enters_if_it_behaves_well;;

(* Yes. We move on to the second contract. *)

refines itf_entry_gate contract_ticket_is_discarded_if_not_pickedup;;

(* This completes the design step *)

(* Just to cross-check things, the specification refines the conjunction of both contracts *)

refines itf_entry_gate requirements;;

(* Exploring an incremental design method *)

(* are we done ? *)

info_interface requirements;;

is_complete requirements;; (* false *)

(* first step *)

let step1 = minimize (quotient requirements itf_entry_gate_receptive);;

info_interface step1;;

is_complete step1;; (* false *)

(* second step *)

let step2 = minimize (quotient step1 itf_entry_gate_request);;

info_interface step2;;

is_complete step2;; (* false *)

(* third step *)

let step3 = minimize (quotient step2 itf_entry_gate_ticket);;

info_interface step3;;

is_complete step3;; (* true *)

(* We are done *)

(* Remark that this does not work in general. It worked here because the following interface : *)

let itf_entry_gate' =
  product
    (product
       itf_entry_gate_receptive
       itf_entry_gate_request)
    itf_entry_gate_ticket;;

info_interface itf_entry_gate';;

if disp then display_interface itf_entry_gate' "itf_entry_gate_prime";;

(* is an abstraction of itf_entry_gate (this is a general property) *)

refines itf_entry_gate itf_entry_gate';; (* true *)

(* that refines the requirements (this is not true in general) *)

refines itf_entry_gate' requirements;; (* true *)

(* A more reasonable method is to use the weak implication operator introduced in: *)
(* Goessler and Raclet. Modal Contracts for Component-based design *)

(* Are the requirements trivial *)

is_trivial requirements;; (* false *)

(* first step *)

let stp1 = minimize (wimply requirements itf_entry_gate_receptive);;

info_interface stp1;;

is_trivial stp1;; (* false *)

(* second step *)

let stp2 = minimize (wimply stp1 itf_entry_gate_request);;

info_interface stp2;;

is_trivial stp2;; (* false *)

(* third step *)

let stp3 = minimize (wimply stp2 itf_entry_gate_ticket);;

info_interface stp3;;

is_trivial stp3;; (* true *)

(* We are done *)

(* The End *)

print_statistics ();;

