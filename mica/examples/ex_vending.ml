(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Vending machine
 *
 * $Id: ex_vending.ml 473 2011-12-05 12:40:10Z bcaillau $
 *
 *)

(* run with mica *)

let max_amount = 300;;

let max_coin = 200;;

(* events *)

let amounts = [ 60 ; 80 ; 120 ; 180 ; 210 ; 260 ];;

let coin n = tuple [| (ident "coin") ; (int n) |];;

let coins = [ 10; 20; 50; 100; 200 ];;

let coin_sorts = List.map coin coins;;

let request n = tuple [| (ident "request") ; (int n) |];;

let insert x = tuple [| (ident "insert") ; x |];;

let change x = tuple [| (ident "change") ; x |];;

let completed = ident "completed";;

let state n = int n;;

let standby = ident "standby";;

(* vending machine with change *)

(* signature *)

let g =
  new_signature ();
  List.iter
    (fun i ->
       input (request i))
    amounts;
  List.iter
    (fun c ->
       input (insert c);
       output (change c))
    coin_sorts;
  output completed;
  define_signature ();;

(* interface *)

let s =
  new_interface g;
  init standby;
  List.iter
    (fun i ->
       may standby (request i) (state i))
    amounts;
  may (state 0) completed standby;
  for i=1 to max_amount do
    List.iter
      (fun j -> may (state i) (insert (coin j)) (state (i-j)))
      coins
  done;
  for i= (-max_coin) to -1 do
    List.iter
      (fun j ->
	 if i+j <= 0
	 then must (state i) (change (coin j)) (state (i+j)))
      coins
  done;
  define_interface ();;

info_interface s;;
  
display_interface s "s";;

(* change is disabled *)

let g' =
  new_signature ();
  List.iter
    (fun c ->
       output (change c))
    coin_sorts;
  define_signature ();;

let s' =
  new_interface g';
  init (int 0);
  define_interface ();;

info_interface s';;

display_interface s' "s_prime";;

(* conjunction : vending machine that can not give change *)

let t = conjunction s s';;

info_interface t;;

display_interface t "t";;

(* statistics *)

print_statistics ();;
