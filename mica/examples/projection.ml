let a = ident "a"
and b = ident "b"
and c = ident "c";;

let f = new_signature (); output a; output b; output c; define_signature ();;

let g = new_signature (); output c; define_signature ();;

let p =
  new_interface f;
  init (int 0);
  may (int 0) a (int 1);
  must (int 0) b (int 2);
  must (int 1) c (int 3);
  define_interface ();;

let q = project g p;;

display_interface p "P";;
display_interface q "Q";;
