(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Pattern examples
 *
 * $Id: ex_pattern.ml 473 2011-12-05 12:40:10Z bcaillau $
 *
 * Run with Mica
 *
 *)

let a = ident "a";;
let b = ident "b";;
let c = ident "c";;
let d = ident "d";;

let g =
  new_signature ();
  input a;
  input b;
  input c;
  input d;
  define_signature ();;

let h =
  new_signature ();
  input a;
  input b;
  define_signature ();;

(*
 *
 * Within every four occurrences of event "a", there should be a "b" and
 * within every four occurrences of event "b", there should be a "a"
 *
 *)

let r =
  minimize
    (conjunction
       (every_cannot h [| a ; a ; a ; a |])
       (every_cannot h [| b ; b ; b ; b |]));;

info_interface r;;

display_interface r "r";;

(* "d" must be enabled after "aabacaab" *)

let s1 = every_must g [| a ; a ; b ; a ; c ; a ; a ; b ; d |];;

info_interface s1;;

display_interface s1 "s1";;

(* "a" must be enabled after "ab" *)

let s2 = every_must g [| a ; b ; a |];;

info_interface s2;;

display_interface s2 "s2";;

(* "a" must be enabled after "abacadab" *)

let s3 = every_must g [| a ; b ; a ; c ; a ; d ; a ; b ; a |];;

info_interface s3;;

display_interface s3 "s3";;

(* The conjunction of the above *)

let req =
  minimize
    (conjunction
       r
       (conjunction
	  s1
	  (conjunction s2 s3)));;

info_interface req;;

display_interface req "req";;
