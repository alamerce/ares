(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Some higher level operators to define modal interfaces
 *
 * $Id: ex_highlevel.ml 473 2011-12-05 12:40:10Z bcaillau $
 *
 *)

(* run with mica *)

(* representation of lists *)

let rec value_of_list f =
    function
	[] -> tuple [||]
      | x::l ->
	  tuple [| (f x) ; (value_of_list f l) |];;

(* higher level transitions *)

let hrel f ql el ql' =
  List.iter
    (fun q ->
       List.iter
	 (fun e ->
	    List.iter
	      (fun q' ->
		 f q e q')
	      ql')
	 el)
    ql;;

let htrans = hrel trans
and hmay = hrel may
and hmust = hrel must;;

(* self-loops *)

let self f l e =
  List.iter
    (fun q -> f q e q)
    l;;

let strans = self trans
and smay = self may
and smust = self must;;
