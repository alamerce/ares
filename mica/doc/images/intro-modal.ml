(* Run with mica *)

let a = (ident "a");;
let b = (ident "b");;
let c = (ident "c");;

let q0 = (int 0);;
let q1 = (int 1);;
let q2 = (int 2);;
let q3 = (int 3);;
let q4 = (int 4);;
let q5 = (int 5);;

let g =
  new_signature ();
  input a;
  input b;
  output c;
  define_signature ();;

let s =
  new_interface g;
  init q0;
  must q0 a q2;
  may q0 b q1;
  must q1 c q0;
  may q1 a q2;
  must q2 a q2;
  may q2 b q2;
  define_interface ();;

display_interface s "s";;

let m =
  new_system g;
  init q0;
  trans q0 a q1;
  trans q1 a q1;
  define_system ();;

display_system m "m";;

satisfies m s;;

let m' =
  new_system g;
  init q0;
  trans q0 b q1;
  trans q1 c q2;
  trans q2 b q3;
  trans q3 c q0;
  trans q0 a q4;
  trans q4 a q4;
  trans q1 a q5;
  trans q2 a q5;
  trans q5 a q5;
  trans q5 b q4;
  define_system ();;

display_system m' "m_prime";;

satisfies m' s;;

let m'' =
  new_system g;
  init q0;
  trans q0 a q2;
  trans q0 b q1;
  trans q1 a q2;
  trans q2 a q2;
  define_system ();;

display_system m'' "m_second";;

satisfies m'' s;;

let g' =
  new_signature ();
  input a;
  input b;
  define_signature ();;

let s' =
  new_interface g';
  init q0;
  may q0 b q1;
  may q1 b q0;
  must q0 a q0;
  may q1 a q1;
  define_interface ();;

display_interface s' "s_prime";;

let t = minimize (conjunction s s');;

display_interface t "t";;

let r = minimize (quotient t s');;

display_interface r "r";;

is_complete r;;
