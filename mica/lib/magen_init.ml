(*
 *
 ******************************************************************************
 *                                                                            *
 *        Mica : A Modal Interface Compositional Analysis Library             *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, Inria, 2011                *                                                                       *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Univ Rennes, 2018  *
 *                                                                            *
 * Distributed under the CeCILL-C Free Software Licence Agreement:            *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html                * 
 *                                                                            *
 ******************************************************************************
 *
 * mica_init: Mica interactive environment initialization
 *
 * This file is used when launching the Mica interpreter. It allows you to 
 * initialize the interpreter by loading the Inter module, by starting a new 
 * session, and by defining additional useful functions.
 *
 *)

(*============================================================================*)
(*=====   OPENING OF MODULE INTER AND STARTING NEW SESSION              ===== *)
(*============================================================================*)

open Inter;;

magen_new_session ();;


(*============================================================================*)
(*===== ADDITIONAL USEFUL FUNCTIONS                                     ===== *)
(*============================================================================*)

(*-----------------------------------------------------------------------------
 * Functions about regular expressions
 *---------------------------------------------------------------------------*)
  
(* non-zero iteration of a regular expression *)
let plus x = concat x (star x)

 
(* 1 letter 1 word expression *)
let token e = prefix e (epsilon ())

 
(* Sum iterator *)
let map_sum f =
  function
      [] -> empty ()
    | x::l ->
	List.fold_right
	  (fun y e -> sum e (f y))
	  l (f x)


(*-----------------------------------------------------------------------------
 * Functions about interfaces and systems
 *---------------------------------------------------------------------------*)

(* function composition operator *)
let ($$) = fun f g x -> f (g x)
  

(* simplified minimal interface *)
let simple = simplify $$ minimize

  
(* terminal max *)
let strong = interface_of_system $$ max_implementation

  
(* simplified minimal system *)
let minimal = min_implementation $$ simple $$ interface_of_system

  
(* Trivial interface *)
let tt =
  new_interface (new_signature (); define_signature ());
  init (int 0);
  define_interface ()

  
(* Conjunction of a list of interfaces *)
let fold_op f =
  function
      [] -> tt
    | c0::l -> List.fold_left f c0 l

let fold_conjunction = fold_op conjunction
                     
let fold_product = fold_op product

let fold_parallel =
  function
      [] -> invalid_arg "fold_parallel: empty list"
    | i0::l -> List.fold_left parallel i0 l

let collate l = simple (fold_conjunction l)
              
let gather l =  simple (fold_product l)

let map_collate f l =
  collate (List.map f l)

  
(* Force receptivity to an input event *)
let receptive x =
  let h = new_signature (); input x; define_signature () in
  every_must h [| x |]

let receptive_dual x =
  let h = new_signature (); output x; define_signature () in
  every_must h [| x |]
  
  
(*-----------------------------------------------------------------------------
 * Comparison function
 *---------------------------------------------------------------------------*)

(* type to describe a comparison relationship between two interfacesn*)
(* Type [comparison] is used by [compare_interface] *)
type comparison =
  | Lt (* Less than *)
  | Gt (* Greater than *)
  | Eq (* Equivalent *)
  | Ic (* Incomparable *)
  
(* compare (inter_1, inter_2): comparison of two interfaces *)
let compare inter_1 inter_2 =
  let _ = Printf.printf "Checking refinement...\n" in
  let refinement = refines inter_1 inter_2 in
  let _ = Printf.printf "Checking abstraction...\n" in
  let abstraction = refines inter_2 inter_1 in
    match (refinement, abstraction) with
    | (true,  true ) -> Eq
    | (true,  false) -> Lt
    | (false, true ) -> Gt
    | (false, false) -> Ic
    
