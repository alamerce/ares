(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Enum: enumerated sets
 *
 * $Id: enum.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

(* Sets may be represented either by enumerating its elements or the elements of its complement *)

type t = bool Mapping.t

(* val make_empty : unit -> t *)

let make_empty () = Mapping.make false

(* val make_universal : unit -> t *)

let make_universal () = Mapping.make true

(* val clone : t -> t *)

let clone s = Mapping.map (fun b -> b) s

(* val to_string : t -> string *)

let to_string s =
  let d = Mapping.default s in
    (if d
     then "C"
     else "")^
      "{ "^
      (Mapping.fold
	 (fun e b r ->
	    if b<>d
	    then r^((Heap.to_string e)^" ")
	    else r)
	 s
	 ""
      )^
      "}"
    
(* val print : t -> unit *)

let print s = print_string (to_string s)

(* val mem : t -> Heap.obj -> bool *)

let mem s x = Mapping.read s x

(* val set : t -> Heap.obj -> bool -> unit *)

let set s x b = Mapping.set s x b

(* Generic composition operator *)

let generic f s1 s2 =
  Mapping.map2 f s1 s2

(* val union : t -> t -> t *)

let union = generic (||)

(* val inter : t -> t -> t *)

let inter = generic (&&)

(* val subtract : t -> t -> t *)

let subtract =
  generic
    (fun b1 b2 -> b1 && (not b2))

(* val complement : t -> t *)

let complement =
  Mapping.map
    (not)

(* val diff : t -> t -> t *)

let diff = generic (<>)

(* val iter_in : (Heap.obj -> unit) -> t -> unit *)

let iter_in f s =
  if not (Mapping.default s)
  then
    Mapping.iter (fun x b -> if b then f x) s
  else
    invalid_arg "Enum.iter_in: can not iterate on the elements of a complement set"

(* val fold_in : (Heap.obj -> 'a -> 'a) -> t -> 'a -> 'a *)

let fold_in f s y0 =
  if not (Mapping.default s)
  then
    Mapping.fold (fun x b y -> if b then f x y else y) s y0
  else
    invalid_arg "Enum.fold_in: can not iterate on the elements of a complement set"

(* val iter_out : (Heap.obj -> unit) -> t -> unit *)

let iter_out f s =
  if Mapping.default s
  then
    Mapping.iter (fun x b -> if not b then f x) s
  else
    invalid_arg "Enum.iter_out: can not iterate on the elements of a complement set"

(* val fold_out : (Heap.obj -> 'a -> 'a) -> t -> 'a -> 'a *)

let fold_out f s y0 =
  if Mapping.default s
  then
    Mapping.fold (fun x b y -> if not b then f x y else y) s y0
  else
    invalid_arg "Enum.fold_out: can not iterate on the elements of a complement set"

(* val is_empty : t -> bool *)

let is_empty s =
  if not (Mapping.default s)
  then fold_in (fun _ _ -> false) s true
  else false

(* val is_universal : t -> bool *)

let is_universal s =
  if Mapping.default s
  then fold_out (fun _ _ -> false) s true
  else false

(* val is_included : t -> t -> t *)

let is_included s1 s2 =
  is_empty (subtract s1 s2)

(* [prod f a b] computes the cartesian product of sets [a] and [b], using function [f] to form pairs *)

(* val prod : (Heap.obj -> Heap.obj -> Heap.obj) -> t -> t -> t *)

let prod f s1 s2 =
  let s = make_empty () in
    begin
      iter_in
	(fun x1 ->
	   iter_in
	     (fun x2 ->
		set s (f x1 x2) true
	     )
	     s2
	)
	s1;
      s
    end

(* val card_in : t -> int *)

let card_in a = fold_in (fun _ n -> 1+n) a 0

(* val card_out : t -> int *)

let card_out a = fold_out (fun _ n -> 1+n) a 0

(* Exception for the witness functions *)

exception Found of Heap.obj

(* val witness_in : t -> Heap.obj *)

(* Returns an element in the set. Raises Not_found if set is empty *)

let witness_in a =
  try
    iter_in
      (fun e -> raise (Found e))
      a;
    raise Not_found
  with
      Found e -> e

(* val witness_out : t -> Heap.obj *)

(* Returns an element out of the set. Raises Not_found if the set is universal *)

let witness_out a =
  try
    iter_out
      (fun e -> raise (Found e))
      a;
    raise Not_found
  with
      Found e -> e

