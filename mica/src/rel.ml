(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 *  Rel: relations represented by extension
 *
 * $Id: rel.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
    {
      left  : Enum.t option Mapping.t;
      right : Enum.t option Mapping.t
    }

(* val make_empty : unit -> t *)

let make_empty () =
  {
    left = Mapping.make None;
    right = Mapping.make None
  }

(* val clone : t -> t *)

let clone r =
  let rl = Mapping.make None
  and rr = Mapping.make None in
    begin
      Mapping.iter
	(fun x1 ->
	   function
	       None -> ()
	     | Some s ->
		 Mapping.write rl x1 (Some (Enum.clone s)))
	r.left;
      Mapping.iter
	(fun x2 ->
	   function
	       None -> ()
	     | Some s ->
		 Mapping.write rr x2 (Some (Enum.clone s)))
	r.right;
      {
	left = rl;
	right = rr
      }
    end

(* val transpose : t -> t *)

let transpose r =
  let r' = clone r in
    {
      left = r'.right;
      right = r'.left
    }

(* val set : t -> Heap.obj -> Heap.obj -> bool -> unit *)

let set r x1 x2 b =
  begin
    begin
      match Mapping.read r.left x1 with
	  None ->
	    if b
	    then
	      let s = Enum.make_empty () in
		begin
		  Enum.set s x2 true;
		  Mapping.write r.left x1 (Some s)
		end
	| Some s ->
	    Enum.set s x2 b
    end;
    begin
      match Mapping.read r.right x2 with
	  None ->
	    if b
	    then
	      let s = Enum.make_empty () in
		begin
		  Enum.set s x1 true;
		  Mapping.write r.right x2 (Some s)
		end
	| Some s ->
	    Enum.set s x1 b
    end
  end

(* val get : t -> Heap.obj -> Heap.obj -> bool *)

let get r x1 x2 =
  match Mapping.read r.left x1 with
      None -> false
    | Some s -> Enum.mem s x2
