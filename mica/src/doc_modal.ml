(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Doc_modal: automated documentation of module Modal
 *
 * $Id: doc_modal.ml 474 2011-12-05 12:48:10Z bcaillau $
 *
 * Run with mica
 *
 *)

let folditer f q x0 =
  f Modal.Inconsistent
    (f (Modal.Must q)
       (f (Modal.May q)
	  (f Modal.Cannot x0)));;

let folditer2 f q1 q2 x0 =
  folditer (fun y1 -> folditer (f y1) q2) q1 x0;;

let handler = { Modal.compose = (fun q1 q2 -> q1^q2); Modal.distinguished = Some "* " };;

let string_of_mod =
  function
      Modal.Cannot -> "C "
    | Modal.May q -> "m"^q
    | Modal.Must q -> "M"^q
    | Modal.Inconsistent -> "I ";;

let truthtable op symb =
  folditer2
    (fun m1 m2 x ->
       ((string_of_mod m1)^" "^symb^" "^(string_of_mod m2)^" = "^(string_of_mod (op handler m1 m2))^"\n")^x)
    "1"
    "2"
    "";;

let printtable op symb =
  print_endline (truthtable op symb);;

printtable Modal.product "*";;
printtable Modal.conjunction "&";;
printtable Modal.quotient "/";;
printtable Modal.contract "@";;
