(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Mapping: mappings with heap objects as domain
 *
 * $Id: mapping.ml 378 2011-10-10 11:12:32Z bcaillau $
 *
 *)

(* Hash tables on Heap.obj keys *)

module Hash = Hashtbl.Make(Heap.Obj)

(* Mappings *)

type 'a t =
    {
      default : 'a; (* default value *)
      hash : 'a Hash.t
    }

(* default hash size *)

let default_hash_size = 37 (* 53 *) (* 227 *) (* hash table is initially rather small *)

(* makes a map *)

(* val make : 'a (* default value *) -> 'a t *)

let make y0 =
  {
    default = y0;
    hash = Hash.create default_hash_size
  }

(* read *)

(* val read : 'a t -> Heap.obj -> 'a *)

let read { default=y0; hash=h } x =
  try
    Hash.find h x
  with
      Not_found -> y0

(* write *)

(* val write : 'a t -> Heap.obj -> 'a -> unit *)

let write { hash=h } x y = Hash.replace h x y

(* unset *)

(* val unset : 'a t -> Heap.obj -> unit *)

let unset { hash=h } x = Hash.remove h x

(* set : uses unset to revert to default value. Tests value equality *)

(* val set : 'a t -> Heap.obj -> 'a -> unit *)

let set ({ default=y0 } as m) x y =
  if y=y0
  then unset m x
  else write m x y

(* sets : uses unset to revert to default value. Tests reference equality *)

(* val sets : 'a t -> Heap.obj -> 'a -> unit *)

let sets ({ default=y0 } as m) x y =
  if y==y0
  then unset m x
  else write m x y

(* default value *)

(* val default : 'a t -> 'a *)

let default { default=y0 } = y0

(* iterates on non trivial values *)

(* val iter : (Heap.obj -> 'a -> unit) -> 'a t -> unit *)

let iter f { hash=h } = Hash.iter f h

(* folds on non trivial values *)

(* val fold : (Heap.obj -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b *)

let fold f { hash=h } z0 = Hash.fold f h z0

(* map *)

(* val map : ('a -> 'b) -> 'a t -> 'b t *)

let map f m =
  let x0 = default m in
  let x0' = f x0 in
  let m' = make x0' in
    begin
      iter
	(fun x y -> sets m' x (f y))
	m;
      m'
    end

(* map2 *)

(* val map2 : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t *)

let map2 f m1 m2 =
  let x1 = default m1
  and x2 = default m2 in
  let x0 = f x1 x2 in
  let m = make x0
  and w = make false in
    begin
      iter
	(fun x y1 ->
	   let y2 = read m2 x in
	     begin
	       sets m x (f y1 y2);
	       write w x true
	     end)
	m1;
      iter
	(fun x y2 ->
	   if not (read w x)
	   then
	     let y1 = read m1 x in
	       sets m x (f y1 y2))
	m2;
      m
    end

(* compose *)

(* val compose : 'a t -> Heap.obj t -> 'a t *)

let compose m1 m2 =
  let x2 = default m2 in
  let x0 = read m1 x2 in
  let m = make x0 in
    begin
      iter
	(fun x y -> sets m x (read m1 y))
	m2;
      m
    end

