(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Ltf: labelled transition functions represented by extension
 *
 * $Id: ltf.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t

val make_empty : unit -> t

val clone : t -> t

val set : t -> Heap.obj (* from *) -> Heap.obj (* label *) -> Heap.obj option (* to *) -> unit

val get : t -> Heap.obj (* from *) -> Heap.obj (* label *) -> Heap.obj option (* to *)

(* iterate on enabled events *)

val ready_out_iter : (Heap.obj (* event *) -> Heap.obj (* to-state *) -> unit) -> t -> Heap.obj (* from-state *) -> unit

val ready_in_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit

(* iterate on (pre-)image states *)

val state_in_iter : (Heap.obj (* from-state *) -> unit) -> t -> Heap.obj (* to-state *) -> Heap.obj (* event *) -> unit

(* dfs *)

val dfs :
  (Heap.obj (* state *) -> unit) (* state procedure *) ->
  (Heap.obj (* from-state *) -> Heap.obj (* event *) -> Heap.obj (* to-state *) -> unit) (* transition procedure *) ->
  t (* relation *) -> Heap.obj (* root *) -> unit
