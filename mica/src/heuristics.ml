(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Heuristics: Compositional analysis heuristics
 *
 * $Id: mi.mli 476 2011-12-06 15:01:07Z bcaillau $
 *
 *)

let default_hash_size = 521 (* Default initial size of hash tables. A prime number *)

(* Debug functions *)

let print_set s =
  let n = Array.length s in
    begin
      Printf.printf "{ ";
      for i=0 to n-1 do
	if s.(i) then Printf.printf "%d " i
      done;
      Printf.printf " }\n"
    end

(*
 *
 * [max_consistent_sets w a] computes the list of maximal consistent subsets of [a]. [a] is an array of pairs identifier * interface.
 *
 *)

(* val max_consistent_sets : Heap.heap -> ('a * Mi.t) array -> 'a list list *)

let max_consistent_sets w r =
  let n = Array.length r in
  let consistent = Hashtbl.create default_hash_size
  and maximal = ref []
  in
  let rec dfs_incr s c i =
    let s' = Array.copy s in
      begin
	s'.(i) <- true;
	try
	  fst (Hashtbl.find consistent s')
	with
	    Not_found ->
	      let c' = Mi.conjunction w c (snd r.(i)) in
	      let b = Mi.is_consistent c' in
		begin
		  Hashtbl.add consistent s' (b,c');
		  (* Printf.printf "Entering "; *)
		  (* print_set s'; *)
		  begin
		    if b then
		      let is_max = ref true in
			begin
			  (* Printf.printf "Consistent "; *)
			  (* print_set s'; *)
			  for j=0 to n-1 do
			    if not s'.(j) then
			      is_max := (not (dfs_incr s' c' j)) && (!is_max)
			  done;
			  if !is_max then
			    begin
			      maximal := s' :: (!maximal);
			      (* Printf.printf "Maximal "; *)
			      (* print_set s' *)
			    end
			end
		  end;
		  (* Printf.printf "Exiting "; *)
		  (* print_set s'; *)
		  b
		end
      end
  and dfs s c  =
    try
      fst (Hashtbl.find consistent s)
    with
	Not_found ->
	  let b = Mi.is_consistent c in
	    begin
	      (* Printf.printf "Entering "; *)
	      (* print_set s; *)
	      Hashtbl.add consistent s (b,c);
	      begin
		if b then
		  let is_max = ref true in
		    begin
		      (* Printf.printf "Consistent "; *)
		      (* print_set s; *)
		      for j=0 to n-1 do
			if not s.(j) then
			  is_max := (not (dfs_incr s c j)) && (!is_max)
		      done;
		      if !is_max then
			begin
			  (* Printf.printf "Maximal "; *)
			  (* print_set s; *)
			  maximal := s :: (!maximal)
			end
		    end
	      end;
	      (* Printf.printf "Exiting "; *)
	      (* print_set s; *)
	      b
	    end
  and unit_vector n i =
    let s = Array.make n false in
      begin
	s.(i) <- true;
	s
      end
  in
  let is_max0 = ref true in
    begin
      for i=0 to n-1 do
	is_max0 := (not (dfs (unit_vector n i) (snd r.(i)))) && (!is_max0)
      done;
      begin
	if !is_max0 then maximal := [Array.make n false]
      end;
      List.map
	(fun s ->
	  let l = ref [] in
	    begin
	      for i=n-1 downto 0 do
		if s.(i) then l := (fst r.(i)) :: (!l)
	      done;
	      !l
	    end)
	(!maximal)
    end
