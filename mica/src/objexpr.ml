(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Objexpr: regular expressions on heap objects
 *
 * $Id: objexpr.ml 479 2011-12-09 08:39:29Z bcaillau $
 *
 *)

module Expr = Regexpr.Expr(Heap.Obj)

(* types *)

type e = Heap.obj
type h = Expr.h
type t = Expr.t

(* Module Hash *)

(* module Hash : Hashtbl.S with type key = t *)

module Hash = Expr.Hash

(* Initialization *)
  
(* val init : unit -> h *) 
(* allocates a fresh heap *)

let init = Expr.init

(* Basic functions on expressions *)

(* val equal : t -> t -> bool *)
(* tests equality *)

let equal = Expr.equal

(* val hash : t -> int *)
(* hash function *)

let hash = Expr.hash

(* val compare : t -> t -> int *)
(* comparison total order *)

let compare = Expr.compare

(* val to_string : t -> string *)
(* returns a string from an expression *)

let to_string = Expr.to_string

(* val print : t -> unit *)
(* prints an expression *)

let print = Expr.print
  
(* Expression factory *)
  
(* val make_empty : h -> t *)

let make_empty = Expr.make_empty

(* val make_epsilon : h -> t *)

let make_epsilon = Expr.make_epsilon

(* val make_prefix : h -> e -> t -> t *)

let make_prefix = Expr.make_prefix

(* val make_concat : h -> t -> t -> t *)

let make_concat = Expr.make_concat

(* val make_sum : h -> t -> t -> t *)

let make_sum = Expr.make_sum

(* val make_star : h -> t -> t *)
  
let make_star = Expr.make_star

(* val make_must : h -> e -> t *)

let make_must = Expr.make_must

(* val make_cannot : h -> e -> t *)

let make_cannot = Expr.make_cannot

(* [is_accepting x] decides whether expression [x] accepts epsilon *)  
(* val is_accepting : t -> bool *)

let is_accepting = Expr.is_accepting
  
(* [must_list x] computes the sorted list without duplicates of must events *)
(* val must_list : t -> e list *)

let must_list = Expr.must_list

(* [cannot_list x] computes the sorted list without duplicates of cannot events *)
(* val cannot_list : t -> e list *)

let cannot_list = Expr.cannot_list

(* [simplify w x] simplifies expression [x] in heap [w] *)
(* val simplify : h -> t -> t *)

let simplify = Expr.simplify
  
(* [reduce w x] reduces expression [x] in heap [w] *)
(* val reduce : h -> t -> t *)

let reduce = Expr.reduce
  
(* Brzozowski derivation of an expression *)
(* val derivate : h -> e -> t -> t *)

let derivate = Expr.derivate
  
(* iterates on derivatives until all derivatives have been produces *)
(* val dfs_derivatives : (t -> e -> t -> unit) -> h -> e list -> t -> unit *)
  
let dfs_derivatives = Expr.dfs_derivatives

(* Iterates on derivation until all derivatives have been produces. *)
(* Exploration of a branch is cut as soon as the operator returns [false] *)
(* val dfs_derivatives : (t -> e -> t -> bool) -> h -> e list -> t -> unit *)
  
let dfs_derivatives_cut = Expr.dfs_derivatives_cut

(* default hash table size *)

let hash_size = 521 (* a prime number *)

(* generates a modal interface from an expression *)

(*
 *
 * val to_interface :
 *   h (* expression heap *) ->
 *   Heap.heap (* object heap *) ->
 *   Signature.t (* signature of the resulting modal interface *) ->
 *   t (* expression *) ->
 *   Mi.t
 *
 *)

let to_interface v w g x =
  let n = ref 0 in
  let h = Hash.create hash_size in
  let state y =
    try
      Hash.find h y
    with
	Not_found ->
	  let i = !n in
	  let q = Heap.make_int w i in
	    begin
	      Hash.add h y q;
	      n := i+1;
	      q
	    end
  and l = Signature.fold (fun e _ k -> e::k) g []
  and x' = reduce v x in
  let s = Mi.make g in
    begin
      Mi.set_initial s (Some (state x'));
      dfs_derivatives_cut
	(fun y e z ->
	   let q = state y
	   and r = state z
	   and m_list = must_list y
	   and c_list = cannot_list y in
	   let m =
	     match ((List.memq e m_list),(List.memq e c_list)) with
		 (false,false) -> Modal.May r 
	       | (true,false) -> Modal.Must r
	       | (false,true) -> Modal.Cannot
	       | (true,true) -> Modal.Inconsistent in
	     begin
	       Mi.set_trans s q e m; (* set transition *)
	       match m with
		   Modal.Cannot | Modal.Inconsistent -> false (* cut branch *)
		 | Modal.May _ | Modal.Must _ -> true (* continue branch *)
	     end)
	v l x';
      (Mi.reduction s)
    end
	    
