(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Regexpr: regular expressions over an arbitrary alphabet
 *
 * $Id: regexpr.mli 479 2011-12-09 08:39:29Z bcaillau $
 *
 *)

(* Event module type *)

module type EVENT_TYPE =
sig
  type t
  val equal : t -> t -> bool
  val hash : t -> int
  val compare : t -> t -> int
  val to_string : t -> string
  val print : t -> unit
end

(* Functor Expr *)

module Expr : functor (Event : EVENT_TYPE) ->  
sig

  (* Types *)

  type e = Event.t (* events *)
  type t (* expressions *)
  type h (* heap *)

  (* Module Hash *)

  module Hash : Hashtbl.S with type key = t

  (* Initialization *)

  val init : unit -> h (* allocates a fresh heap *)

  (* Basic functions on expressions *)

  val equal : t -> t -> bool (* tests equality *)
  val hash : t -> int (* hash function *)
  val compare : t -> t -> int (* comparison total order *)
  val to_string : t -> string (* returns a string from an expression *)
  val print : t -> unit (* prints an expression *)

  (* Expression factory *)

  val make_empty : h -> t
  val make_epsilon : h -> t
  val make_prefix : h -> e -> t -> t
  val make_concat : h -> t -> t -> t
  val make_sum : h -> t -> t -> t
  val make_star : h -> t -> t
  val make_must : h -> e -> t
  val make_cannot : h -> e -> t

  (* [is_accepting x] decides whether expression [x] accepts epsilon *)

  val is_accepting : t -> bool

  (* [must_list x] computes the sorted list without duplicates of must events *)

  val must_list : t -> e list

  (* [cannot_list x] computes the sorted list without duplicates of cannot events *)

  val cannot_list : t -> e list 

  (* [simplify w x] simplifies expression [x] in heap [w] *)

  val simplify : h -> t -> t

  (* [reduce w x] reduces expression [x] in heap [w] *)

  val reduce : h -> t -> t

  (* Brzozowski derivation of an expression *)

  val derivate : h -> e -> t -> t

  (* Iterates on derivatives until all derivatives have been produces *)

  val dfs_derivatives : (t -> e -> t -> unit) -> h -> e list -> t -> unit

  (* iterates on derivatives until all derivatives have been produces *)
  (* Exploration of a branch is cut as soon as the operator returns [false] *)

  val dfs_derivatives_cut : (t -> e -> t -> bool) -> h -> e list -> t -> unit
end
