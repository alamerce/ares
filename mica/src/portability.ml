(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Portability module
 *
 * $Id: portability.ml 475 2011-12-05 12:51:05Z bcaillau $
 *
 *)

open Stringutil

type t =
    Linux
  | Darwin
  | Cygwin
  | Other

let os_assoc =
  [
    ("linux",Linux);
    ("darwin",Darwin);
    ("cygwin",Cygwin)
  ]

let rec find_os os =
  function
      [] -> Other
    | (k,t)::l ->
	match find_string os k with
	    Some _ -> t
	  | None -> find_os os l

let uname =
  try
    let n = Tempfile.make_name () in
      match
	Unix.system
	  ("uname -a > "^n)
      with
	  Unix.WEXITED 0 ->
	    let f = open_in n in
	    let u = input_line f in
	      begin
		close_in f;
		Sys.remove n;
		u
	      end
	| _ -> "Unknown"
  with
      _ -> "Unknown"

(* val os_type : t *)

let os_type =
  try
    find_os (String.lowercase_ascii uname) os_assoc
  with
      Not_found -> Other

