(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Explorer: an interactive modal interface explorer
 *
 * $Id$
 *
 *)

(* Data structure with tick boxes, events, texts and buttons *)

type event =
    {
      event_index : int;
      event_value : Heap.obj;
      event_label : string;
      mutable event_modality : Heap.obj Modal.t
    }

(* Initializes the event table *)

let make_event e d i =
  let t = (Orientation.to_string d)^(Heap.to_string e) in
    {
      event_index = i;
      event_value = e;
      event_label = t;
      event_modality = Modal.Inconsistent
    }

let make_event_table a =
  let s = Mi.signature a in
  let (_,l) =
    Signature.fold
      (fun e d (i,k) -> ((i+1),((make_event e d i)::k)))
      s
      (0,[]) in
    Array.of_list l

let update_event_table a t q =
  let n = Array.length t in
    for i = 0 to n-1 do
      t.(i).event_modality <- Mi.get_trans a q t.(i).event_value 
    done

(* default radius *)

let radius = 1

(* Interactive explorer *)

let explore a =
  match Mi.get_initial a with
      None -> 
	Printf.printf "Inconsistent interface. Exiting explorer.\n"
    | Some q0 ->
	let q = ref q0
	and quit = ref false in
	  begin
	    begin
	      (* Main loop *)
	      while not (!quit) do
		let rdyl = (* compute the list of pairs May or Must events with successor state *)
		  let l = ref [] in
		    begin
		      Mi.mod_out_iter
			  (fun e ->
			    function
				Modal.May q'
			      | Modal.Must q' -> l := (e,q')::(!l)
			      | Modal.Inconsistent
			      | Modal.Cannot -> ())
			a
			(!q);
		      !l
		    end
		  in
		let rdyv = Array.of_list rdyl in (* Turn it into an array *)
		let n = Array.length rdyv in
		  begin
		    Printf.printf "State %s:\n" (Heap.to_string (!q));
		    Display.zoom_specification_display a "Explorer" (!q) radius;
		    Printf.printf "  0) Exit\n";
		    for i=0 to n-1 do
		      Printf.printf "  %d) %s\n" (i+1) (Heap.to_string (fst rdyv.(i)))
		    done;
		    let c = ref (-1) in
		      begin
			while ((!c) < 0) || ((!c) > n) do
			  Printf.printf "  > ";
			  begin
			    try
			      c := read_int ();
			    with
				Failure _ -> c := -1
			  end;
			  if ((!c) < 0) || ((!c) > n)
			  then Printf.printf "  Invalid command.\n"
			done;
			if (!c) <= 0
			then quit := true
			else q := snd rdyv.((!c)-1)
		      end
		  end
	      done
	    end
	  end
	    
