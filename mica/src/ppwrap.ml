(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Wrappers for Pp document generators
 *
 * $Id: ppwrap.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

let line_length = 80

(* [to_channel g f x] writes document [g x] to channel [f] *)

(* val to_channel : ('a -> Pp.doc) -> out_channel -> 'a -> unit *)

let to_channel g f x =
  let d = g x in
    Pp.ppToFile f line_length d 

(* [to_file g n x] writes document [g x] to file [n] *)

(* val to_file : ('a -> Pp.doc) -> string -> 'a -> unit *)
 
let to_file g n x =
  let f = open_out n in
    try
      to_channel g f x;
      close_out f
    with
	y ->
	  begin
	    close_out f;
	    raise y
	  end

(*
 *
 * [to_command f c x] saves the document [f x] in a temporary file and then evaluates the command line [c],
 * replacing every substring $@ by the actual temporary file name. The file is discarded after the
 * completion of the command line
 *
 *)

(* val to_command : ('a -> Pp.doc) -> string  (* command *) -> 'a -> unit *)

let to_command g c x =
  let n = Tempfile.make_name () in
    try
      let c' = Stringutil.substitute "$@" n c in
	begin
	  to_file g n x;
	  begin
	    match Unix.system c' with
		Unix.WEXITED 0 -> ()
	      | Unix.WEXITED i -> failwith ("Ppwrap.to_command: command exited with return code "^(string_of_int i))
	      | Unix.WSIGNALED i -> failwith ("Ppwrap.to_command: command killed with signal "^(string_of_int i))
	      | Unix.WSTOPPED i -> failwith ("Ppwrap.to_command: command stopped with signal "^(string_of_int i))
	  end;
	  Sys.remove n
	end
    with
	z -> 
	  begin
	    Sys.remove n;
	    raise z
	  end

(* [display_dot g x] displays the output of [g x] using the GraphViz dot command *)

(* val display_dot : ('a -> Pp.doc) -> 'a -> unit *)

let display_dot g x =
  match Portability.os_type with
      Portability.Linux ->
	to_command
	  g
	  "dot -Tpdf -o $@.pdf $@; (xpdf $@.pdf &)"
	  x
    | Portability.Darwin ->
	to_command
	  g
	  "dot -Tsvg -o $@.svg $@; open $@.svg"
	  x
    | Portability.Cygwin ->
	failwith "Ppwrap.display_dot: OS type not supported"
    | Portability.Other ->
	failwith "Ppwrap.display_dot: OS type not supported"

(* [dot_to_pdf g n x] generates a PDF file named [n] from the output of [g x] using the GraphViz dot command *)

(* val dot_to_pdf : ('a -> Pp.doc) -> string -> 'a -> unit *)

let dot_to_pdf g n x =
  to_command
    g
    ("dot -Tpdf -o \""^n^"\" $@")
    x
