(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 *  Ltf : labelled transition functions represented by extension
 *
 * $Id: ltf.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
    {
      left : Heap.obj option Mapping.t option Mapping.t;
      right : Enum.t option Mapping.t option Mapping.t
    }

(* val make_empty : unit -> t *)

let make_empty () =
  {
    left = Mapping.make None;
    right = Mapping.make None
  }

(* val clone : t -> t *)

let clone r =
  let r' = make_empty () in
    begin

      (* clone from -> to labelled relation *)

      Mapping.iter
	(fun s ->
	   function
	       None -> ()
	     | Some w ->
		 let w' = Mapping.make None in
		   begin
		     Mapping.iter
		       (fun e ->
			  function
			      None -> ()
			    | Some s' ->
				Mapping.write w' e (Some s'))
		       w;
		     Mapping.write r'.left s (Some w')
		   end)
	r.left;

      (* clone to -> from labelled relation *)

      Mapping.iter
	(fun s ->
	   function
	       None -> ()
	     | Some w ->
		 let w' = Mapping.make None in
		   begin
		     Mapping.iter
		       (fun e ->
			  function
			      None -> ()
			    | Some ss ->
				Mapping.write w' e (Some (Enum.clone ss)))
		       w;
		     Mapping.write r'.right s (Some w')
		   end)
	r.right;      

      (* return clone *)

      r'
    end

(* val set : t -> Heap.obj (* from *) -> Heap.obj (* label *) -> Heap.obj option (* to *) -> unit *)

let set r s e s' =

  (* retrieve current to state if defined *)

  let os' =
    match Mapping.read r.left s with
	None -> None
      | Some u -> Mapping.read u e in

    begin
      
      (* unset right relation if required *)

      begin
	match os' with
	    None -> ()
	  | Some ost' ->
	      begin
		match Mapping.read r.right ost' with
		    None -> invalid_arg "Ltf.set: internal error (impossible case 1)"
		  | Some u ->
		      begin
			match Mapping.read u e with
			    None -> invalid_arg "Ltf.set: internal error (impossible case 2)"
			  | Some v -> Enum.set v s false
		      end
	      end
      end;

      (* set left function *)

      begin
	match Mapping.read r.left s with
	    None ->
	      begin
		match s' with
		    None -> ()
		  | Some st' ->
		      let u = Mapping.make None in
			begin
			  Mapping.write u e s';
			  Mapping.write r.left s (Some u)
			end
	      end
	  | Some u ->
	      Mapping.write u e s'
      end;

      (* set right relation *)
      
      begin
	match s' with
	    None -> ()
	  | Some st' ->
	      begin
		match Mapping.read r.right st' with
		    None ->
		      let u = Mapping.make None
		      and v = Enum.make_empty () in
			begin
			  Enum.set v s true;
			  Mapping.write u e (Some v);
			  Mapping.write r.right st' (Some u)
			end
		  | Some u ->
		      begin
			match Mapping.read u e with
			    None ->
			      let v = Enum.make_empty () in
				begin
				  Enum.set v s true;
				  Mapping.write u e (Some v)
				end
			  | Some v ->
			      Enum.set v s true
		      end
	      end
      end
    end

(* val get : t -> Heap.obj (* from *) -> Heap.obj (* label *) -> Heap.obj option (* to *) *)

let get r s e =
  match Mapping.read r.left s with
      None -> None
    | Some u ->
	Mapping.read u e

(* iterate on enabled events *)

(* val ready_out_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let ready_out_iter f r s =
  match Mapping.read r.left s with
      None -> ()
    | Some u ->
	Mapping.iter
	  (fun e ->
	     function
		 None -> ()
	       | Some s' -> f e s')
	  u

(* val ready_in_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let ready_in_iter f r s =
  match Mapping.read r.right s with
      None -> ()
    | Some u ->
	Mapping.iter
	  (fun e ->
	     function
		 None -> ()
	       | Some v ->
		   if not (Enum.is_empty v)
		   then f e)
	  u

(* iterate on preimage states *)

(* val state_in_iter : (Heap.obj (* from-state *) -> unit) -> t -> Heap.obj (* to-state *) -> Heap.obj (* event *) -> unit *)

let state_in_iter f r s e =
  match Mapping.read r.right s with
      None -> ()
    | Some u ->
	begin
	  match Mapping.read u e with
	      None -> ()
	    | Some v ->
		Enum.iter_in f v
	end

(* dfs *)

(*
 *
 * val dfs :
 *   (Heap.obj (* state *) -> unit) (* state procedure *) ->
 *   (Heap.obj (* from-state *) -> Heap.obj (* event *) -> Heap.obj (* to-state *) -> unit) (* transition procedure *)->
 *   t (* relation *) -> Heap.obj (* root *) -> unit
 *
 *)

let dfs fs ft r s0 =
  let v = Enum.make_empty () in
  let rec d s =
    if not (Enum.mem v s)
    then
      begin
	fs s;
	Enum.set v s true;
	ready_out_iter
	  (fun e s' ->
	     ft s e s';
	     d s')
	  r
	  s
      end
  in
    d s0
