(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 *  Signature: I/O signatures
 *
 * $Id: signature.ml 430 2011-11-03 10:29:46Z bcaillau $
 *
 *)

type t = Orientation.t Mapping.t

(* val make : unit -> t *)

let make () = Mapping.make Orientation.Undefined

(* val clone : t -> t *)

let clone s =
  let s' = make () in
    begin
      Mapping.iter
	(Mapping.set s')
	s;
      s'
    end

(* val set : t -> Heap.obj -> Orientation.t -> unit *)

let set s e d = Mapping.set s e d

(* val get : t -> Heap.obj -> Orientation.t *)

let get s e = Mapping.read s e

(* val is_in : t -> Heap.obj -> bool *)

let is_in s e = (get s e) <> Orientation.Undefined

(* val leq : (Heap.obj -> Orientation.t -> Orientation.t -> unit) -> t -> t -> bool *)

let leq h s1 s2 =
  (Mapping.fold
     (fun e d1 r ->
	r &&
	  (let d2 = get s2 e in
	      (Orientation.leq d1 d2) ||
		(h e d1 d2; false)))
     s1
     true) &&
    (Mapping.fold
       (fun e d2 r ->
	  r &&
	    (let d1 = get s1 e in
	       (Orientation.leq d1 d2) ||
	    (h e d1 d2; false)))
       s2
       true)

(* val is_consistent : t -> bool *)

let is_consistent s =
  Mapping.fold
    (fun e d r -> r && (d <> Orientation.Inconsistent))
    s
    true

(* Checks the consistency (wrt conjunction) of two signatures *)

(* val consistency :  (Heap.obj -> Orientation.t -> Orientation.t -> unit) -> t -> t -> bool *)

let consistency h s1 s2 =
  (Mapping.fold
     (fun e d1 r ->
	r &&
	  (let d2 = get s2 e in
	     (Orientation.consistency d1 d2) ||
	       (h e d1 d2; false)))
     s1
     true) &&
    (Mapping.fold
       (fun e d2 r ->
	  r &&
	    (let d1 = get s1 e in
	       (Orientation.consistency d1 d2) ||
		 (h e d1 d2; false)))
       s2
       true)

(* Generic composition operator for signatures with equal supports *)

let compose f s1 s2 = Mapping.map2 f s1 s2

(* val conjunction : t -> t -> t *)

let conjunction s1 s2 =
  compose Orientation.conjunction s1 s2

(* val lub : t -> t -> t *)

let lub = conjunction

(* val product : t -> t -> t *)

let product s1 s2 =
  compose Orientation.product s1 s2

(* val quotient : t -> t -> t *)

let quotient s1 s2 =
  compose Orientation.quotient s1 s2

(* val wimply : t -> t -> t *)

let wimply = conjunction

(* val contract : t -> t -> t *)

let contract s1 s2 =
  compose Orientation.contract s1 s2

(* Generoc extension operator for signatures with dissimilar supports *)

let extend f s1 s2 =
  let s = Mapping.map2 f s1 s2 in
  let s1' = Mapping.map fst s
  and s2' = Mapping.map snd s in
    (s1',s2')

(* Extensions for signatures with dissimilar supports *)

(* val extend_conj : t -> t -> t*t *)

let extend_conj s1 s2 = extend Orientation.extend_conj s1 s2

(* val extend_lub : t -> t -> t*t *)

let extend_lub = extend_conj

(* val extend_prod : t -> t -> t*t *)

let extend_prod s1 s2 = extend Orientation.extend_prod s1 s2

(* val extend_quot : t -> t -> t*t *)

let extend_quot s1 s2 = extend Orientation.extend_quot s1 s2

(* val extend_wimp : t -> t -> t*t *)

let extend_wimp = extend_conj

(* val extend_cont : t -> t -> t*t *)

let extend_cont s1 s2 = extend Orientation.extend_cont s1 s2

(* Disjoint union of two signatures *)

(* val merge : t -> t -> t *)

let merge s1 s2 = compose Orientation.merge s1 s2

(* val to_string : t -> string *)

let to_string s =
  let x =
    Mapping.fold
      (fun e d r ->
	 if d <> Orientation.Undefined
	 then
	   let z = (Heap.to_string e)^":"^(Orientation.to_string d)^" " in
	     r^z
	 else
	   r)
      s
      "" in
    "<< "^x^">>"

(* val print : t -> unit *)

let print s = print_string (to_string s)

(* val of_list : (Heap.obj * Orientation.t) list -> t *)

let of_list l =
  let s = make () in
    begin
      List.iter
	(fun (e,d) -> set s e d)
	l;
      s
    end

(* Extended composition operators *)

(* Generic extended composition operator *)

let ext_generic f g s1 s2 =
  let (s1',s2') = f s1 s2 in
  let s1'' = merge s1 s1'
  and s2'' = merge s2 s2' in
    g s1'' s2''

(* val ext_conj : t -> t -> t *)

let ext_conj s1 s2 = ext_generic extend_conj conjunction s1 s2

(* val ext_lub : t -> t -> t *)

let ext_lub = ext_conj

(* val ext_prod : t -> t -> t *)

let ext_prod s1 s2 = ext_generic extend_prod product s1 s2

(* val ext_quot : t -> t -> t *)

let ext_quot s1 s2 = ext_generic extend_quot quotient s1 s2

(* val ext_wimp : t -> t -> t *)

let ext_wimp = ext_conj

(* val ext_cont : t -> t -> t *)

let ext_cont s1 s2 = ext_generic extend_cont contract s1 s2

(* Iterators *)

(* val iter : (Heap.obj -> Orientation.t -> unit) -> t -> unit *)

let iter = Mapping.iter
  
(* val fold : (Heap.obj -> Orientation.t -> 'a -> 'a) -> t -> 'a -> 'a *)

let fold = Mapping.fold

(* Mutation *)

(* [mutation g h] tests whether [g] is a mutation of [h] *)

(* val mutation : t -> t -> bool *)

let mutation g h =
  (fold
    (fun e x r -> r && (Orientation.mutation x (get h e)))
    g
    true) &&
    (fold
       (fun e y r -> r && (Orientation.mutation (get g e) y))
       h
       true)

(* [mutate_inplace g e d] in-place mutates event [e] to orientation [d] in signature [g]. *)
(* Raises [Invalid_argument _] if [d] is not a correct mutation of [e] in [g]. *)

(* val mutate_inplace : t -> Heap.obj -> Orientation.t -> unit *)

let mutate_inplace g e d =
  if Orientation.mutation d (get g e)
  then
    set g e d
  else
    invalid_arg "Signature.mutate_inplace: not a valid mutation"

(* [mutate_implace_list g l] in-place mutates signature [g] according to the pairs (e,d) in [l] *)

(* val mutate_inplace_list : t -> (Heap.obj * Orientation.t) list -> unit *)

let mutate_inplace_list g l =
  List.iter
    (fun (e,d) -> mutate_inplace g e d)
    l

(* [mutate g e d] mutates event [e] to orientation [d] in a clone of signature [g]. *)

(* val mutate : t -> Heap.obj -> Orientation.t -> t *)

let mutate g e d =
  let h = clone g in
    begin
      mutate_inplace h e d;
      h
    end

(* [mutate_list g l] mutates a clone of signature [g] according to [l]. *)

(* val mutate_list : t -> (Heap.obj * Orientation.t) list -> t *)

let mutate_list g l =
  let h = clone g in
    begin
      mutate_inplace_list h l;
      h
    end

(* [restrict g s] computes the restriction of signature [g] to alphabet [s] *)

(* val restrict : t -> Enum.t -> t *)

let restrict g s =
  let f = make () in
    begin
      iter
	(fun e d ->
	   if Enum.mem s e
	   then set f e d)
	g;
      f
    end

(* [hidden g s] computes the set of hidden invents when restricting signature [g] to alphabet [s] *)

(* val hidden : t -> Enum.t -> Enum.t *)

let hidden g s =
  let r = Enum.make_empty () in
    begin
      iter
	(fun e d ->
	   if not (Enum.mem s e)
	   then Enum.set r e true)
	g;
      r
    end

(* [hide f g] computes the set of hidden events when restricting signature [f] to [g]. *)

(* val hide : t -> t -> Enum.t *)

let hide f g =
  let r = Enum.make_empty () in
    begin
      iter
	(fun e _ ->
	  match get g e with
	      Orientation.Undefined -> Enum.set r e true
	    | _ -> ())
	f;
      r
    end
