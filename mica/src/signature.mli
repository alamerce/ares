(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Signature: I/O signatures
 *
 * $Id: signature.mli 430 2011-11-03 10:29:46Z bcaillau $
 *
 *)

type t

val make : unit -> t

val clone : t -> t

val to_string : t -> string

val of_list : (Heap.obj * Orientation.t) list -> t

val print : t -> unit

val set : t -> Heap.obj -> Orientation.t -> unit

val get : t -> Heap.obj -> Orientation.t

val is_in : t -> Heap.obj -> bool

val leq : (Heap.obj -> Orientation.t -> Orientation.t -> unit) -> t -> t -> bool

val is_consistent : t -> bool

(* Composition operators for signatures with equal supports *)

val conjunction : t -> t -> t

val lub : t -> t -> t

val product : t -> t -> t

val quotient : t -> t -> t

val wimply : t -> t -> t

val contract : t -> t -> t

(* Extensions for signatures with dissimilar supports *)

val extend_conj : t -> t -> t*t

val extend_lub : t -> t -> t*t

val extend_prod : t -> t -> t*t

val extend_quot : t -> t -> t*t

val extend_wimp : t -> t -> t*t

val extend_cont : t -> t -> t*t

(* Disjoint union of two signatures *)

val merge : t -> t -> t

(* Extended composition operators *)

val ext_conj : t -> t -> t

val ext_lub : t -> t -> t

val ext_prod : t -> t -> t

val ext_quot : t -> t -> t

val ext_wimp : t -> t -> t

val ext_cont : t -> t -> t

(* Checks the consistency (wrt conjunction) of two signatures *)

val consistency :  (Heap.obj -> Orientation.t -> Orientation.t -> unit) -> t -> t -> bool

(* Iterators *)

val iter : (Heap.obj -> Orientation.t -> unit) -> t -> unit

val fold : (Heap.obj -> Orientation.t -> 'a -> 'a) -> t -> 'a -> 'a

(* Mutation *)

(* [mutation g h] tests whether [g] is a mutation of [h] *)

val mutation : t -> t -> bool

(* [mutate_inplace g e d] in-place mutates event [e] to orientation [d] in signature [g]. *)
(* Raises [Invalid_argument _] if [d] is not a correct mutation of [e] in [g]. *)

val mutate_inplace : t -> Heap.obj -> Orientation.t -> unit 

(* [mutate_implace_list g l] in-place mutates signature [g] according to the pairs (e,d) in [l] *)

val mutate_inplace_list : t -> (Heap.obj * Orientation.t) list -> unit

(* [mutate g e d] mutates event [e] to orientation [d] in a clone of signature [g]. *)

val mutate : t -> Heap.obj -> Orientation.t -> t

(* [mutate_list g l] mutates a clone of signature [g] according to [l]. *)

val mutate_list : t -> (Heap.obj * Orientation.t) list -> t

(* [restrict g s] computes the restriction of signature [g] to alphabet [s] *)

val restrict : t -> Enum.t -> t

(* [hidden g s] computes the set of hidden invents when restricting signature [g] to alphabet [s] *)

val hidden : t -> Enum.t -> Enum.t

(* [hide f g] computes the set of hidden events when restricting signature [f] to [g]. *)

val hide : t -> t -> Enum.t  
