(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * State and label identifier expressions
 *
 * $Id: idt.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
    Ident of string
  | Char of char
  | String of string
  | Bool of bool
  | Int of int
  | Tuple of t array
  | Set of t list
  | Observable of t
  | Silent
  | Input of t
  | Output of t

(* [compare x y] returns [0] if [x] is equal to [y], [-1] if [x] is less than [y] and [1] if [x] is greater than [y] *)

val compare : t -> t -> int 

(* [to_string x] returns a string representation of [x]. Control characters are escaped. *)

val to_string : t -> string

(* [print x] prints a representation of [x]. Control characters are escaped. *)

val print : t -> unit

(* [concat x y] concatenates [x] and [y] *)

val concat : t -> t -> t

(* [union x y] computes the union of sets [x] and [y] *)

val union : t -> t -> t

(* [set_of_tuple x] makes a set from tuple [x] *)

val set_of_tuple : t -> t

(* [tuple_of_set x] makes a tuple from set [x] *)

val tuple_of_set : t -> t

(* [tuple_of_scalar x] makes a singleton tuple of [x] *)

val tuple_of_scalar : t -> t

(* [set_of_scalar x] make a singleton set of [x] *)

val set_of_scalar : t -> t
