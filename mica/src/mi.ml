(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Mi: Modal interfaces
 *
 * $Id: mi.ml 478 2011-12-08 21:16:52Z bcaillau $
 *
 *)

type m = Heap.obj Modal.t

type t =
    {
      sgn : Signature.t;
      mutable ini : Heap.obj option;
      mde : m Mmap.t;
      tra : Ltf.t
    }

(* val make : Signature.t -> t *)

let make g =
  {
    sgn = Signature.clone g;
    ini = None;
    mde = Mmap.make Modal.Cannot;
    tra = Ltf.make_empty ()
  }

(* val clone : t -> t *)

let clone { sgn=g; ini=i; mde=m; tra=r } =
  {
    sgn = Signature.clone g;
    ini = i;
    mde = Mmap.clone m;
    tra = Ltf.clone r
  }

(* retrieves the signature *)

(* val signature : t -> Signature.t *)

let signature { sgn=g } = Signature.clone g

(* operators on initial states *)

(* val set_initial : t -> Heap.obj option -> unit *)

let set_initial a i = a.ini <- i

(* val get_initial : t -> Heap.obj option *)

let get_initial { ini=i } = i

(* [demod m] returns [None] if [m] is a [Cannot] or [Inconsistent] and [q] if [m] is [May q] or [Must q] *)

(* val demod : m -> Heap.obj option *)

let demod =
  function
      Modal.Cannot -> None
    | Modal.Inconsistent -> None
    | Modal.May q -> Some q
    | Modal.Must q -> Some q

(* operators on transitions *)

(* val set_trans : t -> Heap.obj -> Heap.obj -> m -> unit *)

let set_trans { sgn=g; mde=m; tra=r } q e x =
  if Signature.is_in g e
  then
    begin
      Mmap.set m q e x;
      Ltf.set r q e (demod x)
    end
  else
    invalid_arg ("Mi.set_trans: event "^(Heap.to_string e)^" does not belong to the signature")

(* val get_trans : t -> Heap.obj -> Heap.obj -> m *)

let get_trans { mde=m } q e = Mmap.read m q e

(* iterate on modalities *)

(* val mod_out_iter : (Heap.obj (* event *) -> m (* to state *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let mod_out_iter f ({ sgn=g; tra=r } as c) q =
  Signature.iter
    (fun e _ -> f e (get_trans c q e))
    g

(* val ready_in_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let ready_in_iter f { tra=r } q =
  Ltf.ready_in_iter f r q

(* iterate on preimage states *)

(* val state_in_iter : (m (* from-state *) -> unit) -> t -> Heap.obj (* to-state *) -> Heap.obj (* event *) -> unit *)

let state_in_iter f { tra=r } q e =
  Ltf.state_in_iter f r q e

(* dfs *)

(*
 *
 * val dfs :
 *   (Heap.obj (* state *) -> unit) (* state procedure *) ->
 *   (Heap.obj (* from-state *) -> Heap.obj (* event *) -> m (* to-state *) -> unit) (* transition procedure *)->
 *   t (* interface *) -> unit
 *
 *)

let dfs f g  =
  function
      { ini = None } -> ()
    | {
	sgn = s;
	ini = Some q0;
	tra = t;
	mde = m
      } as a ->
	let v = Enum.make_empty () in
	let rec d q =
	  if not (Enum.mem v q)
	  then
	    begin
	      f q;
	      Enum.set v q true;
	      Signature.iter
		(fun e _ ->
		   begin
		     g q e (get_trans a q e);
		     match Ltf.get t q e with
			 None -> ()
		       | Some q' -> d q'
		   end
		) s
	    end
	in
	  d q0

(*
 *
 * val dfs_history :
 *   (Heap.obj list (* reversed history *) -> Heap.obj (* state *) -> unit) (* state procedure *) ->
 *   (Heap.obj list (* reversed history *) -> Heap.obj (* from-state *) -> Heap.obj (* event *) ->
 *    m (* to-state *) -> unit) (* transition procedure *)->
 *   t (* interface *) -> unit
 *
 *)

let dfs_history f g  =
  function
      { ini = None } -> ()
    | {
	sgn = s;
	ini = Some q0;
	tra = t;
	mde = m
      } as a ->
	let v = Enum.make_empty () in
	let rec d h q =
	  if not (Enum.mem v q)
	  then
	    begin
	      f h q;
	      Enum.set v q true;
	      Signature.iter
		(fun e _ ->
		   begin
		     g h q e (get_trans a q e);
		     match Ltf.get t q e with
			 None -> ()
		       | Some q' -> d (e::h) q'
		   end
		) s
	    end
	in
	  d [] q0

(*
 *
 * val dfs_fold :
 *   (Heap.obj (* state *) -> 'a -> 'a) (* state operator *) ->
 *   (Heap.obj (* from-state *) -> Heap.obj (* event *) -> m (* to-state *) -> 'a -> 'a) (* transition operator *)->
 *   t (* interface *) -> 'a (* seed *) -> 'a
 *
 *)

let dfs_fold f g a x0 =
  let x = ref x0 in
    begin
      dfs
	(fun q -> x := f q (!x))
	(fun q e q' -> x := g q e q' (!x))
	a;
      !x
    end

(*
 *
 * val dfs_fold_history :
 *   (Heap.obj list (* reversed history *) -> Heap.obj (* state *) -> 'a -> 'a) (* state operator *) ->
 *   (Heap.obj list (* reversed history *) -> Heap.obj (* from-state *) -> Heap.obj (* event *) ->
 *     m (* to-state *) -> 'a -> 'a) (* transition operator *)->
 *   t (* interface *) -> 'a (* seed *) -> 'a
 *
 *)

let dfs_fold_history f g a x0 =
  let x = ref x0 in
    begin
      dfs_history
	(fun t q -> x := f t q (!x))
	(fun t q e m -> x := g t q e m (!x))
	a;
      !x
    end

(* [reachable_states a] computes the set of reachable states of interface [a] *)

(* val reachable_states : t -> Enum.t *)

let reachable_states a =
  let r = Enum.make_empty () in
    begin
      dfs
	(fun q -> Enum.set r q true)
	(fun _ _ _ -> ())
	a;
      r
    end

(* [print a] prints interface [a] *)

(* val print : t -> unit *)

let print ({sgn=g; ini=i} as a) =
  match i with
      None ->
	begin
	  print_string "{} : ";
	  Signature.print g;
	  print_newline ()
	end
    | Some q0 ->
	begin
	  print_string "{\n";
	  dfs
	    (fun q ->
	       if q=q0
	       then
		 begin
		   print_string " init state ";
		   Heap.print q;
		   print_endline ";"
		 end)
	    (fun q e ->
	       function
		   Modal.Cannot -> ()

(*

		     begin
		       print_string " cannot trans ";
		       Heap.print q;
		       print_string " // ";
		       Heap.print e;
		       print_endline " />;"
		     end

*)

		 | Modal.Inconsistent ->
		     begin
		       print_string " incons trans ";
		       Heap.print q;
		       print_string " ** ";
		       Heap.print e;
		       print_endline " *>;"
		     end
		 | Modal.May q' ->
		     begin
		       print_string " may trans ";
		       Heap.print q;
		       print_string " .. ";
		       Heap.print e;
		       print_string " .> ";
		       Heap.print q';
		       print_endline ";"
		     end
		 | Modal.Must q' ->
		     begin
		       print_string " must trans ";
		       Heap.print q;
		       print_string " -- ";
		       Heap.print e;
		       print_string " -> ";
		       Heap.print q';
		       print_endline ";"
		     end)
	    a;
	  print_string "} : ";
	  Signature.print g;
	  print_newline ()
	end

(* [normalize a q] normalizes inconsistent state [q] in-place in specification [a] *)

(* val normalize : t -> Heap.obj -> unit *)

let normalize ({ sgn=g } as a) q =
  Signature.iter (fun e _ -> set_trans a q e Modal.Inconsistent) g

(*
 *
 * [adjust a q e] discards the may modality of the [q] -- [e] -> transisition in specification [a].
 *
 * Returns [true] iff inconsistency is introduced.
 *
 *)

(* val adjust : t -> Heap.Obj -> Heap.obj -> unit *)

let adjust a q e =
  match get_trans a q e with
      Modal.Cannot -> false
    | Modal.Inconsistent -> false
    | Modal.May _ -> begin set_trans a q e Modal.Cannot; false end
    | Modal.Must _ -> begin set_trans a q e Modal.Inconsistent; true end

(* [reduce a] computes the in-place reduction of interface [a]. *)

(* val reduce : t -> unit *)

let reduce =
  function
      { ini=None } -> ()
    | ({ sgn=g; ini=Some q0; tra=t; mde=m } as a) ->
	let i = Enum.make_empty () (* set of inconsistent states *) in
	let rec redux q =
	  if not (Enum.mem i q) (* already found inconsistent? *)
	  then (* no *)
	    let ic =
	      Signature.fold
		(fun e _ r -> r ||
		   (match get_trans a q e with
			Modal.Inconsistent -> true
		      | _ -> false))
		g
		false in
	      if ic (* is state inconsistent? *)
	      then (* state is inconsistent *)
		begin
		  (* mark state visited *)
		  Enum.set i q true;
		  (* normalize state *)
		  normalize a q;
		  (* iterate on pre-states *)
		  Signature.iter
		    (fun e _ -> 
		       state_in_iter
			 (fun q' ->
			    if adjust a q' e
			    then redux q')
			 a q e)
		    g
		end
	in
	let rss = reachable_states a in
	  begin
	    (* iterate on reachable states *)
	    Enum.iter_in redux rss;
	    (* is initial state inconsistent? *)
	    if Enum.mem i q0
	    then (* yes *)
	      (* discard initial state *)
	      a.ini <- None
	  end

(* [reduction a] computes the reduction of [a] *)

(* val reduction : t -> t *)

let reduction a =
  let b = clone a in
    begin
      reduce b;
      b
    end

(* [top w] returns a top state allocated in heap [w] *)

(* val top : Heap.heap -> Heap.obj *)

let top w = Heap.make_empty_tuple w

(* [set_top a q] sets state [q] as a top state in specification [a] *)

(* val set_top : t -> Heap.obj -> unit *)

let set_top ({sgn=g} as a) q =
  Signature.iter
    (fun e _ -> set_trans a q e (Modal.May q))
    g

(*
 *
 * [isocompose w g h f a1 a2] computes the generic composition of specifications [a1] and [a2]
 * Function [f] is used to compute the composition of modal transitions. The resulting specification
 * has signature [g]. Specs [a1] and [a2] are assumed to have compatible signatures with equal supports.
 * States are allocated in heap [w]. Function [h] is used to compute the initial state.
 *
 *)

(*
 *
 * val isocompose :
 *       Heap.heap ->
 *       Signature.t ->
 *       (Heap.obj option -> Heap.obj option -> Heap.obj option) ->
 *       (m -> m -> m) -> t -> t -> t
 *
 *)

let isocompose w g h f a1 a2 =
  let a = make g (* resulting spec *)
  and v = Enum.make_empty () (* visited states *)
  and top_state = top w in
  let rec traverse q q1 q2 =
    if not (Enum.mem v q) (* Is stated already visited? *)
    then (* No *)
      begin
	(* Mark state visited *)
	Enum.set v q true;
	(* Iterate on signature elements *)
	Signature.iter
	  (fun e _ ->
	     let m1 = get_trans a1 q1 e
	     and m2 = get_trans a2 q2 e in
	     let m = f m1 m2 in
	       begin
		 set_trans a q e m;
		 match ((demod m),(demod m1),(demod m2)) with
		     ((Some q'),(Some q1'),(Some q2')) -> traverse q' q1' q2'
		   | (_,_,_) -> ()
	       end)
	  g
      end in
    begin
      set_top a top_state;
      let i1 = get_initial a1
      and i2 = get_initial a2 in
      let i = h i1 i2 in
	begin
	  set_initial a i;
	  match (i,i1,i2) with
	      ((Some q0),(Some q01),(Some q02)) -> traverse q0 q01 q02
	    | (_,_,_) -> ()
	end;
	a
    end

(* [compose_state w q1 q2] composes states [q1] and [q2] in heap [w] *)

let compose_states w q1 q2 = Heap.make_tuple w [| q1; q2 |] 

(* Generic composition handler to be used by all operators but the lub *)

let generic_composition_handler w =
  {
    Modal.compose=compose_states w;
    Modal.distinguished=Some (top w);
    Modal.left=None;
    Modal.right=None
  }

(* [compose_initial_xxx w i1 i2] composes initial states [i1] and [i2] in heap [w] *)
(* [compose_initial_comp] should be used for product and conjunction *)
(* [compose_initial_quot] should be used for quotient and contract   *)
(* [compose_initial_wimp] should be used for wimply *)

let compose_initial_comp w i1 i2 =
  match (i1,i2) with
      ((Some q1),(Some q2)) -> Some (compose_states w q1 q2)
    | (_,_) -> None

let compose_initial_quot w i1 i2 =
  match (i1,i2) with
      ((Some q1),(Some q2)) -> Some (compose_states w q1 q2)
    | (_,None) -> Some (top w)
    | (None,(Some _)) -> None

let compose_initial_wimp w i1 i2 =
  match (i1,i2) with
      ((Some q1),(Some q2)) -> Some (compose_states w q1 q2)
    | (_,None) -> Some (top w)
    | (None,(Some _)) -> None

(* [generic_iso_compose w g h f a1 a2] composes [a1] and [a2] according to modal operator [f] in heap [w]. *)
(* The result has signature [g]. Both components are assumed to have compatible signatures with [g]. *)
(* Parameter [h] is the initial state composition function. *)

(*
 *
 * val generic_iso_compose :
 *   Heap.heap -> Signature.t ->
 *   (Heap.heap -> Heap.obj option -> Heap.obj option -> Heap.obj option) ->
 *   ((Heap.obj, Heap.obj, Heap.obj) Modal.h -> m -> m -> m) ->
 *   t -> t -> t
 *
 *)

let generic_iso_compose w g h f a1 a2 =
  isocompose
    w
    g
    (h w)
    (f (generic_composition_handler w))
    a1
    a2

(* [extend h k a] returns the extension of [a] with signature extension [h] and self-loops of modality [k]. *)

let extend h k { sgn=g; ini=i; tra=r; mde=m } =
  let b =
    {
      sgn = Signature.merge g h;
      ini = i;
      tra = Ltf.clone r;
      mde = Mmap.clone m
    } in
    begin
      dfs
	(fun q ->
	   Signature.iter
	     (fun e _ -> set_trans b q e (k q))
	     h)
	(fun _ _ _ -> ())
	b;
      b
    end

let tomust q = Modal.Must q
and tomay q = Modal.May q

(*
 *
 * [generic_compose w f i k1 k2 a1 a2] composes [a1] and [a2] according to
 * modal operator [f] in heap [w]. [a1] is extended using self-loops with
 * modality [k1]. [a2] is extended using self-loops with modality [k2].
 * Signature extensions are computed with operator [x]. Composition of
 * extended signatures is done by operator [c]. Parameter [i] is the initial
 * state composition function.
 *
 *)

(*
 *
 * val generic_compose :
 *   Heap.heap ->
 *   (Signature.t -> Signature.t -> Signature.t * Signature.t) ->
 *   (Signature.t -> Signature.t -> Signature.t) ->
 *   ((Heap.obj, Heap.obj, Heap.obj) Modal.h -> m -> m -> m) ->
 *   (Heap.heap -> Heap.obj option -> Heap.obj option -> Heap.obj option) ->
 *   (Heap.obj -> m) -> (Heap.obj -> m) ->
 *   t -> t -> t
 *
 *)

let generic_compose w x c f i k1 k2 a1 a2 =
  let g1 = signature a1
  and g2 = signature a2 in
  let (h1,h2) = x g1 g2 in
  let b1 = extend h1 k1 a1
  and b2 = extend h2 k2 a2 in
  let g1' = Signature.merge g1 h1
  and g2' = Signature.merge g2 h2 in
  let g = c g1' g2' in
    generic_iso_compose w g i f b1 b2
    
(*
 *
 * [product w a b] computes the product of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

(* val product : Heap.heap -> t -> t -> t *)

let product w a1 a2 =
  generic_compose
    w
    Signature.extend_prod
    Signature.product
    Modal.product
    compose_initial_comp
    tomust
    tomust
    a1
    a2

(*
 *
 * [conjunction w a b] computes the conjunction of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

(* val conjunction : Heap.heap -> t -> t -> t *)

let conjunction w a1 a2 =
  let b =
    generic_compose
      w
      Signature.extend_conj
      Signature.conjunction
      Modal.conjunction
      compose_initial_comp
      tomay
      tomay
      a1
      a2 in
    reduction b

(*
 *
 * [quotient w a b] computes the quotient of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

(* val quotient : Heap.heap -> t -> t -> t *)

let quotient w a1 a2 =
  let b =
    generic_compose
      w
      Signature.extend_quot
      Signature.quotient
      Modal.quotient
      compose_initial_quot
      tomay
      tomust
      a1
      a2 in
    reduction b

(*
 *
 * [compatible_quotient w a b] computes the compatible quotient of interface [a]
 * by interface [b]. It is the largest [x] such that [x] is compatible with [b]
 * and [product x b] refines [a]. Product states are stored in heap [w].
 *
 *)

(* val compatible_quotient : Heap.heap -> t -> t -> t *)

let compatible_quotient w a1 a2 =
  (* Signatures *)
  let g1 = signature a1
  and g2 = signature a2 in
  let (h1,h2) = Signature.extend_quot g1 g2 in
  let g1' = Signature.merge g1 h1
  and g2' = Signature.merge g2 h2 in
  let g = Signature.quotient g1' g2' in
  (* initialize resulting interface *)
  let a = make g (* resulting spec *)
  and v = Enum.make_empty () (* visited states *)
  and top_state = top w in
  let rec traverse q q1 q2 =
    if not (Enum.mem v q) (* Is stated already visited? *)
    then (* No *)
      begin
	(* Mark state visited *)
	Enum.set v q true;
	(* Iterate on signature elements *)
	Signature.iter
	  (fun e _ ->
	     let d1 = Signature.get g1 e
	     and d2 = Signature.get g2 e
	     and m1 = get_trans a1 q1 e
	     and m2 = get_trans a2 q2 e in
	     let (m,r) = 
	       match (d1,m1,d2,m2) with

		   (* [e] is in both signatures *)

		   (Orientation.Output,Modal.Cannot,Orientation.Output,Modal.Cannot) ->
		     ((Modal.May top_state),None)
		 | (Orientation.Output,Modal.Cannot,Orientation.Input,Modal.Cannot) ->
		     (Modal.Cannot,None)
		 | (Orientation.Output,Modal.Cannot,Orientation.Output,(Modal.May q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Output,Modal.Cannot,Orientation.Input,(Modal.May q2')) ->
		     (Modal.Cannot,None)
		 | (Orientation.Output,Modal.Cannot,Orientation.Output,(Modal.Must q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Output,Modal.Cannot,Orientation.Input,(Modal.Must q2')) ->
		     (Modal.Cannot,None)
		 | (Orientation.Input,Modal.Cannot,Orientation.Output,Modal.Cannot) ->
		     ((Modal.May top_state),None)
		 | (Orientation.Input,Modal.Cannot,Orientation.Input,Modal.Cannot) ->
		     (Modal.Cannot,None)
		 | (Orientation.Input,Modal.Cannot,Orientation.Output,(Modal.May q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Input,Modal.Cannot,Orientation.Input,(Modal.May q2')) ->
		     (Modal.Cannot,None)
		 | (Orientation.Input,Modal.Cannot,Orientation.Output,(Modal.Must q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Input,Modal.Cannot,Orientation.Input,(Modal.Must q2')) ->
		     (Modal.Cannot,None)
		 | (Orientation.Output,(Modal.May q1'),Orientation.Output,Modal.Cannot) ->
		     ((Modal.May top_state),None)
		 | (Orientation.Output,(Modal.May q1'),Orientation.Input,Modal.Cannot) ->
		     (Modal.Cannot,None)
		 | (Orientation.Output,(Modal.May q1'),Orientation.Output,(Modal.May q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))
		 | (Orientation.Output,(Modal.May q1'),Orientation.Input,(Modal.May q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.May q'),(Some (q',q1',q2')))
		 | (Orientation.Output,(Modal.May q1'),Orientation.Output,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))
		 | (Orientation.Output,(Modal.May q1'),Orientation.Input,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.May q'),(Some (q',q1',q2')))
		 | (Orientation.Input,(Modal.May q1'),Orientation.Output,Modal.Cannot) ->
		     ((Modal.May top_state),None)
		 | (Orientation.Input,(Modal.May q1'),Orientation.Input,Modal.Cannot) ->
		     ((Modal.May top_state),None)
		 | (Orientation.Input,(Modal.May q1'),Orientation.Output,(Modal.May q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))
		 | (Orientation.Input,(Modal.May q1'),Orientation.Input,(Modal.May q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.May q'),(Some (q',q1',q2')))
		 | (Orientation.Input,(Modal.May q1'),Orientation.Output,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))
		 | (Orientation.Input,(Modal.May q1'),Orientation.Input,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.May q'),(Some (q',q1',q2')))
		 | (Orientation.Output,(Modal.Must q1'),Orientation.Output,Modal.Cannot) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Output,(Modal.Must q1'),Orientation.Input,Modal.Cannot) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Output,(Modal.Must q1'),Orientation.Output,(Modal.May q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Output,(Modal.Must q1'),Orientation.Input,(Modal.May q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Output,(Modal.Must q1'),Orientation.Output,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))
		 | (Orientation.Output,(Modal.Must q1'),Orientation.Input,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))
		 | (Orientation.Input,(Modal.Must q1'),Orientation.Output,Modal.Cannot) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Input,(Modal.Must q1'),Orientation.Input,Modal.Cannot) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Input,(Modal.Must q1'),Orientation.Output,(Modal.May q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Input,(Modal.Must q1'),Orientation.Input,(Modal.May q2')) ->
		     (Modal.Inconsistent,None)
		 | (Orientation.Input,(Modal.Must q1'),Orientation.Output,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))
		 | (Orientation.Input,(Modal.Must q1'),Orientation.Input,(Modal.Must q2')) ->
		     let q' = compose_states w q1' q2' in ((Modal.Must q'),(Some (q',q1',q2')))

                   (* [e] is not in the numerator's signature. May modality is assumed *)

		 | (Orientation.Undefined,_,Orientation.Output,Modal.Cannot) ->
		     ((Modal.May top_state),None)
		 | (Orientation.Undefined,_,Orientation.Input,Modal.Cannot) ->
		     (Modal.Cannot,None)
		 | (Orientation.Undefined,_,Orientation.Output,(Modal.May q2')) ->
		     let q' = compose_states w q1 q2' in ((Modal.Must q'),(Some (q',q1,q2')))
		 | (Orientation.Undefined,_,Orientation.Input,(Modal.May q2')) ->
		     (Modal.Cannot,None)
		 | (Orientation.Undefined,_,Orientation.Output,(Modal.Must q2')) ->
		     let q' = compose_states w q1 q2' in ((Modal.Must q'),(Some (q',q1,q2')))
		 | (Orientation.Undefined,_,Orientation.Input,(Modal.Must q2')) ->
		     let q' = compose_states w q1 q2' in ((Modal.May q'),(Some (q',q1,q2')))

		   (* [e] is not in the denominator's signature. Must modality is assumed *)

		 | (Orientation.Output,Modal.Cannot,Orientation.Undefined,_) ->
		     (Modal.Cannot,None)
		 | (Orientation.Input,Modal.Cannot,Orientation.Undefined,_) ->
		     (Modal.Cannot,None)
		 | (Orientation.Output,(Modal.May q1'),Orientation.Undefined,_) ->
		     let q' = compose_states w q1' q2 in ((Modal.May q'),(Some (q',q1',q2)))
		 | (Orientation.Input,(Modal.May q1'),Orientation.Undefined,_) ->
		     let q' = compose_states w q1' q2 in ((Modal.May q'),(Some (q',q1',q2)))
		 | (Orientation.Output,(Modal.Must q1'),Orientation.Undefined,_) ->
		     let q' = compose_states w q1' q2 in ((Modal.Must q'),(Some (q',q1',q2)))
		 | (Orientation.Input,(Modal.Must q1'),Orientation.Undefined,_) ->
		     let q' = compose_states w q1' q2 in ((Modal.Must q'),(Some (q',q1',q2)))

                   (* Error cases *)

		 | (Orientation.Inconsistent,_,_,_) ->
		     invalid_arg
		       ("Mi.compatible_quotient: Event "^
			   (Heap.to_string e)^
			   " has an inconsistent orientation in the numerator")
		 | (_,_,Orientation.Inconsistent,_) ->
		     invalid_arg
		       ("Mi.compatible_quotient: Event "^
			   (Heap.to_string e)^
			   " has an inconsistent orientation in the denominator")

		   (* Impossible cases *)

		 | (Orientation.Undefined,_,Orientation.Undefined,_) ->
		     invalid_arg
		       ("Mi.compatible_quotient: Event "^
			   (Heap.to_string e)^
			   " is undefined in both interfaces")
		 | (_,Modal.Inconsistent,_,_) ->
		     invalid_arg
		       ("Mi.compatible_quotient: Event "^
			   (Heap.to_string e)^
			   " has inconsistent modality in the numerator")
		 | (_,_,_,Modal.Inconsistent) ->
		     invalid_arg
		       ("Mi.compatible_quotient: Event "^
			   (Heap.to_string e)^
			   " has inconsistent modality in the denominator")
	     in
	       begin
		 set_trans a q e m;
		 match r with
		     Some (q',q1',q2') -> traverse q' q1' q2'
		   | None -> ()
	       end)
	  g
      end in
    begin
      set_top a top_state;
      let i1 = get_initial a1
      and i2 = get_initial a2 in
      let i = compose_initial_quot w i1 i2 in
	begin
	  set_initial a i;
	  match (i,i1,i2) with
	      ((Some q0),(Some q01),(Some q02)) -> traverse q0 q01 q02
	    | (_,_,_) -> ()
	end;
	reduce a;
	a
    end

(*
 *
 * [wimply w a b] computes the weak implication of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

(* val wimply : Heap.heap -> t -> t -> t *)

let wimply w a1 a2 =
  generic_compose
    w
    Signature.extend_wimp
    Signature.wimply
    Modal.wimply
    compose_initial_wimp
    tomay
    tomay
    a1
    a2

(*
 *
 * [contract w g a] computes the contract where [g] is the guarantee and
 * [a] is the assumption. Product states are stored in heap [w].
 *
 *)

(* val contract : Heap.heap -> t -> t -> t *)

let contract w a1 a2 =
  generic_compose
    w
    Signature.extend_cont
    Signature.contract
    Modal.contract
    compose_initial_quot
    (* tomust *) tomay
    tomust
    a1
    a2

(* Satisfaction failure handler type *)

type satisfaction_failure_handler =
    {
      satisfaction_signature : Heap.obj -> Orientation.t -> Orientation.t -> unit;
      satisfaction_inconsistent : unit -> unit;
      satisfaction_not_a_system : unit -> unit;
      satisfaction_modality : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> unit
    }

(* [string_of_orientation d] returns a string describing orientation [d] *)

let string_of_orientation =
  function
      Orientation.Undefined -> "undefined"
    | Orientation.Input -> "input"
    | Orientation.Output -> "output"
    | Orientation.Inconsistent -> "inconsistent"

(* val print_signature_failure : Heap.obj -> Orientation.t -> Orientation.t -> unit *)

let print_signature_failure e m n =
  Printf.printf "   ### Event %s has orientation %s in left-hand interface and orientation %s in right-hand interface.\n"
    (Heap.to_string e)
    (string_of_orientation m)
    (string_of_orientation n)

(* [string_of_trace t] returns a string describing the path defined by trace [t] *)

let rec string_of_trace =
  function
      [] -> "epsilon"
    | [e] -> Heap.to_string e
    | e::t -> (string_of_trace t)^"."^(Heap.to_string e) 

(* [string_of_modality m] returns a string describing modality [m] *)

let string_of_modality =
  function
      Modal.May _ -> "may"
    | Modal.Must _ -> "must"
    | Modal.Cannot -> "cannot"
    | Modal.Inconsistent -> "inconsistent"

(* val print_satisfies_failure : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> unit *)

let print_satisfies_failure t e m =
  Printf.printf "   ### After %s, event %s has modality %s in interface.\n"
    (string_of_trace t)
    (Heap.to_string e)
    (string_of_modality m)

(* Satisfaction failure handlers *)

(* val satisfaction_handler_print : satisfaction_failure_handler *)

let satisfaction_handler_print =
  {
    satisfaction_signature = print_signature_failure;
    satisfaction_inconsistent =
      (fun () -> Printf.printf "   ### Interface is inconsistent.\n");
    satisfaction_not_a_system =
      (fun () -> Printf.printf "   ### System has no initial state.\n");
    satisfaction_modality = print_satisfies_failure
  }

(* val satisfaction_handler_quiet : satisfaction_failure_handler *)

let satisfaction_handler_quiet =
  {
    satisfaction_signature = (fun _ _ _ -> ());
    satisfaction_inconsistent = (fun _ -> ());
    satisfaction_not_a_system = (fun _ -> ());
    satisfaction_modality = (fun _ _ _ -> ())
  }

(*
 *
 * [satisfies h m a] checks whether system [m] satisfies specification [a].
 * Term [h.satisfaction_signature e c d] is evaluated if orientation [c] of event [e]
 * in [m] does not satisfy orientation [d] of [e] in [a]. Term [h.satisfaction_not_a_system ()]
 * is evaluated whenever [a] has no initial state. Term [h.satisfaction_inconsistent ()] is
 * evaluated whenever [a] is inconsistent. Term [h.satisfaction_modality t e m] is evaluated whenever
 * satisfaction fails after trace [t], where event [e] has modality [m].
 *
 *)

(*
 *
 * val satisfies :
 *   satisfaction_failure_handler ->
 *   System.t -> t -> bool
 *
 *)

let satisfies h m a =
  let v = Rel.make_empty ()
  and s = Rel.make_empty ()
  and x = signature a in
  let rec test t r q =
    if Rel.get v r q (* visited? *)
    then Rel.get s r q 
    else (* No *)
      let z = ref true in
	begin
	  (* mark state pair as visited *)
	  Rel.set v r q true;
	  (* assume state pair in simulation relation *)
	  Rel.set s r q true;
	  (* Tests whether every must event is enabled and no cannot event is enabled and no event is inconsistent *)
	  mod_out_iter
	    (fun e ->
	       function
		   (Modal.Must _) as x ->
		     if not (System.is_enabled m r e)
		     then
		       begin
			 z := false;
			 h.satisfaction_modality t e x
		       end
		 | Modal.Cannot as x ->
		     if System.is_enabled m r e
		     then
		       begin
			 z := false;
			 h.satisfaction_modality t e x
		       end
		 | Modal.Inconsistent as x ->
		     begin
		       z := false;
		       h.satisfaction_modality t e x
		     end
		 | Modal.May _ -> ())
	    a
	    q;
	  (* Tests the existance of a simulation relation *)
	  System.ready_out_iter
	    (fun e ->
	       (* Does [e] belong to the specification's signature [x]? *)
	       if Signature.is_in x e
	       then (* Yes, [e] belongs to [x] *)
		 match demod (get_trans a q e) with
		     None -> ()
		   | Some q' ->
		       System.state_out_iter
			 (fun r' -> 
			    z := (!z) && (test (e::t) r' q'))
			 m
			 r
			 e
	       else (* No, [e] does not belong to [x] *)
		 System.state_out_iter
		   (fun r' ->
		      z := (!z) && (test (e::t) r' q))
		   m
		   r
		   e)
	    m
	    r;
	  (* set/reset state pair in simulation relation *)
	  Rel.set s r q (!z);
	  (* returns the satisfaction flag *)
	  !z
	end in
    match get_initial a with
	None ->
	  begin
	    h.satisfaction_inconsistent ();
	    false
	  end
      | Some q0 ->
	  let z = ref true
	  and i = ref false in
	    begin
	      System.iter_initial
		(fun r0 ->
		   begin
		     i := true;
		     z := (!z) && (test [] r0 q0)
		   end)
		m;
	      (if not (!i) then h.satisfaction_not_a_system ());
	      (!i) && (!z)
	    end

(* Consistency handler *)

type side = Left | Right

type consistency_failure_handler =
    {
      consistency_signature : Heap.obj -> Orientation.t -> Orientation.t -> unit;
      consistency_inconsistent : side -> unit;
      consistency_modality : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> Heap.obj Modal.t -> unit
    }

(*
 *
 * [consistency h a1 a2] decides whether interfaces [a1] and [a2] are mutually consistent (the conjunction of [a1]
 * and [a2] is consistent). [h.consistency_signature e o1 o2] is evaluated whenever a signature inconsitency is
 * found on event [e] with orientation [o1] in [a1] and [o2] in [a2]. [h.consistency_modality t e m1 m2] is
 * evaluated whenever a modal inconsistency is found: after trace [t], event [e] has modality [m1] in [a1] and [m2]
 * in [a2]. [h.consistency_inconsistent s] is evaluated whenever [a1] or [a2] is inconsistent.
 *
 *)

(* val consistency : consistency_failure_handler -> t -> t -> bool *)

let consistency h a b =
  let x = signature a
  and y = signature b in
    (Signature.consistency h.consistency_signature x y) &&
      (let v = Rel.make_empty () (* visited relation *)
       and z = Signature.ext_conj x y in
       let rec test t q r =
	 (Rel.get v q r) || (* visited? if so, the state pair is assumed consistent *)
	   (let d = ref true in (* Not yet *)
	      begin
		(* mark state pair as visited *)
		Rel.set v q r true;
		(* Tests local modal consistency *)
		Signature.iter
		  (fun e _ ->
		     match ((Signature.is_in x e),(Signature.is_in y e)) with
			 (true,true) -> (* event is in both interfaces *)
			   let m = get_trans a q e
			   and n = get_trans b r e in
			     begin
			       match (m,n) with
				   (Modal.Inconsistent,_)
				 | (_,Modal.Inconsistent)
				 | ((Modal.Must _),Modal.Cannot)
				 | (Modal.Cannot,(Modal.Must _)) ->
				     begin
				       d := false;
				       h.consistency_modality t e m n
				     end
				 | _ -> ()
			     end
		       | (true,false) -> (* event is local to the left-hand interface *)
			   let m = get_trans a q e in
			     begin
			       match m with
				   Modal.Inconsistent ->
				     begin
				       d := false;
				       h.consistency_modality t e m (Modal.May r)
				     end
				 | _ -> ()
			     end
		       | (false,true) -> (* event is local to the right-hand interface *)
			   let n = get_trans b r e in
			     begin
			       match n with
				   Modal.Inconsistent ->
				     begin
				       d := false;
				       h.consistency_modality t e (Modal.May q) n
				     end
				 | _ -> ()
			     end
		       | (false,false) -> failwith "Mi.consistency: impossible case 1. Report bug to developpers.")
		  z;
		(* iterate on next state pairs *)
		Signature.iter
		  (fun e _ ->
		     match ((Signature.is_in x e),(Signature.is_in y e)) with
			 (true,true) -> (* event is in both interfaces *)
			   let m = get_trans a q e
			   and n = get_trans b r e in
			     begin
			       match (m,n) with
				   ((Modal.Must q'),(Modal.May r'))
				 | ((Modal.May q'),(Modal.Must r'))
				 | ((Modal.Must q'),(Modal.Must r')) ->
				     d := (!d) && (test (e::t) q' r')
				 | _ -> ()
			     end
		       | (true,false) -> (* event is local to the left-hand interface *)
			   let m = get_trans a q e in
			     begin
			       match m with
				 | Modal.Must q' ->
				     d := (!d) && (test (e::t) q' r)
				 | _ -> ()
			     end
		       | (false,true) -> (* event is local to the right-hand interface *)
			   let n = get_trans b r e in
			     begin
			       match n with
				   Modal.Must r' ->
				     d := (!d) && (test (e::t) q r')
				 | _ -> ()
			     end
		       | (false,false) -> failwith "Mi.consistency: impossible case 2. Report bug to developpers.")
		  z;
		(* done for this state. Return consistency status *)
		!d
	      end) in
	 match ((get_initial a),(get_initial b)) with
	     (None,_) ->
	       begin
		 h.consistency_inconsistent Left;
		 false
	       end
	   | (_,None) ->
	       begin
		 h.consistency_inconsistent Right;
		 false
	       end
	   | ((Some q0),(Some r0)) -> test [] q0 r0)

(* val print_consistency_failure : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> Heap.obj Modal.t -> unit *)

let print_consistency_failure t e m n =
  Printf.printf "   ### After %s, event %s has modality %s in left-hand interface and modality %s in right-hand interface.\n"
    (string_of_trace t)
    (Heap.to_string e)
    (string_of_modality m)
    (string_of_modality n)
    
(* val print_consistency_inconsistent : side -> unit *)

let print_consistency_inconsistent s =
  Printf.printf "   ### %s-hand interface is inconsistent"
    (match s with
	 Left -> "Left"
       | Right -> "Right")

(* printers for the mutual consistency checker *)

let consistency_handler_print =
  {
    consistency_signature = print_signature_failure;
    consistency_inconsistent = print_consistency_inconsistent;
    consistency_modality = print_consistency_failure;
  }

let consistency_handler_quiet =
  {
    consistency_signature = (fun _ _ _ -> ());
    consistency_inconsistent = (fun _ -> ());
    consistency_modality = (fun _ _ _ _ -> ())
  }

(* Refinement failure handler *)

type refinement_failure_handler =
    {
      refinement_signature : Heap.obj -> Orientation.t -> Orientation.t -> unit;
      refinement_inconsistent : unit -> unit;
      refinement_modality : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> Heap.obj Modal.t -> unit
    }

(*
 *
 * [refines f a b] checks whether specification [a] refines specification [b].
 * Term [f t e m n] is evaluated whenever refinement fails after trace [t] ([q],[r])
 * on event [e], because left-hand modality [m] does not refine right-hand modality [n].
 *
 *)

(*
 *
 * val refines :
 *   refinement_failure_handler ->
 *   t -> t -> bool
 *
 *)

let refines h a b =
  let v = Rel.make_empty () (* visited relation *)
  and s = Rel.make_empty () (* refinement relation *)
  and x = signature a
  and y = signature b in
  let rec test t q r =
    if Rel.get v q r (* visited? *)
    then Rel.get s q r 
    else (* No *)
      let z = ref true in
	begin
	  (* mark state pair as visited *)
	  Rel.set v q r true;
	  (* assume state pair in simulation relation *)
	  Rel.set s q r true;
	  (* Tests local modal refinement *)
	  mod_out_iter
	    (fun e ->
	       function
		   Modal.May _ -> ()
		 | (Modal.Must _) as m ->
		     begin
		       match get_trans a q e with
			   Modal.Must _ -> ()
			 | Modal.Inconsistent -> ()
			 | n ->
			     begin
			       z := false;
			       h.refinement_modality t e n m
			     end
		     end
		 | Modal.Cannot ->
		     begin
		       match get_trans a q e with
			   Modal.Cannot -> ()
			 | Modal.Inconsistent -> ()
			 | n ->
			     begin
			       z := false;
			       h.refinement_modality t e n Modal.Cannot
			     end
		     end
		 | Modal.Inconsistent ->
		     begin
		       match get_trans a q e with
			   Modal.Inconsistent -> ()
			 | n ->
			     begin
			       z := false;
			       h.refinement_modality t e n Modal.Inconsistent
			     end
		     end)
	    b
	    r;
	  (* Tests the existance of a simulation relation *)
	  mod_out_iter
	    (fun e m ->
	       match demod m with
		   None -> ()
		 | Some q' ->
		     (* Does [e] belong to the specification's signature [x]? *)
		     if Signature.is_in y e
		     then (* Yes, [e] belongs to [y] *)
		       match demod (get_trans b r e) with
			   None -> ()
			 | Some r' -> z := (!z) && (test (e::t) q' r')
	       else (* No, [e] does not belong to [y] *)
		 z := (!z) && (test (e::t) q' r))
	    a
	    q;
	  (* set/reset state pair in simulation relation *)
	  Rel.set s r q (!z);
	  (* call [g] if simulation fails *)
	  (* (if not (!z) then g r q); *)
	  (* returns the satisfaction flag *)
	  !z
	end in
    (Signature.leq h.refinement_signature x y) &&
      (match ((get_initial a),(get_initial b)) with
	   (None,_) -> true
	 | ((Some _),None) ->
	     begin
	       h.refinement_inconsistent ();
	       false
	     end
	 | ((Some q0),(Some r0)) -> test [] q0 r0)

(* val print_refines_failure : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> Heap.obj Modal.t -> unit *)

let print_refines_failure t e m n =
  Printf.printf "   ### After %s, event %s has modality %s in left-hand interface and modality %s in right-hand interface.\n"
    (string_of_trace t)
    (Heap.to_string e)
    (string_of_modality m)
    (string_of_modality n)
    
(* val print_refines_inconsistence : unit -> unit *)

let print_refines_inconsistence () =
  Printf.printf "   ### Left-hand interface is consistent and right-hand system is inconsistent.\n"

(* val refines_handler_print : refinement_failure_handler *)

let refines_handler_print =
  {
    refinement_signature = print_signature_failure;
    refinement_inconsistent = print_refines_inconsistence;
    refinement_modality = print_refines_failure
  }

(* val refines_handler_quiet : refinement_failure_handler *)

let refines_handler_quiet =
  {
    refinement_signature = (fun _ _ _ -> ());
    refinement_inconsistent = (fun () -> ());
    refinement_modality = (fun _ _ _ _ -> ())
  }

(* val print_fail_event : Heap.obj -> Heap.obj -> Heap.obj -> Modal.t -> unit *)

let print_fail_event r q e m =
  Printf.printf "  ### Failure in state pair (%s,%s): event %s has modality %s.\n"
    (Heap.to_string r)
    (Heap.to_string q)
    (Heap.to_string e)
    (match m with
	 Modal.May _ -> "may"
       | Modal.Must _ -> "must"
       | Modal.Cannot -> "cannot"
       | Modal.Inconsistent -> "inconsistent")

(* val print_fail_state : Heap.obj -> Heap.obj -> unit *)

let print_fail_state r q =
  Printf.printf "   ### State %s does not satisfy/refine state %s.\n"
    (Heap.to_string r)
    (Heap.to_string q)

(* val print_fail_empty : unit -> unit *)

let print_fail_empty () =
  Printf.printf "   ### Empty system or specification.\n"

(* val is_consistent : t -> bool *)

let is_consistent =
  function
      { ini=None } -> false
    | ({ sgn=g; ini=Some q0; tra=t; mde=m } as a) ->
	let v = Enum.make_empty () (* set of visited states *) in
	let rec search q =
	  (not (Enum.mem v q)) (* already visited? *) &&
	    (Enum.set v q true; (* Mark state visited *)
	      (* Check whether state is inconsistent *)
	      (Signature.fold
		 (fun e _ r -> r ||
		    (match get_trans a q e with
			 Modal.Inconsistent -> true
		       | _ -> false))
		 g
		 false) (* stop here if state is inconsistent *) ||
		(* otherwise, reach states through must transitions *)
		(Signature.fold
		    (fun e _ r -> r ||
		       (match get_trans a q e with
			    Modal.Must q' -> search q'
			  | _ -> false))
		    g
		    false)) in
	  not (search q0)

(* Completeness check handlers *)

type completeness_handler =
    {
      completeness_inconsistent : unit -> unit;
      completeness_orientation : Heap.obj (* event *) -> Orientation.t -> unit;
      completeness_modality : Heap.obj list (* trace *) -> Heap.obj (* state *) -> Heap.obj (*event *) -> Heap.obj Modal.t -> unit
    }

(* val completeness_handler_print : completeness_handler *)

let completeness_handler_print =
  {
    completeness_inconsistent =
      (fun () -> Printf.printf "   ### Specification is inconsistent.\n");
    completeness_orientation =
      (fun e d  ->
	 Printf.printf "   ### Event %s has orientation %s.\n"
	   (Heap.to_string e)
	   (string_of_orientation d));
    completeness_modality =
      (fun t q e m ->
	 Printf.printf "   ### After %s, in state %s, event %s has modality %s.\n"
	   (string_of_trace t)
	   (Heap.to_string q)
	   (Heap.to_string e)
	   (string_of_modality m))
  }

(* val completeness_handler_quiet : completeness_handler *)

let completeness_handler_quiet =
  {
    completeness_inconsistent = (fun _ -> ());
    completeness_orientation = (fun _ _  -> ());
    completeness_modality = (fun _ _ _ _ -> ())
  }

(*
 *
 * [is_complete h s] checks whether specification [s] is complete, meaning that:
 * (i) it has no output or inconsistent event and (ii) it has no cannot or inconsistent
 * transition. Term [h.completeness_inconsistent ()] is evaluated if [s] is inconsistent.
 * Term [h.completeness_orientation e d] is evaluated if event [e] has orientation [d]
 * different from [Input]. Term [h.completeness_modality q e m] is evaluated if in state
 * [q] event [e] has modality [m] equal to [Cannot] or [Inconsistent].
 *
 *)

(* val is_complete : completeness_handler -> t -> bool *)

let is_complete h s =
  let g = signature s in
  (Signature.fold
     (fun e d r ->
	r &&
	  (match d with
	       Orientation.Input -> true
	     | Orientation.Undefined -> true
	     | _ ->
		 begin
		   h.completeness_orientation e d;
		   false
		 end))
     g
     true) &&
    (match get_initial s with
	 None ->
	   begin
	     h.completeness_inconsistent ();
	     false
	   end
       | Some _ ->
	   dfs_fold_history
	     (fun _ _ r -> r)
	     (fun t q e m r ->
		r &&
		  (match m with
		       Modal.Cannot ->
			 begin
			   h.completeness_modality (List.rev t) q e m;
			   false
			 end
		     | Modal.Inconsistent ->
			 begin
			   h.completeness_modality (List.rev t) q e m;
			   false
			 end
		     | _ -> true))
	     s
	     true)

(* Triviality check handler *)

type triviality_handler =
    {
      triviality_inconsistent : unit -> unit;
      triviality_orientation : Heap.obj (* event *) -> Orientation.t -> unit;
      triviality_modality : Heap.obj (* state *) -> Heap.obj (*event *) -> Heap.obj Modal.t -> unit
    }

(* val triviality_handler_print : triviality_handler *)

let triviality_handler_print =
  {
    triviality_inconsistent =
      (fun () -> Printf.printf "   ### Specification is inconsistent.\n");
    triviality_orientation =
      (fun e d  ->
	 Printf.printf "   ### Event %s has orientation %s.\n"
	   (Heap.to_string e)
	   (string_of_orientation d));
    triviality_modality =
      (fun q e m ->
	 Printf.printf "   ### In state %s, event %s has modality %s.\n"
	   (Heap.to_string q)
	   (Heap.to_string e)
	   (string_of_modality m))
  }

(* val triviality_handler_quiet : triviality_handler *)

let triviality_handler_quiet =
  {
    triviality_inconsistent = (fun _ -> ());
    triviality_orientation = (fun _ _  -> ());
    triviality_modality = (fun _ _ _ -> ())
  }

(*
 *
 * [is_trivial h s] checks whether specification [s] is trivial, meaning that:
 * (i) it has no inconsistent event and (ii) it has only may transitions. Term
 * [h.triviality_inconsistent ()] is evaluated if [s] is inconsistent.
 * Term [h.triviality_orientation e d] is evaluated if event [e] has orientation
 * [Inconsistent]. Term [h.triviality_modality q e m] is evaluated if in state
 * [q] event [e] has modality [m] different from [May _].
 *
 *)

(* val is_trivial : triviality_handler -> t -> bool *)

let is_trivial h s =
  let g = signature s in
  (Signature.fold
     (fun e d r ->
	r &&
	  (match d with
	       Orientation.Input -> true
	     | Orientation.Output -> true
	     | Orientation.Undefined -> true
	     | _ ->
		 begin
		   h.triviality_orientation e d;
		   false
		 end))
     g
     true) &&
    (match get_initial s with
	 None ->
	   begin
	     h.triviality_inconsistent ();
	     false
	   end
       | Some _ ->
	   dfs_fold
	     (fun _ r -> r)
	     (fun q e m r ->
		r &&
		  (match m with
		       Modal.Cannot ->
			 begin
			   h.triviality_modality q e m;
			   false
			 end
		     | Modal.Must _ ->
			 begin
			   h.triviality_modality q e m;
			   false
			 end
		     | Modal.Inconsistent ->
			 begin
			   h.triviality_modality q e m;
			   false
			 end
		     | _ -> true))
	     s
	     true)

(* Factor check handler *)

type factor_handler =
    {
      factor_inconsistent : unit -> unit;
      factor_orientation : Heap.obj (* event *) -> Orientation.t -> unit;
      factor_modality : Heap.obj list (* trace *) -> Heap.obj (*event *) -> Heap.obj Modal.t -> unit
    }

(* val factor_handler_print : factor_handler *)

let factor_handler_print =
  {
    factor_inconsistent =
      (fun () ->
	Printf.printf "   ### Left-hand interface is consistent and right-hand interface is inconsistent.\n");
    factor_orientation =
    (fun e d ->
      Printf.printf "   ### Event %s has orientation %s in the right-hand interface. However, it is an output of the left-hand interface.\n"
	   (Heap.to_string e)
	   (string_of_orientation d));
    factor_modality = 
    (fun t e m ->
      Printf.printf "   ### After %s, event %s has modality %s in the left-hand interface and modality must in the right-hand interface.\n"
	(string_of_trace t)
	(Heap.to_string e)
	(string_of_modality m));
  }

(* val factor_handler_quiet : factor_handler *)

and factor_handler_quiet =
  {
    factor_inconsistent = (fun () -> ());
    factor_orientation = (fun _ _  -> ());
    factor_modality = (fun _ _ _ -> ());
  }

(*
 *
 * [is_factor h s1 s2] checks whether [s1] is a factor of [s2], meaning that:
 * 
 * (i) Every output of [s1] is either undefined in [s2] or is an output of [s2].
 * Function [h.factor_orientation] is called whenever orientation is not correct.
 *
 * (ii) Right-hand interface is inconsistent implies left-hand interface is also
 * inconsistent. Function [h.factor_inconsistent] is called whenever consistency
 * is not correct.
 *
 * (iii) After every must trace [t] in [s2], event [e] has modality must in [s2]
 * implies event [e] has modality must in [s1]. Function [h.factor_modality] is
 * called whenever modality is not correct.
 *
 *)

(* val is_factor : factor_handler -> t -> t -> bool *)

let is_factor h s1 s2 =
  let g1 = signature s1
  and g2 = signature s2
  and vis = Rel.make_empty () in
  let rec amt q1 q2 t =
    (Rel.get vis q1 q2) ||
      (Rel.set vis q1 q2 true;
       Signature.fold
	 (fun e _ p ->
	   p &&
	     let m2 = get_trans s2 q2 e in
	     let m1 =
	       match Signature.get g1 e with
		   Orientation.Undefined -> Modal.Must q1
		 | _ -> get_trans s1 q1 e in
	       match (m1,m2) with
		   ((Modal.Must r1),(Modal.Must r2)) ->
		     amt r1 r2 (e::t)
		 | (Modal.Inconsistent,_) -> true
		 | (_,(Modal.Must _)) ->
		     begin
		       h.factor_modality t e m1;
		       false
		     end
		 | (_,Modal.Inconsistent) ->
		     begin
		       h.factor_modality t e m1;
		       false
		     end
		 | (_,_) -> true)
	 g2
	 true)
  in
    (Signature.fold
	(fun e ->
	  function
	      Orientation.Output ->
		(fun p ->
		  p &&
		    (match Signature.get g2 e with
			Orientation.Output -> true
		      | Orientation.Undefined -> true
		      |  d ->
			   begin
			     h.factor_orientation e d;
			     false
			   end))
	    | _ -> (fun p -> p))
	g1
	true) &&
      (match ((get_initial s1),(get_initial s2)) with
	  (None,_) -> true
	| ((Some _),None) ->
	    begin
	      h.factor_inconsistent ();
	      false
	    end
	| ((Some q01),(Some q02)) ->
	    amt q01 q02 [])

(* [proj q i] returns the [i]th component of tuple [q] *)

let proj q i = (Heap.tuple_of q).(i)

(*
 *
 * [incompatibility_iter f s1 s2 s] iterates on the set pair states ([q1],[q2]) that are
 * reachable in specification [s] and that are incompatible wrt [s1] and [s2]. For every
 * such states pair, [f q1 q2 e] is evaluated, where [e] is the event that can be sent in
 * one component and not received in the other. States of [s] are assumed to be pairs,
 * that is tuples of length 2 exactly. Exception [Invalid_argument _] is raised otherwise.
 *
 *)

(*
 *
 * val incompatibility_iter :
 *   (Heap.obj (* q1 *) -> Heap.obj (* q2 *) -> Heap.obj (* q *) ->
 *      Heap.obj (* e *) -> unit) ->
 *   t (* s1 *) -> t (* s2 *) -> t (* s *) -> unit
 *
 *)

let incompatibility_iter f s1 s2 s =
  let g1 = signature s1
  and g2 = signature s2
  and comm12 = Enum.make_empty () (* comms from 1 to 2 *)
  and comm21 = Enum.make_empty () (* comms from 2 to 1 *) in
    begin
      (* compute communication alphabet from 1 to 2 *)
      Signature.iter
	(fun e ->
	   function
	       Orientation.Output ->
		 begin
		   match Signature.get g2 e with
		       Orientation.Input ->
			 Enum.set comm12 e true
		     | _ -> ()
		 end
	     | _ -> ())
	g1;
      (* compute communication alphabet from 2 to 1 *)
      Signature.iter
	(fun e ->
	   function
	       Orientation.Output ->
		 begin
		   match Signature.get g1 e with
		       Orientation.Input ->
			 Enum.set comm21 e true
		     | _ -> ()
		 end
	     | _ -> ())
	g2;
      (* iterate on reachable states of the product specification *)
      dfs
	(fun q ->
	   let q1 = proj q 0
	   and q2 = proj q 1 in
	     begin
	       (* iterates on comms from 1 to 2 *)
	       Enum.iter_in
		 (fun e ->
		    match ((get_trans s1 q1 e),(get_trans s2 q2 e)) with
			(Modal.Cannot,_) -> ()
		      | (Modal.Inconsistent,_) -> ()
		      | (_,(Modal.Must _)) -> ()
		      | (_,_) -> f q1 q2 q e)
		 comm12;
	       (* iterate on comms from 2 to 1 *)
	       Enum.iter_in
		 (fun e -> 
		    match ((get_trans s2 q2 e),(get_trans s1 q1 e)) with
			(Modal.Cannot,_) -> ()
		      | (Modal.Inconsistent,_) -> ()
		      | (_,(Modal.Must _)) -> ()
		      | (_,_) -> f q1 q2 q e)
		 comm21;
	     end)
	(fun _ _ _ -> ())
	s
    end

(*
 *
 * [incompatible_states s1 s2 s] computes the set of incompatible states in product
 * specification [s], resulting from the composition of [s1] with [s2].
 *
 *)

let incompatible_states s1 s2 s =
  let r = Enum.make_empty () in
    begin
      incompatibility_iter
	(fun _ _ q _ -> Enum.set r q true)
	s1 s2 s;
      r
    end

(*
 *
 * [illegal_states s1 s2 s] computes the set of illegal states in product specification
 * [s], resulting from the composition of [s1] with [s2].
 *
 *)

let illegal_states s1 s2 s =
  let t = incompatible_states s1 s2 s
  and r = Enum.make_empty ()
  and g = signature s in
  let rec backward q =
    if not (Enum.mem r q)
    then
      begin
	Enum.set r q true;
	ready_in_iter
	  (fun e ->
	     match Signature.get g e with
		 Orientation.Output -> 
		   state_in_iter backward s q e
	       | _ -> ())
	  s q
      end
  in
    begin
      Enum.iter_in
	backward
	t;
      r
    end

(*
 *
 * [prune f s r] prunes specification [s] by evaluating [f q e] for every transition from
 * state [q], labelled [e] and accessing a state in [r].
 *
 *)

let prune f s r =
  Enum.iter_in
    (fun q' ->
       ready_in_iter
	 (fun e ->
	    state_in_iter
	      (fun q -> f q e)
	      s q' e)
	 s q')
    r

(* Pruning operator : sets transition to a May to the universal state [q_top] *)

let strengthen_assumption s q_top q e =
  set_trans s q e (Modal.May q_top)

(* [parallel w s1 s2] computes the optimistic parallel composition of [s1] and [s2] *)

(* val parallel : Heap.heap -> t -> t -> t *)

let parallel w s1 s2 =
  let s = product w s1 s2
  and q_top = top w in
  let r = illegal_states s1 s2 s in
    match get_initial s with (* is [s] consistent? *)
	None -> s (* [s] is inconsistent: parallel composition is inconsistent *)
      | Some q0 -> (* [s] is consistent *)
	  if Enum.mem r q0 (* Is initial state illegal? *)
	  then (* Yes: parallel composition is inconsistent *)
	    begin
	      set_initial s None;
	      s
	    end
	  else (* No: perform pruning *)
	    begin
	      set_top s q_top; (* setup top state *)
	      prune (strengthen_assumption s q_top) s r;
	      s
	    end

(* [of_systen m] maps deterministic system [m] to a rigid specification of same signature *)
(* Raises [Invalid_argument _] if [m] is not deterministic *)

(* val of_system : System.t -> t *)

let of_system m =
  if System.is_deterministic m
  then
    let g = System.signature m in
    let a = make g in
      begin
	System.iter_initial
	  (fun q -> set_initial a (Some q))
	  m;
	System.dfs_iter
	  (fun _ -> ())
	  (fun q e q' -> set_trans a q e (Modal.Must q'))
	  m;
	a
      end
  else
    invalid_arg "Mi.of_system: system is not deterministic"

(* Minimal and maximal implementations. Raises [Invalid_argument _] if specification is inconsistent *)

(* val min_implementation : t -> System.t *)

type extr = Min | Max

let extr_implementation x a =
  if is_consistent a
  then
    let g = signature a in
    let m = System.make g
    and v = Enum.make_empty () in (* visited states *)
    let rec extrdfs q =
      if not (Enum.mem v q)
      then
	begin
	  Enum.set v q true;
	  mod_out_iter
	    (fun e ->
	       function
		   Modal.Inconsistent ->
		     invalid_arg "Mi.extr_implementation: specification is inconsistent"
		 | Modal.Must q' ->
		     begin
		       System.set_trans m q e q' true;
		       extrdfs q'
		     end
		 | Modal.Cannot -> ()
		 | Modal.May q' ->
		     begin
		       match x with
			   Min -> ()
			 | Max ->
			     begin
			       System.set_trans m q e q' true;
			       extrdfs q'
			     end
		     end)
	    a
	    q
	end
    in
      match get_initial a with
	  None -> failwith "Mi.extr_implementation: internal error"
	| Some q0 ->
	    begin
	      System.set_initial m q0 true;
	      extrdfs q0;
	      m
	    end
  else
    invalid_arg "Mi.extr_implementation: specification is inconsistent"

(* val min_implementation : t -> System.t *)

let min_implementation = extr_implementation Min

(* val max_implementation : t -> System.t *)

let max_implementation = extr_implementation Max

(* [mutate s l] mutates interface [s] according to [l] *)

(* val mutate : t -> (Heap.obj,Orientation.t) list -> t *)

let mutate s l =
  let s' = clone s in
    begin
      Signature.mutate_inplace_list s'.sgn l;
      s'
    end

(* Several utility functions used in [minimize] *)

(* [is_loc_equ s q1 q2] tests whether [q1] and [q2] are locally equivalent in [s] *) 

let is_loc_equ s q1 q2 =
  Signature.fold
    (fun e _ x ->
       x && 
	 (match ((get_trans s q1 e),(get_trans s q2 e)) with
	      (Modal.Cannot,Modal.Cannot) -> true
	    | (Modal.Inconsistent,Modal.Inconsistent) -> true
	    | ((Modal.May _),(Modal.May _)) -> true
	    | ((Modal.Must _),(Modal.Must _)) -> true
	    | _ -> false))
    (signature s)
    true

(*
 *
 * [is_loc_equ_distinguished s q q1 q2] tests whether [q1] and [q2] are locally equivalent in [s],
 * under the assumption that may transitions reaching state [q] are similar to any transition.
 *
 *) 

let is_loc_equ_distinguished s q q1 q2 =
  Signature.fold
    (fun e _ x ->
       x && 
	 (match ((get_trans s q1 e),(get_trans s q2 e)) with
	    | ((Modal.May q1'),_) when q1' == q -> true
	    | (_,(Modal.May q2')) when q2' == q -> true
	    | (Modal.Cannot,Modal.Cannot) -> true
	    | (Modal.Inconsistent,Modal.Inconsistent) -> true
	    | ((Modal.May _),(Modal.May _)) -> true
	    | ((Modal.Must _),(Modal.Must _)) -> true
	    | _ -> false))
    (signature s)
    true

(* [is_congruent r u s i1 i2] tests whether [i1] and [i2] are congruent wrt [r] in [s]. [u] is the enumeration of the states of [s] *)

let is_congruent r u s i1 i2 =
  let q1 = u.Enumerate.of_int.(i1)
  and q2 = u.Enumerate.of_int.(i2) in
    (Equiv.mem r i1 i2) &&
      (Signature.fold
	 (fun e _ x ->
	    x &&
	      (match ((get_trans s q1 e),(get_trans s q2 e)) with
		   (Modal.Cannot,Modal.Cannot) -> true
		 | (Modal.Inconsistent,Modal.Inconsistent) -> true
		 | ((Modal.May q1'),(Modal.May q2')) ->
		     let i1' = Mapping.read u.Enumerate.to_int q1'
		     and i2' = Mapping.read u.Enumerate.to_int q2' in
		       Equiv.mem r i1' i2'
		 | ((Modal.Must q1'),(Modal.Must q2')) ->
		     let i1' = Mapping.read u.Enumerate.to_int q1'
		     and i2' = Mapping.read u.Enumerate.to_int q2' in
		       Equiv.mem r i1' i2'
		 | _ -> false))
	 (signature s)
	 true)
	   
(*
 *
 * [is_congruent_distinguished r u s q i1 i2] tests whether [i1] and [i2] are congruent wrt [r] in [s],
 * considering that transitions reaching state [q] are similar to any transition. [u] is the enumeration
 * of the states of [s], except [q].
 *
 * Remark: state [q] is not in enumeration [u]. 
 *
 *)

let is_congruent_distinguished r u s q i1 i2 =
  let q1 = u.Enumerate.of_int.(i1)
  and q2 = u.Enumerate.of_int.(i2) in
    (Equiv.mem r i1 i2) &&
      (Signature.fold
	 (fun e _ x ->
	    x &&
	      (match ((get_trans s q1 e),(get_trans s q2 e)) with
		   ((Modal.May q1'),_) when q1' == q -> true
		 | (_,(Modal.May q2')) when q2' == q -> true
		 | (Modal.Cannot,Modal.Cannot) -> true
		 | (Modal.Inconsistent,Modal.Inconsistent) -> true
		 | ((Modal.May q1'),(Modal.May q2')) ->
			 let i1' = Mapping.read u.Enumerate.to_int q1'
			 and i2' = Mapping.read u.Enumerate.to_int q2' in
			   Equiv.mem r i1' i2'
		 | ((Modal.Must q1'),(Modal.Must q2')) ->
		     let i1' = Mapping.read u.Enumerate.to_int q1'
		     and i2' = Mapping.read u.Enumerate.to_int q2' in
		       Equiv.mem r i1' i2'
		 | _ -> false))
	 (signature s)
	 true)

(* [enumerate s] computes the enumeration of the reachable states of [s] *)

let enumerate s = Enumerate.enumerate (reachable_states s)

(* [enumerate_non_distinguished s q] computes the enumeration of the reachable states of [s] minus [q] *)

let enumerate_non_distinguished s q =
  let r = reachable_states s in
    begin
      Enum.set r q false;
      Enumerate.enumerate r
    end

(* computes the pre-state sparse matrix *)

let sparse_pre_matrix s u =
  let n = u.Enumerate.card in
  let pre = Array.make n [] in
    begin
      for i=0 to n-1 do
	let q = u.Enumerate.of_int.(i) in
	  ready_in_iter
	    (fun e ->
	       state_in_iter
		 (fun q' -> 
		    let j = Mapping.read u.Enumerate.to_int q' in
		      if (j >= 0) && (not (List.mem j pre.(i)))
		      then pre.(i) <- j :: pre.(i))
		 s q e)
	    s q
      done;
      pre
    end

(* For debugging only *)

let dump_pre_matrix p =
  let n = Array.length p in
    begin
      Printf.printf "[|\n";
      for i=0 to n-1 do
	Printf.printf "  %d : [" i;
	List.iter (fun j -> Printf.printf " %d" j) p.(i);
	Printf.printf " ]\n"
      done;
      Printf.printf "|]\n";
    end

let dump_oracle a =
  let n = Array.length a in
    begin
      Printf.printf "{ ";
      for i=0 to n-1 do
	if a.(i) then Printf.printf "%d " i
      done;
      Printf.printf "}\n"
    end

(* [local_equiv s u] computes the local state equivalence relation of [s]. [u] is the enumeration of the states of [s] *)

let local_equiv s u =
  let n = u.Enumerate.card in
  let r = Equiv.make n in
    begin
      for i=0 to n-1 do
	for j=i+1 to n-1 do
	  if (not (Equiv.mem r i j)) &&
	    (is_loc_equ s u.Enumerate.of_int.(i) u.Enumerate.of_int.(j))
	  then Equiv.add r i j
	done
      done;
      r
    end

(*
 *
 * [local_equiv_distinguished s u q] computes the local state equivalence relation of [s],
 * under the assumption that may transitions reaching state [q] are similar to any transition.
 * [u] is the enumeration of the states of [s], except state [q].
 *
 *)

let local_equiv_distinguished s u q =
  let n = u.Enumerate.card in
  let r = Equiv.make n in
    begin
      for i=0 to n-1 do
	for j=i+1 to n-1 do
	  if (not (Equiv.mem r i j)) &&
	    (is_loc_equ_distinguished s q u.Enumerate.of_int.(i) u.Enumerate.of_int.(j))
	  then Equiv.add r i j
	done
      done;
      r
    end

(* Mark predecessor states in oracle table *)

let mark_pre_states p t i =
  List.iter
    (fun j -> t.(j) <- true)
    p.(i)

(* [largest_congruence s u] computes the largest congruence of [s]. [u] is the enumeration of the states of [s] *)

let largest_congruence s u =
  let r = ref (local_equiv s u) in
  let n = Equiv.size (!r) in
  let split = ref true
  and oracle = ref (Array.make n true)
  and pre = sparse_pre_matrix s u in
    begin
      while !split do
	split := false;
	let new_oracle = Array.make n false in
	  begin
	    (* Refine relation *)
	    r :=
	      Equiv.refine_accel
		(fun i -> (!oracle).(i))
		(fun i1 i2 ->
		   (is_congruent (!r) u s i1 i2) ||
		     (split := true;
		      mark_pre_states pre new_oracle i1;
		      mark_pre_states pre new_oracle i2;
		      false))
		(!r);
	    (* Mark classes containing states marked in oracle table *)
	    Equiv.iter_classes
	      (fun i -> new_oracle.(i) <- Equiv.fold_elements (fun j x -> x || new_oracle.(j)) (!r) i false)
	      (!r);
	    (* Use new oracle *)
	    oracle := new_oracle
	  end
      done;
      !r
    end

(*
 *
 * [largest_congruence_distinguished s u q] computes the largest congruence of [s]
 * in which may transitions reaching state [q] are deemed similar to any transition of [s].
 * [u] is the enumeration of the states of [s], except state [q]. State [q] must be a sink.
 *
 *)

let largest_congruence_distinguished s u q =
  let r = ref (local_equiv_distinguished s u q) in
  let n = Equiv.size (!r) in
  let split = ref true
  and oracle = ref (Array.make n true)
  and pre = sparse_pre_matrix s u in
    begin
      dump_pre_matrix pre;
      while !split do
	split := false;
	Equiv.print !r;
	dump_oracle !oracle;
	let new_oracle = Array.make n false in
	  begin
	    (* Refine relation *)
	    r :=
	      Equiv.cautious_refine_accel
		(fun i -> (!oracle).(i))
		(fun i1 i2 ->
		   (is_congruent_distinguished (!r) u s q i1 i2) ||
		     (split := true;
		      mark_pre_states pre new_oracle i1;
		      mark_pre_states pre new_oracle i2;
		      Printf.printf "(%d#%d)" i1 i2;
		      false))
		(!r);
	    (* Mark classes containing states marked in oracle table *)
	    Equiv.iter_classes
	      (fun i -> new_oracle.(i) <- Equiv.fold_elements (fun j x -> x || new_oracle.(j)) (!r) i false)
	      (!r);
	    (* Use new oracle *)
	    oracle := new_oracle
	  end
      done;
      !r
    end

(* [repr r u q] finds the representant of the class of state [q] in [r] *)

let repr r u q =
  let i = Mapping.read u.Enumerate.to_int q in
  let i' = Equiv.repr r i in
  u.Enumerate.of_int.(i')

(* [minimize s] minimizes modal interface [s] *)

(* val minimize : t -> t *)

let minimize s =
  let u = enumerate s in
  let r = largest_congruence s u in
  let s' = make (signature s) in
    begin
      (match get_initial s with
	   None -> set_initial s' None
	 | Some q0 -> set_initial s' (Some (repr r u q0)));
      dfs
	(fun _ -> ())
	(fun q e ->
	   function
	       Modal.Cannot ->
		 set_trans s' (repr r u q) e Modal.Cannot
	     | Modal.Inconsistent ->
		 set_trans s' (repr r u q) e Modal.Inconsistent
	     | Modal.May q' ->
		 set_trans s' (repr r u q) e (Modal.May (repr r u q'))
	     | Modal.Must q' ->
		 set_trans s' (repr r u q) e (Modal.Must (repr r u q')))
	s;
      s'
    end

(*
 *
 * [minimize_distinguished s q] minimizes modal interface [s],
 * considering that may transitions reaching state [q] are
 * similar to any transition in [s]. State [q] must
 * be a sink, reachable only by a may transition.
 *
 * This function is used only by the [facet] operator.
 *
 *) 

let minimize_distinguished s qd =
  let u = enumerate_non_distinguished s qd in
  let r = largest_congruence_distinguished s u qd in
  let s' = make (signature s) 
  and qd_reached = ref false in
    begin
      (match get_initial s with
	  None -> set_initial s' None
	| Some q0 ->
	    begin
	      set_initial s' (Some (repr r u q0));
	      qd_reached := q0 == qd
	    end);
      dfs
	(fun _ -> ())
	(fun q e m ->
	  if q != qd (* ignore transitions from state [qd] *)
	  then
	    match m with
		Modal.Cannot ->
		  set_trans s' (repr r u q) e Modal.Cannot
	      | Modal.Inconsistent ->
		  set_trans s' (repr r u q) e Modal.Inconsistent
	      | Modal.May q' ->
		  if q' == qd
		  then
		    (* Is there an equivalent state with a [e] transition reaching another state than [qd]? *)
		    let i = Equiv.repr r (Mapping.read u.Enumerate.to_int q) in (* index of state [q] un [u] *)
		    match
		      Equiv.fold_elements
			(fun j ->
			  function
			      (Some _) as w -> w
			    | None ->
				let qq = u.Enumerate.of_int.(j) in
				  match get_trans s qq e with
				      Modal.Inconsistent as m -> Some m
				    | Modal.Cannot as m -> Some m
				    | (Modal.Must qq') as m when qq' != qd -> Some m
				    | (Modal.May qq') as m when qq' != qd -> Some m
				    | _ -> None)
			r i None
		    with
			None -> (* No equivalent state has a [e] transition not reaching [qd] *)
			  begin
			    set_trans s' (repr r u q) e (Modal.May qd);
			    qd_reached := true
			  end
		      | Some m -> (* Found an equivalent state with a [e] transition of modality [m], not reaching [qd] *)
			  begin
			    match m with
				Modal.Inconsistent
			      | Modal.Cannot ->
				  set_trans s' (repr r u q) e m
			      | Modal.Must q'' ->
				  set_trans s' (repr r u q) e (Modal.Must (repr r u q''))
			      | Modal.May q'' ->
				  set_trans s' (repr r u q) e (Modal.May (repr r u q''))
			  end
		  else
		    set_trans s' (repr r u q) e (Modal.May (repr r u q'))
	      | Modal.Must q' ->
		  if q' == qd
		  then
		    failwith "Mi.minimize_distinguished: Distinguished state can be reached by a must transition."
		  else
		    set_trans s' (repr r u q) e (Modal.Must (repr r u q')))
	s;
	(if !qd_reached (* if [qd] has been reached then transitions exiting [qd] must be copied *)
	 then
	  Signature.iter
	    (fun e _ ->
	      match get_trans s qd e with
		  Modal.Cannot as m -> set_trans s' qd e m
		| Modal.Inconsistent as m -> set_trans s' qd e m
		| Modal.May q as m when q == qd -> set_trans s' qd e m
		| Modal.Must q as m when q == qd -> set_trans s' qd e m
		| _ -> failwith "Mi.minimize_distinguished: Distinguished state can be reached and is not a sink.")
	    (signature s));
      s'
    end

(* functions used in [simplify] *)

let optional f =
  function
      None -> None
    | Some x -> Some (f x)

let make_int_state w i = Heap.make_int w i

let emap w u q = make_int_state w (Mapping.read u.Enumerate.to_int q)

let rmap u i = u.Enumerate.of_int.(i)

let dfs_enumerate a =
  let s = signature a
  and card = ref 0
  and to_int = Mapping.make (-1) in
  let rec cpt q =
    if (Mapping.read to_int q) < 0 (* Has state been already visited? *)
    then (* No *)
      begin
	Mapping.write to_int q (!card); (* mark visited *)
	card := (!card)+1; (* increment counter *)
	Signature.iter (* iterate on transitions *)
	  (fun e _ ->
	    match get_trans a q e with
		Modal.Cannot -> ()
	      | Modal.Inconsistent -> ()
	      | Modal.May q' -> cpt q'
	      | Modal.Must q' -> cpt q')
	  s
      end
  in
    match get_initial a with
	None ->
	  {
	    Enumerate.card = !card;
	    Enumerate.to_int = to_int;
	    Enumerate.of_int = [||]
	  }
      | Some q0 ->
	  begin
	    cpt q0;
	    let of_int = Array.make (!card) q0 in
	      begin
		Mapping.iter
		  (fun q i -> of_int.(i) <- q)
		  to_int;
		{
		  Enumerate.card = !card;
		  Enumerate.to_int = to_int;
		  Enumerate.of_int = of_int
		}
	      end
	  end

(* [simplify w s] simplifies the state structure of interface [s] by mapping state labels to integers *)

(* val simplify : Heap.heap -> t -> t *)

let simplify w s =
  let g = signature s
  and u = dfs_enumerate s in
  let s' = make g
  and n = u.Enumerate.card in
    begin
      set_initial s' (optional (emap w u) (get_initial s));
      for i=0 to n-1 do
	let q = rmap u i
	and r = make_int_state w i in
	  Signature.iter
	    (fun e _ ->
	       match get_trans s q e with
		   Modal.Cannot -> set_trans s' r e Modal.Cannot
		 | Modal.Inconsistent -> set_trans s' r e Modal.Inconsistent
		 | Modal.May q' ->
		     let r' = emap w u q' in
		       set_trans s' r e (Modal.May r')
		 | Modal.Must q' ->
		     let r' = emap w u q' in
		       set_trans s' r e (Modal.Must r'))
	    g
      done;
      s'
    end

(* [print_statistics s] prints statistics about interface [s] *)

(* val print_statistics : t -> unit *)

let print_statistics s =
  let st = ref 0
  and tr = ref 0
  and ip = ref 0
  and op = ref 0
  and ic = ref 0 in
    begin
      Signature.iter
	(fun _ ->
	   function
	       Orientation.Input -> ip := (!ip)+1
	     | Orientation.Output -> op := (!op)+1
	     | Orientation.Inconsistent -> ic := (!ic)+1
	     | Orientation.Undefined -> ())
	(signature s);
      dfs
	(fun _ -> st := (!st)+1)
	(fun _ _ ->
	   function
	       Modal.Cannot -> ()
	     | _ -> tr := (!tr)+1)
	s;
      Printf.printf "Interface has : %d input, %d output and %d inconsistent events; %d states and %d transitions.\n"
	(!ip) (!op) (!ic) (!st) (!tr)
    end

(* Forward closure of a set of states by silent transitions *)

let forward_closure { tra = t } (* interface *) s (* silent events *) x0 (* state set *) =
  let x = ref x0
  and y = ref (Enum.make_empty ())
  and m = ref true in
    begin
      while !m do
	m := false;
	Enum.iter_in
	  (fun q -> 
	     Enum.set (!y) q true;
	     Enum.iter_in
	       (fun e ->
	       match Ltf.get t q e with
		   None -> ()
		 | Some r ->
		     if not (Enum.mem (!x) r)
		     then
		       begin
			 m := true;
			 Enum.set (!y) r true
		       end)
	       s) (!x);
	x := (!y);
	y := Enum.make_empty ()
      done;
      !x
    end

(*
 *
 * [modality_at_set w m0 a x e] computes the modality resulting from the composition with operator [w]
 * of modalities of event [e] in interface [a] at states in [x]. Modality of empty state set is [m0].
 *
 *)

let modality_at_set w m0 { mde = z } x e =
  let h =
    {
      Modal.compose =
	(fun y r -> 
	   begin
	     Enum.set y r true;
	     y
	   end);
      Modal.left =
	Some (fun y -> y);
      Modal.right =
	Some (fun r ->
		let y = Enum.make_empty () in
		  begin
		    Enum.set y r true;
		    y
		  end);
      Modal.distinguished =
	Some (Enum.make_empty ())
    }
  and m = ref m0 in
    begin
      Enum.iter_in
	(fun q ->
	   let n = Mmap.read z q e in
	     m := w h (!m) n)
	x;
      !m
    end

(* [lub_modality_at_set a x e] computes the abstraction (lub) of modalities of event [e] in interface [a] at states in set [x] *)

let lub_modality_at_set a x e = modality_at_set Modal.lub Modal.Inconsistent a x e
and glb_modality_at_set a x e = modality_at_set Modal.conjunction (Modal.May (Enum.make_empty ())) a x e
and project_modality_at_set a x e = modality_at_set Modal.project Modal.Cannot a x e

(* modality forward closure *)

let modality_forward_closure a s =
  function
      Modal.May y -> Modal.May (forward_closure a s y)
    | Modal.Must y -> Modal.Must (forward_closure a s y)
    | Modal.Cannot -> Modal.Cannot
    | Modal.Inconsistent -> Modal.Inconsistent

(* maps value sets to values *)

let value_of_set h (* heap *) s (* set *) =
  let l = Enum.fold_in (fun x k -> x::k) s [] in (* computes the list of all set elements *)
    Heap.make_set h l

(* [set_of_init a] returns the empty set if interface [a] is empty and otherwise, a singleton containing the initial state *)

let set_of_init =
  function
      { ini = None } -> Enum.make_empty ()
    | { ini = Some q0 } ->
	let s = Enum.make_empty () in
	  begin
	    Enum.set s q0 true;
	    s
	  end

(*
 *
 * [abstract h s a] computes a projection of interface [a] on sub-alphabet [s]. The resulting interface is the
 * least abstraction of [a] in the class of interfaces over signature [Signature.restrict (signature a) s].
 *
 *)

(* val abstract : Heap.heap -> Enum.t -> interface *)

let abstract h s a =
  let f = signature a in
  let g = Signature.restrict f s (* signature of the resulting interface *)
  and k = Signature.hidden f s in (* set of hidden events *)
  let b = make g (* resulting interface *)
  and v = Enum.make_empty () (* set of visited states in the resulting interface *)
  and x0 = forward_closure a k (set_of_init a) in (* initial state set *)
  let r0 = value_of_set h x0 in
  let rec explore r x =
    if not (Enum.mem v r) (* has it been explored yet? *)
    then (* no *)
      begin
	Enum.set v r true; (* mark state [r] visited *)
	Signature.iter (* iterate on observable events *)
	  (fun e _ ->
	     let m = modality_forward_closure a k (lub_modality_at_set a x e) in
	       match m with
		   Modal.Cannot -> set_trans b r e Modal.Cannot
		 | Modal.Inconsistent -> set_trans b r e Modal.Inconsistent
		 | Modal.May y ->
		     let q = value_of_set h y in
		       begin
			 set_trans b r e (Modal.May q);
			 explore q y
		       end
		 | Modal.Must y ->
		     let q = value_of_set h y in
		       begin
			 set_trans b r e (Modal.Must q);
			 explore q y
		       end)
	  g
      end
  in
    begin
      set_initial b (Some r0);
      explore r0 x0;
      reduce b; (* in-place reduction of the resulting interface *)
      b
    end 

(*
 *
 * [project h s a] computes a projection of interface [a] signature [s]. The resulting interface is the
 * largest projection of [a] that is receptive to all its inputs and that controls all its outputs.
 *
 *)

(* val project : Heap.heap -> Signature.t -> t -> t *)

let project h g a =
  let f = signature a in
  let k = Signature.hide f g in (* set of hidden events *)
  let b = make g (* resulting interface *)
  and v = Enum.make_empty () (* set of visited states in the resulting interface *)
  and x0 = forward_closure a k (set_of_init a) in (* initial state set *)
  let r0 = value_of_set h x0 in
  let rec explore r x =
    if not (Enum.mem v r) (* has it been explored yet? *)
    then (* no *)
      begin
	Enum.set v r true; (* mark state [r] visited *)
	Signature.iter (* iterate on observable events *)
	  (fun e ->
	    function
		Orientation.Undefined ->
		  failwith "Mi.project: Impossible case. Please report bug to developpers"
	      | Orientation.Input -> (* Event is an input *)
		  let m = modality_forward_closure a k (project_modality_at_set a x e) in
		    begin
		      match m with
			  Modal.Cannot -> set_trans b r e Modal.Cannot
			| Modal.Inconsistent -> set_trans b r e Modal.Inconsistent
			| Modal.May y ->
			    let q = value_of_set h y in
			      begin
				set_trans b r e (Modal.May q);
				explore q y
			      end
			| Modal.Must y ->
			    let q = value_of_set h y in
			      begin
				set_trans b r e (Modal.Must q);
				explore q y
			      end
		    end
	      | _ -> (* Event is an output or inconsistent *)
		  let m = modality_forward_closure a k (glb_modality_at_set a x e) in
		    begin
		      match m with
			  Modal.Cannot -> set_trans b r e Modal.Cannot
			| Modal.Inconsistent -> set_trans b r e Modal.Inconsistent
			| Modal.May y ->
			    let q = value_of_set h y in
			      begin
				set_trans b r e (Modal.May q);
				explore q y
			      end
			| Modal.Must y ->
			    let q = value_of_set h y in
			      begin
				set_trans b r e (Modal.Must q);
				explore q y
			      end
		    end)
	  g
      end
  in
    begin
      set_initial b (Some r0);
      explore r0 x0;
      reduce b; (* in-place reduction of the resulting interface *)
      b
    end

(*
 *
 * [facet h s a] computes an abstraction [b] of a reduced interface [a], such that:
 *   - [b] is on the same signature as [a] 
 *   - the state structure of [b] is isomorphic to the state structure of [a], except that a universal (top) state has been added to [b]
 *   - every transition of [a] labeled by an event in [s] is preserved in [b], with the same modality
 *   - every "may" or "must" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition of [b]
 *   - every "cannot" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition to the top state
 *
 *)

(* val facet : Heap.heap -> Enum.t -> t -> t *)

let facet h s a =
  let f = signature a in
  let b = make f (* resulting interface *) in
    (* is [a] inconsistent? *)
    match (get_initial a) with
	None -> (* [a] is inconsistent *)
	  begin
	    set_initial b None; (* [b] is also inconsistent *)
	    b
	  end
      | Some x0 -> (* [a] is consistent *)
	  let encode x = Heap.make_scalar h x in
	  let yt = Heap.make_empty_tuple h
	  and y0 = encode x0
	  and v = Enum.make_empty () in (* set of visited states in [a] *)
	  let rec explore x y =
	    if not (Enum.mem v x) (* has state [x] been explored yet? *)
	    then (* no *)
	      begin
		Enum.set v x true; (* mark state [x] visited *)
		Signature.iter (* iterate on events *)
		  (fun e _ ->
		    if Enum.mem s e (* is event [e] controllable? *)
		    then (* yes, [e] is controllable *)
		      match get_trans a x e with
			  Modal.Cannot ->
			    set_trans b y e Modal.Cannot
			| Modal.May x' ->
			    let y' = encode x' in
			      begin
				set_trans b y e (Modal.May y');
				explore x' y'
			      end
			| Modal.Must x' ->
			    let y' = encode x' in
			      begin
				set_trans b y e (Modal.Must y');
				explore x' y'
			      end
			| Modal.Inconsistent ->
			    failwith "Mi.facet: state is inconsistent. Interface is not reduced."
		    else (* no, [e] is not controllable *)
		      match get_trans a x e with
			  Modal.Cannot ->
			    set_trans b y e (Modal.May yt) 
			| Modal.May x'
			| Modal.Must x' ->
			    let y' = encode x' in
			      begin
				set_trans b y e (Modal.May y');
				explore x' y'
			      end
			| Modal.Inconsistent ->
			    failwith "Mi.facet: state is inconsistent. Interface is not reduced.")
		  f
	      end
	  in
	    begin
	      (* [y0] is the initial state *)
	      set_initial b (Some y0);
	      (* [yt] is a universal state *)
	      Signature.iter
		(fun e _ -> set_trans b yt e (Modal.May yt))
		f;
	      (* compute image of modal interface *)
	      explore x0 y0;
	      (* return [b] *)
	      (minimize_distinguished b yt)
	      (* minimize b *)
	      (* b *)
	    end
