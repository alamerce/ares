(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * System: implementations
 *
 * $Id: system.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t

val make : Signature.t -> t

val clone : t -> t

(* retrieves the signature *)

val signature : t -> Signature.t

(* operators on initial states *)

val set_initial : t -> Heap.obj -> bool -> unit

val get_initial : t -> Heap.obj -> bool

val iter_initial : (Heap.obj -> unit) -> t -> unit

(* operators on transitions *)

val set_trans : t -> Heap.obj -> Heap.obj -> Heap.obj -> bool -> unit

val get_trans : t -> Heap.obj -> Heap.obj -> Heap.obj -> bool

(* iterate on enabled events *)

val ready_out_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit

val ready_out_fold : (Heap.obj (* event *) -> 'a -> 'a) -> t -> Heap.obj (* state *) -> 'a -> 'a

val ready_in_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit

(* iterate on (pre-)image states *)

val state_out_iter : (Heap.obj (* to-state *) -> unit) -> t -> Heap.obj (* from-state *) -> Heap.obj (* event *) -> unit

val state_out_fold : (Heap.obj (* to-state *) -> 'a -> 'a) -> t -> Heap.obj (* from-state *) -> Heap.obj (* event *) -> 'a -> 'a

val state_in_iter : (Heap.obj (* from-state *) -> unit) -> t -> Heap.obj (* to-state *) -> Heap.obj (* event *) -> unit

(* Tests whether an event is enabled *)

val is_enabled : t -> Heap.obj (* from-state *) -> Heap.obj (* event *) -> bool

(* dfs *)

val dfs_iter :
  (Heap.obj (* state *) -> unit) (* state procedure *) ->
  (Heap.obj (* from-state *) -> Heap.obj (* event *) -> Heap.obj (* to-state *) -> unit) (* transition procedure *)->
  t (* system *) -> unit

val dfs_fold :
  (Heap.obj (* state *) -> 'a -> 'a) (* state operator *) ->
  (Heap.obj (* from-state *) -> Heap.obj (* event *) -> Heap.obj (* to-state *) -> 'a -> 'a) (* transition operator *)->
  t (* system *) -> 'a -> 'a

(* [reachable_states a] computes the set of reachable states of system [a] *)

val reachable_states : t -> Enum.t

(* [print a] prints system [a] *)

val print : t -> unit

(*
 *
 * [parallel w f a b] computes the parallel composition of systems [a] and [b]
 * Product states are stored in heap [w]. Function [f] is evaluated whenever
 * incompatible states are encountered.
 *
 *)

val parallel :
  Heap.heap ->
  (Heap.obj (* left state *) -> Heap.obj (* right state *) -> Heap.obj (* event *) -> unit) ->
  t -> t -> t

val incompatibility_print : Heap.obj (* left state *) -> Heap.obj (* event *) -> Heap.obj (* right state *) -> unit

val are_compatible : Heap.heap -> t -> t -> bool

val is_deterministic : t -> bool




