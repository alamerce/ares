(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Enum: enumerated sets
 *
 * $Id: enum.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

(* Sets may be represented either by enumerating its elements or the elements of its complement *)

type t

val make_empty : unit -> t

val make_universal : unit -> t

val clone : t -> t

val to_string : t -> string

val print : t -> unit

val mem : t -> Heap.obj -> bool

val set : t -> Heap.obj -> bool -> unit

val union : t -> t -> t

val inter : t -> t -> t

val subtract : t -> t -> t

val complement : t -> t

val diff : t -> t -> t

(* [prod f a b] computes the cartesian product of sets [a] and [b], using function [f] to form pairs *)

val prod : (Heap.obj -> Heap.obj -> Heap.obj) -> t -> t -> t

val is_empty : t -> bool

val is_universal : t -> bool

val is_included : t -> t -> bool

val iter_in : (Heap.obj -> unit) -> t -> unit

val fold_in : (Heap.obj -> 'a -> 'a) -> t -> 'a -> 'a

val iter_out : (Heap.obj -> unit) -> t -> unit

val fold_out : (Heap.obj -> 'a -> 'a) -> t -> 'a -> 'a

val card_in : t -> int

val card_out : t -> int

val witness_in : t -> Heap.obj (* Returns an element in the set. Raises Not_found if set is empty *)

val witness_out : t -> Heap.obj (* Returns an element out of the set. Raises Not_found if the set is universal *)
