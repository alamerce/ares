(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Heap: object heap with structural equality
 *
 * $Id: heap.ml 470 2011-11-30 10:02:39Z bcaillau $
 *
 *)

exception Typecheck of string

(* type obj *)

type data =
    Ident of string
  | Char of char
  | String of string
  | Bool of bool
  | Int of int
  | Tuple of obj array
  | Set of obj list
  | Observable of obj
  | Silent
  | Input of obj
  | Output of obj

and obj = 
    {
      obj_hash : int;
      obj_data : data
    }

(* hash functions *)

(* hash modulus *)

let hash_modulus = 39916801 (* a prime number *)

let hash_factor = 7

let hash_step =  769

(* hash basic types *)

let hash_string s =
  let h = ref 0
  and n = String.length s in
    begin
      for i=0 to n-1 do
	h := (hash_factor*(!h)+(Char.code s.[i])) mod hash_modulus
      done;
      !h
    end

let hash_ident s = hash_modulus - (hash_string s) - 1

let hash_char c = hash_step + (Char.code c)

let hash_int n = n mod hash_modulus

let hash_array a =
  let h = ref (2*hash_step)
  and n = Array.length a in
    begin
      for i=0 to n-1 do
	h := (hash_factor*(!h)+a.(i).obj_hash) mod hash_modulus
      done;
      !h
    end

let rec hash_list =
  function
    [] -> 3*hash_step
  | x::l -> (x.obj_hash + hash_factor*(hash_list l)) mod hash_modulus

let rec hash_bool =
  function
      false -> 0
    | true -> 1

(* hash data values *)

let hash_data =
  function
      Ident s -> hash_ident s
    | Char c -> hash_char c
    | String s -> hash_string s
    | Bool b -> hash_bool b
    | Int n -> hash_int n
    | Tuple a -> hash_array a
    | Set l -> hash_list l
    | Observable x -> x.obj_hash
    | Silent -> hash_modulus-1
    | Input x -> x.obj_hash
    | Output x -> hash_modulus-x.obj_hash-1

(* data and obj equality *)

let data_equal =
  function
      Ident s1 ->
	begin
	  function
	      Ident s2 -> s1 = s2
	    | _ -> false
	end
    | Char c1 ->
	begin
	  function
	      Char c2 -> c1 = c2
	    | _ -> false
	end
    | String s1 ->
	begin
	  function
	      String s2 -> s1 = s2
	    | _ -> false
	end
    | Bool b1 ->
	begin
	  function
	      Bool b2 -> b1 = b2
	    | _ -> false
	end
    | Int n1 ->
	begin
	  function
	      Int n2 -> n1 = n2
	    | _ -> false
	end
    | Tuple a1 ->
	begin
	  function
	      Tuple a2 ->
		let r = ref true
		and n1 = Array.length a1
		and n2 = Array.length a2 in
		  (n1 = n2) &&
		    begin
		      for i=0 to n1-1 do
			r := (!r) && (a1.(i) == a2.(i))
		      done;
		      !r
		    end
	    | _ -> false
	end
    | Set l1 ->
	begin
	  function
	      Set l2 ->
		((List.length l1) = (List.length l2)) &&
		  (List.fold_right2
		     (fun x1 x2 r -> r && (x1 == x2))
		     l1
		     l2
		     true)
	    | _ -> false
	end
    | Observable x1 ->
	begin
	  function
	      Observable x2 ->
		x1 == x2
	    | _ -> false
	end
    | Silent ->
	begin
	  function
	      Silent -> true
	    | _ -> false
	end
    | Input x1 ->
	begin
	  function
	      Input x2 ->
		x1 == x2
	    | _ -> false
	end
    | Output x1 ->
	begin
	  function
	      Output x2 ->
		x1 == x2
	    | _ -> false
	end

let obj_equal { obj_hash=h1; obj_data=d1 } { obj_hash=h2; obj_data=d2 } =
  (h1=h2) && (data_equal d1 d2)
  
let obj_hash { obj_hash=h } = h

(* [compare x y] returns [0] if [x] is equal to [y], [-1] if [x] is less than [y] and [1] if [x] is greater than [y] *)

(* val compare : obj -> obj -> int *)

let kind =
  function
      Ident _ -> 0
    | Char _ -> 1
    | String _ -> 2
    | Bool _ -> 3
    | Int _ -> 4
    | Tuple _ -> 5
    | Set _ -> 6
    | Observable _ -> 7
    | Silent -> 8
    | Input _ -> 9
    | Output _ -> 10

(* Comparison according to hash-type-size-lexicographic order *)

let rec compare
    ({ obj_hash=h1; obj_data=d1 } as y1)
    ({ obj_hash=h2; obj_data=d2 } as y2) =
  if y1==y2
  then
    0
  else
    if h1 < h2
    then -1
    else
      if h1 > h2
      then 1
      else
	let k1 = kind d1
	and k2 = kind d2 in
	  if k1 < k2
	  then -1
	  else
	    if k1 > k2
	    then 1
	    else
	      match (d1,d2) with
		  (Ident s1, Ident s2) -> Pervasives.compare s1 s2
		| (Char c1, Char c2) -> Pervasives.compare c1 c2
		| (String s1, String s2) -> Pervasives.compare s1 s2
		| (Bool b1, Bool b2) -> Pervasives.compare b1 b2
		| (Int n1, Int n2) -> Pervasives.compare n1 n2
		| (Tuple a1, Tuple a2) ->
		    let n1 = Array.length a1
		    and n2 = Array.length a2 in
		      if n1 < n2
		      then -1
		      else
			if n1 > n2
			then 1
			else
			  let r = ref 0
			  and i = ref 0 in
			    begin
			      while ((!r) = 0) && ((!i) < n1)
			      do
				r := compare a1.(!i) a2.(!i);
				i := (!i)+1
			      done;
			      !r
			    end
		| (Set l1, Set l2) ->
		    let n1 = List.length l1
		    and n2 = List.length l2 in
		      if n1 < n2
		      then -1
		      else
			if n1 > n2
			then 1
			else
			  List.fold_right2
			    (fun x1 x2 r ->
			       if r=0
			       then
				 compare x1 x2
			       else
				 r)
			    l1
			    l2
			    0
		| (Observable x1, Observable x2) -> compare x1 x2
		| (Silent, Silent) -> 0
		| (Input x1, Input x2) -> compare x1 x2
		| (Output x1, Output x2) -> compare x1 x2
		| _ -> failwith "Heap.compare: impossible case. Please report bug to developper"

(* Mapping to/from Idt.t *)

(* val ident_of_obj : obj -> Idt.t *)

let rec ident_of_obj =
  function
      { obj_data = Ident s } -> Idt.Ident s
    | { obj_data = Char c } -> Idt.Char c
    | { obj_data = String s } -> Idt.String s
    | { obj_data = Bool b } -> Idt.Bool b
    | { obj_data = Int n } -> Idt.Int n
    | { obj_data = Tuple a } -> Idt.Tuple (Array.map ident_of_obj a)
    | { obj_data = Set l } ->
	List.fold_right
	  (fun x r -> Idt.union (Idt.set_of_scalar (ident_of_obj x)) r)
	  l
	  (Idt.Set [])
    | { obj_data = Observable x } -> Idt.Observable (ident_of_obj x)
    | { obj_data = Silent } -> Idt.Silent
    | { obj_data = Input x } -> Idt.Input (ident_of_obj x)
    | { obj_data = Output x} -> Idt.Output (ident_of_obj x)

(* [to_string x] returns a string representation of [x]. Control characters are escaped. *)

(* val to_string : obj -> string *)

let to_string x = Idt.to_string (ident_of_obj x)

(* [print x] prints a representation of [x]. Control characters are escaped. *)

(* val print : obj -> unit *)

let print x = Idt.print (ident_of_obj x)

(* PrimitiveObj and Obj module *)

module type ObjType =
  sig
    type t = obj
    val equal : t -> t -> bool
    val hash : t -> int
    val compare : t -> t -> int
    val to_string : t -> string
    val print : t -> unit
  end

(* PrimitiveObj is used in weak hash tables. Equality is to depth one. *)

module PrimitiveObj =
  struct
    type t = obj
    let equal = obj_equal
    let hash = obj_hash
    let compare = compare
    let to_string = to_string
    let print = print
  end (* : ObjType *)

(* StructuralObj is used on heap objects. Equality is reference equality. *)

module StructuralObj =
  struct
    type t = obj
    let equal x y = x==y
    let hash = obj_hash
    let compare = compare
    let to_string = to_string
    let print = print
  end (* : ObjType *)

(* Obj *)

IFNDEF JAVASCRIPT
THEN

module Obj = StructuralObj

ELSE

module Obj = PrimitiveObj

END

(* Comparison and hash functions *)

(* val equal : obj -> obj -> bool *)

let equal = Obj.equal

(* val hash : obj -> int *)

let hash = Obj.hash

(* val compare : obj -> obj -> int *)

let compare = Obj.compare

(*
 *
 * obj weak hash tables
 *
 *)

IFNDEF JAVASCRIPT
THEN

module ObjHash = Weak.Make(PrimitiveObj)

ELSE

module ObjHash = Hashtbl.Make(PrimitiveObj)

END

(* type heap *)

IFNDEF JAVASCRIPT
THEN 

type heap =
    {
      heap_table : ObjHash.t
    }

ELSE

type heap =
    {
      heap_table : Unit
    }

END

(* initial hash table size *)

let heap_size = 79397 (* a prime number *)

(* val init : unit -> heap *)

IFNDEF JAVASCRIPT
THEN 

let init () =
  {
    heap_table = ObjHash.create heap_size
  }

ELSE

let init () = { heap_tbale = () }

END

(* val print_stats : heap -> unit *)

IFNDEF JAVASCRIPT
THEN

let print_stats { heap_table = t } =
  let (len,nb,tbl,sbl,mbl,lbl) = ObjHash.stats t in
    Printf.printf "Heap statistics: table_length=%d, number_of_entries=%d, sum_of_bucket_lengths=%d, minimal_bucket_length=%d, mean_bucket_length=%d, maximal_bucket_length=%d\n" len nb tbl sbl mbl lbl  

ELSE

let print_stats { heap_table = t } =
  let s = ObjHash.stats t in
    Printf.printf "No statistics available.\n";

END

(* merge sort *)

let rec ms =
  function
      ([],l) -> l
    | (k,[]) -> k
    | (((x::k') as k),((y::l') as l)) ->
	let c = compare x y in
	  if c < 0
	  then x::(ms (k',l))
	  else
	    if c > 0
	    then y::(ms (k,l'))
	    else x::(ms (k',l'))

let merge_sort k l = ms (k,l)

(* drop twins in a sorted list *)

let rec dt =
  function
      (x,[]) -> []
    | (x,(y::l)) ->
	let c = Pervasives.compare x y in
	  if c < 0
	  then y::(dt (y,l))
	  else
	    if c > 0
	    then invalid_arg "Idt.dt: list is not sorted."
	    else dt (x,l)

let drop_twins =
  function
      x::l -> x::(dt (x,l))
    | [] -> []

(* generic make function *)

IFNDEF JAVASCRIPT
THEN

let make_generic { heap_table=h } d =
  let k = hash_data d in
  let x = { obj_hash=k; obj_data=d } in
    try
      ObjHash.find h x 
    with
	Not_found ->
	  begin
	    ObjHash.add h x;
	    x
	  end

ELSE

let make_generic _ d =
  let k = hash_data d in
  { obj_hash=k; obj_data=d }
    
END

(* val make_ident : heap -> string -> obj *)

let make_ident w s = make_generic w (Ident s)

(* val make_char : heap -> char -> obj *)

let make_char w c = make_generic w (Char c)

(* val make_string : heap -> string -> obj *)

let make_string w s = make_generic w (String s)

(* val make_bool : heap -> bool -> obj *)

let make_bool w b = make_generic w (Bool b)

(* val make_int : heap -> int -> obj *)

let make_int w n = make_generic w (Int n)

(* val make_tuple : heap -> obj array -> obj *)

let make_tuple w a = make_generic w (Tuple a)

(* val make_set : heap -> obj list -> obj *)

let make_set w l =
  make_generic
    w
    (Set
       (drop_twins
	  (List.sort compare l)))

(* val make_observable : heap -> obj -> obj *)

let make_observable w x = make_generic w (Observable x)

(* val make_silent : heap -> obj *)

let make_silent w = make_generic w Silent

(* val make_input : heap -> obj -> obj *)

let make_input w x = make_generic w (Input x)

(* val make_output : heap -> obj -> obj *)

let make_output w x = make_generic w (Output x)

(* val make_empty_tuple : heap -> obj *)

let make_empty_tuple w = make_tuple w [||]

(* val make_empty_set : heap -> obj *)

let make_empty_set w = make_set w []

(* val make_singleton : heap -> obj -> obj *)

let make_singleton w x = make_set w [ x ]

(* val make_scalar : heap -> obj -> obj *)

let make_scalar w x = make_tuple w [| x |]

(* val make_true : heap -> obj *)

let make_true w = make_bool w true

(* val make_false : heap -> obj *)

let make_false w = make_bool w false

(* val is_ident : obj -> bool *)

let is_ident =
  function
      { obj_data=Ident _ } -> true
    | _ -> false

(* val is_char : obj -> bool *)

let is_char =
  function
      { obj_data=Char _ } -> true
    | _ -> false

(* val is_string : obj -> bool *)

let is_string =
  function
      { obj_data=String _ } -> true
    | _ -> false

(* val is_bool : obj -> bool *)

let is_bool =
  function
      { obj_data=Bool _ } -> true
    | _ -> false

(* val is_int : obj -> bool *)

let is_int =
  function
      { obj_data=Int _ } -> true
    | _ -> false

(* val is_tuple : obj -> bool *)

let is_tuple =
  function
      { obj_data=Tuple _ } -> true
    | _ -> false

(* val is_set : obj -> bool *)

let is_set =
  function
      { obj_data=Set _ } -> true
    | _ -> false

(* val is_observable : obj -> bool *)

let is_observable =
  function
      { obj_data=Observable _ } -> true
    | _ -> false

(* val is_silent : obj -> bool *)

let is_silent =
  function
      { obj_data=Silent } -> true
    | _ -> false

(* val is_input : obj -> bool *)

let is_input =
  function
      { obj_data=Input _ } -> true
    | _ -> false

(* val is_output : obj -> bool *)

let is_output =
  function
      { obj_data=Output _ } -> true
    | _ -> false

(* val ident_of : obj -> string *)

let ident_of =
  function
      { obj_data=Ident s } -> s
    | _ -> raise (Typecheck "Heap.ident_of: Typecheck error")

(* val char_of : obj -> char *)

let char_of =
  function
      { obj_data=Char c } -> c
    | _ -> raise (Typecheck "Heap.char_of: Typecheck error")

(* val string_of : obj -> char *)

let string_of =
  function
      { obj_data=String s } -> s
    | _ -> raise (Typecheck "Heap.string_of: Typecheck error")

(* val bool_of : obj -> bool *)

let bool_of =
  function
      { obj_data=Bool b } -> b
    | _ -> raise (Typecheck "Heap.bool_of: Typecheck error")

(* val int_of : obj -> int *)

let int_of =
  function
      { obj_data=Int n } -> n
    | _ -> raise (Typecheck "Heap.int_of: Typecheck error")

(* val tuple_of : obj -> obj array *)

let tuple_of =
  function
      { obj_data=Tuple a } -> a
    | _ -> raise (Typecheck "Heap.tuple_of: Typecheck error")

(* val set_of : obj -> obj list *)

let set_of =
  function
      { obj_data=Set l } -> l
    | _ -> raise (Typecheck "Heap.set_of: Typecheck error")

(* val observable_of : obj -> obj *)

let observable_of =
  function
      { obj_data=Observable x } -> x
    | _ -> raise (Typecheck "Heap.observable_of: Typecheck error")

(* val silent_of : obj -> unit *)

let silent_of =
  function
      { obj_data=Silent } -> ()
    | _ -> raise (Typecheck "Heap.silent_of: Typecheck error")

(* val input_of : obj -> obj *)

let input_of =
  function
      { obj_data=Input x } -> x
    | _ -> raise (Typecheck "Heap.input_of: Typecheck error")

(* val output_of : obj -> obj *)

let output_of =
  function
      { obj_data=Output x } -> x
    | _ -> raise (Typecheck "Heap.output_of: Typecheck error")

(* [concat x y] concatenates tuples [x] and [y] *)

(* val concat : heap -> obj -> obj -> obj *)

let concat w x y = make_tuple w (Array.append (tuple_of x) (tuple_of y))

(* [union x y] computes the union of sets [x] and [y] *)

(* val union : heap -> obj -> obj -> obj *)

let union w =
  function
      { obj_data=Set l1 } ->
	begin
	  function
	      { obj_data=Set l2 } ->
		make_set w (merge_sort l1 l2)
	    | _ -> raise (Typecheck "Second parameter of union is not a set")
	end
    | _ -> raise (Typecheck "First parameter of union is not a set")

(* [set_add x y] computes { [x] } \/ [y] *)

(* val set_add : heap -> obj -> obj -> obj *)

let set_add w x y = union w (make_singleton w x) y

(* val obj_of_ident : Idt.t -> obj *)

let obj_of_ident w y =
  let rec ooi =
    function
	Idt.Ident s -> make_ident w s
      | Idt.Char c -> make_char w c
      | Idt.String s -> make_string w s
      | Idt.Bool b -> make_bool w b
      | Idt.Int n -> make_int w n
      | Idt.Tuple a -> make_tuple w (Array.map ooi a)
      | Idt.Set l ->
	  List.fold_right
	    (fun x r -> union w (make_singleton w (ooi x)) r)
	    l
	    (make_empty_set w)
      | Idt.Observable x -> make_observable w (ooi x)
      | Idt.Silent -> make_silent w
      | Idt.Input x -> make_input w (ooi x)
      | Idt.Output x -> make_output w (ooi x) in
    ooi y

(* [set_of_tuple x] makes a set from tuple [x] *)

(* val set_of_tuple : heap -> obj -> obj *)

let set_of_tuple w x = make_set w (Array.to_list (tuple_of x))

(* [tuple_of_set x] makes a tuple from set [x] *)

(* val tuple_of_set : heap -> obj -> obj *)

let tuple_of_set w x = make_tuple w (Array.of_list (set_of x))

(* [tuple_of_scalar x] makes a singleton tuple of [x] *)

(* val tuple_of_scalar : heap -> obj -> obj *)

let tuple_of_scalar = make_scalar

(* [set_of_scalar x] make a singleton set of [x] *)

(* val set_of_scalar : heap -> obj -> obj *)

let set_of_scalar = make_singleton
