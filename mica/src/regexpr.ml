(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Regexpr: modal regular expressions over an arbitrary alphabet
 *
 * $Id: regexpr.ml 479 2011-12-09 08:39:29Z bcaillau $
 *
 *)

type 'a e =
    Empty
  | Epsilon
  | Must of 'a
  | Cannot of 'a
  | Prefix of 'a * 'a expr
  | Concat of 'a expr * 'a expr
  | Sum of 'a expr * 'a expr
  | Star of 'a expr

and 'a expr =
    {
      hash : int;
      expr : 'a e
    }

(* hash modulus and factor *)

let hash_modulus = 999331 (* a prime number *)

let hash_factor = 373 (* another prime, not too large *)

(* hash functions *)

let hash_e h =
  function
      Empty -> 3
    | Epsilon -> 5
    | Must a -> (7 + ((h a) mod hash_modulus)) mod hash_modulus
    | Cannot a -> (11 + ((h a) mod hash_modulus)) mod hash_modulus
    | Prefix (a,x) -> (13 + ((h a) mod hash_modulus) + hash_factor * x.hash) mod hash_modulus
    | Concat (x,y) -> (17 + x.hash + hash_factor * y.hash) mod hash_modulus
    | Sum (x,y) -> (19 + x.hash + hash_factor * y.hash) mod hash_modulus
    | Star x -> (23 + x.hash) mod hash_modulus

let hash_expr { hash=n } = n 

(* equality functions *)

let equal_e eq x y =
  match (x,y) with
      (Empty,Empty) -> true
    | (Epsilon,Epsilon) -> true
    | ((Must e),(Must f)) -> eq e f
    | ((Cannot e),(Cannot f)) -> eq e f
    | ((Prefix (a,x)),(Prefix (b,y))) ->
	(eq a b) && (x == y)
    | ((Concat (x1,y1)),(Concat (x2,y2))) ->
	(x1 == x2) && (y1 == y2)
    | ((Sum (x1,y1)),(Sum (x2,y2))) ->
	(x1 == x2) && (y1 == y2)
    | ((Star x),(Star y)) ->
	(x == y)
    | (_,_) -> false

let equal_expr eq { hash=h1; expr=e1 } { hash=h2; expr=e2 } =
  (h1 = h2) && (equal_e eq e1 e2)

(* to_string and print *)

let rec stringer pr n =
  function
      { expr = Empty } -> "0"
    | { expr = Epsilon } -> "1"
    | { expr = Must e } -> "[+"^(pr e)^"]"
    | { expr = Cannot e } -> "[-"^(pr e)^"]"
    | { expr = Prefix (a,x) } ->
	let s = (pr a)^"."^(stringer pr 1 x) in
	  if n <= 1
	  then s
	  else "("^s^")"
    | { expr = Concat (x,y) } ->
	let s = (stringer pr 1 x)^"."^(stringer pr 1 y) in
	  if n <= 1
	  then s
	  else "("^s^")"
    | { expr = Sum (x,y) } ->
	let s = (stringer pr 0 x)^"+"^(stringer pr 0 y) in
	  if n <= 0
	  then s
	  else "("^s^")"
    | { expr = Star x } ->
	let s = (stringer pr 2 x)^"*" in
	  if n <= 2
	  then s
	  else "("^s^")"

and to_string_expr pr = stringer pr 0

let print_expr pr x =
  print_string (to_string_expr pr x)

let symbol_base = 26
and symbol_offset = Char.code 'a'

let rec symbol_of_index n =
  let x = n mod symbol_base
  and y = n / symbol_base in
  let c = Char.chr (x+symbol_offset) in
    if y <= 0
    then String.make 1 c
    else (symbol_of_index (y-1))^(String.make 1 c)
      
(* comparison function *)

let kind =
  function
      Empty -> 0
    | Epsilon -> 1
    | Must _ -> 2
    | Cannot _ -> 3
    | Prefix (_,_) -> 4
    | Concat (_,_) -> 5
    | Sum (_,_) -> 6
    | Star _ -> 7

let rec compare_expr
    cmp
    ({ hash=h1; expr=e1 } as x1)
    ({ hash=h2; expr=e2 } as x2) =
  if x1==x2
  then 0
  else
    if h1 < h2
    then -1
    else
      if h1 > h2
      then 1
      else
	let k1 = kind e1
	and k2 = kind e2 in
	  if k1 < k2
	  then -1
	  else
	    if k1 > k2
	    then 1
	    else
	      match (e1,e2) with
		  ((Must a1),(Must a2)) ->
		    cmp a1 a2
		| ((Cannot a1),(Cannot a2)) ->
		    cmp a1 a2
		| ((Prefix (a1,y1)),(Prefix (a2,y2))) ->
		    let c = cmp a1 a2 in
		      if c=0
		      then compare_expr cmp y1 y2
		      else c
		| ((Concat (y1,z1)),(Concat (y2,z2))) ->
		    let c = compare_expr cmp y1 y2 in
		      if c=0
		      then compare_expr cmp z1 z2
		      else c
		| ((Sum (y1,z1)),(Sum (y2,z2))) ->
		    let c = compare_expr cmp y1 y2 in
		      if c=0
		      then compare_expr cmp z1 z2
		      else c
		| ((Star y1),(Star y2)) ->
		    compare_expr cmp y1 y2
		| (_,_) -> failwith "Regexpr.compare_expr: impossible case. Please report bug to developper"

(* Merge sort on event lists *)

let merge_sorted_lists cmp l1 l2 =
  let rec msl =
    function
	([],l2) -> l2
      | (l1,[]) -> l1
      | (((e1::k1) as l1),((e2::k2) as l2)) ->
	  let c = cmp e1 e2 in
	    if c < 0
	    then e1 :: (msl (k1,l2))
	    else
	      if c > 0
	      then e2 :: (msl (l1,k2))
	      else e1 :: (msl (k1,k2))
  in
    msl (l1,l2)

(* Event/Expr module type *)

module type EVENT_TYPE =
sig
  type t
  val equal : t -> t -> bool
  val hash : t -> int
  val compare : t -> t -> int
  val to_string : t -> string
  val print : t -> unit
end

(* Functor BasicExpr *)

module BasicExpr = functor (Event : EVENT_TYPE) ->  
struct
  type e = Event.t
  type t = e expr
  let equal = equal_expr Event.equal
  let hash = hash_expr
  let compute_hash = hash_e Event.hash
  let compare = compare_expr Event.compare
  let to_string = to_string_expr Event.to_string
  let print = print_expr Event.to_string
end 

(* initial hash table size *)

let heap_size = 79397 (* a prime number *)

(* default hash table size *)

let hash_size = 521 (* a prime number *)

(* Heap of regexpr *)

module Expr = functor (Event : EVENT_TYPE) ->
struct

  (* WeakHash module *)

  module Reg = BasicExpr(Event)

IFNDEF JAVASCRIPT
THEN

  module TheHash = Weak.Make(Reg)

ELSE

  module TheHash = Hashtbl.Make(Reg)

END

  (* Types *)
    
  type e = Event.t (* Event type *)
  type t = Reg.t (* Regexpr type *)

IFNDEF JAVASCRIPT
THEN

  type h = { heap : TheHash.t } (* Heap type *)

ELSE

  type h = { heap : t TheHash.t } (* Heap type *)

END
      
  (* Hash module *)

  module Xpr =
  struct
    type t = Reg.t
    let equal x y = x==y
    let hash = Reg.hash
  end
    
  module Hash = Hashtbl.Make(Xpr)

  (* basic function on regexpr *)
      
  let equal x y = x==y
  let hash =  Reg.hash
  let compare =  Reg.compare
  let to_string = Reg.to_string
  let print = Reg.print
    
  (* [init ()] returns a fresh heap *)

  let init () =
    {
      heap = TheHash.create heap_size
    }

  (* val print_stats : h -> unit *)

IFNDEF JAVASCRIPT
THEN

let print_stats { heap = t } =
  let (len,nb,tbl,sbl,mbl,lbl) = TheHash.stats t in
    Printf.printf "Heap statistics: table_length=%d, number_of_entries=%d, sum_of_bucket_lengths=%d, minimal_bucket_length=%d, mean_bucket_length=%d, maximal_bucket_length=%d\n" len nb tbl sbl mbl lbl  

ELSE

let print_stats { heap = t } =
  let s = TheHash.stats t in
    Printf.printf "Expect a large heap since the garbage collector cannot free objects with this JavaScript version of Mica\n";
    Printf.printf
      "Heap statistics: number_of_bindings=%d, number_of_buckets=%d, maximal_bucket_length=%d\n"
       s.Hashtbl.num_bindings s.Hashtbl.num_buckets s.Hashtbl.max_bucket_length  

END

  (* generic make function *)

  let make_generic { heap=t } e =
    let k = Reg.compute_hash e in
    let x = { hash=k; expr=e } in
      try
	TheHash.find t x 
      with
	  Not_found ->
	    begin
	      IFNDEF JAVASCRIPT THEN TheHash.add t x ELSE TheHash.add t x x END;
	      x
	    end

  (* Factory *)

  let make_empty w = make_generic w Empty
  let make_epsilon w = make_generic w Epsilon
  let make_prefix w e x = make_generic w (Prefix (e,x))
  let make_concat w x y = make_generic w (Concat (x,y))
  let make_sum w x y = make_generic w (Sum (x,y))
  let make_star w x = make_generic w (Star x)
  let make_must w e = make_generic w (Must e)
  let make_cannot w e = make_generic w (Cannot e)

  (* val is_accepting : t -> bool *)

  let rec is_accepting =
    function
	{ expr = Empty } -> false
      | { expr = Epsilon } -> true
      | { expr = Must _ } -> false
      | { expr = Cannot _ } -> false
      | { expr = Prefix (_,_) } -> false
      | { expr = Concat (x,y) } -> (is_accepting x) && (is_accepting y)
      | { expr = Sum (x,y) } -> (is_accepting x) || (is_accepting y)
      | { expr = Star _ } -> true

  (* [must_list x] computes the sorted list without duplicates of must events *)

  (* val must_list : t -> e list *)

  let rec must_list =
    function
	{ expr = Empty } -> []
      | { expr = Epsilon } -> []
      | { expr = Must e } -> [e]
      | { expr = Cannot _ } -> []
      | { expr = Prefix (_,_) } -> []
      | { expr = Concat (x,y) } ->
	  if is_accepting x
	  then merge_sorted_lists Event.compare (must_list x) (must_list y)
	  else must_list x
      | { expr = Sum (x,y) } ->
	  merge_sorted_lists Event.compare (must_list x) (must_list y)
      | { expr = Star x } ->
	  must_list x

  (* [cannot_list x] computes the sorted list without duplicates of cannot events *)

  (* val cannot_list : t -> e list *)

  let rec cannot_list =
    function
	{ expr = Empty } -> []
      | { expr = Epsilon } -> []
      | { expr = Must _ } -> []
      | { expr = Cannot e } -> [e]
      | { expr = Prefix (_,_) } -> []
      | { expr = Concat (x,y) } ->
	  if is_accepting x
	  then merge_sorted_lists Event.compare (cannot_list x) (cannot_list y)
	  else cannot_list x
      | { expr = Sum (x,y) } ->
	  merge_sorted_lists Event.compare (cannot_list x) (cannot_list y)
      | { expr = Star x } ->
	  cannot_list x

  (* Flattening functions *)

  (* val flatten_sum : t -> t list *)

  let rec flatten_sum =
    function
	{ expr = Sum (x,y) } ->
	  (flatten_sum x) @ (flatten_sum y)
      | x -> [x]

  (* flatten_concat : t -> t list *)

  let rec flatten_concat =
    function
	{ expr = Concat (x,y) } ->
	  (flatten_concat x) @ (flatten_concat y)
      | x -> [x]

  (* sort : t list -> t list *)

  let sort l = List.stable_sort compare l

  (* discard_redudant : t list -> t list *)

  let discard_redundant k =
    let rec dr s =
      function
	  [] -> []
	| x::l ->
	    match s with
		None -> x :: (dr (Some x) l)
	      | Some y ->
		  if x==y
		  then dr (Some y) l
		  else x :: (dr (Some x) l) in
      dr None k

  (* discard_empty : t list -> t list *)
	
  let rec discard_empty =
    function
	[] -> []
      | { expr = Empty }::l -> discard_empty l
      | x::l -> x :: (discard_empty l)

  (* discard_epsilon : t list -> t list *)

  let rec discard_epsilon =
    function
	[] -> []
      | { expr = Epsilon }::l -> discard_epsilon l
      | x::l -> x :: (discard_epsilon l)
	  
  (* sum_list : t list -> t *)

  let rec sum_list w =
    function
	[] -> make_empty w
      | [x] -> x
      | x::l -> make_sum w x (sum_list w l)

  (* concat_list : t list -> t *)

  let rec concat_list w =
    function
	[] -> make_epsilon w
      | [x] -> x
      | x::l -> make_concat w x (concat_list w l)

  (* val simplify_step : h -> t -> t *)

  let simplify_step w =
    function
	{ expr = Prefix (_,({ expr = Empty } as x)) } -> x
      | { expr = Concat ({ expr = Epsilon },x) } -> x
      | { expr = Concat (x,{ expr = Epsilon }) } -> x
      | { expr = Concat (({ expr = Empty } as x),_) } -> x
      | { expr = Concat (_,({ expr = Empty } as x)) } -> x
      | { expr = Concat ({ expr = Prefix (e,x) },y) } ->
	  make_prefix w e (make_concat w x y)
      | { expr = Sum ({ expr = Empty },x) } -> x
      | { expr = Sum (x,{ expr = Empty }) } -> x
      | { expr = Sum (x,y) } when x==y -> x
      | { expr = Sum (x,({ expr = Sum (y,z)} as v)) } when (x==y) || (x==z) -> v
      | { expr = Sum (({ expr = Sum (x,y)} as v),z) } when (x==z) || (y==z) -> v
      | x -> x

  (* val simplify_iter : h -> t -> t *)

  let simplify_iter w x =
    let y = ref x
    and b = ref true in
      begin
	while !b do
	  let y' = simplify_step w (!y) in
	    begin
	      b := (!y) != y';
	      y := y'
	    end
	done;
	!y
      end

  (* val simplify : h -> t -> t *)

  let rec simplify w =
    function
	{ expr = Empty } as x -> x
      | { expr = Epsilon } as x -> x
      | { expr = Must _ } as x -> x
      | { expr = Cannot _ } as x -> x
      | { expr = Prefix (e,x) } as y ->
	  let x' = simplify w x in
	    if x==x'
	    then simplify_iter w y
	    else simplify_iter w (make_prefix w e x')
      | { expr = Concat (x,y) } as z ->
	  let x' = simplify w x
	  and y' = simplify w y in
	    if (x==x') && (y==y')
	    then simplify_iter w z
	    else simplify_iter w (make_concat w x' y')
      | { expr = Sum (x,y) } as z ->
	  let x' = simplify w x
	  and y' = simplify w y in
	    if (x==x') && (y==y')
	    then simplify_iter w z
	    else simplify_iter w (make_sum w x' y')
      | { expr = Star x } as y ->
	  let x' = simplify w x in
	    if x==x'
	    then simplify_iter w y
	    else simplify_iter w (make_star w x')

  (* val reduce : h -> t -> t *)

  let reduce_concat w x =
    let l = discard_epsilon (flatten_concat x) in
      concat_list w l

  let reduce_sum w x =
    let l = discard_redundant (sort (discard_empty (flatten_sum x))) in
    let l' = List.map (reduce_concat w) l in
      sum_list w l'

  let rec reduce w x =
    let y = reduce_sum w (simplify w x) in
      if x==y then x else reduce w y

  (* Brzozowski derivation of an expression *)

  (* val derivate : h -> e -> t -> t *)

  let rec derivate w e =
    function
	{ expr = Empty } as x -> x
      | { expr = Epsilon } -> make_empty w
      | { expr = Must _ } -> make_empty w
      | { expr = Cannot _ } -> make_empty w
      | { expr = Prefix (f,x) } ->
	if Event.equal e f
	then x
	else make_empty w
      | { expr = Concat (x,y) } ->
	  if is_accepting x
	  then
	    let x' = derivate w e x
	    and y' = derivate w e y in
	      make_sum w (make_concat w x' y) y'
	  else
	    let x' = derivate w e x in
	      make_concat w x' y
      | { expr = Sum (x,y) } ->
	  let x' = derivate w e x
	  and y' = derivate w e y in
	    make_sum w x' y'
      | { expr = Star x } as u ->
	  let x' = derivate w e x in
	    make_concat w x' u

  (* dfs_derivatives : (t -> e -> t -> unit) -> h -> e list -> t  *)

  let dfs_derivatives f w g z =
    let v = Hash.create hash_size in (* table containing visited expressions *)
    let rec dfs x =
      (* has the expression been visited? *)
      if not (Hash.mem v x)
      then (* no: proceed with this expression *)
	begin
	  Hash.add v x ();
	  List.iter
	    (fun e ->
	       let y = reduce w (derivate w e x) in
		 begin
		   f x e y;
		   dfs y
		 end)
	    g
	end
    in
      dfs (reduce w z)

  (* Alphabet of an expression *)

  let rec alphabet =
    function
	{ expr = Empty } -> []
      | { expr = Epsilon } -> []
      | { expr = Must a } -> [a]
      | { expr = Cannot a } -> [a]
      | { expr = Prefix (a,x) } -> merge_sorted_lists Event.compare [a] (alphabet x) 
      | { expr = Concat (x,y) } -> merge_sorted_lists Event.compare (alphabet x) (alphabet y)
      | { expr = Sum (x,y) } -> merge_sorted_lists Event.compare (alphabet x) (alphabet y)
      | { expr = Star x } -> alphabet x
  
  let index_in_alphabet e a =
    let rec search =
      function
	  [] -> failwith "Regexpr.index_in_alphabet: not member of alphabet"
	| f::l ->
	    if Event.equal e f then 0 else 1+(search l)
    in search a
      
  (* Print expression skeleton, assigning unique labels to events *)
      
  let print_skeleton x =
    let a = alphabet x in
      print_expr (fun e -> symbol_of_index (index_in_alphabet e a)) x

  (* iterates on derivatives until all derivatives have been produced *)
  (* Exploration of a branch is cut as soon as the operator returns [false] *)

  (* val dfs_derivatives_cut : (t -> e -> t -> bool) -> h -> e list -> t -> unit *)
	
  let dfs_derivatives_cut f w g z =
    let v = Hash.create hash_size in (* table containing visited expressions *)
    let rec dfs x =
      (* has the expression been visited? *)
      if not (Hash.mem v x)
      then (* no: proceed with this expression *)
	begin
	  Hash.add v x ();
	  (* print_skeleton x; *)
	  (* print_string "\n"; *)
	  List.iter
	    (fun e ->
	       let y = reduce w (derivate w e x) in
		 if f x e y;
		 then dfs y)
	    g
	end
    in
      dfs (reduce w z)
	  
end (* of functor ExprHeap *)

