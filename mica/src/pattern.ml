(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Pattern: library of modal interfaces
 *
 * $Id: pattern.ml 479 2011-12-09 08:39:29Z bcaillau $
 *
 *)

(*
 *
 * [greatest_suprefix u i e] computes the length of the largest
 * suffix of array [v@[e]] that is a prefix of [u], where [v] is
 * the prefix of length [i] of [u].
 *
 *)

let greatest_suprefix u i e = 
  let p = Array.length u in
  let n = ref (min p (i+1))
  and ok = ref false in
    begin
      while not (!ok) do
	ok := true;
	if (!n) > 0 then ok := (e == u.((!n)-1));
	for j=0 to (!n)-2 do
	  ok := (!ok) && (u.(j) == u.(j+i+1-(!n)))
	done;
	if not (!ok) then n := (!n)-1
      done;
      !n
    end

(* Generic every pattern *)

let every_generic m w g u =
  let n = Array.length u
  and s = Mi.make g in
    begin
      Mi.set_initial s (Some (Heap.make_int w 0));
      for i = 0 to n do
	Signature.iter
	  (fun e _ ->
	     let j = greatest_suprefix u i e in
	       if (j = n) && (e == u.(n-1))
	       then
		 Mi.set_trans
		   s
		   (Heap.make_int w i)
		   e
		   (m (Heap.make_int w j))
	       else
		 Mi.set_trans
		   s
		   (Heap.make_int w i)
		   e
		   (Modal.May (Heap.make_int w j)))
	  g
      done;
      s
    end

(*
 *
 * [every_must w g u] produces an interface with signature [g]
 * and allocated in heap [w] such that for every occurrence of
 * word [u], the last event of [u] has modality must.
 * Exception [Invalid_argument _] is raised if [u] is not in signature [g].
 *
 *)

(* val every_must : Heap.heap -> Signature.t -> Heap.obj array -> Mi.t *)

let every_must = every_generic (fun q -> Modal.Must q)

(*
 *
 * [every_cannot w g u] produces an interface with signature [g]
 * and allocated in heap [w] such that for every occurrence of
 * word [u], the last event of [u] has modality cannot.
 * Exception [Invalid_argument _] is raised if [u] is not in signature [g].
 *
 *)

(* val every_cannot : Heap.heap -> Signature.t -> Heap.obj array -> Mi.t *)

let every_cannot = every_generic (fun _ -> Modal.Cannot)

(*
 *
 * [after_must v w g x e] produces an interface with signature [g]
 * allocated in heap [v] such that after the occurrence of a word 
 * accepted by expression [x], event [e] has modality must. Heap
 * [w] is used to compute the derivatives of expression [x].
 * Exception [Invalid_argument _] is raised if [x] or [e] are not
 * in signature [g].
 *
 *)

(* val after_must : Heap.heap -> Objexpr.h -> Signature.t -> Objexpr.t -> Heap.obj -> Mi.t *)

let after_must v w g x e =
  let s =
    Objexpr.to_interface
      w
      v
      g
      (Objexpr.make_concat w x (Objexpr.make_must w e)) in
    Mi.minimize s

(*
 *
 * [after_cannot v w g x e] produces an interface with signature [g]
 * allocated in heap [w] such that after the occurrence of a word 
 * accepted by expression [x], event [e] has modality must. Heap
 * [w] is used to compute the derivatives of expression [x].
 * Exception [Invalid_argument _] is raised if [x] or [e] are not
 * in signature [g].
 *
 *)

(* val after_cannot : Heap.heap -> Objexpr.h -> Signature.t -> Objexpr.t -> Heap.obj -> Mi.t *)

let after_cannot v w g x e =
  let s =
    Objexpr.to_interface
      w
      v
      g
      (Objexpr.make_concat w x (Objexpr.make_cannot w e)) in
    Mi.minimize s
      
