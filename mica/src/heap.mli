(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Heap: object heap with structural equality
 *
 * $Id: heap.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type heap

type obj

module type ObjType =
  sig
    type t = obj
    val equal : t -> t -> bool
    val hash : t -> int
    val compare : t -> t -> int
    val to_string : t -> string
    val print : t -> unit
  end

module Obj : ObjType

exception Typecheck of string

val init : unit -> heap

val print_stats : heap -> unit

val equal : obj -> obj -> bool
val hash : obj -> int
val compare : obj -> obj -> int

val make_ident : heap -> string -> obj
val make_char : heap -> char -> obj
val make_string : heap -> string -> obj
val make_bool : heap -> bool -> obj
val make_int : heap -> int -> obj
val make_tuple : heap -> obj array -> obj
val make_set : heap -> obj list -> obj
val make_observable : heap -> obj -> obj
val make_silent : heap -> obj
val make_input : heap -> obj -> obj
val make_output : heap -> obj -> obj

val make_empty_tuple : heap -> obj
val make_empty_set : heap -> obj

val make_singleton : heap -> obj -> obj
val make_scalar : heap -> obj -> obj

val make_true : heap -> obj
val make_false : heap -> obj

val is_ident : obj -> bool
val is_char : obj -> bool
val is_string : obj -> bool
val is_bool : obj -> bool
val is_int : obj -> bool
val is_tuple : obj -> bool
val is_set : obj -> bool
val is_observable : obj -> bool
val is_silent : obj -> bool
val is_input : obj -> bool
val is_output : obj -> bool

val ident_of : obj -> string
val char_of : obj -> char
val string_of : obj -> string
val bool_of : obj -> bool
val int_of : obj -> int
val tuple_of : obj -> obj array
val set_of : obj -> obj list
val observable_of : obj -> obj
val silent_of : obj -> unit
val input_of : obj -> obj
val output_of : obj -> obj

(* Mapping to/from Idt.t *)

val ident_of_obj : obj -> Idt.t

val obj_of_ident : heap -> Idt.t -> obj

(* [compare x y] returns [0] if [x] is equal to [y], [-1] if [x] is less than [y] and [1] if [x] is greater than [y] *)

val compare : obj -> obj -> int

(* [to_string x] returns a string representation of [x]. Control characters are escaped. *)

val to_string : obj -> string

(* [print x] prints a representation of [x]. Control characters are escaped. *)

val print : obj -> unit

(* [concat x y] concatenates tuples [x] and [y] *)

val concat : heap -> obj -> obj -> obj

(* [set_add x y] computes { [x] } \/ [y] *)

val set_add : heap -> obj -> obj -> obj

(* [union x y] computes the union of sets [x] and [y] *)

val union : heap -> obj -> obj -> obj

(* [set_of_tuple x] makes a set from tuple [x] *)

val set_of_tuple : heap -> obj -> obj

(* [tuple_of_set x] makes a tuple from set [x] *)

val tuple_of_set : heap -> obj -> obj

(* [tuple_of_scalar x] makes a singleton tuple of [x] *)

val tuple_of_scalar : heap -> obj -> obj

(* [set_of_scalar x] make a singleton set of [x] *)

val set_of_scalar : heap -> obj -> obj
