 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************

This software requires:

     - Ocaml, version 4.03 or later
     - GraphViz

It compiles and runs on the following platforms:

     - Linux, OpenBSD, FreeBSD, Solaris
     - Windows NT/XP/7 with Cygwin
     - MacOS X version 10.4 or later

To compile this software:

     - Copy file Makefile_config_generic to Makefile_config

     - Edit file Makefile_config and set the installation folders.

     - Run the following make command:

       	   make all all.opt

     - This produces the following libraries and interactive commands:

       	   mica.cma: Ocaml bytecode library
           mica.cmxa/mica.a: Native codee library
	       ocaml_mica: Ocaml toplevel linked with the mica library
	       mica: the interactive environment
	       magen: command to generate a modal automata

     - Installation is done as follows:

       	   make install

	   	or as follows:

	   sudo make install

	   	if superuser/administrator privileges are required.
