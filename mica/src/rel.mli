(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Rel: relations represented by extension
 *
 * $Id: rel.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t

val make_empty : unit -> t

val clone : t -> t

val transpose : t -> t

val set : t -> Heap.obj -> Heap.obj -> bool -> unit

val get : t -> Heap.obj -> Heap.obj -> bool

