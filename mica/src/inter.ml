(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Inter: evaluation machine for the interactive environment
 *
 * $Id: inter.ml 485 2012-03-08 09:40:02Z bcaillau $
 *
 *)

exception Error of string

(* Types *)

type value = Heap.obj

type signature = Signature.t

type system = System.t

type interface = Mi.t

type expression = Objexpr.t

(* Session handles *)

type session_handle =
    {
      mutable obj : Heap.heap option;
      mutable exp : Objexpr.h option
    }

let handle =
  {
    obj = None;
    exp = None
  }
  
(* printers for the above types *)

(* val print_value : value -> unit *)

let print_value = Heap.print

(* val print_signature : signature -> unit *)

let print_signature = Signature.print

(* val print_system : system -> unit *)

let print_system = System.print

(* val print_interface : interface -> unit *)

let print_interface = Mi.print

(* val print_expression : expression -> unit *)

let print_expression = Objexpr.print

(* Print informations about interfaces and systems *)

(* val info_interface : interface -> unit *)

let info_interface = Mi.print_statistics

(* val new_session : unit -> unit *)

let new_session () =
  begin
    handle.obj <- Some (Heap.init ());
    handle.exp <- Some (Objexpr.init ()); 
    print_endline "Mica Interactive Environment";
    print_endline ""
  end

let obj_heap () =
  match handle.obj with
      None -> raise (Error "No session has been opened.")
    | Some w -> w

let exp_heap () =
  match handle.exp with
      None -> raise (Error "No session has been opened.")
    | Some w -> w

type it =
    Nothing
  | Signature of signature
  | System of system
  | Interface of interface

let it = ref Nothing

let it_sig () =
  match !it with
      Signature g -> g
    | _ -> raise (Error "No signature opened.")

let it_sys () =
  match !it with
      System m -> m
    | _ -> raise (Error "No system opened.")

let it_int () =
  match !it with
      Interface s -> s
    | _ -> raise (Error "No interface opened.")

(* val new_signature : unit -> unit *)

let new_signature () =
  it := Signature (Signature.make ())

(* val define_signature : unit -> signature *)

let define_signature () =
  let g = it_sig () in
    begin
      it := Nothing;
      g
    end

(* val output : value -> unit *)

let output x =
  let g = it_sig () in
    Signature.set g x Orientation.Output

(* val input : value -> unit *)

let input x =
  let g = it_sig () in
    Signature.set g x Orientation.Input

(* val new_system : signature -> unit *)

let new_system g =
  it := System (System.make g)

(* val define_system : unit -> system *)

let define_system () =
  let m = it_sys () in
    begin
      it := Nothing;
      m
    end

(* val void : unit -> unit *)

let void () =
  let s = it_int () in
    Mi.set_initial s None

(* val init : value -> unit *)

let init q =
  match !it with
      System m -> System.set_initial m q true
    | Interface s -> Mi.set_initial s (Some q)
    | _ -> raise (Error "No system or interface opened.")

(* val trans : value -> value -> value -> unit *)

let trans q e q' =
  let m = it_sys () in
    System.set_trans m q e q' true

(* val new_interface : signature -> unit *)

let new_interface g =
  it := Interface (Mi.make g)

(* val define_interface : unit -> interface *)

let define_interface () =
  let s = it_int () in
    begin
      it := Nothing;
      Mi.reduction s
    end

(* val may : value -> value -> value -> unit *)

let may q e q' =
  let s = it_int () in
    Mi.set_trans s q e (Modal.May q')

(* val must : value -> value -> value -> unit *)

let must q e q' =
  let s = it_int () in
    Mi.set_trans s q e (Modal.Must q')

(* val inconsistent : value -> value -> unit *)

let inconsistent q e =
  let s = it_int () in
    Mi.set_trans s q e Modal.Inconsistent

(* val cannot : value -> value -> unit *)

let cannot q e =
  let s = it_int () in
    Mi.set_trans s q e Modal.Cannot

(* val print_statistics : unit -> unit *)

let print_statistics () = Heap.print_stats (obj_heap ())

(* val signature_of_system : system -> signature *)

let signature_of_system = System.signature

(* val signature_of_interface : interface -> signature *)

let signature_of_interface = Mi.signature

(* val parallel : system -> system -> system *)

let parallel m n = System.parallel (obj_heap ()) System.incompatibility_print m n

(* val dual : system -> system *)

(* val satisfies : system -> interface -> bool *)

let satisfies = Mi.satisfies Mi.satisfaction_handler_print


(* val refines : interface -> interface -> bool *)

let refines = Mi.refines Mi.refines_handler_print

(* val consistency : interface -> interface -> bool *)

let consistency = Mi.consistency Mi.consistency_handler_print
    
(* val is_consistent : interface -> unit *)

let is_consistent = Mi.is_consistent

(* [is_complete s] checks whether interface [s] is complete *)

(* val is_complete : interface -> bool *)

let is_complete = Mi.is_complete Mi.completeness_handler_print

(* [is_trivial s] checks whether interface [s] is trivial *)

(* val is_trivial : interface -> bool *)

let is_trivial = Mi.is_trivial Mi.triviality_handler_print

(* [is_factor s1 s2] checks whether interface [s1] is a factor of [s2] *)

(* val is_factor : interface -> interface -> bool *)

let is_factor = Mi.is_factor Mi.factor_handler_print

(* val conjunction : interface -> interface -> interface *)

let conjunction a b = Mi.conjunction (obj_heap ()) a b

(* val product : interface -> interface -> interface *)

let product a b = Mi.product (obj_heap ()) a b

(* val quotient : interface -> interface -> interface *)

let quotient a b = Mi.quotient (obj_heap ()) a b

(*
 *
 * [compatible_quotient a b] computes the compatible quotient of interface [a]
 * by interface [b]. It is the largest [x] such that [x] is compatible with [b]
 * and [product x b] refines [a].
 *
 *)

(* val compatible_quotient : interface -> interface -> interface *)

let compatible_quotient a b = Mi.compatible_quotient (obj_heap ()) a b

(* [wimply s1 s2] computes the weak implication of [s1] by [s2] *)

(* val wimply : interface -> interface -> interface *)

let wimply a b = Mi.wimply (obj_heap ()) a b

(* [contract g a] computes contract ([g]x[a])/[a] *)

(* val contract : interface -> interface -> interface *)

let contract g a = Mi.contract (obj_heap ()) g a

(*
 *
 * [parallel_optimistic s1 s2] computes the optimistic parallel composition
 * of interfaces [s1] and [s2].
 *
 *)

(* val parallel_optimistic : interface -> interface -> interface *)

let parallel_optimistic a b = Mi.parallel (obj_heap ()) a b

(* [interface_of_system m] maps deterministic system [m] to a rigid interface *)
(* Raises [Error _] if [m] is not deterministic *)

(* val interface_of_system : system -> interface *)

let interface_of_system =
  try
    Mi.of_system
  with
      Invalid_argument s -> raise (Error ("System is not deterministic ("^s^")"))

(* Extremal implementations. Raises [Error _] if specification is inconsistent *)

(* val min_implementation : interface -> system *)

let min_implementation =
  try
    Mi.min_implementation
  with
      Invalid_argument s ->
	raise (Error ("Inter.min_implementation/"^s))

(* val max_implementation : interface -> system *)

let max_implementation =
  try
    Mi.max_implementation
  with
      Invalid_argument s ->
	raise (Error ("Inter.max_implementation/"^s))

(* val ident : string -> value *)

let ident i = Heap.make_ident (obj_heap ()) i

(* val char : char -> value *)

let char c = Heap.make_char (obj_heap ()) c

(* val string : string -> value *)

let string s = Heap.make_string (obj_heap ()) s

(* val bool : bool -> value *)

let bool b = Heap.make_bool (obj_heap ()) b

(* val int : int -> value *)

let int i = Heap.make_int (obj_heap ()) i

(* val tuple : value array -> value *)

let tuple a = Heap.make_tuple (obj_heap ()) a

(* val set : value list -> value *)

let set l = Heap.make_set (obj_heap ()) l

(* Regular expression factory *)

(* val empty : unit -> expression *)

let empty () = Objexpr.make_empty (exp_heap ())

(* val epsilon : unit -> expression *)

let epsilon () = Objexpr.make_epsilon (exp_heap ())

(* val prefix : value -> expression -> expression *)

let prefix e x = Objexpr.make_prefix (exp_heap ()) e x

(* val concat : expression -> expression -> expression *)

let concat x y = Objexpr.make_concat (exp_heap ()) x y

(* val sum :expression -> expression -> expression *)

let sum x y = Objexpr.make_sum (exp_heap ()) x y

(* val star : expression -> expression *)

let star x = Objexpr.make_star (exp_heap ()) x

(* val shall : value -> expression *)

let shall e = Objexpr.make_must (exp_heap ()) e

(* val shallnot : value -> expression *)

let shallnot e = Objexpr.make_cannot (exp_heap ()) e

(* Generates an interface from an expression *)

(* val interface_of_expression : signature -> expression -> interface *)

let interface_of_expression h x = Mi.minimize (Objexpr.to_interface (exp_heap ()) (obj_heap ()) h x)

IFNDEF JAVASCRIPT THEN

(* Display systems and interfaces *)

(* val display_system : system -> string -> unit *)

let display_system a n = Display.system_display a n

(* val display_interface : interface -> string -> unit *)

let display_interface a n = Display.specification_display a n

(* val zoom_interface : interface -> string (* graph identifier *) -> value (* disc center *) -> int (* disc radius *) -> unit *)

let zoom_interface a n q r = Display.zoom_specification_display a n q r

(* Generates dot files from systems and interfaces *)

(* val dot_file_of_system : system -> string (* graph identifier *) -> string (* file name *) -> unit *)

let dot_file_of_system a i n = Display.system_to_file a i n

(* val dot_file_of_interface : interface -> string (* graph identifier *) -> string (* file name *) -> unit *)

let dot_file_of_interface a i n = Display.specification_to_file a i n

(* Generate PDF files from systems and interfaces *)

(* val system_to_pdf : system -> string (* graph identifier *) -> string (* PDF file name *) -> unit *)

let system_to_pdf s n m = Display.system_to_pdf s n m

(* val interface_to_pdf : interface -> string (* graph identifier *) -> string (* PDF file name *) -> unit *)

let interface_to_pdf a n m = Display.specification_to_pdf a n m

(* Explore interfaces *)

(* val explore : interface -> unit *)

let explore = Explorer.explore

  END;;

(* [mutate_interface s l] turns events in [l] to be outputs in interface [s] *)

(* val mutate_interface : interface -> value list -> interface *)

let mutate_interface s l = Mi.mutate s (List.map (fun e -> (e,Orientation.Output)) l)

(* [minimize s] minimizes interface [s] *)

(* val minimize : interface -> interface *)

let minimize s = Mi.minimize s

(* [simplify s] simplifies the state structure of interface [s] by mapping state labels to integers *)

(* val simplify : interface -> interface *)

let simplify s = Mi.simplify (obj_heap ()) s

(*
 *
 * [abstract l a] computes a projection of interface [a] on sub-alphabet [l]. The resulting interface is the
 * least abstraction of [a] in the class of interfaces over alphabet [l].
 *
 *)

(* val abstract : value list -> interface -> interface *)

let abstract l a =
  let s = Enum.make_empty () in
    begin
      List.iter
	(fun e -> Enum.set s e true)
	l;
      Mi.abstract
	(obj_heap ())
	s
	a
    end

(*
 *
 * [project h s a] computes a projection of interface [a] signature [s]. The resulting interface is the
 * largest projection of [a] that is receptive to all its inputs and that controls all its outputs.
 *
 *)

(* val project : signature -> interface -> interface *)

let project s a = Mi.project (obj_heap ()) s a

(*
 *
 * [facet s a] computes an abstraction [b] of a reduced interface [a], such that:
 *   - [b] is on the same signature as [a] 
 *   - the state structure of [b] is isomorphic to the state structure of [a], except that a universal (top) state has been added to [b]
 *   - every transition of [a] labeled by an event in [s] is preserved in [b], with the same modality
 *   - every "may" or "must" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition of [b]
 *   - every "cannot" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition to the top state
 *
 *)

(* val facet : value list -> interface -> interface *)

let facet l a =
  let s = Enum.make_empty () in
    begin
      List.iter
	(fun e -> Enum.set s e true)
	l;
      Mi.facet
	(obj_heap ())
	s
	a
    end

(* Patterns *)

(*
 *
 * [every_must g u] produces an interface with signature [g]
 * such that for every occurrence of word [u], the last event
 * of [u] has modality must. Exception [Error _] is raised if
 * [u] is not in signature [g].
 *
 *)

(* val every_must : signature -> value array -> interface *)

let every_must g u = Pattern.every_must (obj_heap ()) g u

(*
 *
 * [every_cannot g u] produces an interface with signature [g]
 * such that after every occurrence of word [u], last event excepted,
 * the last event of [u] has modality cannot. Exception
 * [Invalid_argument _] is raised if [u] is not in signature [g].
 *
 *)

(* val every_cannot : signature -> value array -> interface *)

let every_cannot g u = Pattern.every_cannot (obj_heap ()) g u

(*
 *
 * [after_must g x e] produces an interface with signature [g]
 * such that after the occurrence of a word accepted by
 * expression [x], event [e] has modality must.
 * Exception [Error _] is raised if [x] or [e] are not in
 * signature [g].
 *
 *)

(* val after_must : signature -> expression -> value -> interface *)

let after_must g x e = Pattern.after_must (obj_heap ()) (exp_heap ()) g x e

(*
 *
 * [after_cannot g x e] produces an interface with signature [g]
 * such that after the occurrence of a word accepted by
 * expression [x], event [e] has modality cannot.
 * Exception [Error _] is raised if [x] or [e] are not in
 * signature [g].
 *
 *)

(* val after_cannot : signature -> expression -> value -> interface *)

let after_cannot g x e = Pattern.after_cannot (obj_heap ()) (exp_heap ()) g x e

(*** Heuristics ***)

(*
 *
 * [max_consistent_sets a] computes the list of maximal consistent subsets of [a]. [a] is an array of pairs identifier * interface.
 *
 *)

(* val max_consistent_sets : ('a * interface) array -> 'a list list *)

let max_consistent_sets r = Heuristics.max_consistent_sets (obj_heap ()) r
  
  
(*-----------------------------------------------------------------------------
 * Comparison function
 *---------------------------------------------------------------------------*)

(* type to describe a comparison relationship between two interfacesn*)
(* Type [comparison] is used by [compare_interface] *)
type comparison =
  | Lt (* Less than *)
  | Gt (* Greater than *)
  | Eq (* Equivalent *)
  | Ic (* Incomparable *)

(* [compare_interfaces s1 s2] decides whether [s1] refines [s2] and whether [s2] refines [s1] *)
(* val compare_interfaces : interface -> interface -> comparison *)
let compare_interfaces x y =
  let _ = Printf.printf "Checking refinement...\n" in
  let c = refines x y in
  let _ = Printf.printf "Checking abstraction...\n" in
  let d = refines y x in
    match (c,d) with
	(true,true) -> Eq
      | (true,false) -> Lt
      | (false,true) -> Gt
      | (false,false) -> Ic
