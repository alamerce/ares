(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Enumerate : bijective mapping of an enumerated set to integers
 *
 * $Id: enumerate.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
    {
      card : int;
      to_int : int Mapping.t;
      of_int : Heap.obj array
    }

(* val enumerate : Enum.t -> t *)

let enumerate a =
  let n = Enum.card_in a in
    if n <= 0
    then
      {
	card = 0;
	to_int = Mapping.make (-1);
	of_int = [||]
      }
    else
      let m = Mapping.make (-1)
      and r = Array.make n (Enum.witness_in a) in
	begin
	  ignore
	    (Enum.fold_in
	       (fun e i ->
		  begin
		    Mapping.set m e i;
		    r.(i) <- e;
		    i+1
		  end)
	       a 0);
	  {
	    card = n;
	    to_int = m;
	    of_int = r
	  }
	end
