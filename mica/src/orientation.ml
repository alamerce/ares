(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Orientation : I/O orientation lattice
 *
 * I(N)consistent is the minimum element.
 * (I)nput and (O)utput are incomparable.
 * (U)ndefined is the maximum element
 *
 * $Id: orientation.ml 430 2011-11-03 10:29:46Z bcaillau $
 *
 *)

(* Orientation lattice *)

type t =
    Input
  | Output
  | Inconsistent
  | Undefined

(* Refinement partial order *)

(* val leq : t -> t -> bool *)

let leq =
  function
      Input ->
	begin
	  function
	    | Input -> true
	    | Undefined -> true
	    | _ -> false
	end
    | Output ->
	begin
	  function
	    | Output -> true
	    | Undefined -> true
	    | _ -> false
	end
    | Undefined ->
	begin
	  function
	      Undefined -> true
	    | _ -> false
	end
    | Inconsistent ->
	(fun _ -> true)

(* Printers *)

(* val to_string : t -> string *)

let to_string =
  function
      Undefined -> "_"
    | Inconsistent -> "#"
    | Input -> "?"
    | Output -> "!"

(* val print : t -> unit *)

let print d = print_string (to_string d)

(*
 *
 * [conjunction a b] computes the conjunction of [a] and [b]
 * Beware: this is not the glb in the orientation lattice.
 *
 * &  I O U N
 *
 * I  I N N N
 * O  N O N N
 * U  N N U N
 * N  N N N N
 *
 *)

(* val conjunction : t -> t -> t *)

let conjunction =
  function
      Input ->
	begin
	  function
	      Input -> Input
	    | _ -> Inconsistent
	end
    | Output ->
	begin
	  function
	      Output -> Output
	    | _ -> Inconsistent
	end
    | Undefined ->
	begin
	  function
	      Undefined -> Undefined
	    | _ -> Inconsistent
	end
    | Inconsistent ->
	(fun _ -> Inconsistent)

(*
 *
 * [lub a b] computes the weak disjunction of [a] and [b].
 * Beware: this is not the lub in the orientation lattice.
 *
 * &  I O U N
 *
 * I  I N N N
 * O  N O N N
 * U  N N U N
 * N  N N N N
 *
 *)

(* val lub : t -> t -> t *)

let lub = conjunction

(*
 *
 * [product a b] computes the parallel composition of [a] and [b]
 *
 * *  I O U N
 *
 * I  I O N N
 * O  O N N N
 * U  N N U N
 * N  N N N N
 *
 *)

(* val product : t -> t -> t *)

let product =
  function
      Input ->
	begin
	  function
	      Input -> Input
	    | Output -> Output
	    | Inconsistent -> Inconsistent
	    | Undefined -> Inconsistent
	end
    | Output ->
	begin
	  function
	      Input -> Output
	    | Output -> Inconsistent
	    | Inconsistent -> Inconsistent
	    | Undefined -> Inconsistent
	end
    | Inconsistent ->
	(fun _ -> Inconsistent)
    | Undefined ->
	begin
	  function
	      Undefined -> Undefined
	    | _ -> Inconsistent
	end

(*
 *
 * [quotient a b] computes the adjoint of parallel composition of [a] by [b]
 *
 * / b  I O U N
 *
 * a
 *
 * I    I N N N
 * O    O I N N
 * U    N N U N
 * N    N N N N
 *
 *)

(* val quotient : t -> t -> t *)

let quotient =
  function
      Input ->
	begin
	  function
	      Input -> Input
	    | _ -> Inconsistent
	end
    | Output ->
	begin
	  function
	      Input -> Output
	    | Output -> Input
	    | Inconsistent -> Inconsistent
	    | Undefined -> Inconsistent
	end
    | Inconsistent ->
	(fun _ -> Inconsistent)
    | Undefined ->
	begin
	  function
	      Undefined -> Undefined
	    | _ -> Inconsistent
	end

(*
 *
 * [wimply a b] computes the weak implication of [a] and [b].
 * Beware: this is not the adjoint of the conjunction in the
 * orientation lattice.
 *
 * \  I O U N
 *
 * I  I N N N
 * O  N O N N
 * U  N N U N
 * N  N N N N
 *
 *)

(* val wimply : t -> t -> t *)

let wimply = conjunction

(*
 *
 * [contract g a] computes the orientation of the contract ([g],[a]),
 * where [g] is the guaranteed orientation and [a] is the assumed
 * orientation. The result is consistent iff [g] and [a] are
 * complementary, or one or both of them are undefined.
 *
 *   A  I O U N
 *
 * G
 *
 * I    N I I N
 * O    O N O N
 * U    O I U N
 * N    N N N N
 *
 *)

(* val contract : t -> t -> t *)

let contract =
  function
      Input ->
	begin
	  function
	      Input -> Inconsistent
	    | Output -> Input
	    | Inconsistent -> Inconsistent
	    | Undefined -> Input
	end
    | Output ->
	begin
	  function
	      Input -> Output
	    | Output -> Inconsistent
	    | Inconsistent -> Inconsistent
	    | Undefined -> Output
	end
    | Inconsistent -> fun _ -> Inconsistent
    | Undefined ->
	begin
	  function
	      Input -> Output
	    | Output -> Input
	    | Inconsistent -> Inconsistent
	    | Undefined -> Undefined
	end

(*
 *
 * [extend_conj a b] computes the extensions to be applied to both components
 * [a] and [b] so that the conjunction of [a] and [b] can be computed.
 *
 * &  I   O   U   N
 *
 * I  UU  UU  UI  UU
 * O  UU  UU  UO  UU
 * U  IU  OU  UU  NU
 * N  UU  UU  UN  UU
 *
 *)

(* val extend_conj : t -> t -> t*t *)

let extend_conj d1 d2 =
  let f =
    function
	(Undefined,x) -> (x,Undefined)
      | (x,Undefined) -> (Undefined,x)
      | (_,_) -> (Undefined,Undefined)
  in
    f (d1,d2)

(*
 *
 * [extend_lub a b] computes the extensions to be applied to both components
 * [a] and [b] so that the lub of [a] and [b] can be computed.
 *
 * +  I   O   U   N
 *
 * I  UU  UU  UI  UU
 * O  UU  UU  UO  UU
 * U  IU  OU  UU  NU
 * N  UU  UU  UN  UU
 *
 *)

(* val extend_lub : t -> t -> t*t *)

let extend_lub = extend_conj

(*
 *
 * [extend_prod a b] computes the extensions to be applied to both components
 * [a] and [b] so that the product of [a] and [b] can be computed.
 *
 * *  I   O   U   N
 *
 * I  UU  UU  UI  UU
 * O  UU  UU  UI  UU
 * U  IU  IU  UU  NU
 * N  UU  UU  UN  UU
 *
 *)

(* val extend_prod : t -> t -> t*t *)

let extend_prod d1 d2 =
  let f =
    function
	(Undefined,Undefined) -> (Undefined,Undefined)
      | (Undefined,Inconsistent) -> (Inconsistent,Undefined)
      | (Undefined,_) -> (Input,Undefined)
      | (Inconsistent,Undefined) -> (Undefined,Inconsistent)
      | (_,Undefined) -> (Undefined,Input)
      | (_,_) -> (Undefined,Undefined)
  in
    f (d1,d2)

(*
 *
 * [extend_quot a b] computes the extensions to be applied to both components
 * [a] and [b] so that the quotient of [a] by [b] can be computed.
 *
 * / b  I   O   U   N
 *
 * a
 *
 * I    UU  UU  UI  UU
 * O    UU  UU  UI  UU
 * U    OU  OU  UU  NU
 * N    UU  UU  UN  UU
 *
 *)

(* val extend_quot : t -> t -> t*t *)

let extend_quot d1 d2 =
  let f =
    function
	(Undefined,Undefined) -> (Undefined,Undefined)
      | (Undefined,Inconsistent) -> (Inconsistent,Undefined)
      | (Inconsistent,Undefined) -> (Undefined,Inconsistent)
      | (Undefined,_) -> (Output,Undefined)
      | (_,Undefined) -> (Undefined,Input)
      | (_,_) -> (Undefined,Undefined)
  in
    f (d1,d2)

(*
 *
 * [extend_wimp a b] computes the extensions to be applied to both components
 * [a] and [b] so that the weak implication of [a] and [b] can be computed.
 *
 * &  I   O   U   N
 *
 * I  UU  UU  UI  UU
 * O  UU  UU  UO  UU
 * U  IU  OU  UU  NU
 * N  UU  UU  UN  UU
 *
 *)

(* val extend_wimp : t -> t -> t*t *)

let extend_wimp = extend_conj

(*
 *
 * [extend_cont g a] computes the extensions to be applied to both components
 * [g] and [a] so that the contract of ([g],[a]) can be computed.
 *
 *   a  I   O   U   N
 *
 * g
 *
 * I    UU  UU  UO  UU
 * O    UU  UU  UI  UU
 * U    OU  IU  UU  NU
 * N    UU  UU  UN  UU
 *
 *)

(* val extend_cont : t -> t -> t*t *)

let extend_cont d1 d2 =
  let f =
    function
	(Undefined,Undefined) -> (Undefined,Undefined)
      | (Undefined,Inconsistent) -> (Inconsistent,Undefined)
      | (Inconsistent,Undefined) -> (Undefined,Inconsistent)
      | (Undefined,Input) -> (Output,Undefined)
      | (Undefined,Output) -> (Input,Undefined)
      | (Input,Undefined) -> (Undefined,Output)
      | (Output,Undefined) -> (Undefined,Input)
      | (_,_) -> (Undefined,Undefined)
  in
    f (d1,d2)

(*
 *
 * [merge a b] computes the disjoint union of [a] and [b]
 *
 * &  I O U N
 *
 * I  N N I N
 * O  N N O N
 * U  I O U N
 * N  N N N N
 *
 *)

(* val merge : t -> t -> t *)

let merge d1 d2 =
  let f =
    function
	(Undefined,x) -> x
      | (x,Undefined) -> x
      | (_,_) -> Inconsistent
  in
    f (d1,d2)

(* [mutation a b] tests whether [a] is a correct mutation of [b] *)

(* (O)utput is the only non-trivial mutation of (I)nput *)

(* val mutation : t -> t -> bool *)

let mutation =
  function
      Output ->
	begin
	  function
	      Input -> true
	    | Output -> true
	    | _ -> false
	end
    | x -> fun y -> x=y

(* checks the consistency of two orientations wrt conjunction *)

(* val consistency : t -> t -> bool *)

let consistency =
  function
      Input ->
	begin
	  function
	    | Input -> true
	    | Undefined -> true
	    | _ -> false
	end
    | Output ->
	begin
	  function
	    | Output -> true
	    | Undefined -> true
	    | _ -> false
	end
    | Undefined ->
	begin
	  function
	      _ -> true
	end
    | Inconsistent ->
	begin
	  function
	      Undefined -> true
	    | Inconsistent -> true
	    | _ -> false
	end

      
