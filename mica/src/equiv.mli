(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Equiv : equivalence relations over interval 0 .. n-1
 *
 * $Id: equiv.mli 474 2011-12-05 12:48:10Z bcaillau $
 *
 *)

type t

(* [clone r] returns a clone of relation [r] *)

val clone : t -> t

(* [size r] returns the size of the support interval *)
(* [index r] returns the index of relation [r] *)
(* [card r i] returns the cardinality of class [i] in [r] *)

val size : t -> int
val index : t -> int
val card : t -> int -> int

(* [mem r i j] tests with pair [i] [j] is in [r] *)

val mem : t -> int -> int -> bool

(* [repr r i] finds the representant of class [i] in [r] *)

val repr : t -> int -> int

(* [make n] computes the equality relation over interval 0 .. [n]-1 *)

val make : int -> t

(* [iter_classes f r] evaluates [f i] for each representant [i] of the classes of [r] *)

val iter_classes : (int -> unit) -> t -> unit

(* [iter_elements f r i] evaluates [f j] for each element [j] of class [i] in [r] *)

val iter_elements : (int -> unit) -> t -> int -> unit

(* [fold_elements f r i x0] evaluates [f j1 (f j2 (f ... (f jn x0)))] where [j1]...[jn] are the elements of class [i] in [r] *)

val fold_elements : (int -> 'a -> 'a) -> t -> int-> 'a -> 'a

(* [print r] prints relation [r] class per class *)

val print : t -> unit

(* [add r i j] adds ([i],[j]) to equivalence relation [r] *)

val add : t -> int -> int -> unit

(*
 *
 * [refine_accel t f r] refines relation [r] by computing the largest equivalence relation contained in [f] intersected with [r].
 * Oracle [t i] is used as a necessary condition to split class [i]. If [t i] is true, then [f] is evaluated first on pairs [i] [j],
 * where [i] is the class representant and [j] is a class element distinct from [i]. If the class needs to be refined, [f j k] is
 * evaluated for all [j]<[k] in the class of [i].
 *
 *)

val refine_accel : (int -> bool) -> (int -> int -> bool) -> t -> t

(*
 *
 * [cautious_refine_accel t f r] refines relation [r] by computing the largest equivalence relation contained in [f] intersected with [r].
 * Oracle [t i] is used as a necessary condition to split class [i]. If [t i] is true, then [f] is evaluated on pairs [j] < [k] of equivalence
 * class [i]. [f] does not need to be transitive, but needs to be reflexive and symmetric.
 *
 *)

val cautious_refine_accel : (int -> bool) -> (int -> int -> bool) -> t -> t

(*
 *
 * [refine f r] refines relation [r] by computing the largest equivalence relation contained in [f] intersected with [r].
 * [f] is evaluated first on pairs [i] [j], where [i] is the class representant and [j] is a class element distinct from [i].
 * If the class needs to be refined, [f j k] is evaluated for [j]<[k] in the class of [i].
 *
 *)

val refine : (int -> int -> bool) -> t -> t

(* [inter r1 r2] computes the intersection of relations [r1] and [r2]. Relations should have identical support sets *)

val inter : t -> t -> t

