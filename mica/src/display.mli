(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Display : display System.t and Mi.t using the dot command (part of graphViz)
 *
 * $Id: display.mli 485 2012-03-08 09:40:02Z bcaillau $
 *
 *)

(* [system_to_doc a n] generates dot code in doc format from system [a]. The graph is named [n] *)

val system_to_doc : System.t -> string -> Pp.doc

(* [system_to_file a n m] generates dot code in file [m] from system [a]. The graph is named [n] *)

val system_to_file : System.t -> string -> string -> unit

(* [system_display a n] displays system [a] using the GraphViz dot command. The graph is named [n] *)

val system_display : System.t -> string -> unit

(* [system_to_pdf a n m] generates a PDF file named [m] from system [a] using the GraphViz dot command. The graph is named [n] *)

val system_to_pdf : System.t -> string -> string -> unit

(* [specification_to_doc a n] generates dot code in doc format from specification [a]. The graph is named [n] *)

val specification_to_doc : Mi.t -> string -> Pp.doc

(* [specification_to_file a n m] generates dot code in file [m] from specification [a]. The graph is named [n] *)

val specification_to_file : Mi.t -> string -> string -> unit

(* [specification_display a n] displays specification [a] using the GraphViz dot command. The graph is named [n] *)

val specification_display : Mi.t -> string -> unit

(* [specification_to_pdf a n m] generates a PDF file named [m] from specification [a] using the GraphViz dot command. The graph is named [n] *)

val specification_to_pdf : Mi.t -> string -> string -> unit

(*
 *
 * [zoom_specification_to_doc a n q r] generates dot code in doc format providing a zoom around state [q],
 * going backward and forward up to radius [r]. Graph is named [n].
 *
 *)

val zoom_specification_to_doc : Mi.t -> string -> Heap.obj -> int -> Pp.doc

(*
 *
 * [zoom_specification_to_file a n m q r] generates dot code in file [m] zooming specification [a] on a
 * disc of radius [r] centered on [q]. The graph is named [n].
 *
 *)

val zoom_specification_to_file : Mi.t -> string -> string -> Heap.obj -> int -> unit

(*
 *
 * [zoom_specification_display a n q r] displays part of specification [a] using the GraphViz dot command.
 * A disc of radius [r] centered on [q] is displayed. The graph is named [n].
 *
 *)

val zoom_specification_display : Mi.t -> string -> Heap.obj -> int -> unit

