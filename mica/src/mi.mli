(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Mi: Modal interfaces
 *
 * $Id: mi.mli 476 2011-12-06 15:01:07Z bcaillau $
 *
 *)

type m = Heap.obj Modal.t

type t

val make : Signature.t -> t

val clone : t -> t

(* retrieves the signature *)

val signature : t -> Signature.t

(* operators on initial states *)

val set_initial : t -> Heap.obj option -> unit

val get_initial : t -> Heap.obj option

(* operators on transitions *)

val set_trans : t -> Heap.obj -> Heap.obj -> m -> unit

val get_trans : t -> Heap.obj -> Heap.obj -> m

(* iterate on modalities *)

val mod_out_iter : (Heap.obj (* event *) -> m (* to state and modality *) -> unit) -> t -> Heap.obj (* state *) -> unit

val ready_in_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit

(* iterate on preimage states *)

val state_in_iter : (Heap.obj (* from-state *) -> unit) -> t -> Heap.obj (* to-state *) -> Heap.obj (* event *) -> unit

(* dfs *)

val dfs :
  (Heap.obj (* state *) -> unit) (* state procedure *) ->
  (Heap.obj (* from-state *) -> Heap.obj (* event *) -> m (* to-state *) -> unit) (* transition procedure *)->
  t (* interface *) -> unit

val dfs_fold :
  (Heap.obj (* state *) -> 'a -> 'a) (* state operator *) ->
  (Heap.obj (* from-state *) -> Heap.obj (* event *) -> m (* to-state *) -> 'a -> 'a) (* transition operator *)->
  t (* interface *) -> 'a (* seed *) -> 'a

(* [reachable_states a] computes the set of reachable states of interface [a] *)

val reachable_states : t -> Enum.t

(* [print a] prints interface [a] *)

val print : t -> unit

(* [reduction a] computes the reduction of [a] *)

val reduction : t -> t

(*
 *
 * [product w a b] computes the product of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

val product : Heap.heap -> t -> t -> t

(* [parallel w a b] computes the optimistic parallel composition of [a] and [b] *)

val parallel : Heap.heap -> t -> t -> t

(*
 *
 * [conjunction w a b] computes the conjunction of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

val conjunction : Heap.heap -> t -> t -> t

(*
 *
 * [quotient w a b] computes the quotient of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

val quotient : Heap.heap -> t -> t -> t

(*
 *
 * [compatible_quotient w a b] computes the compatible quotient of interface [a]
 * by interface [b]. It is the largest [x] such that [x] is compatible with [b]
 * and [product x b] refines [a]. Product states are stored in heap [w].
 *
 *)

val compatible_quotient : Heap.heap -> t -> t -> t

(*
 *
 * [wimply w a b] computes the weak implication of interfaces [a] and [b]
 * Product states are stored in heap [w].
 *
 *)

val wimply : Heap.heap -> t -> t -> t

(*
 *
 * [contract w g a] computes the contract where [g] is the guarantee and
 * [a] is the assumption. Product states are stored in heap [w].
 *
 *)

val contract : Heap.heap -> t -> t -> t

(*
 *
 * [incompatibility_iter f s1 s2 s] iterates on the set pair states [q]=([q1],[q2]) that are
 * reachable in specification [s] and that are incompatible wrt [s1] and [s2]. For every
 * such states pair, [f q1 q2 q e] is evaluated, where [e] is the event that can be sent in
 * one component and not received in the other. States of [s] are assumed to be pairs,
 * that is tuples of length 2 exactly. Exception [Invalid_argument _] is raised otherwise.
 *
 *)

val incompatibility_iter :
  (Heap.obj (* q1 *) -> Heap.obj (* q2 *) -> Heap.obj (* e *) -> Heap.obj (* q *) -> unit) ->
  t (* s1 *) -> t (* s2 *) -> t (* s *) -> unit

(* Satisfaction failure handler type *)

type satisfaction_failure_handler =
    {
      satisfaction_signature : Heap.obj -> Orientation.t -> Orientation.t -> unit;
      satisfaction_inconsistent : unit -> unit;
      satisfaction_not_a_system : unit -> unit;
      satisfaction_modality : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> unit
    }

(*
 *
 * [satisfies h m a] checks whether system [m] satisfies specification [a].
 * Term [h.satisfaction_signature e c d] is evaluated if orientation [c] of event [e]
 * in [m] does not satisfy orientation [d] of [e] in [a]. Term [h.satisfaction_not_a_system ()]
 * is evaluated whenever [a] has no initial state. Term [h.satisfaction_inconsistent ()] is
 * evaluated whenever [a] is inconsistent. Term [h.satisfaction_modality t e m] is evaluated whenever
 * satisfaction fails after trace [t], where event [e] has modality [m].
 *
 *)

val satisfies :
  satisfaction_failure_handler ->
  System.t -> t -> bool

(* Satisfaction failure handlers *)

val satisfaction_handler_print : satisfaction_failure_handler
val satisfaction_handler_quiet : satisfaction_failure_handler

(*
 *
 * [is_consistent a] decides whether specification [a] is consistent
 *
 *)

val is_consistent : t -> bool

(* Consistency handler *)

type side = Left | Right

type consistency_failure_handler =
    {
      consistency_signature : Heap.obj -> Orientation.t -> Orientation.t -> unit;
      consistency_inconsistent : side -> unit;
      consistency_modality : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> Heap.obj Modal.t -> unit
    }

(*
 *
 * [consistency h a1 a2] decides whether interfaces [a1] and [a2] are mutually consistent (the conjunction of [a1]
 * and [a2] is consistent). [h.consistency_signature e o1 o2] is evaluated whenever a signature inconsitency is
 * found on event [e] with orientation [o1] in [a1] and [o2] in [a2]. [h.consistency_modality t e m1 m2] is
 * evaluated whenever a modal inconsistency is found: after trace [t], event [e] has modality [m1] in [a1] and [m2]
 * in [a2]. [h.consistency_inconsistent s] is evaluated whenever [a1] or [a2] is inconsistent.
 *
 *)

val consistency : consistency_failure_handler -> t -> t -> bool

(* Consistency handlers *)

val consistency_handler_print : consistency_failure_handler
val consistency_handler_quiet : consistency_failure_handler

(* Refinement failure handler *)

type refinement_failure_handler =
    {
      refinement_signature : Heap.obj -> Orientation.t -> Orientation.t -> unit;
      refinement_inconsistent : unit -> unit;
      refinement_modality : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> Heap.obj Modal.t -> unit
    }

val refines_handler_print : refinement_failure_handler
val refines_handler_quiet : refinement_failure_handler

(*
 *
 * [refines h a b] checks whether specification [a] refines specification [b].
 * Term [h.refinement_modality t e m n] is evaluated whenever refinement fails after trace [t] ([q],[r])
 * on event [e], because left-hand modality [m] does not refine right-hand modality [n].
 * Term [h.refinement_signature e o1 o2] is evaluated whenever orientation [o1] of event [e] does not
 * refine orientation [o2]. [h.refinement_inconsistent ()] is evaluated whenever [a] is consistent and [b] is
 * inconsistent.
 *
 *)

val refines : refinement_failure_handler -> t -> t -> bool

(* Completeness check handlers *)

type completeness_handler =
    {
      completeness_inconsistent : unit -> unit;
      completeness_orientation : Heap.obj (* event *) -> Orientation.t -> unit;
      completeness_modality :
	Heap.obj list (* trace *) -> Heap.obj (* state *) -> Heap.obj (*event *) -> Heap.obj Modal.t -> unit
    }

val completeness_handler_print : completeness_handler
val completeness_handler_quiet : completeness_handler

(*
 *
 * [is_complete h s] checks whether specification [s] is complete, meaning that:
 * (i) it has no output or inconsistent event and (ii) it has no cannot or inconsistent
 * transition. Term [h.completeness_inconsistent ()] is evaluated if [s] is inconsistent.
 * Term [h.completeness_orientation e d] is evaluated if event [e] has orientation [d]
 * different from [Input]. Term [h.completeness_modality q e m] is evaluated if in state
 * [q] event [e] has modality [m] equal to [Cannot] or [Inconsistent].
 *
 *)

val is_complete : completeness_handler -> t -> bool

(* Triviality check handler *)

type triviality_handler =
    {
      triviality_inconsistent : unit -> unit;
      triviality_orientation : Heap.obj (* event *) -> Orientation.t -> unit;
      triviality_modality : Heap.obj (* state *) -> Heap.obj (*event *) -> Heap.obj Modal.t -> unit
    }

val triviality_handler_print : triviality_handler
val triviality_handler_quiet : triviality_handler

(*
 *
 * [is_trivial h s] checks whether specification [s] is trivial, meaning that:
 * (i) it has no inconsistent event and (ii) it has only may transitions. Term
 * [h.triviality_inconsistent ()] is evaluated if [s] is inconsistent.
 * Term [h.triviality_orientation e d] is evaluated if event [e] has orientation
 * [Inconsistent]. Term [h.triviality_modality q e m] is evaluated if in state
 * [q] event [e] has modality [m] different from [May _].
 *
 *)

val is_trivial : triviality_handler -> t -> bool

(* Factor check handler *)

type factor_handler =
    {
      factor_inconsistent : unit -> unit;
      factor_orientation : Heap.obj (* event *) -> Orientation.t -> unit;
      factor_modality : Heap.obj list (* trace *) -> Heap.obj (*event *) -> Heap.obj Modal.t -> unit
    }

val factor_handler_print : factor_handler
val factor_handler_quiet : factor_handler

(*
 *
 * [is_factor h s1 s2] checks whether [s1] is a factor of [s2], meaning that:
 * 
 * (i) Every output of [s1] is either undefined in [s2] or is an output of [s2].
 * Function [h.factor_orientation] is called whenever orientation is not correct.
 *
 * (ii) Right-hand interface is inconsistent implies left-hand interface is also
 * inconsistent. Function [h.factor_inconsistent] is called whenever consistency
 * is not correct.
 *
 * (iii) After every must trace [t] in [s2], event [e] has modality must in [s2]
 * implies event [e] has modality must in [s1]. Function [h.factor_modality] is
 * called whenever modality is not correct.
 *
 *)

val is_factor : factor_handler -> t -> t -> bool

(* print function to be used with [refines]. *)

val print_refines_failure : Heap.obj list -> Heap.obj -> Heap.obj Modal.t -> Heap.obj Modal.t -> unit

(* [of_systen m] maps deterministic system [m] to a rigid specification of same signature *)
(* Raises [Invalid_argument _] if [m] is not deterministic *)

val of_system : System.t -> t

(* Minimal and maximal implementations. Raises [Invalid_argument _] if specification is inconsistent *)

val min_implementation : t -> System.t

val max_implementation : t -> System.t

(* [mutate s l] mutates interface [s] according to [l] *)

val mutate : t -> (Heap.obj * Orientation.t) list -> t

(* [minimize s] minimizes modal interface [s] *)

val minimize : t -> t

(* [simplify w s] simplifies the state structure of interface [s] by mapping state labels to integers *)

val simplify : Heap.heap -> t -> t

(*
 *
 * [abstract h s a] computes a projection of interface [a] on sub-alphabet [s]. The resulting interface is the
 * least abstraction of [a] in the class of interfaces over signature [Signature.restrict (signature a) s].
 *
 *)

val abstract : Heap.heap -> Enum.t -> t -> t

(*
 *
 * [project h s a] computes a projection of interface [a] signature [s]. The resulting interface is the
 * largest projection of [a] that is receptive to all its inputs and that controls all its outputs.
 *
 *)

val project : Heap.heap -> Signature.t -> t -> t

(*
 *
 * [facet h s a] computes an abstraction [b] of a reduced interface [a], such that:
 *   - [b] is on the same signature as [a] 
 *   - the state structure of [b] is isomorphic to the state structure of [a], except that a universal (top) state has been added to [b]
 *   - every transition of [a] labeled by an event in [s] is preserved in [b], with the same modality
 *   - every "may" or "must" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition of [b]
 *   - every "cannot" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition to the top state
 *
 *)

val facet : Heap.heap -> Enum.t -> t -> t

(* [print_statistics s] prints statistics about interface [s] *)

val print_statistics : t -> unit

