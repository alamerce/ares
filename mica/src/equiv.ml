(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Equiv :  equivalence relations over interval 0 .. n-1
 *
 * $Id: equiv.ml 474 2011-12-05 12:48:10Z bcaillau $
 *
 *)

type t =
    {
      size : int; (* cardinal of the support interval *)
      mutable index : int; (* number of classes *)
      card : int array; (* cardinal of each class *)
      prec : int array; (* maps representants to representant of the precedant class and -1 if first class *)
      next : int array; (* maps representants to representant of the next class and -1 if last class *)
      repr : int array; (* maps elements to representants *)
      succ : int array; (* maps elements to successor element in class and -1 if last class element *)
    }

(* [size r] returns the size of the support interval *)
(* [index r] returns the index of relation [r] *)
(* [card r i] returns the cardinality of class [i] in [r] *)

(* val size : t -> int *)
(* val index : t -> int *)
(* val card : t -> int -> int *)

let size r = r.size

let index r = r.index

let card r i = if (i >= 0) && (i < r.size) then r.card.(i) else invalid_arg "Equiv.card: not an element"

(* [mem r i j] tests whether pair [i] [j] is in [r] *)

(* val mem : t -> int -> int -> bool *)

let mem r i j =
  if (i >= 0) && (i < r.size) && (j >= 0) && (j < r.size)
  then r.repr.(i) = r.repr.(j)
  else invalid_arg "Equiv.mem: not an element"

(* [repr r i] finds the representant of class [i] in [r] *)

(* val repr : t -> int -> int *)

let repr r i =
  if (i >= 0) && (i < r.size)
  then r.repr.(i)
  else invalid_arg "Equiv.repr: not an element"

(* [clone r] returns a clone of relation [r] *)

(* val clone : t -> t *)

let clone r =
  {
    size = r.size;
    index = r.index;
    card = Array.copy r.card;
    prec = Array.copy r.prec;
    next = Array.copy r.next;
    repr = Array.copy r.repr;
    succ = Array.copy r.succ
  }

(* [make n] computes the equality relation over interval 0 .. [n]-1 *)

(* val make : int -> t *)

let make n =
  if n < 0
  then
    invalid_arg "Equiv.make_equality: negative size"
  else
    let r =
      {
	size = n;
	index = n;
	card = Array.make n 1;
	prec = Array.make n (-1);
	next = Array.make n (-1);
	repr = Array.make n (-1);
	succ = Array.make n (-1)
      }
    in
      begin
	for i=1 to n-1 do
	  r.prec.(i) <- i-1
	done;
	for i=0 to n-2 do
	  r.next.(i) <- i+1
	done;
	for i=0 to n-1 do
	  r.repr.(i) <- i
	done;
	r
      end

(* [iter_classes f r] evaluates [f i] for each representant [i] of the classes of [r] *)

(* val iter_classes : (int -> unit) -> t -> unit *)

let iter_classes f r =
  if r.size > 0
  then
    let i = ref 0 in
      while (!i) >= 0 do
	f (!i);
	i := r.next.(!i)
      done

(* [iter_elements f r i] evaluates [f j] for each element [j] of class [i] in [r] *)

(* val iter_elements : (int -> unit) -> t -> int -> unit *)

let iter_elements f r i =
  let j = ref i in
    while (!j) >= 0 do
      f (!j);
      j := r.succ.(!j)
    done

(* [fold_elements f r i x0] evaluates [f j1 (f j2 (f ... (f jn x0)))] where [j1]...[jn] are the elements of class [i] in [r] *)

(* val fold_elements : (int -> 'a -> 'a) -> t -> int-> 'a -> 'a *)

let fold_elements f r i x0 =
  let x = ref x0 in
    begin
      iter_elements
	(fun j -> x := f j (!x))
	r i;
      !x
    end

(* [iter2_elements f r i] evaluates [f j k] for each pair of elements [j]  [k] of class [i] in [r] with [j] < [k] *)

(* val iter2_elements : (int -> int -> unit) -> t -> int -> unit *)

let iter2_elements f r i =
  let j = ref i in
    while (!j) >= 0 do
      let k = ref r.succ.(!j) in
	while (!k) >= 0 do
	  f (!j) (!k);
	  k := r.succ.(!k)
	done;
	j := r.succ.(!j)
    done

(* [print r] prints relation [r] class per class *)

(* val print : t -> unit *)

let print r =
  begin
    print_string "{ ";
    iter_classes
      (fun i ->
	 begin
	   print_string "{ ";
	   iter_elements
	     (fun j ->
		begin
		  print_string (string_of_int j);
		  print_string " "
		end)
	     r
	     i;
	   print_string "} "
	 end)
      r;
    print_endline "}"
  end

(* [assign r i j] merges class [i] into [j]. It is assumed that [j] < [i] and that [i] and [j] are class representants *)

let assign r i j =
  begin
    r.index <- r.index-1; (* one class less *)
    r.card.(j) <- r.card.(j) + r.card.(i); (* add cardinalities *)
    r.card.(i) <- 0; (* i is no longer a class representative *)
    (* remove class i in the linked list of classes *)
    (if r.prec.(i) >= 0 then r.next.(r.prec.(i)) <- r.next.(i));
    (if r.next.(i) >= 0 then r.prec.(r.next.(i)) <- r.prec.(i));
    (* assign every element of class i to class j *)
    (let k = ref i in
       while (!k) >= 0 do
	 r.repr.(!k) <- j;
	 k := r.succ.(!k)
       done);
    (* merge sort classes i and j *)
    (let k = ref i (* max *)
     and l = ref j  (* min *) in
       begin
	 while ((!k) >= 0) && ((!l) >= 0) do
	   let m = r.succ.(!l) in
	     (* should we skip or insert? *)
	     if (m >= 0) && (m < (!k)) 
	     then (* skip *)
	       l := m
	     else (* insert *)
	       begin
		 r.succ.(!l) <- !k;
		 l := !k;
		 k := m
	       end
	 done;
       end)
  end

(* [add r i j] adds ([i],[j]) to equivalence relation [r] *)

(* val add : t -> int -> int -> unit *)

let add r i j =
  if (i >= 0) && (i < r.size) && (j >= 0) && (j < r.size)
  then
    let ci = r.repr.(i)
    and cj = r.repr.(j) in
      if ci < cj
      then
	assign r cj ci
      else
	if cj < ci
	then
	  assign r ci cj
	else ()
  else
    invalid_arg "Equiv.add: not an element"

(*
 *
 * [copy_class r r' i] copies class [i] in [r] to [r']. No sanity check is made. It is assumed that all classes of [r']
 * with a non empty intersection with class [i] in [r] should be a subset of it.
 *
 *)

let copy_class r r' i = iter_elements (fun j -> if j > i then add r' i j) r i

(*
 *
 * [refine_accel t f r] refines relation [r] by computing the largest equivalence relation contained in [f] intersected with [r].
 * Oracle [t i] is used as a necessary condition to split class [i]. If [t i] is true, then [f] is evaluated first on pairs [i] [j],
 * where [i] is the class representant and [j] is a class element distinct from [i]. If the class needs to be refined, [f j k] is
 * evaluated for all [j]<[k] in the class of [i]. [f] must be symmetric, reflexive and transitive.
 *
 *)

(* val refine_accel : (int -> bool) -> (int -> int -> bool) -> t -> t *)

let refine_accel t f r =
  let r' = make (size r) in
    begin
      iter_classes
	(fun i ->
	   if t i
	   then (* may split: equivalence class is tested *)
	     let split = ref false in
	       begin
		 iter_elements
		   (fun j ->
		      if j > i then
			if f i j
			then add r' i j
			else split := true)
		   r
		   i;
		 if !split
		 then
		   iter2_elements
		     (fun j k ->
			if (not (mem r' j k)) && (f j k)
			then add r' j k)
		     r
		     i
	       end
	   else (* no split: equivalence class is copied *)
	     copy_class r r' i)
	r;
      r'
    end

(*
 *
 * [cautious_refine_accel t f r] refines relation [r] by computing the largest equivalence relation contained in [f] intersected with [r].
 * Oracle [t i] is used as a necessary condition to split class [i]. If [t i] is true, then [f] is evaluated on pairs [j] < [k] of equivalence
 * class [i]. [f] does not need to be transitive, but needs to be reflexive and symmetric.
 *
 *)

(* val cautious_refine_accel : (int -> bool) -> (int -> int -> bool) -> t -> t *)

let cautious_refine_accel t f r =
  let r' = make (size r) in
    begin
      iter_classes
	(fun i ->
	   if t i
	   then (* may split: equivalence class is tested *)
	     iter2_elements
	       (fun j k ->
		 if (not (mem r' j k)) && (f j k)
		 then add r' j k)
	       r
	       i)
	r;
      r'
    end

(* [refine f r] refines relation [r] by computing the largest equivalence relation contained in [f] intersected with [r]. *)
(* [f] is evaluated first on pairs [i] [j], where [i] is the class representant and [j] is a class element distinct from [i]. *)
(* If the class needs to be refined, [f j k] is evaluated for all [j]<[k] in the class of [i]. *)

(* val refine : (int -> int -> bool) -> t -> t *)

let refine = refine_accel (fun _ -> true)

(* [inter r1 r2] computes the intersection of relations [r1] and [r2]. Relations should have identical support sets *)

(* val inter : t -> t -> t *)

let inter r1 r2 =
  if (size r1) = (size r2)
  then
    if (index r1) >= (index r2)
    then
      refine
	(mem r2)
	r1
    else
      refine
	(mem r1)
	r2
  else
    invalid_arg "Equiv.inter: sizes differ"
