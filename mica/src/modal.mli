(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Modal: algebra of modalities
 *
 * $Id: modal.mli 428 2011-11-03 08:52:11Z bcaillau $
 *
 *)

(*
 *
 * May : top element
 * Must, Cannot : incomparable
 * Inconsistent : bottom element
 *
 *)

type 'a t =
    Cannot
  | May of 'a
  | Must of 'a
  | Inconsistent

(* composition operator handler *)

type ('a,'b,'c) h =
    {
      compose : 'a -> 'b -> 'c; (* state composition function *)
      left : ('a -> 'c) option; (* optional left state mapping *)
      right : ('b -> 'c) option; (* optional right state mapping *)
      distinguished : 'c option (* optional distinguished state *)
    }

(* type of composition operators *)

type ('a,'b,'c) op = ('a,'b,'c) h (* composition handler *) -> 'a t -> 'b t -> 'c t

(* refinement preorder *)

val leq : 'a t -> 'b t -> bool

(* composition operator *)

val product : ('a,'b,'c) op

val conjunction : ('a,'b,'c) op

val quotient : ('a,'b,'c) op

(* Least upper bound *)

val lub : ('a,'b,'c) op

(*
 *
 * the projection operator is the max wrt. the following total order:
 *
 *   cannot < may < must < inconsistent
 *
 *)

val project : ('a,'b,'c) op

(* Weak implication. See Goessler and Raclet. Modal Contracts for Component-based Design. SEFM'09 *)

val wimply : ('a,'b,'c) op 

(*
 *
 * [contract g a] computes the contract where [g] is the guaranteed modality and [a]
 * is the assumed modality
 *
 *)

val contract : ('a,'b,'c) op
