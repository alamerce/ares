(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Modal : algebra of modalities
 *
 * $Id: modal.ml 428 2011-11-03 08:52:11Z bcaillau $
 *
 *)

type 'a t =
    Cannot
  | May of 'a
  | Must of 'a
  | Inconsistent

(* composition operator handle *)

type ('a,'b,'c) h =
    {
      compose : 'a -> 'b -> 'c; (* state composition function *)
      left : ('a -> 'c) option; (* optional left state mapping *)
      right : ('b -> 'c) option; (* optional right state mapping *)
      distinguished : 'c option (* optional distinguished state *)
    }

let get_distinguished =
  function
      { distinguished = Some q } -> q
    | _ -> invalid_arg "Modal.get_distinguished: distinguished state is undefined"

let get_left =
  function
      { left = Some f } -> f
    | _ -> invalid_arg "Modal.get_left: left state mapping function is undefined"

let get_right =
  function
      { right = Some f } -> f
    | _ -> invalid_arg "Modal.get_right: right state mapping function is undefined"

(* type of composition operators *)

type ('a,'b,'c) op = ('a,'b,'c) h (* composition  handle *) -> 'a t -> 'b t -> 'c t

(* val leq : 'a t -> 'b t -> bool *)

let leq =
  function
      Cannot ->
	begin
	  function
	      Cannot -> true
	    | May _ -> true
	    | Must _ -> false
	    | Inconsistent -> false
	end
    | May _ ->
	begin
	  function
	      Cannot -> false
	    | May _ -> true
	    | Must _ -> false
	    | Inconsistent -> false
	end
    | Must _ ->
	begin
	  function
	      Cannot -> false
	    | May _ -> true
	    | Must _ -> true
	    | Inconsistent -> false
	end
    | Inconsistent -> (fun _ -> true)

(* val product : ('a,'b,'c) op *)

let product h =
  function
      Cannot ->
	begin
	  function
	      Cannot -> Cannot
	    | May _ -> Cannot
	    | Must _ -> Cannot
	    | Inconsistent -> Inconsistent
	end
    | May q1 ->
	begin
	  function
	      Cannot -> Cannot
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> May (h.compose q1 q2)
	    | Inconsistent -> Inconsistent
	end
    | Must q1 ->
	begin
	  function
	      Cannot -> Cannot
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> Inconsistent
	end
    | Inconsistent ->
	begin
	  function
	      Cannot -> Inconsistent
	    | May _ -> Inconsistent
	    | Must _ -> Inconsistent
	    | Inconsistent -> Inconsistent
	end

(* val conjunction : ('a,'b,'c) op *)

let conjunction h =
  function
      Cannot ->
	begin
	  function
	      Cannot -> Cannot
	    | May _ -> Cannot
	    | Must _ -> Inconsistent
	    | Inconsistent -> Inconsistent
	end
    | May q1 ->
	begin
	  function
	      Cannot -> Cannot
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> Inconsistent
	end
    | Must q1 ->
	begin
	  function
	      Cannot -> Inconsistent
	    | May q2 -> Must (h.compose q1 q2)
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> Inconsistent
	end
    | Inconsistent ->
	begin
	  function
	      Cannot -> Inconsistent
	    | May _ -> Inconsistent
	    | Must _ -> Inconsistent
	    | Inconsistent -> Inconsistent
	end

(* val quotient : ('a,'b,'c) op *)

let quotient h =
  function
      Cannot ->
	begin
	  function
	      Cannot -> May (get_distinguished h)
	    | May _ -> Cannot
	    | Must _ -> Cannot
	    | Inconsistent -> May (get_distinguished h)
	end
    | May q1 ->
	begin
	  function
	      Cannot -> May (get_distinguished h)
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> May (h.compose q1 q2)
	    | Inconsistent -> May (get_distinguished h)
	end
    | Must q1 ->
	begin
	  function
	      Cannot -> Inconsistent
	    | May _ -> Inconsistent
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> May (get_distinguished h)
	end
    | Inconsistent ->
	begin
	  function
	      Cannot -> Inconsistent
	    | May _ -> Inconsistent
	    | Must _ -> Inconsistent
	    | Inconsistent -> May (get_distinguished h)
	end

(*
 *
 * [contract g a] computes the contract where [g] is the guaranteed modality and [a]
 * is the assumed modality
 *
 *)

(* val contract : ('a,'b,'c) op *)

let contract h =
  function
      Cannot ->
	begin
	  function
	      Cannot -> May (get_distinguished h)
	    | May q2 -> Cannot
	    | Must q2 -> Cannot
	    | Inconsistent -> May (get_distinguished h)
	end
    | May q1 ->
	begin
	  function
	      Cannot -> May (get_distinguished h)
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> May (h.compose q1 q2)
	    | Inconsistent -> May (get_distinguished h)
	end
    | Must q1 ->
	begin
	  function
	      Cannot -> May (get_distinguished h)
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> May (get_distinguished h)
	end
    | Inconsistent ->
	begin
	  function
	      Cannot -> Inconsistent
	    | May q2 -> Inconsistent
	    | Must q2 -> Inconsistent
	    | Inconsistent -> May (get_distinguished h)
	end

(* Least upper bound *)

(* val lub : ('a,'b,'c) op *)

let lub h =
  function
      Cannot ->
	begin
	  function
	      Cannot -> Cannot
	    | May q2 -> May ((get_right h) q2)
	    | Must q2 -> May ((get_right h) q2)
	    | Inconsistent -> Cannot
	end
    | May q1 ->
	begin
	  function
	      Cannot -> May ((get_left h) q1)
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> May (h.compose q1 q2)
	    | Inconsistent -> May ((get_left h) q1)
	end
    | Must q1 ->
	begin
	  function
	      Cannot -> May ((get_left h) q1)
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> Must ((get_left h) q1)
	end
    | Inconsistent ->
	begin
	  function
	      Cannot -> Cannot
	    | May q2 -> May ((get_right h) q2)
	    | Must q2 -> Must ((get_right h) q2)
	    | Inconsistent -> Inconsistent
	end

(* Weak implication. See Goessler and Raclet. Modal Contracts for Component-based Design. SEFM'09 *)
(* Details in file test_adjoint.ml *)

(* val wimply : ('a,'b,'c) op *)

let wimply h =
  function
      Cannot ->
	begin
	  function
	      Cannot -> May (get_distinguished h)
	    | May q2 -> Cannot
	    | Must q2 -> Cannot
	    | Inconsistent -> May (get_distinguished h)
	end
    | May q1 ->
	begin
	  function
	      Cannot -> May (get_distinguished h)
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> May (h.compose q1 q2)
	    | Inconsistent -> May (get_distinguished h)
	end
    | Must q1 ->
	begin
	  function
	      Cannot -> Must (get_distinguished h)
	    | May q2 -> Must (h.compose q1 q2)
	    | Must q2 -> May (h.compose q1 q2)
	    | Inconsistent -> May (get_distinguished h)
	end
    | Inconsistent ->
	begin
	  function
	      Cannot -> Must (get_distinguished h)
	    | May q2 -> Inconsistent
	    | Must q2 -> Cannot
	    | Inconsistent -> May (get_distinguished h)
	end

(*
 *
 * Projection operator is the max wrt. the following total order:
 *
 *   cannot < may < must < inconsistent
 *
 *)

(* val project : ('a,'b,'c) op *)

let project h =
  function
      Cannot ->
	begin
	  function
	      Cannot -> Cannot
	    | May q2 -> May ((get_right h) q2)
	    | Must q2 -> Must ((get_right  h) q2)
	    | Inconsistent -> Inconsistent
	end
    | May q1 ->
	begin
	  function
	      Cannot -> May ((get_left h) q1)
	    | May q2 -> May (h.compose q1 q2)
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> Inconsistent
	end
    | Must q1 ->
	begin
	  function
	      Cannot -> Must ((get_left h) q1)
	    | May q2 -> Must (h.compose q1 q2)
	    | Must q2 -> Must (h.compose q1 q2)
	    | Inconsistent -> Inconsistent
	end
    | Inconsistent ->
	begin
	  function
	      Cannot -> Inconsistent
	    | May _ -> Inconsistent
	    | Must _ -> Inconsistent
	    | Inconsistent -> Inconsistent
	end

