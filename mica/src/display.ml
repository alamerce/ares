(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Display: display System.t and Mi.t using the dot command (part of graphViz)
 *
 * $Id: display.ml 485 2012-03-08 09:40:02Z bcaillau $
 *
 *)

open Pp

(* Color constants *)

let sienna = "\"#a0522d\""
and bisque = "\"#ffe4c4\""
and firebrick = "\"#92000e\""
and darkmagenta = "\"#8b008b\""
and navy = "\"#000080\""

let global_attr =
  (text "rankdir=LR;") ^^ break ^^ (text "size=\"10.5,6.5\";") ^^ break ^^
    (text "ratio=\"auto\";") ^^ break ^^ (text "center=true;") ^^ break ^^
    (text "margin=\"0.0\";") ^^ break ^^
    (text ("node [style=\"rounded,filled\", shape=box, color="^sienna^", fillcolor="^bisque^"];"))

let init_attr = (text "shape=diamond")

let code b q = Mapping.read b.Enumerate.to_int q

let decode b i = b.Enumerate.of_int.(i)

let node b q =
  let i = code b q in
    "q"^(string_of_int i)

let label e = String.escaped (Heap.to_string e)

let orient s e =
  match Signature.get s e with
      Orientation.Input -> "?"
    | Orientation.Output -> "!"
    | Orientation.Undefined -> "*"
    | Orientation.Inconsistent -> "#"

let rev r x y = r y x

(* aspect ratio *)

let ratio = 8.0

(* exception to break out of a loop *)

exception Break

(* Format a list of labels into a square text with balanced lines *)

let format l =
  let l' = List.sort (rev Stringutil.silex) l in (* sort the list in reverse size-lexicographic order *)
  let m = List.fold_right (fun s x -> x+(String.length s)+1) l' 0 in (* compute total length *)
  let opt = sqrt ((float m) /. ratio) in (* compute optimal height *)
  let width = ref (truncate (ceil (opt *. ratio)))
  and height = truncate (ceil opt) in
  let line = Array.make height "" (* lines *)
  and len = Array.make height 0 (* length of each line *)
  and suitable = ref false in
    begin
      while not (!suitable) do
	try
	  (* try allocate labels to lines, using the taylor's algorithm *)
	  List.iter
	    (fun s ->
	       let k = String.length s
	       and i = ref 0 in
		 begin
		   while ((!i) < height) && (len.(!i) > 0) && ((len.(!i) + k + 1) > (!width)) do
		     i := (!i)+1
		   done;
		   if (!i) < height
		   then
		     if len.(!i) <= 0
		     then
		       begin
			 len.(!i) <- k;
			 line.(!i) <- s
		       end
		     else
		       begin
			 len.(!i) <- len.(!i) + k + 1;
			 line.(!i) <- s ^ "," ^ line.(!i)
		       end
		   else
		     raise Break
		 end)
	    l';
	  (* Allocation is a success: exit loop *)
	  suitable := true
	with
	    (* Allocation failed: increase width and resets arrays *)
	    Break ->
	      begin
		width := (!width)+1;
		for j=0 to height-1 do
		  len.(j) <- 0;
		  line.(j) <- ""
		done
	      end
      done;
      (* concatenate lines, skipping empty lines *)
      Array.fold_right
	(fun h r ->
	   if (String.length r) <= 0
	   then h
	   else
	     if (String.length h) <= 0
	     then r
	     else h ^ "\\n" ^ r)
	line
	""
    end
      
(* apply function to collated binary transitions *)

let fold_left_collated_binary_trans f a x0 =
  let x = ref x0
  and n = Array.length a in
    begin
      for i=0 to n-1 do
	for j=0 to n-1 do
	  if a.(i).(j) <> []
	  then x := f (!x) i j a.(i).(j)
	done
      done;
      !x
    end

(* apply function to collated unary transitions *)

let fold_left_collated_unary_trans f a x0 =
  let x = ref x0
  and n = Array.length a in
    begin
      for i=0 to n-1 do
	if a.(i) <> []
	then x := f (!x) i a.(i)
      done;
      !x
    end

(* [system_to_doc a n] generates dot code in doc format from system [a]. The graph is named [n] *)

let system_to_doc a n =
  let s = System.signature a
  and b = Enumerate.enumerate (System.reachable_states a) in
  let k = b.Enumerate.card in
  let trs = Array.make_matrix k k [] in
    begin
      (* Collate transitions *)
      System.dfs_iter
	(fun _ -> ())
	(fun q e r ->
	   let i = code b q
	   and j = code b r in
	     trs.(i).(j) <- ((orient s e) ^ (label e)) :: trs.(i).(j))
	a;
      vgrp
	((hgrp ((text "digraph") ^^ break ^^ (text n) ^^ break ^^ (text "{"))) ^^ break ^^
	   (nest 2 (fgrp (
		      global_attr ^^ break ^^
			(* iterates on states *)
			(System.dfs_fold
			   (fun q d ->
			      d ^^ (hgrp
				      ((text (node b q)) ^^ break ^^
					 (text "[") ^^
					 (text ("label=\"" ^ (label q) ^ "\"")) ^^
					 (if System.get_initial a q
					  then (text ",") ^^ break ^^ init_attr
					  else empty) ^^
					 (text "]") ^^ (text ";"))) ^^ break)
			   (fun _ _ _ d -> d)
			   a
			   empty) ^^
			(* iterates on transitions *)
			(fold_left_collated_binary_trans
			   (fun d i i' l ->
			      let s = format l
			      and q = decode b i
			      and q' = decode b i' in
				d ^^
				  (hgrp
				     ((text (node b q)) ^^ break ^^
					(text "->") ^^ break ^^
					(text (node b q')) ^^ break ^^
					(text "[") ^^
					(text ("label=\"" ^ s ^ "\"")) ^^
					(text "]") ^^ (text ";"))) ^^ break)
			   trs
			   empty)))) ^^ break ^^ (text "}"))
    end
(* [system_to_file a n m] generates dot code in file [m] from system [a]. The graph is named [n] *)

(* val system_to_file : System.t -> string -> string -> unit *)

let system_to_file a n m = Ppwrap.to_file (system_to_doc a) m n

(* [system_display a n] displays system [a] using the GraphViz dot command. The graph is named [n] *)

(* val system_display : System.t -> string -> unit *)

let system_display a n = Ppwrap.display_dot (system_to_doc a) n

(* [system_to_pdf a n m] generates a PDF file named [m] from system [a] using the GraphViz dot command. The graph is named [n] *)

(* val system_to_pdf : System.t -> string -> string -> unit *)

let system_to_pdf a n m = Ppwrap.dot_to_pdf (system_to_doc a) m n

(* [is_initial a q] tests if [q] is initial in [a] *)

let is_initial a q =
  match Mi.get_initial a with
      None -> false
    | Some q' -> q == q'

(*
 *
 * [filter_specification_to_doc f a n] generates dot code in doc format for the subgraph defined by [f] of  specification [a].
 * The graph is named [n]
 *
 *)

let filter_specification_to_doc f a n =
  let s = Mi.signature a
  and b = Enumerate.enumerate (Mi.reachable_states a) in
  let k = b.Enumerate.card in
  let must = Array.make_matrix k k []
  and may = Array.make_matrix k k []
  and cannot = Array.make k []
  and inconsistent = Array.make k [] in
    begin
      (* Collate transitions *)
      Mi.dfs
	(fun _ -> ())
	(fun q e ->
	   if f q
	   then
	     function
		 Modal.Must r ->
		   if f r
		   then
		     let i = code b q
		     and j = code b r in
		       must.(i).(j) <- ((orient s e) ^ (label e)) :: must.(i).(j)
	       | Modal.May r ->
		   if f r
		   then
		     let i = code b q
		     and j = code b r in
		       may.(i).(j) <- ((orient s e) ^ (label e)) :: may.(i).(j)
	       | Modal.Cannot ->
		   let i = code b q in
		     cannot.(i) <- ((orient s e) ^ (label e)) :: cannot.(i)
	       | Modal.Inconsistent ->
		   let i = code b q in
		     inconsistent.(i) <- ((orient s e) ^ (label e)) :: inconsistent.(i)
	   else
	     fun _ -> ())
	a;
      (* generate Dot syntax *)
      vgrp
	((hgrp ((text "digraph") ^^ break ^^ (text n) ^^ break ^^ (text "{"))) ^^ break ^^
	   (nest 2 (fgrp (
		      global_attr ^^ break ^^
			(* iterates on states *)
			(Mi.dfs_fold
			   (fun q d ->
			      if f q
			      then
				d ^^ (hgrp
					((text (node b q)) ^^ break ^^
					   (text "[") ^^
					   (text ("label=\"" ^ (label q) ^ "\"")) ^^
					   (if is_initial a q
					    then (text ",") ^^ break ^^ init_attr
					    else empty) ^^
					   (text "]") ^^ (text ";"))) ^^ break
			      else d)
			   (fun _ _ _ d -> d)
			   a
			   empty) ^^
			(* iterate on inconsistent transitions *)
			(fold_left_collated_unary_trans
			   (fun d i l ->
			      let s = format (List.map (fun t -> "!!"^t) l)
			      and q = decode b i in
				d ^^
				  (hgrp
				     ((text (node b q)) ^^ break ^^
					(text "->") ^^ break ^^
					(text (node b q)) ^^ break ^^
					(text "[") ^^
					(text "style=bold,") ^^ break ^^
					(text "dir=both,") ^^ break ^^
					(text ("color="^firebrick^",")) ^^ break ^^
					(text ("fontcolor="^firebrick^",")) ^^ break ^^
					(text ("label=\"" ^ s ^ "\"")) ^^
					(text "]") ^^ (text ";"))) ^^ break)
			   inconsistent
			   empty) ^^
			(* iterate on must transitions *)
			(fold_left_collated_binary_trans
			   (fun d i i' l ->
			      let s = format l
			      and q = decode b i
			      and q' = decode b i' in
				d ^^
				  (hgrp
				     ((text (node b q)) ^^ break ^^
					(text "->") ^^ break ^^
					(text (node b q')) ^^ break ^^
					(text "[") ^^
					(text ("color="^darkmagenta^",")) ^^ break ^^
					(text ("fontcolor="^darkmagenta^",")) ^^ break ^^
					(text ("label=\"" ^ s ^ "\"")) ^^
					(text "]") ^^ (text ";"))) ^^ break)
			   must
			   empty) ^^
			(* iterate on may transitions *)
			(fold_left_collated_binary_trans
			   (fun d i i' l ->
			      let s = format l
			      and q = decode b i
			      and q' = decode b i' in
				d ^^
				  (hgrp
				     ((text (node b q)) ^^ break ^^
					(text "->") ^^ break ^^
					(text (node b q')) ^^ break ^^
					(text "[") ^^
					(text ("color="^navy^",")) ^^ break ^^
					(text ("fontcolor="^navy^",")) ^^ break ^^
					(text "style=dashed,") ^^ break ^^
					(text ("label=\"" ^ s ^ "\"")) ^^
					(text "]") ^^ (text ";"))) ^^ break)
			   may
			   empty)))) ^^ break ^^ (text "}"))
 end
      
(* [specification_to_doc a n] generates dot code in doc format from specification [a]. The graph is named [n] *)

let specification_to_doc = filter_specification_to_doc (fun _ -> true)

(* [specification_to_file a n m] generates dot code in file [m] from specification [a]. The graph is named [n] *)

(* val specification_to_file : Mi.t -> string -> string -> unit *)

let specification_to_file a n m = Ppwrap.to_file (specification_to_doc a) m n

(* [specification_display a n] displays specification [a] using the GraphViz dot command. The graph is named [n] *)

(* val specification_display : Mi.t -> string -> unit *)

let specification_display a n = Ppwrap.display_dot (specification_to_doc a) n

(* [specification_to_pdf a n m] generates a PDF file named [m] from specification [a] using the GraphViz dot command. The graph is named [n] *)

(* val specification_to_pdf : Mi.t -> string -> string -> unit *)

let specification_to_pdf a n m = Ppwrap.dot_to_pdf (specification_to_doc a) m n

(* compute set of states reached by backward/forward paths to a given radius *)

let reachable_to_radius a q0 r0 =
  let s = Enum.make_empty () in
  let rec dfs q r =
    if not (Enum.mem s q) (* Has the state already been visited? *)
    then (* no: explore state *)
      begin
	Enum.set s q true;
	if r > 0 (* have we reached the maximal radius? *)
	then (* no: explore backward then forward *)
	  begin
	    Mi.ready_in_iter
	      (fun e -> 
		 Mi.state_in_iter
		   (fun q' -> dfs q' (r-1))
		   a q e)
	      a q;
	  Mi.mod_out_iter
	    (fun e -> 
	       function
		   Modal.May q' | Modal.Must q' -> dfs q' (r-1)
		 | Modal.Inconsistent | Modal.Cannot -> ())
	    a q
	  end
      end
  in
    begin
      dfs q0 r0;
      s
    end

(*
 *
 * [zoom_specification_to_doc a n q r] generates dot code in doc format providing a zoom around state [q],
 * going backward and forward up to radius [r]. Graph is named [n].
 *
 *)

(* val zoom_specification_to_doc : Mi.t -> string -> Heap.obj -> int -> unit *)

let zoom_specification_to_doc a n q r =
  let s = reachable_to_radius a q r in
  let f = Enum.mem s in  
    filter_specification_to_doc f a n

(*
 *
 * [zoom_specification_to_file a n m q r] generates dot code in file [m] zooming specification [a] on a
 * disc of radius [r] centered on [q]. The graph is named [n].
 *
 *)

(* val zoom_specification_to_file : Mi.t -> string -> string -> Heap.obj -> int -> unit *)

let zoom_specification_to_file a n m q r = Ppwrap.to_file (fun n' -> zoom_specification_to_doc a n' q r) m n

(*
 *
 * [zoom_specification_display a n q r] displays part of specification [a] using the GraphViz dot command.
 * A disc of radius [r] centered on [q] is displayed. The graph is named [n].
 *
 *)

(* val zoom_specification_display : Mi.t -> string -> Heap.obj -> int -> unit *)

let zoom_specification_display a n q r = Ppwrap.display_dot (fun n' -> zoom_specification_to_doc a n' q r) n
