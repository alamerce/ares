(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Heuristics: Compositional analysis heuristics
 *
 * $Id: mi.mli 476 2011-12-06 15:01:07Z bcaillau $
 *
 *)

(*
 *
 * [max_consistent_sets w a] computes the list of maximal consistent subsets of [a]. [a] is an array of pairs identifier * interface.
 *
 *)

val max_consistent_sets : Heap.heap -> ('a * Mi.t) array -> 'a list list
