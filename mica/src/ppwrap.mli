(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Wrappers for Pp document generators
 *
 * $Id: ppwrap.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

(* [to_channel g f x] writes document [g x] to channel [f] *)

val to_channel : ('a -> Pp.doc) -> out_channel -> 'a -> unit

(* [to_file g n x] writes document [g x] to file [n] *)

val to_file : ('a -> Pp.doc) -> string -> 'a -> unit

(*
 *
 * [to_command f c x] saves the document [f x] in a temporary file and then evaluates the command line [c],
 * replacing every substring $@ by the actual temporary file name. The file is discarded after the
 * completion of the command line
 *
 *)

val to_command : ('a -> Pp.doc) -> string  (* command *) -> 'a -> unit

(* [display_dot g x] displays the output of [g x] using the GraphViz dot command *)

val display_dot : ('a -> Pp.doc) -> 'a -> unit

(* [dot_to_pdf g n x] generates a PDF file named [n] from the output of [g x] using the GraphViz dot command *)

val dot_to_pdf : ('a -> Pp.doc) -> string -> 'a -> unit
