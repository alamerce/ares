(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Pattern: library of modal interfaces
 *
 * $Id: pattern.mli 474 2011-12-05 12:48:10Z bcaillau $
 *
 *)

(*
 *
 * [every_must w g u] produces an interface with signature [g]
 * and allocated in heap [w] such that for every occurrence of
 * word [u], the last event of [u] has modality must.
 * Exception [Invalid_argument _] is raised if [u] is not in signature [g].
 *
 *)

val every_must : Heap.heap -> Signature.t -> Heap.obj array -> Mi.t

(*
 *
 * [every_cannot w g u] produces an interface with signature [g]
 * and allocated in heap [w] such that for every occurrence of
 * word [u], the last event of [u] has modality cannot.
 * Exception [Invalid_argument _] is raised if [u] is not in signature [g].
 *
 *)

val every_cannot : Heap.heap -> Signature.t -> Heap.obj array -> Mi.t

(*
 *
 * [after_must v w g x e] produces an interface with signature [g]
 * allocated in heap [v] such that after the occurrence of a word 
 * accepted by expression [x], event [e] has modality must. Heap
 * [w] is used to compute the derivatives of expression [x].
 * Exception [Invalid_argument _] is raised if [x] or [e] are not
 * in signature [g].
 *
 *)

val after_must : Heap.heap -> Objexpr.h -> Signature.t -> Objexpr.t -> Heap.obj -> Mi.t

(*
 *
 * [after_cannot v w g x e] produces an interface with signature [g]
 * allocated in heap [w] such that after the occurrence of a word 
 * accepted by expression [x], event [e] has modality must. Heap
 * [w] is used to compute the derivatives of expression [x].
 * Exception [Invalid_argument _] is raised if [x] or [e] are not
 * in signature [g].
 *
 *)

val after_cannot : Heap.heap -> Objexpr.h -> Signature.t -> Objexpr.t -> Heap.obj -> Mi.t
