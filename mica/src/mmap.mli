(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Mmap: mappings of mappings with heap objects as domain
 *
 * $Id: mmap.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

(* Mappings *)

type 'a t

(* makes a map *)

val make : 'a (* default value *) -> 'a t

(* [clone m] returns a clone of [m] *)

val clone : 'a t -> 'a t

(* read *)

val read : 'a t -> Heap.obj -> Heap.obj -> 'a

(* write *)

val write : 'a t -> Heap.obj -> Heap.obj -> 'a -> unit

(* unset *)

val unset : 'a t -> Heap.obj -> Heap.obj -> unit

(* unset_fst *)

val unset_fst : 'a t -> Heap.obj -> unit

(* set : uses unset to revert to default value. Tests value equality *)

val set : 'a t -> Heap.obj -> Heap.obj -> 'a -> unit

(* sets : uses unset to revert to default value. Tests reference equality *)

val sets : 'a t -> Heap.obj -> Heap.obj -> 'a -> unit

(* default value *)

val default : 'a t -> 'a

(* iterates on non trivial values *)

val iter : (Heap.obj -> Heap.obj -> 'a -> unit) -> 'a t -> unit

(* folds on non trivial values *)

val fold : (Heap.obj -> Heap.obj -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

(* map *)

val map : ('a -> 'b) -> 'a t -> 'b t

(* map2 *)

val map2 : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t

