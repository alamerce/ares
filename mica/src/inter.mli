(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Inter: evaluation machine for the interactive environment
 *
 * $Id: inter.mli 485 2012-03-08 09:40:02Z bcaillau $
 *
 *)

(* Exception [Error _] is raised whenever an error occurs *)

exception Error of string

(* type [value]: heap elements *)

type value

(* type [signature]: system and interface I/O signatures *)

type signature

(* type [system]: systems (component implementations) *)

type system

(* type [interface]: interfaces (component behavioral types) *)

type interface

(* type [expression]: regular expressions on events *)

type expression

(* [new_session ()] initializes the heap *)

val new_session : unit -> unit

(* Value factory *)

val ident : string -> value

val char : char -> value

val string : string -> value

val bool : bool -> value

val int : int -> value

val tuple : value array -> value

val set : value list -> value

(* Regular expression factory *)

val empty : unit -> expression

val epsilon : unit -> expression

val prefix : value -> expression -> expression

val concat : expression -> expression -> expression

val sum :expression -> expression -> expression

val star : expression -> expression

val shall : value -> expression

val shallnot : value -> expression

(* Generates an interface from an expression *)

val interface_of_expression : signature -> expression -> interface

(* Printers for the above types *)

val print_value : value -> unit

val print_signature : signature -> unit

val print_system : system -> unit

val print_interface : interface -> unit

val print_expression : expression -> unit

(* [print_statistics ()] prints statistics about the heap *)

val print_statistics : unit -> unit

(* Print informations about interfaces and systems *)

val info_interface : interface -> unit

IFNDEF JAVASCRIPT THEN

(* Display systems and interfaces *)

val display_system : system -> string (* graph identifier *) -> unit

val display_interface : interface -> string (* graph identifier *) -> unit

val zoom_interface : interface -> string (* graph identifier *) -> value (* disc center *) -> int (* disc radius *) -> unit

(* Generate PDF files from systems and interfaces *)

val system_to_pdf : system -> string (* graph identifier *) -> string (* PDF file name *) -> unit

val interface_to_pdf : interface -> string (* graph identifier *) -> string (* PDF file name *) -> unit

(* Generates dot files from systems and interfaces *)

val dot_file_of_system : system -> string (* graph identifier *) -> string (* file name *) -> unit

val dot_file_of_interface : interface -> string (* graph identifier *) -> string (* file name *) -> unit

(* Explore interfaces *)

val explore : interface -> unit

  END;;

(* SIGNATURES *)

(* [new_signature ()] starts the definition of a signature *)

val new_signature : unit -> unit

(* [define_signature ()] terminates the definition of a signature and returns this signature *)

val define_signature : unit -> signature

(* [output e] defines value [e] to be an output in the current signature *)

val output : value -> unit

(* [input e] defines value [e] to be an input in the current signature *)

val input : value -> unit

(* SYSTEMS AND INTERFACES *)

(* [new_system g] starts the definition of a system of signature [g] *)

val new_system : signature -> unit

(* [define_system ()] terminates the definition of a system and returns this system *)

val define_system : unit -> system

(* [void ()] forces the current interface to be inconsistent *)

val void : unit -> unit

(* [init q] defines value [q] as initial state of the current system or interface *)

val init : value -> unit

(* [trans q e q'] defines a transition ([q],[e],[q']) in the current system *)

val trans : value -> value -> value -> unit

(* [new_interface g] initiates the definition of an interface with signature [g] *)

val new_interface : signature -> unit

(* [define_interface ()] terminates the definition of an interface and returns it in reduced form *)

val define_interface : unit -> interface

(* [may q e q'] defines a may transition ([q],[e],[q']) in the current interface *)

val may : value -> value -> value -> unit

(* [must q e q'] defines a may transition ([q],[e],[q']) in the current interface *)

val must : value -> value -> value -> unit

(* [inconsistent q e] sets event [e] to be inconsistent in state [q] in the current interface *)

val inconsistent : value -> value -> unit

(* [cannot q e] sets event [q] to cannot in state [q] in the current interface *)

val cannot : value -> value -> unit

(* [signature_of_system m] returns the signature of system [m] *)

val signature_of_system : system -> signature

(* [signature_of_interface s] returns the signature of interface [s] *)

val signature_of_interface : interface -> signature

(* Mappings between interfaces and systems *)

(* [interface_of_system m] maps deterministic system [m] to a rigid interface *)
(* Raises [Error _] if [m] is not deterministic *)

val interface_of_system : system -> interface

(* Extremal implementations. Raises [Error _] if specification is inconsistent *)

val min_implementation : interface -> system

val max_implementation : interface -> system

(* Composition operator on systems *)

(* [parallel m1 m2] computes the parallel composition of [m1] and [m2] *)

val parallel : system -> system -> system

(* Composition operators on interfaces *)

(* [conjunction s1 s2] computes the conjunction of interfaces [s1] and [s2] *)

val conjunction : interface -> interface -> interface

(* [product s1 s2] computes the product of interfaces [s1] and [s2] *)

val product : interface -> interface -> interface

(* [quotient s1 s2] computes the residuation of [s1] by [s2] *)

val quotient : interface -> interface -> interface

(*
 *
 * [compatible_quotient a b] computes the compatible residuation of interface [a]
 * by interface [b]. It is the largest [x] such that [x] is compatible with [b]
 * and [product x b] refines [a].
 *
 *)

val compatible_quotient : interface -> interface -> interface

(* [wimply s1 s2] computes the weak implication of [s1] by [s2] *)

val wimply : interface -> interface -> interface

(* [contract g a] computes contract ([g]x[a])/[a] *)

val contract : interface -> interface -> interface

(*
 *
 * [parallel_optimistic s1 s2] computes the optimistic parallel composition
 * of interfaces [s1] and [s2].
 *
 *)

val parallel_optimistic : interface -> interface -> interface

(* Relations on systems and interfaces *)

(* [satisfies m s] decides whether system [m] satisfies interface [s] *)

val satisfies : system -> interface -> bool

(* [refines s1 s2] decides whether interface [s1] refines interface [s2] *)

val refines : interface -> interface -> bool

(* [consistency s1 s2] decides whether interfaces [s1] and [s2] are mutually consistent *)

val consistency : interface -> interface -> bool

(* [is_consistent s] decides whether interface [s] is consistent *)

val is_consistent : interface -> bool

(* [is_complete s] checks whether interface [s] is complete *)

val is_complete : interface -> bool

(* [is_trivial s] checks whether interface [s] is trivial *)

val is_trivial : interface -> bool

(* [is_factor s1 s2] checks whether interface [s1] is a factor of [s2] *)

val is_factor : interface -> interface -> bool

(* [mutate_interface s l] turns events in [l] to be outputs in interface [s] *)

val mutate_interface : interface -> value list -> interface

(* [minimize s] minimizes interface [s] *)

val minimize : interface -> interface

(* [simplify s] simplifies the state structure of interface [s] by mapping state labels to integers *)

val simplify : interface -> interface

(*
 *
 * [abstract l a] computes a projection of interface [a] on sub-alphabet [l]. The resulting interface is the
 * least abstraction of [a] in the class of interfaces over alphabet [l].
 *
 *)

val abstract : value list -> interface -> interface

(*
 *
 * [project s a] computes a projection of interface [a] on signature [s]. The resulting interface is the
 * largest projection of [a] that is permissive to inputs and that controls its outputs.
 *
 *)

val project : signature -> interface -> interface

(*
 *
 * [facet s a] computes an abstraction [b] of a reduced interface [a], such that:
 *   - [b] is on the same signature as [a] 
 *   - the state structure of [b] is isomorphic to the state structure of [a], except that a universal (top) state has been added to [b]
 *   - every transition of [a] labeled by an event in [s] is preserved in [b], with the same modality
 *   - every "may" or "must" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition of [b]
 *   - every "cannot" transition of [a] labeled by an event outside of [s] is mapped to a "may" transition to the top state
 *
 *)

val facet : value list -> interface -> interface

(* Patterns *)

(*
 *
 * [every_must g u] produces an interface with signature [g]
 * such that for every occurrence of word [u], the last event
 * of [u] has modality must. Exception [Error _] is raised if
 * [u] is not in signature [g].
 *
 *)

val every_must : signature -> value array -> interface

(*
 *
 * [every_cannot g u] produces an interface with signature [g]
 * such that after every occurrence of word [u], last event excepted,
 * the last event of [u] has modality cannot. Exception
 * [Invalid_argument _] is raised if [u] is not in signature [g].
 *
 *)

val every_cannot : signature -> value array -> interface

(*
 *
 * [after_must g x e] produces an interface with signature [g]
 * such that after the occurrence of a word accepted by
 * expression [x], event [e] has modality must.
 * Exception [Error _] is raised if [x] or [e] are not in
 * signature [g].
 *
 *)

val after_must : signature -> expression -> value -> interface

(*
 *
 * [after_cannot g x e] produces an interface with signature [g]
 * such that after the occurrence of a word accepted by
 * expression [x], event [e] has modality cannot.
 * Exception [Error _] is raised if [x] or [e] are not in
 * signature [g].
 *
 *)

val after_cannot : signature -> expression -> value -> interface

(*** Heuristics ***)

(*
 *
 * [max_consistent_sets a] computes the list of maximal consistent subsets of [a]. [a] is an array of pairs identifier * interface.
 *
 *)

val max_consistent_sets : ('a * interface) array -> 'a list list

  
(*-----------------------------------------------------------------------------
 * Comparison function
 *---------------------------------------------------------------------------*)

(* type to describe a comparison relationship between two interfaces*)
(* Type [comparison] is used by [compare_interface] *)
type comparison =
    Lt (* Less than *)
  | Gt (* Greater than *)
  | Eq (* Equivalent *)
  | Ic (* Incomparable *)

(* [compare_interfaces s1 s2] decides whether [s1] refines [s2] and whether [s2] refines [s1] *)
val compare_interfaces : interface -> interface -> comparison
