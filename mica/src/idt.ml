(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * State and label identifier expressions
 *
 * $Id: idt.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
    Ident of string
  | Char of char
  | String of string
  | Bool of bool
  | Int of int
  | Tuple of t array
  | Set of t list
  | Observable of t
  | Silent
  | Input of t
  | Output of t

(* [compare x y] returns [0] if [x] is equal to [y], [-1] if [x] is less than [y] and [1] if [x] is greater than [y] *)

(* val compare : t -> t -> int *)

let compare = Pervasives.compare

(* [to_string x] returns a string representation of [x]. Control characters are escaped. *)

(* val to_string : t -> string *)

let rec to_string =
  function
      Ident i -> i
    | Char c -> "'" ^ (Char.escaped c) ^ "'"
    | String s -> "\"" ^ (String.escaped s) ^ "\""
    | Bool b ->	if b then "true" else "false"
    | Int n -> string_of_int n
    | Tuple l ->
	"(" ^ (array_to_string l) ^ ")"
    | Set l ->
	"{" ^ (list_to_string l) ^ "}"
    | Observable x -> "<" ^ (to_string x) ^ ">"
    | Silent -> "<>"
    | Input x -> "?" ^ (to_string x)
    | Output x -> "!" ^ (to_string x)

and list_to_string =
  function
      [] -> ""
    | [x] -> to_string x
    | x::l -> (to_string x) ^ "," ^ (list_to_string l)

and array_to_string a =
  let n = Array.length a in
    if n <= 0
    then ""
    else
      let s = ref (to_string a.(0)) in
	begin
	  for i=1 to n-1 do
	    s := (!s) ^ "," ^ (to_string a.(i))
	  done;
	  !s
	end

(* [print x] prints a representation of [x]. Control characters are escaped. *)

(* val print : t -> unit *)

let print x = print_string (to_string x)

(* merge sort *)

let rec ms =
  function
      ([],l) -> l
    | (k,[]) -> k
    | (((x::k') as k),((y::l') as l)) ->
	let c = compare x y in
	  if c < 0
	  then x::(ms (k',l))
	  else
	    if c > 0
	    then y::(ms (k,l'))
	    else x::(ms (k',l'))

let merge_sort k l = ms (k,l)

(* [concat x y] concatenates tuples [x] and [y] *)

(* val concat : t -> t -> t *)

let concat =
  function
      Tuple l1 ->
	begin
	  function
	      Tuple l2 -> Tuple (Array.append l1 l2)
	    | _ -> invalid_arg "Idt.concat: Typecheck error."
	end
    | _ ->
	invalid_arg "Idt.concat: Typecheck error."

(* [union x y] computes the union of sets [x] and [y] *)

(* val union : t -> t -> t *)

let union =
  function
      Set l1 ->
	begin
	  function
	      Set l2 -> Set (merge_sort l1 l2)
	    | _ ->
		invalid_arg "Idt.union: Typecheck error."
	end
    | _ ->
	invalid_arg "Idt.union: Typecheck error."
	  
(* drop twins in a sorted list *)

let rec dt =
  function
      (x,[]) -> []
    | (x,(y::l)) ->
	let c = Pervasives.compare x y in
	  if c < 0
	  then y::(dt (y,l))
	  else
	    if c > 0
	    then invalid_arg "Idt.dt: list is not sorted."
	    else dt (x,l)

let drop_twins =
  function
      x::l -> x::(dt (x,l))
    | [] -> []

(* [set_of_tuple x] makes a set from tuple [x] *)

(* val set_of_tuple : t -> t *)

let set_of_tuple =
  function
      Tuple l1 ->
	Set
	  (drop_twins
	     (List.sort Pervasives.compare (Array.to_list l1)))
    | _ ->
	invalid_arg "Idt.set_of_tuple: Typecheck error."

(* [tuple_of_set x] makes a tuple from set [x] *)

(* val tuple_of_set : t -> t *)

let tuple_of_set =
  function
      Set l -> Tuple (Array.of_list l)
    | _ -> invalid_arg "Idt.tuple_of_set: typecheck error"

(* [tuple_of_scalar x] makes a singleton tuple of [x] *)

(* val tuple_of_scalar : t -> t *)

let tuple_of_scalar x = Tuple [| x |]

(* [set_of_scalar x] make a singleton set of [x] *)

(* val set_of_scalar : t -> t *)

let set_of_scalar x = Set [x]
