(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 *  Ltr: labelled transition relations represented by extension
 *
 * $Id: ltr.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
    {
      left : Enum.t option Mapping.t option Mapping.t;
      right : Enum.t option Mapping.t option Mapping.t
    }

(* val make_empty : unit -> t *)

let make_empty () =
  {
    left = Mapping.make None;
    right = Mapping.make None
  }

(* val clone : t -> t *)

let clone r =
  let r' = make_empty () in
    begin

      (* clone from -> to labelled relation *)

      Mapping.iter
	(fun s ->
	   function
	       None -> ()
	     | Some w ->
		 let w' = Mapping.make None in
		   begin
		     Mapping.iter
		       (fun e ->
			  function
			      None -> ()
			    | Some ss ->
				Mapping.write w' e (Some (Enum.clone ss)))
		       w;
		     Mapping.write r'.left s (Some w')
		   end)
	r.left;

      (* clone to -> from labelled relation *)

      Mapping.iter
	(fun s ->
	   function
	       None -> ()
	     | Some w ->
		 let w' = Mapping.make None in
		   begin
		     Mapping.iter
		       (fun e ->
			  function
			      None -> ()
			    | Some ss ->
				Mapping.write w' e (Some (Enum.clone ss)))
		       w;
		     Mapping.write r'.right s (Some w')
		   end)
	r.right;      

      (* return clone *)

      r'
    end

(* val transpose : t -> t *)

let transpose r =
  let r' = clone r in
    {
      left = r'.right;
      right = r'.left
    }

(* val set : t -> Heap.obj (* from *) -> Heap.obj (* label *) -> Heap.obj (* to *) -> bool -> unit *)

let set r s e s' b =
  begin
    begin
      match Mapping.read r.left s with
	  None ->
	    if b
	    then
	      let u = Mapping.make None
	      and v = Enum.make_empty () in
		begin
		  Enum.set v s' true;
		  Mapping.write u e (Some v);
		  Mapping.write r.left s (Some u)
		end
	| Some u ->
	    begin
	      match Mapping.read u e with
		  None ->
		    if b
		    then
		      let v = Enum.make_empty () in
			begin
			  Enum.set v s' true;
			  Mapping.write u e (Some v)
			end
		| Some v ->
		    Enum.set v s' b
	    end
    end;
    begin
      match Mapping.read r.right s' with
	  None ->
	    if b
	    then
	      let u = Mapping.make None
	      and v = Enum.make_empty () in
		begin
		  Enum.set v s true;
		  Mapping.write u e (Some v);
		  Mapping.write r.right s' (Some u)
		end
	| Some u ->
	    begin
	      match Mapping.read u e with
		  None ->
		    if b
		    then
		      let v = Enum.make_empty () in
			begin
			  Enum.set v s true;
			  Mapping.write u e (Some v)
			end
		| Some v ->
		    Enum.set v s b
	    end
    end
  end

(* val get : t -> Heap.obj (* from *) -> Heap.obj (* label *) -> Heap.obj (* to *) -> bool *)

let get r s e s' =
  match Mapping.read r.left s with
      None -> false
    | Some u ->
	begin
	  match Mapping.read u e with
	      None -> false
	    | Some v ->
		Enum.mem v s'
	end

(* iterate on enabled events *)

(* val ready_out_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let ready_out_iter f r s =
  match Mapping.read r.left s with
      None -> ()
    | Some u ->
	Mapping.iter
	  (fun e ->
	     function
		 None -> ()
	       | Some v ->
		   if not (Enum.is_empty v)
		   then f e)
	  u

(* val ready_in_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let ready_in_iter f r s =
  match Mapping.read r.right s with
      None -> ()
    | Some u ->
	Mapping.iter
	  (fun e ->
	     function
		 None -> ()
	       | Some v ->
		   if not (Enum.is_empty v)
		   then f e)
	  u

(* iterate on (pre-)image states *)

(* val state_out_iter : (Heap.obj (* to-state *) -> unit) -> t -> Heap.obj (* from-state *) -> Heap.obj (* event *) -> unit *)

let state_out_iter f r s e =
  match Mapping.read r.left s with
      None -> ()
    | Some u ->
	begin
	  match Mapping.read u e with
	      None -> ()
	    | Some v ->
		Enum.iter_in f v
	end

(* val state_in_iter : (Heap.obj (* from-state *) -> unit) -> t -> Heap.obj (* to-state *) -> Heap.obj (* event *) -> unit *)

let state_in_iter f r s e =
  match Mapping.read r.right s with
      None -> ()
    | Some u ->
	begin
	  match Mapping.read u e with
	      None -> ()
	    | Some v ->
		Enum.iter_in f v
	end

(* dfs *)

(*
 *
 * val dfs :
 *   (Heap.obj (* state *) -> unit) (* state procedure *) ->
 *   (Heap.obj (* from-state *) -> Heap.obj (* event *) -> Heap.obj (* to-state *) -> unit) (* transition procedure *)->
 *   t (* relation *) -> Enum.t (* roots *) -> unit
 *
 *)

let dfs fs ft r rts =
  let v = Enum.make_empty () in
  let rec d s =
    if not (Enum.mem v s)
    then
      begin
	fs s;
	Enum.set v s true;
	ready_out_iter
	  (fun e ->
	     state_out_iter
	       (fun s' ->
		  ft s e s';
		  d s')
	       r
	       s
	       e
	  )
	  r
	  s
      end
  in
    Enum.iter_in d rts
