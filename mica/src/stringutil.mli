(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Utilities on strings
 *
 * $Id: stringutil.mli 441 2011-11-10 08:50:43Z bcaillau $
 *
 *)

(* [find_string s s'] returns the index in [s] of the first occurrence of [s'] and None otherwise *)

val find_string : string -> string -> int option

(* [substitute u v s] iteratively replaces the left-most occurrence of [u] by [v] in [s] *)

val substitute : string -> string -> string -> string

(* [silex s s'] compares [s] and [s'] according to the size-lexicographic order *)

val silex : string -> string -> int
