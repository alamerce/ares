(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Mmap: mappings of mappings with heap objects as domain
 *
 * $Id: mmap.ml 378 2011-10-10 11:12:32Z bcaillau $
 *
 *)

(* Mappings *)

type 'a t =
    {
      dft : 'a; (* default value *)
      mmp : 'a Mapping.t option Mapping.t
    }

(* makes a map *)

(* val make : 'a (* default value *) -> 'a t *)

let make z0 =
  {
    dft = z0;
    mmp = Mapping.make None
  }

(* read *)

(* val read : 'a t -> Heap.obj -> Heap.obj -> 'a *)

let read { dft=z0; mmp=u } x y =
  match Mapping.read u x with
      None -> z0
    | Some v -> Mapping.read v y

(* write *)

(* val write : 'a t -> Heap.obj -> Heap.obj -> 'a -> unit *)

let write { dft=z0; mmp=u } x y z =
  match Mapping.read u x with
      None ->
	let v = Mapping.make z0 in
	  begin
	    Mapping.write v y z;
	    Mapping.write u x (Some v)
	  end
    | Some v -> Mapping.write v y z

(* unset *)

(* val unset : 'a t -> Heap.obj -> Heap.obj -> unit *)

let unset { mmp=u } x y =
  match Mapping.read u x with
      None -> ()
    | Some v -> Mapping.unset v y

(* unset_fst *)

(* val unset_fst : 'a t -> Heap.obj -> unit *)

let unset_fst { mmp=u } x = Mapping.unset u x

(* set : uses unset to revert to default value. Tests value equality *)

(* val set : 'a t -> Heap.obj -> Heap.obj -> 'a -> unit *)

let set ({ dft=z0 } as m)  x y z =
  if z = z0
  then unset m x y
  else write m x y z

(* sets : uses unset to revert to default value. Tests reference equality *)

(* val sets : 'a t -> Heap.obj -> Heap.obj -> 'a -> unit *)

let sets ({ dft=z0 } as m)  x y z =
  if z == z0
  then unset m x y
  else write m x y z

(* default value *)

(* val default : 'a t -> 'a *)

let default { dft=z0 } = z0

(* iterates on non trivial values *)

(* val iter : (Heap.obj -> Heap.obj -> 'a -> unit) -> 'a t -> unit *)

let iter f { mmp=u } =
  Mapping.iter
    (fun x ->
       function
	   None -> ()
	 | Some v ->
	     Mapping.iter
	       (fun y z -> f x y z)
	       v)
    u

(* folds on non trivial values *)

(* val fold : (Heap.obj -> Heap.obj -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b *)

let fold f { mmp=u } w0 =
  Mapping.fold
    (fun x ->
       function
	   None -> (fun w -> w)
	 | Some v ->
	     (fun w ->
		Mapping.fold 
		  (fun y z w' -> f x y z w')
		  v
		  w))
    u
    w0

(* map *)

(* val map : ('a -> 'b) -> 'a t -> 'b t *)

let map f m =
  let m' = make (f (default m)) in
    begin
      iter
	(fun x y z -> write m' x y (f z))
	m;
      m'
    end

(* map2 *)

(* val map2 : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t *)

let map2 f m1 m2 =
  let m' = make (f (default m1) (default m2))
  and v = make false in
    begin
      iter
	(fun x y z1 ->
	   let z2 = read m2 x y in
	     begin
	       write v x y true;
	       write m' x y (f z1 z2)
	     end)
	m1;
      iter
	(fun x y z2 ->
	   if not (read v x y)
	   then
	     let z1 = read m1 x y in
	       write m' x y (f z1 z2))
	m2;
      m'
    end

(* [clone m] returns a clone of [m] *)

(* val clone : 'a t -> 'a t *)

let clone m =
  let m' = make (default m) in
    begin
      iter
	(write m')
	m;
      m'
    end
  

