(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Enumerate: bijective mapping of an enumerated set to integers
 *
 * $Id: enumerate.mli 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
    {
      card : int;
      to_int : int Mapping.t;
      of_int : Heap.obj array
    }

val enumerate : Enum.t -> t
