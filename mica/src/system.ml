(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2011.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * System: implementations
 *
 * $Id: system.ml 374 2011-10-05 11:47:23Z bcaillau $
 *
 *)

type t =
{
  sgn : Signature.t;
  ini : Enum.t;
  tra : Ltr.t
}

(* val make : Signature.t -> t *)

let make s =
  {
    sgn = Signature.clone s;
    ini = Enum.make_empty ();
    tra = Ltr.make_empty ()
  }

(* val clone : t -> t *)

let clone { sgn=s; ini=i; tra=t } =
{
  sgn = Signature.clone s;
  ini = Enum.clone i;
  tra = Ltr.clone t
}

(* val signature : t -> Signature.t *)

let signature { sgn=s } = Signature.clone s

(* asserts that event [e] belongs to signature [g]. If false, raise Invalid_argument with string [t] *)

let assert_sig g e t =
  match Signature.get g e with
      Orientation.Undefined -> invalid_arg t
    | _ -> ()

(* val set_initial : t -> Heap.obj -> bool -> unit *)

let set_initial { ini=u } s b =
  Enum.set u s b

(* val get_initial : t -> Heap.obj -> bool *)

let get_initial { ini=u } s =
  Enum.mem u s

(* val iter_initial : (Heap.obj -> unit) -> t -> unit *)

let iter_initial f { ini=u } =
  Enum.iter_in f u

(* val set_trans : t -> Heap.obj -> Heap.obj -> Heap.obj -> bool -> unit *)

let set_trans { sgn=g; tra=r } s e s' b =
  begin
    
    (* checks whether e belongs to the signature *)
    
    if b then assert_sig g e "System.set_trans: event does not belong to signature";

    (* (re-)sets the transition *)

    Ltr.set r s e s' b

  end

(* val get_trans : t -> Heap.obj -> Heap.obj -> Heap.obj -> bool *)

let get_trans { tra=r } s e s' = Ltr.get r s e s'

(* iterate on enabled events *)

(* val ready_out_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let ready_out_iter f { tra=r } s = Ltr.ready_out_iter f r s

(* val ready_out_fold : (Heap.obj (* event *) -> 'a -> 'a) -> t -> Heap.obj (* state *) -> 'a -> 'a *)

let ready_out_fold f a s x0 =
  let x = ref x0 in
    begin
      ready_out_iter
	(fun e -> x := f e (!x))
	a
	s;
      !x
    end

(* val ready_in_iter : (Heap.obj (* event *) -> unit) -> t -> Heap.obj (* state *) -> unit *)

let ready_in_iter f { tra=r } s = Ltr.ready_in_iter f r s

(* iterate on (pre-)image states *)

(* val state_out_iter : (Heap.obj (* to-state *) -> unit) -> t -> Heap.obj (* from-state *) -> Heap.obj (* event *) -> unit *)

let state_out_iter f { tra=r } s e = Ltr.state_out_iter f r s e

(* val state_out_fold : (Heap.obj (* to-state *) -> 'a -> 'a) -> t -> Heap.obj (* from-state *) -> Heap.obj (* event *) -> 'a -> 'a *)

let state_out_fold f a q e x0 =
  let x = ref x0 in
    begin
      state_out_iter
	(fun q' -> x := f q' (!x))
	a
	q
	e;
      !x
    end

(* val state_in_iter : (Heap.obj (* from-state *) -> unit) -> t -> Heap.obj (* to-state *) -> Heap.obj (* event *) -> unit *)

let state_in_iter f { tra=r } s e = Ltr.state_in_iter f r s e

(* Tests whether an event is enabled *)

(* val is_enabled : t -> Heap.obj (* from-state *) -> Heap.obj (* event *) -> bool *)

let is_enabled a q e = state_out_fold (fun _ _ -> true) a q e false

(* dfs *)

(*
 *
 * val dfs_iter :
 *   (Heap.obj (* state *) -> unit) (* state procedure *) ->
 *   (Heap.obj (* from-state *) -> Heap.obj (* event *) -> Heap.obj (* to-state *) -> unit) (* transition procedure *)->
 *   t (* system *) -> unit
 *
 *)

let dfs_iter f g { tra=r; ini=i } = Ltr.dfs f g r i

(*
 *
 * val dfs_fold :
 *   (Heap.obj (* state *) -> 'a -> 'a) (* state operator *) ->
 *   (Heap.obj (* from-state *) -> Heap.obj (* event *) -> Heap.obj (* to-state *) -> 'a -> 'a) (* transition operator *)->
 *   t (* system *) -> 'a -> 'a
 *
 *)

let dfs_fold f g a x0 =
  let x = ref x0 in
    begin
      dfs_iter
	(fun q -> x := f q (!x))
	(fun q e q' -> x := g q e q' (!x))
	a;
      !x
    end

(* [reachable_states a] computes the set of reachable states of system [a] *)

(* val reachable_states : t -> Enum.t *)

let reachable_states a =
  let r = Enum.make_empty () in
    begin
      dfs_iter
	(fun s -> Enum.set r s true)
	(fun _ _ _ -> ())
	a;
      r
    end

(* [print a] prints system [a] *)

(* val print : t -> unit *)

let print ({ sgn=g; ini=i } as a) =
  begin
    print_string "{\n";
    dfs_iter
      (fun s ->
	 if Enum.mem i s
	 then
	   begin
	     print_string " init";
	     print_string " state ";
	     Heap.print s;
	     print_endline ";"
	   end)
      (fun s e s' ->
	 print_string " trans ";
	 Heap.print s;
	 print_string " -- ";
	 Heap.print e;
	 print_string " -> ";
	 Heap.print s'; 
	 print_endline ";")
      a;
    print_string "} : ";
    Signature.print g;
    print_newline ()
  end

(*
 *
 * [parallel w fa b] computes the parallel composition of systems [a] and [b]
 * Product states are stored in heap [w]. Function [f] is evaluated whenever
 * incompatible states are encountered
 *
 *)

(*
 *
 * val parallel : Heap.heap ->
 *                (Heap.obj (* left state *) -> Heap.obj (* right state *) -> Heap.obj (* event *) -> unit) ->
 *                t -> t -> t
 *
 *)

let parallel w f { sgn=g1; ini=i1; tra=r1 } { sgn=g2; ini=i2; tra=r2 } =
  let pair q1 q2 = Heap.make_tuple w [| q1 ; q2 |] in
  let (h1,h2) = Signature.extend_prod g1 g2 in
  let g1' = Signature.merge g1 h1
  and g2' = Signature.merge g2 h2 in
  let g = Signature.product g1' g2' in
  let a = make g
  and v = Enum.make_empty () in
  let rec traverse q q1 q2 =
    if not (Enum.mem v q) (* do nothing is already visited *)
    then
      begin
	Enum.set v q true; (* set state visited *)
	Signature.iter (* iterate on defined events *)
	  (fun e d ->
	     match ((Signature.is_in g1 e),(Signature.is_in g2 e)) with
		 (false,false) -> invalid_arg "System.parallel: internal error (excluded case 1)"
	       | (true,false) ->
		   Ltr.state_out_iter
		     (fun q1' ->
			let q' = pair q1' q2 in
			  begin
			    set_trans a q e q' true;
			    traverse q' q1' q2
			  end)
		     r1
		     q1
		     e
	       | (false,true) ->
		   Ltr.state_out_iter
		     (fun q2' ->
			let q' = pair q1 q2' in
			  begin
			    set_trans a q e q' true;
			    traverse q' q1 q2'
			  end)
		     r2
		     q2
		     e
	       | (true,true) ->
		   begin
		     match Signature.get g e with
			 Orientation.Output ->
			   let ic = ref false in
			     begin
			       begin
				 match ((Signature.get g1 e),(Signature.get g2 e)) with
				     (Orientation.Output,Orientation.Input) ->
				       Ltr.state_out_iter
					 (fun q1' ->
					    begin
					      ic := true;
					      Ltr.state_out_iter
						(fun q2' ->
						   let q' = pair q1' q2' in
						     begin
						       ic := false;
						       set_trans a q e q' true;
						       traverse q' q1' q2'
						     end)
						r2
						q2
						e
					    end)
					 r1
					 q1
					 e
				   | (Orientation.Input,Orientation.Output) ->
				       Ltr.state_out_iter
					 (fun q2' ->
					    begin
					      ic := true;
					      Ltr.state_out_iter
						(fun q1' ->
						   let q' = pair q1' q2' in
						     begin
						       ic := false;
						       set_trans a q e q' true;
						       traverse q' q1' q2'
						     end)
						r1
						q1
						e
					    end)
					 r2
					 q2
					 e
				   | _ -> invalid_arg "System.parallel: internal error (excluded case 2)"
			       end;
			       if !ic then f q1 q2 e
			     end
		       | _ ->
			   Ltr.state_out_iter
			     (fun q1' ->
				Ltr.state_out_iter
				  (fun q2' ->
				     let q' = pair q1' q2' in
				       begin
					 set_trans a q e q' true;
					 traverse q' q1' q2'
				       end)
				  r2
				  q2
				  e)
			     r1
			     q1
			     e
		   end)
	  g
      end
  in
    begin
      Enum.iter_in
	(fun q1 ->
	   Enum.iter_in
	     (fun q2 ->
		let q = pair q1 q2 in
		  begin
		    set_initial a q true;
		    traverse q q1 q2
		  end)
	     i2)
	i1;
      a
    end

(* val incompatibility_print : Heap.obj -> Heap.obj -> Heap.obj -> unit *)

let incompatibility_print q1 q2 e =
  begin
    print_string "States ";
    Heap.print q1;
    print_string " and ";
    Heap.print q2;
    print_string " are incompatible wrt to event ";
    Heap.print e;
    print_newline ()
  end

(* [are_compatible a b] decides whether systems [a] and [b] are compatible wrt to their signatures and I/O compatibility *)

(* val are_compatible : Heap.heap -> t -> t -> bool *)

let are_compatible w a1 a2 =
  let g1 = signature a1
  and g2 = signature a2 in
  let g = Signature.ext_prod g1 g2 in
    (Signature.is_consistent g) &&
      let cpt = ref true in
	begin
	  ignore
	    (parallel
	       w
	       (fun _ _ _ -> cpt := false)
	       a1
	       a2);
	  !cpt
	end

(* val is_deterministic : t -> bool *)

let is_deterministic a =
  dfs_fold
    (fun q x ->
       ready_out_fold
	 (fun e y ->
	    y &&
	      ((state_out_fold
		  (fun _ n -> n+1)
		  a q e 0) <= 1))
	 a q x)
    (fun _ _ _ x -> x)
    a
    true

