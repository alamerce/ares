(*
 *
 ***********************************************************************
 *                                                                     *
 *        Mica : A Modal Interface Compositional Analysis Library      *
 *                                                                     *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Parking system (dev test version)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Univ Rennes, 2018
 *
 * Run with mica under emacs with the Tuareg mode
 *
 * $Id$
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

open Inter;;

(* new_session ();; *)

(*-----------------------------------------------------------------
 * --
 *-----------------------------------------------------------------*)

(*** You may change the following parameters ***)

let n0 = 2;; (* Parking size *)

(*
 * Adjust display level as follows:
 * 0 = none
 * 1 = little
 * 2 = more
 * 3 = all
 *
 * Warning: with display_level >= 3, Graphviz can be very slow on large graphs
 *)

let display_level = 1;; 

(*** Utilities ***)

(* function composition operator *)

let ($$) = fun f g x -> f (g x);;

(* simplified minimal interface *)

let simple = simplify $$ minimize;;

(* terminal max *)

let strong = interface_of_system $$ max_implementation;;

(* simplified minimal system *)

let minimal = min_implementation $$ simple $$ interface_of_system;;

(* Trivial interface *)

let tt =
  new_interface (new_signature (); define_signature ());
  init (int 0);
  define_interface ();;

(* Conjunction of a list of interfaces *)

let fold_op f =
  function
      [] -> tt
    | c0::l -> List.fold_left f c0 l;;

let fold_conjunction = fold_op conjunction
and fold_product = fold_op product;;

let fold_parallel =
  function
      [] -> invalid_arg "fold_parallel: empty list"
    | i0::l -> List.fold_left parallel i0 l;;

let collate l = simple (fold_conjunction l)
and gather l =  simple (fold_product l);;

let map_collate f l =
  collate (List.map f l);;

(* non-zero iteration of a regular expression *)

let plus x = concat x (star x);;

(* 1 letter 1 word expression *)

let token e = prefix e (epsilon ());;

(* Sum iterator *)

let map_sum f =
  function
      [] -> empty ()
    | x::l ->
	List.fold_right
	  (fun y e -> sum e (f y))
	  l (f x);;

(* Force receptivity to an input event *)

let receptive x =
  let h = new_signature (); input x; define_signature () in
  every_must h [| x |]

and receptive_dual x =
  let h = new_signature (); output x; define_signature () in
  every_must h [| x |];;

(* comparison of two interfaces *)

type comparison =
    Lt (* Less than *)
  | Gt (* Greater than *)
  | Eq (* Equivalent *)
  | Ic (* Incomparable *);;

let compare x y =
  let _ = Printf.printf "Checking refinement...\n" in
  let c = refines x y in
  let _ = Printf.printf "Checking abstraction...\n" in
  let d = refines y x in
    match (c,d) with
	(true,true) -> Eq
      | (true,false) -> Lt
      | (false,true) -> Gt
      | (false,false) -> Ic;;

(*** Events ***)

let v_en = ident "vehicle_enter" (* vehicle is entering the parking *)
and v_ex = ident "vehicle_exit" (* vehicle is exiting the parking *)
and ng_op = ident "entry_gate_open" (* command to open the entry gate *)
and ng_cl = ident "entry_gate_close" (* command to close the entry gate *)
and xg_op = ident "exit_gate_open" (* command to open the exit gate *)
and xg_cl = ident "exit_gate_close" (* command to close the exit gate *)
and r_en = ident "request_enter" (* request to enter parking *)
and r_ex = ident "exit_ticket_insert" (* exit ticket is inserted in exit gate *) 
and t_is = ident "ticket_issue" (* command to issue a fresh ticket *) 
and t_ip = ident "ticket_insert_payment" (* ticket has been inserted in payment machine *)
and c_ip = ident "coin_insert_payment" (* coin has been inseted in payment machine *)
and x_is = ident "exit_ticket_issue";; (* command to issue an exit ticket *)

(*** Generic events ***)

let v_t = ident "vehicle_pass"
and g_o = ident "gate_open"
and g_c = ident "gate_close";;

(*** System parameters ***) 

module type ParkingParametersType =
  sig
    val capacity : int (* number of slots *)
  end;;

module ParkingParameters =
  struct
    let capacity = n0
  end
;;

(*** Gates ***)

module type GateProfileType =
sig
  val through : value (* vehicle has passed the gate *)
  val lift : value (* gate lifts up *)
  val lower : value (* gate lowers down *)
end;;

module GenericGateProfile =
struct
  let through = v_t
  let lift = g_o
  let lower = g_c
end;;

module EntryGateProfile =
struct
  let through = v_en
  let lift = ng_op
  let lower = ng_cl
end;;

module ExitGateProfile =
struct
  let through = v_ex
  let lift = xg_op
  let lower = xg_cl
end;;

(*** Assumptions ranging over all requirements ***)

module GenericGateAssumption = functor (P:GateProfileType) ->
  struct

    (* Events *)
    
    let through = P.through
    let lift = P.lift
    let lower = P.lower

    (* Assumptions *)
    (* A1a: assumption that vehicles pass only when gate is open *)

    let h1a =
      new_signature ();
      input lift;
      input lower;
      output through;
      define_signature ()

    let h1a_dual =
      new_signature ();
      output lift;
      output lower;
      input through;
      define_signature ()

    let gate_is_closed =
      star
	(sum
	   (sum (token lower) (token through))
	   (concat
	      (token lift)
	      (concat
		 (star (sum (token lift) (token through)))
		 (token lower))))
	     
    let gate_is_open =
      concat
	gate_is_closed
	(concat
	   (token lift)
	   (star (sum (token lift) (token through))))

    let a1a =
      collate
	[
	  after_cannot h1a gate_is_closed through;
	  receptive lift;
	  receptive lower
	]
    
    let a1a_dual =
      collate
	[
	  after_cannot h1a_dual gate_is_closed through;
	  receptive_dual lift;
	  receptive_dual lower
	]

    (* A1b: assumption that atmost one vehicle passes at a time *)

    let h1b =
      new_signature ();
      output through;
      input lift;
      define_signature ()

    let h1b_dual =
      new_signature ();
      input through;
      output lift;
      define_signature ()

    let a1b =
      collate
	[
	  every_cannot h1b [| through ; through |];
	  receptive lift
	]

    let a1b_dual =
      collate
	[
	  every_cannot h1b_dual [| through ; through |];
	  receptive_dual lift
	]

    (* Resulting assumption *)

    let assume =
      collate
	[
	  a1a;
	  a1b
	]
	
    let assume_dual =
      collate
	[
	  a1a_dual;
	  a1b_dual
	]
	
  end;;

(*** Generic viewpoints ***)

module GenericGate = functor (P:GateProfileType) ->
  struct

    (* Events *)
    
    let through = P.through
    let lift = P.lift
    let lower = P.lower

    (* Assumptions *)

    module Assume = GenericGateAssumption(P)

    (* Global assumption *)

    let assume = Assume.assume

    let assume_dual = Assume.assume_dual

    (* Requirements *)

    (* R2b: when the gate is open, gate_open is not allowed; when the gate is closed, gate_close is not allowed *)

    let h2b =
      new_signature ();
      output lift;
      output lower;
      define_signature ()
	
    let r2b =
      collate
	[
	  every_cannot h2b [| lift ; lift |];
	  every_cannot h2b [| lower ; lower |];
	  after_cannot h2b (epsilon ()) lower
	]

    (* Guarantee *)

    let guarantee = r2b

    (* Gate contract *)
	
    let contr =
      simple
	(contract
	   guarantee
	   (strong assume))

  end;;

module GateAssumption = GenericGateAssumption(GenericGateProfile);;
module Gate = GenericGate(GenericGateProfile);;

module EntryGate = GenericGate(EntryGateProfile);;
module ExitGate = GenericGate(ExitGateProfile);;

if display_level >= 2 then
  begin
    display_interface GateAssumption.a1a "GateA1a";
    display_interface GateAssumption.a1a_dual "GateA1aDual";
    display_system (max_implementation GateAssumption.a1a) "GateA1aIO";
    display_system (max_implementation GateAssumption.a1a_dual) "GateA1aDualIO";
    display_interface GateAssumption.a1b "GateA1b";
    display_interface GateAssumption.a1b_dual "GateA1bDual";
    display_system (max_implementation GateAssumption.a1b) "GateA1bIO";
    display_system (max_implementation GateAssumption.a1b_dual) "GateA1bDualIO";
    display_interface (minimize (conjunction GateAssumption.a1a GateAssumption.a1b)) "ConjunctionOfAssumptions";
    display_interface (conjunction GateAssumption.a1a GateAssumption.a1b) "ConjunctionOfAssumptionsNotMin";
    display_interface (minimize (conjunction GateAssumption.a1a_dual GateAssumption.a1b_dual)) "ConjunctionOfAssumptionsDual"
  end;;

if display_level >= 2 then
  begin
    display_interface Gate.assume "GateAssume";
    display_interface Gate.assume_dual "GateAssumeDual";
    display_interface (strong Gate.assume_dual) "StrongGateAssumeDual";
    display_interface Gate.guarantee "GateGuarantee";
    display_system (max_implementation Gate.guarantee) "GateGuaranteeIO";
    display_interface (product Gate.guarantee (strong Gate.assume)) "GProductA"
  end;;

if display_level >= 1 then
  display_interface Gate.contr "GateContract";;

print_signature (signature_of_interface Gate.assume);
print_signature (signature_of_interface Gate.guarantee);
print_signature (signature_of_interface Gate.contr);;

(*** Supervisor ***)

module GenericSupervisor =
  functor (E:GateProfileType (* entry *)) -> 
    functor (X:GateProfileType (* exit *)) ->
      functor (P:ParkingParametersType (* capacity *)) ->
struct

  let v_enter = E.through
  let v_exit = X.through
  let t_issue = t_is
  let eg_close = E.lower
  let eg_open = E.lift
  let xg_close = X.lower
  let xg_open = X.lift
  let r_enter = r_en
  let r_exit = r_ex
  let capacity = P.capacity

  (* Assumptions *)

  (* Assumptions inherited from entry and exit gates *)
    
  module EntryGateAssume = GenericGateAssumption(E)
  module ExitGateAssume = GenericGateAssumption(X)

  (* Resulting assumptions *)

  let assume =
    collate
      [
	EntryGateAssume.assume;
	ExitGateAssume.assume
      ]

  let assume_dual =
    collate
      [
	EntryGateAssume.assume_dual;
	ExitGateAssume.assume_dual
      ]

  (* R1: Entry gate can not open until a ticket has been issued and it must open once a ticket has been issued *)

  let h1 =
    new_signature ();
    output eg_open;
    output t_issue;
    output eg_close;
    define_signature ()

  let r1 =
    collate
      [
	every_cannot h1 [| eg_close ; eg_open |];
	after_cannot h1 (epsilon ()) eg_open;
        every_must h1 [| t_issue ; eg_open |]
      ]

  (* R2a: entry ticket must be issued only when parking is not full and entry is requested *)
  
  let h2a =
    new_signature ();
    input r_enter;
    output t_issue;
    input v_enter;
    input v_exit;
    define_signature ()

  let r2a n =
    let a i = tuple [| (ident "a"); (int i) |]
    and b i = tuple [| (ident "b"); (int i) |] in
      new_interface h2a;
      init (a 0);
      for i=0 to n-1 do
	may (b i) t_issue (b i);
	must (a i) v_enter (a (i+1));
        must (b i) v_enter (a (i+1));
	must (a (i+1)) v_exit (a i);
	must (b (i+1)) v_exit (b i)
      done;
      must (b n) v_en (a n);
      must (a n) v_en (a n);
      must (a 0) v_ex (a 0);
      must (b 0) v_ex (b 0);
      for i=0 to n do
	must (a i) r_enter (b i);
	must (b i) r_enter (b i)
      done;
      define_interface ()
  
  (* r2a for infinite capacity parking *)

  let r2a_infty =
    new_interface h2a;
    init (int 0);
    for i=0 to 2 do
      must (int i) v_exit (int i)
    done;
    must (int 0) r_enter (int 1);
    may (int 1) t_issue (int 2);
    must (int 2) v_enter (int 0);
    must (int 1) r_enter (int 1);
    must (int 2) r_enter (int 2);
    must (int 0) v_enter (int 0);
    must (int 1) v_enter (int 1);
    define_interface ()

  (* R3a: When the entry gate is closed, the entry gate may not open, unless a ticket has been issued *)

  let gate_is_closed_and_no_ticket =
    concat
      (star
	 (concat
	    (star (sum (token eg_close) (token t_issue)))
	    (concat
	       (token eg_open)
	       (concat
		  (star (sum (token eg_open) (token t_issue)))
		  (token eg_close)))))
      (star (token eg_close))
  
  let h3a =
    new_signature ();
    output eg_open;
    output t_issue;
    output eg_close;
    define_signature ()
  
  let r3a = after_cannot h3a gate_is_closed_and_no_ticket eg_open
  
  (* R3b: the entry gate must open when a ticket is issued and only then *)
  
  let h3b =
    new_signature ();
    output eg_open;
    output t_issue;
    define_signature ()
  
  let r3b =
    collate
      [
	every_must h3b [| t_issue ; eg_open |];
	every_cannot h3b [| t_issue ; t_issue |];
	after_cannot h3b (epsilon ()) eg_open
      ]
  
  (* R3c : request_enter can always be pressed *)
  
  let r3c = receptive r_enter
  
  (* R4a : exit gate must open after an exit ticket is inserted and only then *)

  let h4a =
    new_signature ();
    output xg_open;
    input r_exit;
    output xg_close;
    define_signature ()

  let r4a =
    collate
      [
	every_cannot h4a [| xg_close ; xg_open |];
	after_cannot h4a (epsilon ()) xg_open;
        every_must h4a [| r_exit ; eg_open |]
      ]

  (* R4a' (bogus requirement): exit gate must open after an exit ticket is inserted and only then *)

  let r4a' =
    collate
      [
	(* every_cannot h4a [| xg_close ; xg_open |]; *)
	after_cannot h4a (epsilon ()) xg_open;
        every_must h4a [| r_exit ; eg_open |]
      ]

  (* R4b : receptive to the insertion of exit tickets and to exit of vehicles *)

  let r4b = collate [ receptive r_exit; receptive v_exit ]

  (* R4c : exit gate closes only after vehicle has exited parking *)

  let h4c =
    new_signature ();
    input v_exit;
    output xg_close;
    define_signature ()

  let r4c =
    collate
      [
	every_must h4c [| v_exit; xg_close |];
	every_cannot h4c [| xg_close; xg_close |];
	after_cannot h4c (epsilon ()) xg_close
      ]

  (* Guarantee *)

  let guarantee =
    collate
      [
        r1;
	r2a capacity;
	(* r2a_infty; *)
	r3a;
	r3b;
	r3c;
	r4a;
	r4b;
	r4c
      ]

(* Bogus guarantee *)

  let guarantee' =
    collate
      [
        r1;
	r2a capacity;
	(* r2a_infty; *)
	r3a;
	r3b;
	r3c;
	r4a';
	r4b;
	r4c
      ]

  (* Resulting contract *)
  
  let contr =
    simple
      (contract
	 guarantee
	 (strong assume))
      
(* Bogus contract *)

  let contr' =
    simple
      (contract
	 guarantee'
	 (strong assume))
      
end;;

module Supervisor = GenericSupervisor(EntryGateProfile)(ExitGateProfile)(ParkingParameters);;

if display_level >= 2 then
  begin
    display_interface (Supervisor.r2a Supervisor.capacity)  "SupervisorR2a";
    display_interface Supervisor.r2a_infty  "SupervisorR2aInfty";
    display_system (max_implementation (Supervisor.r2a Supervisor.capacity)) "SupervisorR2aIO";
    display_interface Supervisor.assume "SupervisorAssume";
    display_interface Supervisor.assume_dual "SupervisorAssumeDual";
    display_system (max_implementation Supervisor.assume_dual) "SupervisorAssumeDualIO";
    display_interface Supervisor.guarantee "SupervisorGuarantee"
  end;;

if display_level >= 3 then
  display_interface Supervisor.contr "SupervisorContract";;

(*** Payment machine profile ***)

module type PaymentProfileType =
sig
  val insert_ticket : value
  val insert_coin : value
  val issue_exit_ticket : value
end;;

module PaymentProfile =
struct
  let insert_ticket = t_ip
  let insert_coin = c_ip
  let issue_exit_ticket = x_is 
end;;

(*** Payment machine ***)

module GenericPayment = functor (P:PaymentProfileType) ->
struct
  let insert_ticket = P.insert_ticket
  let insert_coin = P.insert_coin
  let issue_exit_ticket = P.issue_exit_ticket

  (* assumption R3.1: user inserts a coin every time a ticket is inserted, only then *)

  let h3a =
    new_signature ();
    output insert_ticket;
    output insert_coin;
    define_signature ()

  let h3a_dual =
    new_signature ();
    input insert_ticket;
    input insert_coin;
    define_signature ()

  let a3a =
    collate
      [
	every_must h3a [| insert_ticket; insert_coin |];
	every_cannot h3a [| insert_coin; insert_coin |];
        after_cannot h3a (epsilon ()) insert_coin
      ]

  let a3a_dual =
    collate
      [
	every_must h3a_dual [| insert_ticket; insert_coin |];
	every_cannot h3a_dual [| insert_coin; insert_coin |];
        after_cannot h3a_dual (epsilon ()) insert_coin
      ]

  (* assumption R3.2: user may insert a ticket only initially or after an exit ticket has been issued *)

  let h3b =
    new_signature ();
    output insert_ticket;
    input issue_exit_ticket;
    define_signature ()

  let h3b_dual =
    new_signature ();
    input insert_ticket;
    output issue_exit_ticket;
    define_signature ()

  let a3b =
    collate
      [
	every_must h3b [| issue_exit_ticket; insert_ticket |];
        after_must h3b (epsilon ()) insert_ticket;
	every_cannot h3b [| insert_ticket; insert_ticket |];
	receptive issue_exit_ticket;
      ]
 
  let a3b_dual =
    collate
      [
	every_must h3b_dual [| issue_exit_ticket; insert_ticket |];
        after_must h3b_dual (epsilon ()) insert_ticket;
	every_cannot h3b_dual [| insert_ticket; insert_ticket |];
	receptive_dual issue_exit_ticket;
      ]
 
  (* Assumption *)

  let assume = collate [ a3a; a3b ]

  let assume_dual = collate [ a3a_dual; a3b_dual ]

  (* guarantee R3.3: exit ticket is issued after ticket is inserted and payment is made and only then *)

  let h3c =
    new_signature ();
    input insert_ticket;
    input insert_coin;
    output issue_exit_ticket;
    define_signature ()

  let no_ticket_inserted =
    star
      (concat
	 (star (sum (token insert_ticket) (sum (token insert_coin) (token issue_exit_ticket))))
	 (token issue_exit_ticket))

  let ticket_inserted =
    concat no_ticket_inserted (token insert_ticket)

  let coin_inserted =
    concat ticket_inserted (token insert_coin)

  let no_coin_inserted =
    sum no_ticket_inserted ticket_inserted

  let g3c =
    collate
      [
	receptive insert_ticket;
	receptive insert_coin;
	after_cannot h3c no_coin_inserted issue_exit_ticket;
	after_must h3c coin_inserted issue_exit_ticket
      ]

  (* Guarantee *)

  let guarantee = collate [ g3c ]

  (* Contract *)

  let contr = simple (contract guarantee (strong assume))

end;;

(*** Entry and exit gate, supervisor and payment ***)

module EntryGate = GenericGate(EntryGateProfile);;

module ExitGate = GenericGate(ExitGateProfile);;

module Supervisor = GenericSupervisor(EntryGateProfile)(ExitGateProfile)(ParkingParameters);;

module Payment = GenericPayment(PaymentProfile);;

if display_level >= 2 then
  begin
    display_interface Payment.a3a "PaymentA3a";
    display_interface Payment.a3a_dual "PaymentA3aDual";
    display_system (max_implementation Payment.a3a_dual) "PaymentA3aDualIO";
    display_interface Payment.a3b "PaymentA3b";
    display_interface Payment.a3b_dual "PaymentA3bDual";
    display_system (max_implementation Payment.a3b_dual) "PaymentA3bDualIO";
    display_interface Payment.assume "PaymentAssume";
    display_interface Payment.assume_dual "PaymentAssumeDual";
    display_system (max_implementation Payment.assume_dual) "PaymentAssumeDualIO";
    display_interface Payment.guarantee "PaymentGuarantee"
  end;;

if display_level >= 1 then
  display_interface Payment.contr "PaymentContract";;

(*** Global interface ***)

let global_interface =
  collate
    [
      EntryGate.contr;
      ExitGate.contr;
      Supervisor.contr;
      Payment.contr
    ];;

(*** Bogus global interface ***)

let global_interface' =
  collate
    [
      EntryGate.contr;
      ExitGate.contr;
      Supervisor.contr';
      Payment.contr
    ];;

(* Deal with correct requirements *)

info_interface global_interface;;

print_signature (signature_of_interface global_interface);;

if display_level >= 3 then
  display_interface global_interface "Global";;

consistency EntryGate.contr ExitGate.contr;;
consistency EntryGate.contr Supervisor.contr;;
consistency ExitGate.contr Supervisor.contr;;

(* Deal with bogus requirements *)

info_interface global_interface';;

compare global_interface global_interface';; (* the bogus requirements are weaker *)

(* Abstractions *)

let projGlobalEntry = simple (abstract [ng_op; ng_cl; v_en] global_interface)
and projGlobalExit = simple (abstract [xg_op; xg_cl; v_ex] global_interface)
and projGlobalCount = simple (abstract [v_en; v_ex; t_is; r_en] global_interface)
and projGlobalEntryCtrl = simple (abstract [v_en; ng_op; r_en; ng_cl; t_is; v_ex] global_interface)
and projGlobalExitCtrl = simple (abstract [v_ex; xg_op; r_ex; xg_cl] global_interface)
and projGlobalPayment = simple (abstract [t_ip; c_ip; x_is] global_interface);;

if display_level >= 2 then
  begin
    display_interface projGlobalEntry "GlobalProjEntry";
    display_interface projGlobalExit "GlobalProjExit";
    display_interface projGlobalCount "GlobalProjCount";
    display_interface projGlobalEntryCtrl "GlobalProjEntryCtrl";
    display_interface projGlobalExitCtrl "GlobalProjExitCtrl";
    display_interface projGlobalPayment "GlobalProjPayment"
  end;;

let prodProj =
  gather
    [
      projGlobalEntry;
      projGlobalExit;
      projGlobalCount;
      projGlobalEntryCtrl;
      projGlobalExitCtrl;
      projGlobalPayment
    ];;

compare prodProj global_interface;; (*  They are incomparable *)

(* Astractions of the bogus requirements *) 

let projGlobalEntry' = simple (abstract [ng_op; ng_cl; v_en] global_interface')
and projGlobalExit' = simple (abstract [xg_op; xg_cl; v_ex] global_interface')
and projGlobalCount' = simple (abstract [v_en; v_ex; t_is; r_en] global_interface')
and projGlobalEntryCtrl' = simple (abstract [v_en; ng_op; r_en; ng_cl; t_is; v_ex] global_interface')
and projGlobalExitCtrl' = simple (abstract [v_ex; xg_op; r_ex; xg_cl] global_interface')
and projGlobalPayment' = simple (abstract [t_ip; c_ip; x_is] global_interface');;

if display_level >= 2 then
  begin
    display_interface projGlobalEntry' "GlobalProjEntryPrime";
    display_interface projGlobalExit' "GlobalProjExitPrime";
    display_interface projGlobalCount' "GlobalProjCountPrime";
    display_interface projGlobalEntryCtrl' "GlobalProjEntryCtrlPrime";
    display_interface projGlobalExitCtrl' "GlobalProjExitCtrlPrime";
    display_interface projGlobalPayment' "GlobalProjPaymentPrime"
  end;;

let prodProj' =
  gather
    [
      projGlobalEntry';
      projGlobalExit';
      projGlobalCount';
      projGlobalEntryCtrl';
      projGlobalExitCtrl';
      projGlobalPayment'
    ];;

compare prodProj' global_interface';; (*  They are incomparable *)

compare prodProj prodProj';; (* the composition of the abstractions can not distinguish the correct and bogus requirements *)

(*** Architecture synthesis: iterated projections and quotients ***)

let glob0 = global_interface;;
info_interface glob0;;
print_signature (signature_of_interface glob0);;
is_complete glob0;;
let sigGlobPayment = new_signature (); input t_ip; input c_ip; output x_is; define_signature ();;
let projGlobPayment = simple (project sigGlobPayment glob0);;
info_interface projGlobPayment;;
if display_level >= 1 then
  display_interface projGlobPayment "projGlobPayment";;
assert (is_factor projGlobPayment glob0);;
let glob1 = simple (quotient glob0 projGlobPayment);;
assert (refines (product glob1 projGlobPayment) glob0);;
is_complete glob1;;
let sigGlobEntry = new_signature (); input r_en; output t_is; output ng_op; output ng_cl; input v_en; input v_ex; define_signature ();;
let projGlobEntry = simple (project sigGlobEntry glob1);;
info_interface projGlobEntry;;
if display_level >= 1 then
  display_interface projGlobEntry "projGlobEntry";;
assert (is_factor projGlobEntry glob1);;
let glob2 = simple (quotient glob1 projGlobEntry);;
assert (refines (product glob2 projGlobEntry) glob1);;
info_interface glob2;;
is_complete glob2;;
let sigGlobExit = new_signature (); output xg_op; output xg_cl; input v_ex; input r_ex; define_signature ();;
let projGlobExit = simple (project sigGlobExit glob1);;
info_interface projGlobExit;;
if display_level >= 1 then
  display_interface projGlobExit "projGlobExit";;
assert (is_factor projGlobExit glob2);;
let glob3 = simple (quotient glob2 projGlobExit);;
info_interface glob3;;
assert (refines (product glob3 projGlobExit) glob2);;
is_complete glob3;; (* Yes. We are done *)

(*** Architecture synthesis from the bogus requirements ***)

let glob0' = global_interface';;
info_interface glob0';;
print_signature (signature_of_interface glob0');;
is_complete glob0';;
let projGlobPayment' = simple (project sigGlobPayment glob0');;
info_interface projGlobPayment';;
if display_level >= 1 then
  display_interface projGlobPayment' "projGlobPaymentPrime";;
assert (is_factor projGlobPayment' glob0');;
let glob1' = simple (quotient glob0' projGlobPayment');;
assert (refines (product glob1' projGlobPayment') glob0');;
is_complete glob1';;
let projGlobEntry' = simple (project sigGlobEntry glob1');;
info_interface projGlobEntry';;
if display_level >= 1 then
  display_interface projGlobEntry' "projGlobEntryPrime";;
assert (is_factor projGlobEntry' glob1');;
let glob2' = simple (quotient glob1' projGlobEntry');;
assert (refines (product glob2' projGlobEntry') glob1');;
info_interface glob2';;
is_complete glob2';;
let projGlobExit' = simple (project sigGlobExit glob1');;
info_interface projGlobExit';;
if display_level >= 1 then
  display_interface projGlobExit' "projGlobExitPrime";;
assert (is_factor projGlobExit' glob2');;
let glob3' = simple (quotient glob2' projGlobExit');;
info_interface glob3';;
assert (refines (product glob3' projGlobExit') glob2');;
is_complete glob3';; (* Yes. We are done *)

(* How does it compare with the first architecture? *)

compare projGlobPayment projGlobPayment';; (* Payment machines are equivalent *)
compare projGlobEntry projGlobEntry';; (* Entry gates are equivalent *)
compare projGlobExit projGlobExit';; (* the exit gate derived from the bogus requirement is weaker *)

(* Independent implementations *)

let sysPayment = minimal (max_implementation projGlobPayment)
and sysEntry = minimal (max_implementation projGlobEntry)
and sysExit = minimal (max_implementation projGlobExit);;

if display_level >= 1 then
  begin
    display_system sysPayment "sysPayment";
    display_system sysEntry "sysEntry";
    display_system sysExit "sysExit"
  end;;

let system = parallel (parallel sysPayment sysEntry) sysExit;;

if display_level >= 3 then
  display_system system "system";;

assert (satisfies system global_interface);;

(* explore global_interface;; *)

(*** Manual architecture design ***)

(* The gates *)

module ComponentGenericGate = functor (G:GateProfileType) ->
struct

    (* Events *)
    
    let through = G.through
    let lift = G.lift
    let lower = G.lower

    (* Signature *)

    let signature =
      new_signature ();
      input through;
      input lift;
      output lower;
      define_signature ()

    (* Behaviour *)

    let behaviour =
      new_interface signature;
      init (int 0);
      must (int 0) lift (int 1);
      must (int 0) through (int 0);
      must (int 1) through (int 2);
      must (int 1) lift (int 1);
      must (int 2) lower (int 0);
      must (int 2) through (int 2);
      must (int 2) lift (int 2);
      define_interface ()
      
end;;

module ComponentGenericPayment = functor (P:PaymentProfileType) ->
struct

  (* Events *)

  let insert_ticket = P.insert_ticket
  let insert_coin = P.insert_coin
  let issue_exit_ticket = P.issue_exit_ticket

  (* Signature *)

  let signature =
    new_signature ();
    input insert_ticket;
    input insert_coin;
    output issue_exit_ticket;
    define_signature ()

  (* Behaviour *)

  let behaviour =
    new_interface signature;
    init (int 0);
    must (int 0) insert_ticket (int 1);
    must (int 1) insert_coin (int 2);
    must (int 2) issue_exit_ticket (int 0);
    may (int 0) insert_coin (int 0);
    may (int 1) insert_ticket (int 1);
    may (int 2) insert_ticket (int 2);
    may (int 2) insert_coin (int 2);
    define_interface ()

end;;

module GenericComponentEntryGateControl =
  functor (P:ParkingParametersType (* capacity *)) ->
struct

  (* Events *)

  let entry_request = r_en
  let entry_gate_open = ng_op
  let entry_gate_close = ng_cl
  let entry_ticket_issue = t_is
  let vehicle_enter = v_en
  let vehicle_exit = v_ex

  (* Capacity *)

  let capacity = P.capacity

  (* Signature *)

  let signature =
    new_signature ();
    input entry_request;
    input entry_gate_close;
    input vehicle_enter;
    input vehicle_exit;
    output entry_gate_open;
    output entry_ticket_issue;
    define_signature ()

  (* Behaviour *)

  let behaviour =
    let a i = tuple [| (ident "a"); (int i) |]
    and b i = tuple [| (ident "b"); (int i) |]
    and c i = tuple [| (ident "c"); (int i) |]
    and d i = tuple [| (ident "d"); (int i) |] in
      new_interface signature;
      init (a 0);
      for i=0 to capacity-1 do
	must (a i) entry_request (b i);
	must (b i) entry_request (b i);
	must (c i) entry_request (c i);
	must (d i) entry_request (d i);
	must (a i) entry_gate_close (a i);
	must (b i) entry_gate_close (a i);
	must (c i) entry_gate_close (a i);
	must (d i) entry_gate_close (a i);
	must (c i) entry_gate_open (d i);
	must (b i) entry_ticket_issue (c i);
	must (a i) vehicle_enter (a (i+1));
	must (b i) vehicle_enter (b (i+1));
	must (c i) vehicle_enter (c (i+1));
	must (d i) vehicle_enter (d (i+1));
      done;
      must (a capacity) entry_request (b capacity);
      must (b capacity) entry_request (b capacity);
      must (c capacity) entry_request (c capacity);
      must (d capacity) entry_request (d capacity);
      must (a capacity) entry_gate_close (a capacity);
      must (b capacity) entry_gate_close (a capacity);
      must (a capacity) vehicle_enter (a capacity);
      must (b capacity) vehicle_enter (b capacity);
      must (c capacity) vehicle_enter (c capacity);
      must (d capacity) vehicle_enter (d capacity);
      for i=1 to capacity do
	must (a i) vehicle_exit (a (i-1));
	must (b i) vehicle_exit (b (i-1));
	must (c i) vehicle_exit (c (i-1));
	must (d i) vehicle_exit (d (i-1));
      done;
      must (a 0) vehicle_exit (a 0);
      must (b 0) vehicle_exit (b 0);
      must (c 0) vehicle_exit (c 0);
      must (d 0) vehicle_exit (d 0);	
      define_interface ();

end;;

module ComponentExitGateControl =
struct

  (* Events *)

  let exit_ticket_insert = r_ex
  let exit_gate_open = xg_op
  let exit_gate_close = xg_cl

  (* Signature *)

  let signature =
    new_signature ();
    input exit_ticket_insert;
    output exit_gate_open;
    input exit_gate_close;
    define_signature ()

  (* Behaviour *)

  let behaviour =
    new_interface signature;
    init (int 0);
    must (int 0) exit_ticket_insert (int 1);
    must (int 1) exit_gate_open (int 2);
    for i=0 to 2 do
      must (int i) exit_gate_close (int 0)
    done;
    for i=1 to 2 do
      must (int i) exit_ticket_insert (int i)
    done;
    define_interface ();

end;;

(* Instances of generic components *)

module ComponentEntryGate = ComponentGenericGate(EntryGateProfile);;
module ComponentExitGate = ComponentGenericGate(ExitGateProfile);;
module ComponentEntryGateControl = GenericComponentEntryGateControl(ParkingParameters);;

if display_level >= 1 then
  begin
    display_interface ComponentEntryGate.behaviour "ComponentEntryGateBehaviour";
    display_interface ComponentExitGate.behaviour "ComponentExitGateBehaviour"
  end;;

module ComponentPayment = ComponentGenericPayment(PaymentProfile);;

if display_level >= 1 then
  begin
    display_interface ComponentPayment.behaviour "ComponentPaymentBehaviour";
    display_interface ComponentEntryGateControl.behaviour "ComponentEntryGateControlBehaviour";
    display_interface ComponentExitGateControl.behaviour "ComponentExitGateControlBehaviour"
  end;;

(* First design step: residuation by the entry gate *)

info_interface global_interface;;

is_complete global_interface;;

is_factor ComponentEntryGate.behaviour global_interface;;
is_factor ComponentEntryGateControl.behaviour global_interface;;

let global_interface1 =
  simple
    (quotient
       (quotient global_interface ComponentEntryGate.behaviour)
       ComponentEntryGateControl.behaviour);;

info_interface global_interface1;;

is_complete global_interface1;;

(* second design step: residuation by the exit gate *)

let global_interface2 =
  simple
    (quotient
       (quotient global_interface1 ComponentExitGate.behaviour)
       ComponentExitGateControl.behaviour);;

info_interface global_interface2;;

is_complete global_interface2;;

(* third design step : residuation by the payment machine *)

let global_interface3 = simple (quotient global_interface2 ComponentPayment.behaviour);;

info_interface global_interface3;;

is_complete global_interface3;;

(* We are done. *)

(* This means that the following specification : *)

let refined_interface =
  gather
    [
      ComponentEntryGate.behaviour;
      ComponentEntryGateControl.behaviour;
      ComponentExitGate.behaviour;
      ComponentExitGateControl.behaviour;
      ComponentPayment.behaviour
    ];;

info_interface refined_interface;;

if display_level >= 3 then
  display_interface refined_interface "Refined";;

(* Is a refinement of the global specification : *)

refines refined_interface global_interface;;

(* Independent implementations *)

let implementEntryGate = min_implementation ComponentEntryGate.behaviour
and implementEntryGateControl = min_implementation ComponentEntryGateControl.behaviour
and implementExitGate = min_implementation ComponentExitGate.behaviour
and implementExitGateControl = min_implementation ComponentExitGateControl.behaviour
and implementComponentPayment = min_implementation ComponentPayment.behaviour;;

if display_level >= 2 then
  begin
    display_system implementEntryGate "implementEntryGate";
    display_system implementEntryGateControl "implementEntryGateControl";
    display_system implementExitGate "implementExitGate";
    display_system implementExitGateControl "implementExitGateControl";
    display_system implementComponentPayment "implementComponentPayment"
  end;;

let implement =
  fold_parallel
    [
      implementEntryGate;
      implementEntryGateControl;
      implementExitGate;
      implementExitGateControl;
      implementComponentPayment
    ];;

if display_level >= 3 then
  display_system implement "implement";;

satisfies implement global_interface;;

info_interface (interface_of_system implement);;
