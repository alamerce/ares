#!/usr/bin/python3.5
# -*-coding:Utf-8 -*

#==============================================================================
# ARES - Main script
#------------------------------------------------------------------------------
# ...
#==============================================================================

#==============================================================================
# Importing required modules
#==============================================================================

import subprocess
import argparse
from scripts import utils
from enum import Enum, auto


#==============================================================================
# Parameters and useful function(s)
#==============================================================================

# Useful directories and references 
TEXT_SUFFIX = ".txt"
PREP_SUFFIX = ".prep"
FEATURES_SUFFIX = ".features" 
PREPROC_SUFFIX = ".preproc"
PREDICT_SUFFIX = ".pred"
POSTPROC_SUFFIX = ".postproc"
AMR_SUFFIX = ".amrep"
TEMP_SUFFIX = ".temp" 
INPUT_DIR = "input/"
WD = "wd"
WD_DIR = WD + "/"
CAMR_DIR = "camr/"
CAMR_WD = CAMR_DIR + WD
CAMR_PARSER = "amr_parsing.py"
CAMR_MODEL = "semeval.m"
CAMR_OUT_SUFFIX = ".txt.all.semeval.parsed"
STOG_DIR = "stog/"
STOG_WD = STOG_DIR + WD
ATP_DIR = "atp/"
ATP_WD = ATP_DIR + WD
ATP_JAR = "build/libs/atp-1.0-SNAPSHOT.jar"
MICA_DIR = "mica/"
MICA_LIB = "lib"
MICA_WD = MICA_DIR + WD
MICA_EX_PARKING = "parking.def"

# Commandes
COMMANDS = "config, prepare, parse, learn, evaluate, transform, model, clean"

# Enum of Filename Type
class FilenameType(Enum):
    BASE = auto()
    WD_BASE_TEXT = auto()
    WD_BASE_PREP = auto()
    WD_BASE_FEATURES = auto()
    WD_BASE_PREPROC = auto()
    WD_BASE_PREDICT = auto()
    WD_BASE_POSTPROC = auto()
    WD_BASE_AMR = auto()
    BASE_AMR = auto()
    CAMR_OUT = auto()
    WD_CAMR_OUT = auto()
    MICA_IN = auto()
    WD_MICA_IN = auto()  
    
# Verbose level
VERBOSE_MIN = 0
VERBOSE_MAX = 99
        
# Default values
DEFAULT_INPUT = "*"
NO_FILE = "_NO-FILE_"
DEFAULT_PARSER = "stog"
DEFAULT_VERBOSE = 1


# Function to get a filename according to name type and basename
def get_filename(name_type, basename):
    switcher = { 
        FilenameType.BASE: 
            basename, 
        FilenameType.WD_BASE_TEXT: 
            WD + "/" + basename + TEXT_SUFFIX, 
        FilenameType.WD_BASE_PREP: 
            WD + "/" + basename + PREP_SUFFIX, 
        FilenameType.WD_BASE_FEATURES: 
            WD + "/" + basename + FEATURES_SUFFIX, 
        FilenameType.WD_BASE_PREPROC: 
            WD + "/" + basename + PREPROC_SUFFIX, 
        FilenameType.WD_BASE_PREDICT: 
            WD + "/" + basename + PREDICT_SUFFIX,  
        FilenameType.WD_BASE_POSTPROC: 
            WD + "/" + basename + POSTPROC_SUFFIX, 
        FilenameType.WD_BASE_AMR: 
            WD + "/" + basename + AMR_SUFFIX, 
        FilenameType.BASE_AMR: 
            basename + AMR_SUFFIX, 
        FilenameType.CAMR_OUT: 
            basename + CAMR_OUT_SUFFIX, 
        FilenameType.WD_CAMR_OUT: 
            WD + "/" + basename + CAMR_OUT_SUFFIX, 
        FilenameType.MICA_IN: 
            MICA_EX_PARKING, 
        FilenameType.WD_MICA_IN: 
            WD + "/" + MICA_EX_PARKING, 
    } 
  
    # get() method of dictionary data type returns  
    # value of passed argument if it is present  
    # in dictionary otherwise second argument will 
    # be assigned as default value of passed argument 
    return switcher.get(name_type, basename) 
        

# Function to evaluate the verbose level
def is_verbose_level(verbose_level, expected_level):
    return verbose_level >= expected_level 


# Function to print a text according to verbose level
def verbose_print(args, level, text):
    indent = ""
    for i in range(level):
        indent += "-"
    if level > 0:
        indent += " "
    if is_verbose_level(args.verbose, level):
        print(indent + text)


#==============================================================================
# Scripts for subprocess
#==============================================================================
    
# Script references
SCRIPT_DIR = "scripts/" 
CONFIG_SCRIPT = SCRIPT_DIR + "config.py"
PREPARE_PARSING_SCRIPT = SCRIPT_DIR + "prepare_parse_data.py"
PREPARE_ATP_SCRIPT = SCRIPT_DIR + "prepare_atp_data.py"
CAMR_SCRIPT = SCRIPT_DIR + "camr_parse.py"
STOG_FEATURES_ANNOT_SCRIPT = SCRIPT_DIR + "stog_annotate_features.py"
#STOG_STARTING_SCRIPT = SCRIPT_DIR + "stog_starting.py"
STOG_PREPROCESS_SCRIPT = SCRIPT_DIR + "stog_preprocess.py"
STOG_PREDICT_SCRIPT = SCRIPT_DIR + "stog_predict.py"
STOG_POSTPROCESS_SCRIPT = SCRIPT_DIR + "stog_postprocess.py"
#STOG_ENDING_SCRIPT = SCRIPT_DIR + "stog_ending.py"
LEARNING_SCRIPT = SCRIPT_DIR + "learn.py"
EVALUATE_SCRIPT = SCRIPT_DIR + "evaluate.py"
TRANSFORM_SCRIPT = SCRIPT_DIR + "evaluate.py"
MICA_SCRIPT = SCRIPT_DIR + "mica_model.py"
CLEAN_SCRIPT = SCRIPT_DIR + "clean_working_data.py"

# Script: configuration
def config_script_call(args):
    valid_stage = True
    try:
        verbose_print(args, 1, "Configuration: NA")  
        script_ref = CONFIG_SCRIPT
        command_args = ["python3", script_ref]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: data preparation
def prepare_parsing_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 1, "Prepare data for parsing")
        script_ref = PREPARE_PARSING_SCRIPT
        input_files = args.input
        input_dir = INPUT_DIR
        output_dir = WD_DIR
        command_args = ["python3", script_ref, input_files, input_dir, output_dir]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: data preparation
def prepare_atp_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 1, "Prepare data for transduction processing")
        script_ref = PREPARE_ATP_SCRIPT
        input_files = args.input
        input_dir = INPUT_DIR
        output_dir = WD_DIR
        command_args = ["python3", script_ref, input_files, input_dir, output_dir]
        subprocess.run(command_args)
        if args.input2 != NO_FILE:
            input_files = args.input2
            command_args = ["python3", script_ref, input_files, input_dir, output_dir]
            subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: camr parsing
def camr_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 3, "CAMR Starting")
        script_ref = CAMR_SCRIPT
        work_file = get_filename(FilenameType.WD_BASE_TEXT, args.input) 
        command_args = ["python3", script_ref, work_file]
        subprocess.run(command_args)
        verbose_print(args, 3, "CAMR Ending")
        work_file = get_filename(FilenameType.WD_CAMR_OUT, args.input)
        output_file = get_filename(FilenameType.WD_BASE_AMR, args.input)
        #os.rename(work_file, output_file)
        utils.copy_file(work_file, output_file)
        utils.convert_to_amrep_format(output_file)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: stog starting
def stog_starting_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 3, "STOG Starting")
        subprocess.run(["cp", "-r", WD, STOG_DIR]) # copying input file(s)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: stog feature annotation
def stog_features_script_call(args):
    valid_stage = True
    try:
        verbose_print(args, 3, "Features annotation")
        script_ref = STOG_FEATURES_ANNOT_SCRIPT
        work_file = get_filename(FilenameType.WD_BASE_PREP, args.input)
        verbose = str(args.verbose)
        command_args = ["python3", script_ref, work_file, "--verbose", verbose]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: stog preprocessing
def stog_preprocess_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 3, "Preprocessing")
        script_ref = STOG_PREPROCESS_SCRIPT
        work_file = get_filename(FilenameType.WD_BASE_FEATURES, args.input)
        command_args = ["python3", script_ref, work_file]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: stog prediction
def stog_predict_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 3, "Prediction")
        script_ref = STOG_PREDICT_SCRIPT
        work_file = get_filename(FilenameType.WD_BASE_PREPROC, args.input)
        command_args = ["python3", script_ref, work_file]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: stog postprocessing
def stog_postprocess_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 3, "Postprocessing")
        script_ref = STOG_POSTPROCESS_SCRIPT
        work_file = get_filename(FilenameType.WD_BASE_PREDICT, args.input)
        command_args = ["python3", script_ref, work_file]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: stog ending
def stog_ending_script_call(args): 
    valid_stage = True
    try:
        verbose_print(args, 3, "STOG Ending")
        subprocess.run(["cp", "-r", STOG_WD, "."])
        work_file = get_filename(FilenameType.WD_BASE_POSTPROC, args.input)
        output_file = get_filename(FilenameType.WD_BASE_AMR, args.input)
        #os.rename(work_file, output_file)
        utils.copy_file(work_file, output_file)
        utils.convert_to_amrep_format(output_file)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: semantic transformation (learn)
def learn_script_call(args):
    valid_stage = True
    try:
        verbose_print(args, 1, "Semantic Transformation")
        script_ref = LEARNING_SCRIPT
        work_file_1 = get_filename(FilenameType.WD_BASE_AMR, args.input)
        work_file_2 = get_filename(FilenameType.WD_BASE_AMR, args.input2)
        utils.convert_to_amrep_format(work_file_1)
        utils.convert_to_amrep_format(work_file_2)
        command_args = ["python3", script_ref, work_file_1, work_file_2]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: semantic transformation (evaluate)
def evaluate_script_call(args):
    valid_stage = True
    try:
        verbose_print(args, 1, "Semantic Transformation")
        script_ref = EVALUATE_SCRIPT
        work_file = get_filename(FilenameType.WD_BASE_AMR, args.input)
        # utils.convert_to_amrep_format(work_file)
        command_args = ["python3", script_ref, work_file]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: semantic transformation (evaluate)
def transform_script_call(args):
    valid_stage = True
    try:
        verbose_print(args, 1, "Semantic Transformation")
        script_ref = TRANSFORM_SCRIPT
        work_file = get_filename(FilenameType.WD_BASE_AMR, args.input)
        # utils.convert_to_amrep_format(work_file)
        command_args = ["python3", script_ref, work_file]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: mica modeling
def mica_script_call(args):
    valid_stage = True
    try:
        verbose_print(args, 1, "System Modeling") 
        script_ref = MICA_SCRIPT
        work_file = get_filename(FilenameType.WD_MICA_IN, args.input)
        command_args = ["python3", script_ref, work_file]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage

# Script: work data cleaning
def clean_script_call(args):
    valid_stage = True
    try:
        verbose_print(args, 1, "Work Data Cleaning")  
        script_ref = CLEAN_SCRIPT
        command_args = ["python3", script_ref]
        subprocess.run(command_args)
    except:
        verbose_print(args, 3, "Unexpected error")
        valid_stage = False
    return valid_stage


#==============================================================================
# Script steps
#==============================================================================
    
def control_arguments(): 
    arg_parser = argparse.ArgumentParser(
            description="Abstract Requirements Extraction for Systems")   
    arg_parser.add_argument("command", 
                            help="command: " + COMMANDS)   
    arg_parser.add_argument("input", nargs='?',
                            default=DEFAULT_INPUT,
                            help="input document for working")   
    arg_parser.add_argument("input2", nargs='?',
                            default=NO_FILE,
                            help="second input document for working")  
    arg_parser.add_argument("--parser",
                            default=DEFAULT_PARSER,
                            help="parser: camr or stog")  
    arg_parser.add_argument("--verbose",
                            default=DEFAULT_VERBOSE,
                            type=int,
                            help="verbose level: 0, 1, .. , 9")  
    args = arg_parser.parse_args()
    verbose_print(args, 1, "Argument checking")
    verbose_print(args, 2, "command: " + args.command)
    verbose_print(args, 2, "input(s): " + args.input + ", " + args.input2)
    verbose_print(args, 2, "parser: " + args.parser)
    verbose_print(args, 2, "verbose: " + str(args.verbose))
    return args


def start_process(args):
    verbose_print(args, 1, "Process starting")
    #if args.input: # if input file(s)
        #subprocess.run(["cp", args.input, WD]) # copying input file(s)
        
    
def launch_requested_operation(args):
    """ Launch the requested operation according to command argument.
    """
    
    # Useful params
    is_valid_stage = True
    is_stog_start = False
    
    # Configuration
    if args.command == "config":
        config_script_call(args) 
    
    # Data preparation only
    elif args.command == "prepare" :
        prepare_parsing_script_call(args)
    
    # Parsing process (with CAMR or STOG)
    elif args.command == "parse":
        if is_valid_stage: 
            is_valid_stage = prepare_parsing_script_call(args)
        verbose_print(args, 1, "AMR Parsing")
        if is_valid_stage & (args.parser == "camr"):
            verbose_print(args, 2, "Launch transition-based parsing (CAMR)")
            camr_script_call(args)
        elif is_valid_stage & (args.parser == "stog"):
            verbose_print(args, 2, "Launch sequence-to-graph parsing (STOG)")
            if is_valid_stage: 
                is_valid_stage = stog_starting_script_call(args)
                is_stog_start = is_valid_stage
            if is_valid_stage: 
                is_valid_stage = stog_features_script_call(args)
            if is_valid_stage: 
                is_valid_stage = stog_preprocess_script_call(args)
            if is_valid_stage: 
                is_valid_stage = stog_predict_script_call(args)
            if is_valid_stage: 
                is_valid_stage = stog_postprocess_script_call(args)
            if is_stog_start:
                stog_ending_script_call(args)
                is_stog_start = False
        else:
            verbose_print(args, 1, "Error: unknown parser")
    
    # Semantic transformation
    elif args.command == "learn":
        if is_valid_stage: 
            is_valid_stage = prepare_atp_script_call(args)
        if is_valid_stage: 
            learn_script_call(args)
    
    # Semantic transformation
    elif args.command == "evaluate":
        if is_valid_stage: 
            is_valid_stage = prepare_atp_script_call(args)
        if is_valid_stage: 
            evaluate_script_call(args)
    
    # Semantic transformation
    elif args.command == "transform":
        transform_script_call(args)
    
    # Modeling
    elif args.command == "model":
        mica_script_call(args) 
    
    # Data Cleaning
    elif args.command == "clean":
        clean_script_call(args)
    
    # Unknown command
    else:
        verbose_print(args, 1, "Error: unknown command ")
        

#==============================================================================
# Main process
#==============================================================================

if __name__ == '__main__':
    print("ARES: Abstract Requirement Extraction for System")
    args = control_arguments()
    start_process(args)
    launch_requested_operation(args)
    print("Done")


