# ARES - Abstract Requirement Extraction for System

This software extract the requirements contained in one or more documents written in English. 
It provides as output a set of abstract definitions that can be used to model the behavior of systems.

The treatment is carried out in two main stages. 
Firstly, an AMR parser is used to produce some intermediate representations. 
In a second step, these representations are transformed to build sets of higher-level abstract definitions. 
The system entities, properties and requirements are differentiated in output.

This project is no longer under development. It is a prototype, designed for experimental purposes, and is not destined to be used any more.


## 1 - Environment Setup 

The code has been tested on Python 3.7, PyCoreNLP 0.3  and PyTorch 0.4.1. 
Create a conda environment following the directions in the ares_conda_env_configuration.txt.
All dependencies are also listed in ares_conda_env_requirements.txt. 


## 2 - Execution

The application runs in a terminal using the ares.py script (python3 ares.py <command> <args>).


## 3 - Steps / commands

Several commands are offered to execute the different steps.

CONFIG: command to configure the process.
PREPARE: command to prepare the date from input directory to working directory.
PARSE: command to parse the prepared file and evaluate AMR representations.
TRANSFORM: command to transform the amrep file into requirement definitions.
MODEL: command to model a system using requirement definitions.
CLEAN: command to clean the working directories.
