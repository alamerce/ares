lexer grammar AmrLexer;

options {
  language = Java;
}

@header {
  //package AMR;
}

// ignore whitespaces
WS : (' '|'\n'|'\t'|'\r'|'\u000C')+ -> skip
   ;

// ASCII
fragment ASCII  : ~('\n'|'/'|'('|')'|' '|':');

// keywords
SHARP : '#' ;
LPAREN : '(' ;
RPAREN : ')' ;
DIV : '/' ;
COLON : ':';
IDKEY : '::id';
SNTKEY : '::snt';
OUTKEY : '::out';

// other tokens
IDENT : (ASCII)+ { setText(getText().substring(0, getText().length())); };