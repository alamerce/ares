parser grammar AmrParser;

options {
  language = Java;
  tokenVocab = AmrLexer;
}

@header {
  //package AMR;
}

representationList returns [List<SourceAnalyzer.Amr.Asd.Representation> out]
  :                             { List<SourceAnalyzer.Amr.Asd.Representation> representationList = new ArrayList<>(); }
    ( r=representation          { representationList.add($r.out); } )*
    EOF                         { $out = representationList; }
  ;

representation returns [SourceAnalyzer.Amr.Asd.Representation out]
  : n=node                      { $out = new SourceAnalyzer.Amr.Asd.Representation($n.out); }
  | SHARP IDKEY id=IDENT
    n=node                      { $out = new SourceAnalyzer.Amr.Asd.Representation($id.text, $n.out); }
  | SHARP IDKEY id=IDENT
    SHARP SNTKEY                { String snt = ""; }
    ( s=IDENT                   { snt += $s.text + " "; } )*
    n=node                      { $out = new SourceAnalyzer.Amr.Asd.Representation($id.text, snt, $n.out); }
  | SHARP IDKEY id=IDENT
    SHARP SNTKEY                { String snt = ""; }
    ( s=IDENT                   { snt += $s.text + " "; } )*
                                { List<String> outList = new ArrayList<>(); }
    ( SHARP OUTKEY              { String out = ""; }
      ( o=IDENT                 { out += $o.text + " "; } )*
                                { outList.add(out); } )*
    n=node                      { $out = new SourceAnalyzer.Amr.Asd.Representation($id.text, snt, outList, $n.out); }
  ;

node returns [SourceAnalyzer.Amr.Asd.Node out]
  : LPAREN
    i=instance                  { SourceAnalyzer.Amr.Asd.Instance instance = $i.out; }
    DIV
    c=concept                   { SourceAnalyzer.Amr.Asd.Concept concept = $c.out; }
                                { List<SourceAnalyzer.Amr.Asd.Edge> edgeList = new ArrayList<>(); }
    ( e=edge                    { edgeList.add($e.out); } )*
                                { $out = new SourceAnalyzer.Amr.Asd.Node(instance, concept, edgeList ); }
    RPAREN
  | i=instance                  { SourceAnalyzer.Amr.Asd.Instance instance = $i.out;
                                  SourceAnalyzer.Amr.Asd.Concept concept = new SourceAnalyzer.Amr.Asd.Concept("");
                                  List<SourceAnalyzer.Amr.Asd.Edge> edgeList = new ArrayList<>();
                                  $out = new SourceAnalyzer.Amr.Asd.Node(instance, concept, edgeList); }
  ;

edge returns [SourceAnalyzer.Amr.Asd.Edge out]
  : COLON
    r=role                      { SourceAnalyzer.Amr.Asd.Role role = $r.out; }
    n=node                      { $out = new SourceAnalyzer.Amr.Asd.Edge(role, $n.out); }
  ;

instance returns [SourceAnalyzer.Amr.Asd.Instance out]
  : IDENT                       { $out = new SourceAnalyzer.Amr.Asd.Instance($IDENT.text); }
  ;

concept returns [SourceAnalyzer.Amr.Asd.Concept out]
  : IDENT                       { $out = new SourceAnalyzer.Amr.Asd.Concept($IDENT.text); }
  ;

role returns [SourceAnalyzer.Amr.Asd.Role out]
  : IDENT                       { $out = new SourceAnalyzer.Amr.Asd.Role($IDENT.text); }
  ;