package LogicalQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ValuesSet {

    //==================================================================
    // Attributes
    //==================================================================

    private List<String> baseList;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public ValuesSet(String... values) {
        this.baseList = new ArrayList<>();
        Collections.addAll(this.baseList, values);
    }

    public ValuesSet(List<String> values) {
        this.baseList = new ArrayList<>(values);
    }

    public ValuesSet(Set<String> values) {
        this.baseList = new ArrayList<>(values);
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    public List<String> getBaseList() {
        return baseList;
    }


    //==================================================================
    // Method(s)
    //==================================================================

    private void initValuations(int number, List<List<String>> valuationsList) {
        if (number > 0) {
            for (String v : baseList) {
                List<String> valuation = new ArrayList<>();
                valuation.add(v);
                valuationsList.add(valuation);
            }
        }
    }

    public List<String> substractList (List<String> a, List<String> b) {
        List<String> result = new ArrayList<>(a);
        for(String v1 : a) {
            for (String v2 : b) {
                if (v1.equals(v2)) {
                    result.remove(v1);
                }
            }
        }
        return result;
    }

    private List<List<String>> addElementToValuation(List<List<String>> initList) {
        List<List<String>> resultList = new ArrayList<>();
        for (List<String> valuation : initList) {
            List<String> othersValues = substractList(baseList, valuation);
            for (String v : othersValues) {
                List<String> newValuation = new ArrayList<>(valuation);
                newValuation.add(v);
                resultList.add(newValuation);
            }
        }
        return resultList;
    }

    public List<List<String>> getValuations(int number) {
        List<List<String>> valuationsList = new ArrayList<>();
        initValuations(number, valuationsList);
        for (int i = 1 ; i < number ; i++) {
            valuationsList = addElementToValuation(valuationsList);
        }
        return valuationsList;
    }


}
