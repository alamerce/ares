package LogicalQuery;

import java.util.*;

import static Parameters.Configuration.ABSTRACTION_MARK;

public class Atom implements Comparable<Atom>  {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final Boolean negation;
    private final String relation;
    private List<String> args;


    // ---------- Negation markers

    private static final String[] NEGATION_MARKERS = {
            "not ", "no "
    };


    //==================================================================
    // Method(s) to analyze the relation and extract the negation marker
    //==================================================================

    private Boolean extractNegation(String originRelation) {
        Boolean result = false;
        for (String negMark: NEGATION_MARKERS) {
            if (originRelation.toLowerCase().startsWith(negMark)) {
                result = true;
                break;
            }
        }
        return result;
    }

    private String extractRelation(String originRelation) {
        String result = originRelation;
        for (String negMark: NEGATION_MARKERS) {
            if (originRelation.toLowerCase().startsWith(negMark)) {
                result = originRelation.substring(negMark.length());
            }
        }
        return result;
    }

    //==================================================================
    // Constructor(s)
    //==================================================================

    public Atom(String relation, String ... args) {
        this.negation = extractNegation(relation);
        this.relation = extractRelation(relation);
        this.args = new ArrayList<>();
        this.args.addAll(Arrays.asList(args));
    }

    public Atom(Atom a) {
        this.negation = a.isNegation();
        this.relation = a.getRelation();
        this.args = new ArrayList<>();
        this.args.addAll(a.getArgs());
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public String getRelation() {
        return this.relation;
    }

    public List<String> getArgs() {
        return this.args;
    }


    // ---------- Predicates

    public Boolean isNegation() {
        return this.negation;
    }

    /**
     * An abstraction mark is used to identify an atom as abstract.
     */
    public Boolean isAbstract() {
        return this.relation.contains(ABSTRACTION_MARK);
    }

    /**
     * True if the atom is relative to a set of atoms, ie its concept
     * corresponds to a concept of the given atom set.
     */
    public Boolean isRelativeTo(Set<Atom> atomSet) {
        Boolean result = false;
        for (Atom compAtom: atomSet) {
            String concept1 = this.relation.toLowerCase();
            String concept2 = compAtom.getRelation().toLowerCase();
            result = result || concept1.equals(concept2);
        }
        return result;
    }

    //==================================================================
    // Base Method(s)
    //==================================================================

    // ---------- To String

    /**
     * Get an empty string if the current argument is the first argument,
     * a string with a comma (", ") else.
     *
     * @param firstArg (true if current argument is the first)
     * @return string with a comma only if the current argument is not
     * the first
     */
    private String getCommaIfNecessary(boolean firstArg) {
        String result = "";
        if (!firstArg) { result = ", "; }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (isNegation()) result.append("!");
        result.append(this.relation);
        if (this.args.size() > 0) {
            result.append("(");
            boolean firstArg = true;
            for (String arg : this.args) {
                result.append(getCommaIfNecessary(firstArg));
                result.append(arg);
                firstArg = false;
            }
            result.append(")");
        }
        return result.toString();
    }


    // ---------- Substitution

    /**
     * Substitute the argument a1 by a2.
     *
     * @param a1, a2 (arguments)
     */
    public void substitute(String a1, String a2) {
        if (this.args.contains(a1)) {
            int index = this.args.indexOf(a1);
            this.args.set(index, a2);
        }
    }


    // ---------- Comparison

    @Override
    public boolean equals(Object o) {
        return this.toString().equals(o.toString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(negation, getRelation(), getArgs());
    }

    @Override
    public int compareTo(Atom atom) {
        return this.toString().compareTo(atom.toString());
    }


    //==================================================================
    // Satisfaction Relation
    //==================================================================

    /**
     * True if the proposition arguments are compatible with a given
     * (partial) assignment, ie if the arguments of the proposition are
     * equal to the assigned values (only for the assigned values).
     *
     */
    private boolean isAssignmentCompatible(
            List<String> propArgs, Assignment assignment) {

        boolean result = true;

        for (int i = 0 ; i < propArgs.size() ; i++) {
            if (assignment.isAssigned(args.get(i))) {
                String assignValue = assignment.getValue(args.get(i));
                result = result && (assignValue.equals(propArgs.get(i)));
            }
        }

        return result;

    }

    /**
     * True if the atom is satisfiable by a given atomic proposition
     * and a given (partial) assignment.
     *
     */
    public boolean satisfy(Atom proposition, Assignment assignment) {
        String propRelation = proposition.getRelation();
        List<String> propArgs = proposition.getArgs();
        return ((this.relation.equals(propRelation))
                && (this.isNegation() == proposition.isNegation())
                && (this.args.size() == propArgs.size())
                && (isAssignmentCompatible(propArgs, assignment)));
    }
}
