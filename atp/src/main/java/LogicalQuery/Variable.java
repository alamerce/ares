package LogicalQuery;

public class Variable {

    //==================================================================
    // Attributes
    //==================================================================

    // Main attributes
    private final String ident;
    private String value;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Variable(String ident) {
        this.ident = ident;
        this.value = ident;
    }

    public Variable(String ident, String value) {
        this.ident = ident;
        this.value = value;
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    public String getIdent() {
        return this.ident;
    }

    public String getValue() {
        return this.value;
    }

    public String getCurrentSignature() {
        return this.ident + "-" + this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "(" +
                this.ident +
                ", " +
                this.value +
                ")";
    }

}
