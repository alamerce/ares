package LogicalQuery;

import java.util.*;

public class Assignment {

    //==================================================================
    // Attributes
    //==================================================================

    // Main attributes
    private final Set<String> variableIdSet;
    private Map<String, String> assignMap;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Assignment(Set<String> variableIdSet) {
        this.variableIdSet = variableIdSet;
        this.assignMap = new HashMap<>();
    }

    public Assignment(List<String> variableList) {
        this.variableIdSet = new HashSet<>();
        this.variableIdSet.addAll(variableList);
        this.assignMap = new HashMap<>();
    }

    public Assignment(Assignment assignment) {
        this.variableIdSet = new HashSet<>();
        this.variableIdSet.addAll(assignment.getVariableIdSet());
        this.assignMap = new HashMap<>();
        this.assignMap.putAll(assignment.getAssignMap());
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ----------- Getter(s)

    public Set<String> getVariableIdSet() {
        return variableIdSet;
    }

    public Set<String> getAssignVariableIdSet() {
        return assignMap.keySet();
    }

    public List<String> getAssignVariableValueList() {
        List<String> valueList = new ArrayList<>();
        valueList.addAll(assignMap.values());
        return valueList;
    }

    public Map<String, String> getAssignMap() {
        return assignMap;
    }

    public String getValue(String id) {
        String result = id;
        if (assignMap.containsKey(id)) {
            result = assignMap.get(id);
        }
        return result;
    }


    // ---------- Predicate(s)

    public boolean isCompleted() {
        return assignMap.size() == variableIdSet.size();
    }

    public boolean isVariable(String id) {
        return variableIdSet.contains(id);
    }

    public boolean isAssigned(String id) {
        return assignMap.containsKey(id);
    }

    public boolean isAssignedAll(Set<String> idSet) {
        boolean result = true;
        for (String id: idSet) result = result && isAssigned(id);
        return result;
    }


    // ---------- Setter(s) (assign)

    public void assign(String id, String value) {
        if (isVariable(id)
                && !isAssigned(id)) {
            assignMap.put(id, value);
        }
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Assignment{" +
                "variableSet=" + variableIdSet +
                ", assignment=" + assignMap +
                '}';
    }

}
