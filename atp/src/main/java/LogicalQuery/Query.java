package LogicalQuery;

import AbstractSemantic.Interpretation;
import Parameters.DefaultValues;
import Utility.IdCreator;

import java.util.*;

public class Query {

    //==================================================================
    // Attribute
    //==================================================================

    // ---------- Formula definition

    private static final String NORM_BASE_ID = "x";
    private List<String> variableIdList; // TODO: change to TreeSet (dev, test and validation)
    private List<Variable> variableList; // TODO: to remove (?)
    private List<Atom> atomList;
    private int precision;

    
    // ---------- Data current valuation
    
    private List<Atom> affectedAtomList; // TODO: to remove (?)


    // ---------- Satisfaction Result

    private boolean satResult;
    private List<Assignment> assignmentList;


    //==================================================================
    // Construction Method
    //==================================================================

    private void initVariableList(List<String> varList) {
        this.variableIdList = varList;
        this.variableList = new ArrayList<>();
        for(String idVar : varList) {
            this.variableList.add(new Variable(idVar));
        }
        this.precision = this.variableList.size(); // TODO: to remove?
    }

    private void initAtomList(List<Atom> atomList) {
        this.atomList = new ArrayList<Atom>();
        for(Atom a : atomList) {
            this.atomList.add(new Atom(a));
        }
    }

    private void initValuation(List<Atom> atomList) {
        this.affectedAtomList = new ArrayList<>();
        for(Atom a : atomList) {
            this.affectedAtomList.add(new Atom(a));
        }
    }

    private void initSatResult() {
        this.satResult = false;
        this.assignmentList = new ArrayList<>();
        this.assignmentList.add(new Assignment(this.getVariableIdSet()));
    }


    //==================================================================
    // Constructor
    //==================================================================

    public Query(List<String> variableList, List<Atom> atomList) {
        this.initVariableList(variableList);
        this.initAtomList(atomList);
        this.initValuation(atomList);
        this.initSatResult();
        this.sortDefinition();
    }

    public Query(List<String> variableList, Set<Atom> atomSet) {
        List<Atom> atomList = new ArrayList<>();
        atomList.addAll(atomSet);
        this.initVariableList(variableList);
        this.initAtomList(atomList);
        this.initValuation(atomList);
        this.initSatResult();
        this.sortDefinition();
    }

    public Query() {
        this.initVariableList(new ArrayList<>());
        this.initAtomList(new ArrayList<>());
        this.initValuation(new ArrayList<>());
        this.initSatResult();
        this.sortDefinition();
    }


    //==================================================================
    // Accessor
    //==================================================================

    // ---------- Getter (Base)

    public List<String> getVariableIdList() {
        return this.variableIdList;
    }

    public Set<String> getVariableIdSet() {
        Set<String> varIdSet = new HashSet<>();
        varIdSet.addAll(this.variableIdList);
        return varIdSet;
    }

    public List<Atom> getAtomList() {
        return atomList;
    }

    public int getVariablesNumber() {
        return variableList.size();
    }

    public int getPrecision() { // TODO: to remove (?)
        return precision;
    }


    // ---------- Predicate

    /**
     * A query is abstract if one of its atoms is abstract.
     */
    public boolean isAbstract() {
        boolean result = false;
        for (Atom atom: this.atomList)
            result = result || atom.isAbstract();
        return result;
    }

    /**
     * A query is relative to a set of atoms if it
     * contains an abstract atom included in the given set.
     */
    public boolean isRelativeTo(Set<Atom> atomSet) {
        boolean result = false;
        for (Atom atom: this.atomList)
            result = result
                    || (atom.isAbstract()
                    && (atom.isRelativeTo(atomSet)));
        return result;
    }

    /**
     * A query is deeper abstract relative to a set of atoms if it
     * contains an abstract atom not included in the given set.
     */
    public boolean isDeeperAbstract(Set<Atom> atomSet) {
        boolean result = false;
        for (Atom atom: this.atomList)
            result = result
                    || (atom.isAbstract()
                        && !(atom.isRelativeTo(atomSet)));
        return result;
    }


    // ---------- Getter/Predicate (Satisfaction Result)

    public boolean isSatisfiable() {
        return satResult;
    }

    public List<Assignment> getAssignmentList() {
        List<Assignment> resultList = new ArrayList<>();
        if (isSatisfiable()) resultList = this.assignmentList;
        return resultList;
    }


    // ---------- Setter (Base)

    public void changeVariableId(String actualId, String newId) {

        // -- Update Variable Id List
        for (int i = 0 ; i < variableIdList.size() ; i++) {
            if (variableIdList.get(i).equals(actualId))
                variableIdList.set(i, newId);
        }

        // -- Update Predicate List
        for (Atom a: this.atomList)
            a.substitute(actualId, newId);

    }

    public void sortDefinition() {
        Collections.sort(this.variableIdList);
        Collections.sort(this.atomList);
    }


    // ##########################################################
    // ---------- Old -> TODO: to remove (?)

    public List<Variable> getVariableList() { // TODO: to remove (?)
        return variableList;
    }

    public String getVariableId(int indexVar) { // TODO: to remove (?)
        String result = DefaultValues.NO_ID;
        if (indexVar >= 0 && indexVar < this.variableList.size()) {
            Variable v = this.variableList.get(indexVar);
            result = v.getIdent();
        }
        return result;
    }

    public String getVariableValue(String idVar) { // TODO: to remove (?)
        String result = DefaultValues.NO_VALUE;
        for(Variable v : variableList) {
            if (v.getIdent().equals(idVar)) {
                result = v.getValue();
                break;
            }
        }
        return result;
    }

    public void setPrecision(int precision) { // TODO: to remove (?)
        this.precision = precision;
    }

    private void setVariables(int i, String newValue) { // TODO: to remove (?)
        if (this.variableList.size() > i && i >= 0) {
            this.variableList.get(i).setValue(newValue);
        }
        sortDefinition();
    }

    private void setVariables(String id, String newValue) { // TODO: to remove (?)
        for(Variable v : variableList)
            if (v.getIdent().equals(id)) v.setValue(newValue);
        sortDefinition();
    }

    public List<String> getAffectedVarList() { // TODO: to remove (?)
        List<String> result = new ArrayList<>();
        for(Variable v : variableList) result.add(v.getValue());
        return result;
    }

    // ##########################################################

    //==================================================================
    // Base Method(s)
    //==================================================================

    /**
     * Get an empty string if the current argument is the first argument,
     * a string with AND ("AND ") else.
     *
     * @param firstArg (true if current argument is the first)
     * @return string with a comma only if the current argument is not
     * the first
     */
    private String getAndIfNecessary(boolean firstArg) {
        String result = "";
        if (!firstArg) { result = " AND "; }
        return result;
    }

    private String toString(List<String> vList, List<Atom> aList) {
        StringBuilder result = new StringBuilder();
        for (String v : vList) {
            result.append("lambda ");
            result.append(v);
            result.append(". ");
        }
        boolean firstArg = true;
        for (Atom a : aList) {
            result.append(getAndIfNecessary(firstArg));
            result.append(a);
            firstArg = false;
        }
        return result.toString();
    }

    @Override
    public String toString() {
        return this.toString(this.getVariableIdList(), this.getAtomList());
    }


    public String getValuationString() { // TODO: to remove (?)
        return this.toString(this.getAffectedVarList(), this.affectedAtomList);
    }

    @Override
    public boolean equals(Object o) {
        return this.toString().equals(o.toString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVariableIdList(), getAtomList());
    }

    //==================================================================
    // Normalizing Method
    //==================================================================

    /**
     * Normalize the query definition (using id creator for
     * bound variable id), pass 1.
     *
     * @return normalized id mapping
     */
    public Map<String, String> normalizePass1() {

        Map<String, String> normIdMap = new HashMap<>();

        // -- Rejected id: actual id and standardized id
        Set<String> rejectedId = new HashSet<>();
        rejectedId.addAll(this.variableIdList);
        for (int i = 0 ; i < this.variableIdList.size() ; i++)
            rejectedId.add(NORM_BASE_ID + i);
        IdCreator idCreator = new IdCreator(rejectedId);

        // -- Pass 1 to obtain id strictly independent of standardized id
        for (int i = 0 ; i < this.variableIdList.size() ; i++) {
            String actualId = variableIdList.get(i);
            String newId = idCreator.getNewId();
            normIdMap.put(actualId, newId);
            this.changeVariableId(actualId, newId);
        }

        // -- Sort variable id by order of appearance in the atom List
        List<String> newIdList = new ArrayList<>();
        for (Atom a: this.atomList)
            for (String arg: a.getArgs())
                if (!newIdList.contains(arg))
                    newIdList.add(arg);
        if (this.variableIdList.size() == newIdList.size()) {
            for (int i = 0 ; i < this.variableIdList.size() ; i++)
                this.variableIdList.set(i, newIdList.get(i));
        }

        return normIdMap;

    }

    /**
     * Normalize the query definition (using normalized names for
     * bound variable id), pass 2.
     *
     * @return normalized id mapping
     */
    public Map<String, String> normalizePass2() {

        Map<String, String> normIdMap = new HashMap<>();

        // -- Pass 2 to obtain the standardized id
        for (int i = 0 ; i < this.variableIdList.size() ; i++) {
            String actualId = variableIdList.get(i);
            String newId = NORM_BASE_ID + i;
            normIdMap.put(actualId, newId);
            this.changeVariableId(actualId, newId);
        }

        // -- Sort Definition
        this.sortDefinition();

        return normIdMap;

    }

    /**
     * Normalize the query definition (using normalized names for
     * bound variable id).
     */
    public void normalize() {
        this.normalizePass1();
        this.normalizePass2();
    }




    //==================================================================
    // Affectation Method(s) -> TODO: to remove (?)
    //==================================================================

    /**
     * Replacing all free occurrences of a given variable in the atomic formulas
     * by a new value.
     *
     * @param currentValue, newValue (variables)
     */
    private void substitute(String currentValue, String newValue) {
        for (Atom atom : this.affectedAtomList) {
            atom.substitute(currentValue, newValue);
        }
    }

    public void affectVariable(String idVar, String newValue) {

        // -- Updating variables
        this.setVariables(idVar, newValue);

        // -- Initialization of affected atom list
        this.affectedAtomList = new ArrayList<>();
        for(Atom a : this.atomList)
            this.affectedAtomList.add(new Atom(a));

        // -- Substitution in two stages (to handle the case where
        // the id of a variable is the value of another variable)
        for (Variable v : variableList)
            substitute(v.getIdent(), v.getCurrentSignature());
        for (Variable v : variableList)
            substitute(v.getCurrentSignature(), v.getValue());
    }

    public void affectVariable(int indexVar, String newValue) {
        this.affectVariable(this.getVariableId(indexVar), newValue);
    }

    public void affectVariables(String... newValuesTab) {
        for(int i = 0 ; i < newValuesTab.length ; i++) {
            this.affectVariable(i, newValuesTab[i]);
        }
    }

    public void affectVariables(List<String> newValuesList) {

        // -- Updating variables
        for(int i = 0 ; i < newValuesList.size() ; i++)
            this.setVariables(i, newValuesList.get(i));

        // -- Initialization of affected atom list
        this.affectedAtomList = new ArrayList<>();
        for(Atom a : this.atomList)
            this.affectedAtomList.add(new Atom(a));

        // -- Substitution in two stages (to handle the case where
        // the id of a variable is the value of another variable)
        for (Variable v : variableList)
            substitute(v.getIdent(), v.getCurrentSignature());
        for (Variable v : variableList)
            substitute(v.getCurrentSignature(), v.getValue());

    }


    //==================================================================
    // Satisfaction Methods (Old)
    //==================================================================

    /**
     * True if the world w satisfies the atomic formula a.
     *
     * @param atom (atomic formula), world (world as atom set)
     * @return True if world satisfies atom
     */
    public boolean satisfy(Set<Atom> world, Atom atom) {
        boolean result;
        if (atom.isNegation()) {
            result = true;
            for (Atom node: world)
                if (atom.equals(node))
                    result = false;
        } else {
            result = false;
            for (Atom node: world)
                if (atom.equals(node))
                    result = true;
        }
        return result;
    }


    /**
     * True if the world w satisfies the query.
     *
     * @param world (world as atom set)
     * @return True if the world satisfies the query
     *
     */
    public boolean isSatisfied(Set<Atom> world) {
        boolean result = true;
        for (Atom a: this.affectedAtomList)
            result = result && satisfy(world, a);
        return result;
    }


    /**
     * True if the world w satisfies the query.
     *
     * @param interpretation (with world as atom set)
     * @return True if the interpretation satisfies the query
     *
     */
    public boolean isSatisfied(Interpretation interpretation) {
        return isSatisfied(interpretation.getPredicateSet());
    }




    //==================================================================
    // Satisfaction Relation (New)
    //==================================================================

    public Assignment updateAssignment(Assignment assignment,
                                       Atom queryAtom, Atom propAtom) {

        List<String> queryArgs = queryAtom.getArgs();
        List<String > propArgs = propAtom.getArgs();

        assert queryArgs.size() == propArgs.size():
                ("LogicalQuery.Query: args disorder.");

        for (int i = 0 ; i < queryArgs.size() ; i++) {
            String a1 = queryArgs.get(i);
            String a2 = propArgs.get(i);
            assignment.assign(a1, a2);
        }

        return assignment;
    }


    public Set<String> getAssignedVariableIdList(List<Assignment> aList) {
        Set<String> assignVariableIdList = new HashSet<>();
        for (Assignment assignment: aList)
            assignVariableIdList.addAll(assignment.getAssignVariableIdSet());
        return  assignVariableIdList;
    }


    public List<Assignment> cleanInvalidAssignment(
            List<Assignment> aList, Set<String> assignVariableIdList) {

        List<Assignment> cleanAssignmentList = new ArrayList<>();

        for (Assignment assignment: aList) {
            if (assignment.isAssignedAll(assignVariableIdList))
                cleanAssignmentList.add(assignment);

        }

        return cleanAssignmentList;

    }

    public boolean satisfy(Interpretation interpretation) {

        // -- Satisfaction Result Initialization
        this.initSatResult();
        boolean currentSatResult = true;
        List<Assignment> currentAssignmentList = new ArrayList<>();
        currentAssignmentList.addAll(this.assignmentList);

        Set<Atom> propSet = interpretation.getPredicateSet();
        Iterator<Atom> it = this.atomList.iterator();
        List<Variable> notAssignedVariableList = variableList;

        // -- Main Loop
        while(currentSatResult
                && it.hasNext()
                && !notAssignedVariableList.isEmpty()) {

            Atom queryAtom = it.next();
            boolean atomSat = false;

            // int aListSize = currentAssignmentList.size(); // TODO: to remove
            List<Assignment> workAssignmentList = new ArrayList<>();
            int devCounter = 0; // TODO (DEV): to remove
            for (Assignment assignment: currentAssignmentList) {

                //Assignment assignment = currentAssignmentList.get(index); // TODO: to remove

                for (Atom propAtom: propSet) {
                    if (queryAtom.satisfy(propAtom, assignment)) {
                        //devCounter++;  // TODO (DEV): to remove
                        atomSat = true;
                        Assignment newAssignment = new Assignment(assignment);
                        newAssignment = updateAssignment(newAssignment, queryAtom, propAtom);
                        workAssignmentList.add(newAssignment);
                        //System.out.println(" ### DEV (Query-2A): " + devCounter);  // TODO (DEV): to remove
                        //System.out.println(" ### DEV (Query-2B): " + assignment.toString());  // TODO (DEV): to remove
                        //System.out.println(" ### DEV (Query-2C): " + newAssignment.toString());  // TODO (DEV): to remove
                    }
                }

            }

            // --- Update Current Result
            currentSatResult = currentSatResult && atomSat;
            currentAssignmentList.clear();
            currentAssignmentList.addAll(workAssignmentList);

            //System.out.println(" ### DEV (Query-1A): " + this.toString());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (Query-1B): " + queryAtom.toString());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (Query-1C): " + devCounter);  // TODO (DEV): to remove
            //System.out.println(" ### DEV (Query-1D): " + propSet.size());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (Query-1E): " + currentAssignmentList.size());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (Query-1F): " + currentSatResult);  // TODO (DEV): to remove
            //System.out.println(" ### DEV (Query-1G): " + currentAssignmentList.toString());  // TODO (DEV): to remove

            // Current Assignment List Cleaning // TODO: to remove
            //Set<String> assignedVariableIdList = this.getAssignedVariableIdList(currentAssignmentList);
            //currentAssignmentList = cleanInvalidAssignment(currentAssignmentList, assignedVariableIdList);

        }

        // -- Assignment Main List Cleaning (keep only complete assignments)
        this.assignmentList.clear();
        for (Assignment assignment: currentAssignmentList)
            if (assignment.isCompleted()) this.assignmentList.add(assignment);

        this.satResult = currentSatResult && (!assignmentList.isEmpty());

        return  this.satResult;

    }
}
