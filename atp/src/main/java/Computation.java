import AbstractSemantic.*;
import AbstractSemantic.LearnerObject.TrainingData;
import ComputationObject.*;
import LogicalQuery.Atom;
import SourceAnalyzer.Amr.Asd;
import SourceAnalyzer.Amr.Classifier;
import SourceAnalyzer.CapturedData;
import SourceAnalyzer.SourceType;
import SourceAnalyzer.SymbolTable.Table;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class Computation {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attribute(s)

    private static TransPatternSet transPatternSet = new TransPatternSet();

    private static String trainingDataFileName = "";
    private static List<WorkData> trainingDataList = new ArrayList<>();

    private static String evalDataFileName = "";
    private static List<WorkData> evalDataList = new ArrayList<>();


    // ---------- Processes Monitoring

    private static boolean learningSourceParseStepValid = false;
    private static boolean learningSymbolTableBuildStepValid = false;
    private static boolean patternLearnStepValid = false;

    private static boolean analysisSourceParseStepValid = false;
    private static boolean analysisSymbolTableBuildStepValid = false;
    private static boolean semanticAnalyzeStepValid = false;


    // ---------- Statistics

    private static Statistics dataStatistics = new Statistics("Analysis Data");



    //==================================================================
    // Accessors
    //==================================================================

    // ---------- Getters


    public static TransPatternSet getTransPatternSet() {
        return transPatternSet;
    }

    static List<WorkData> getTrainingDataList() { return trainingDataList; }

    static List<WorkData> getEvalDataList() { return evalDataList; }

    public static Statistics getDataStatistics() {
        return dataStatistics;
    }

// ---------- Getters (old) TODO: to remove (after revision of tests)

    static List<Asd.Representation> getAstList() {
        return Utility.getAstList(evalDataList);
    }

    static List<Table> getSymbolTableList() {
        return Utility.getSymbolTableList(evalDataList);
    }

    static Set<String> getEntitySet() {
        return Utility.getEntitySet(evalDataList);
    }

    static Set<String> getPropertySet() {
        return Utility.getPropertySet(evalDataList);
    }

    public static List<String> getResultStatementList() {
        return Utility.getResultStatementList(evalDataList);
    }


    // ---------- Predicates

    static boolean isLearningSourceParseStepValid() { return learningSourceParseStepValid; }
    static boolean isLearningSymbolTableBuildStepValid() { return learningSymbolTableBuildStepValid; }
    static boolean isPatternLearnStepValid() { return patternLearnStepValid; }

    static boolean isAnalysisSourceParseStepValid() { return analysisSourceParseStepValid; }
    static boolean isAnalysisSymbolTableBuildStepValid() { return analysisSymbolTableBuildStepValid; }
    static boolean isSemanticAnalyzeStepValid() { return semanticAnalyzeStepValid; }

    static boolean hasIdentifiedRequirement() { return !(getResultStatementList().isEmpty()); }


    // ---------- Setters (work data update)

    /**
     * Update work data set by adding new work data corresponding to
     * each parsed input representation, from an AST list.
     *
     * @param astList (List of abstract syntax trees)
     */
    private static void feedWorkData(
            List<Asd.Representation> astList, List<WorkData> dataList) {

        for (Asd.Representation ast: astList)
            dataList.add(new WorkData(ast));

    }


    //==================================================================
    // Processes
    //==================================================================

    /**
     * Initialize the computation processses, including working data and
     * processes monitoring.
     */
    private static void initialize() {

        // --- Working Data initialization
        trainingDataList.clear();
        evalDataList.clear();

        // --- Transduction Pattern Set Loading
        transPatternSet = new TransPatternSet();

        // --- Processes monitoring initialization
        analysisSourceParseStepValid = false;
        analysisSymbolTableBuildStepValid = false;
        patternLearnStepValid = false;
        semanticAnalyzeStepValid = false;

    }


    /**
     * Parse the source file, using Lexer and Parser, construct an AST
     * representation and feed the designed data list.
     *
     * @param dataFileName, dataList
     * @return True if step valid
     */
    private static Boolean parseSource(
            String dataFileName, List<WorkData> dataList) throws IOException {

        // --- Set Data Stream
        CharStream inputStream = CharStreams.fromPath(Paths.get(dataFileName));

        // --- Instantiate Lexer and Parser
        AmrLexer lexer = new AmrLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        AmrParser parser = new AmrParser(tokens);

        // --- Parsing and AST definition
        List<Asd.Representation> astList = parser.representationList().out;
        feedWorkData(astList, dataList);

        return (!astList.isEmpty());

    }


    /**
     * Analyze AST representations to build the symbol tables.
     *
     * @return True if step valid
     */
    private static Boolean buildSymbolTables(List<WorkData> dataList) {
        Table symbolTable;
        Classifier classifier = new Classifier(); // TODO: specific classifier AMR -> to move
        for (WorkData wd: dataList) {
            symbolTable = wd.getAst().analyse();
            symbolTable.categorize(classifier);
            wd.setSymbolTable(symbolTable);
        }
        return true;
    }


    /**
     * Analyze the training data (from symbol tables) to learn new
     * transduction patterns using AS.Learner.
     *
     * @return True if step valid
     */
    private static Boolean patternLearn(List<WorkData> dataList) {

        // -- Training Data Set Construction
        Set<TrainingData> trainingDataSet = new HashSet<>();
        int dataCount = 0;
        for (WorkData workData: dataList) {

            // Data Count Increment (to Training Data Ref. Number)
            dataCount++;

            // --- Capture data from symbol table
            Table table = workData.getSymbolTable();
            CapturedData capturedData = new CapturedData(SourceType.AMR);
            capturedData.analyze(table);
            workData.setCapturedData(capturedData);

            // --- Interpretation from Captured Data
            Map<String, Map<String, String>> dataMap = capturedData.getDataMap();
            Set<Atom> atomSet = capturedData.getAtomSet();
            Interpretation interpretation = new Interpretation(workData.getId(), dataMap, atomSet);

            // --- Update Training Data Set
            for(String expectedStatement: workData.getExpectedOutputStatementList())
                trainingDataSet.add(new TrainingData(dataCount, interpretation, expectedStatement));

        }

        // --- Learner Initalization
        NewLearner learner = new NewLearner(trainingDataSet);

        // --- Work Data Browsing
        learner.learnPatterns();
        //for (TrainingData trainingData: trainingDataSet) learner.learnPatterns(trainingData); // TODO: (DEV) to remove
        //System.out.println("Computation (learning) Dev-11"); // TODO: (DEV) to remove

        // --- Accuracies Estimation of Patterns (and cleaning the worst patterns)
        //System.out.println("Computation (learning) Dev-12"); // TODO: (DEV) to remove
        //learner.estimateAccuracies(); // TODO: to remove ?
        //System.out.println("Computation (learning) Dev-13"); // TODO: (DEV) to remove
        //learner.cleanWorstPatternsByPpi2(); // TODO: to remove ?
        //System.out.println("Computation (learning) Dev-14"); // TODO: (DEV) to remove

        // --- Update transduction pattern set from learning result
        transPatternSet.addAll(learner.getTransductionPatternSet());
        //System.out.println("Computation (learning) Dev-15"); // TODO: (DEV) to remove

        return true;

    }


    /**
     * Analyze the captured data (from symbol tables) to generate
     * statement(s) using AS.analyzer.
     *
     * @return True if step valid
     */
    private static Boolean semanticAnalyze(List<WorkData> dataList) {

        // --- Work Data Browsing
        CapturedData capturedData;
        Map<String, Map<String, String>> dataMap;
        Set<Atom> atomSet;
        //OldAnalyzer analyzer;
        NewAnalyzer analyzer;
        Interpretation interpretation;
        Set<OutputStatement> outputStatementSet;
        for (WorkData workData: dataList) {
            //System.out.println(" ///// SemanticAnalyze: WorkData Browse (2231)"); // TODO: (DEV) to remove
            //System.out.println(" ///// -- work data: " + workData.getId()); // TODO: (DEV) to remove

            // - Captured Data from Symbol Table
            Table table = workData.getSymbolTable();
            capturedData = new CapturedData(SourceType.AMR);
            capturedData.analyze(table);
            workData.setCapturedData(capturedData);

            // - Interpretation from Captured Data
            dataMap = capturedData.getDataMap();
            atomSet = capturedData.getAtomSet();
            interpretation = new Interpretation(workData.getId(), dataMap, atomSet);
            //System.out.println(" ///// -- interpretation: "); // TODO: (DEV) to remove
            //System.out.println(" ///// ----- concepts: " + interpretation.getConceptSet()); // TODO: (DEV) to remove
            //System.out.println(" ///// ----- instances: " + interpretation.getInstanceIdSet()); // TODO: (DEV) to remove
            //System.out.println(" ///// ----- predicates: " + interpretation.getPredicateSet()); // TODO: (DEV) to remove


//            analyzer = new OldAnalyzer(table.getReqId(), dataMap, atomSet, transPatternSet);
//            interpretation = analyzer.computeInterpretation();
//            outputStatementSet = analyzer.produceOutputStatementSet(interpretation);

            // - Generation of statement using AS.Analyzer and captured data
            // System.out.println(" ### DEV (C): " + interpretation); // TODO (DEV): to remove
            analyzer = new NewAnalyzer(interpretation, transPatternSet);
            //System.out.println(" ///// -- analyzer: " + analyzer.getStatementId()); // TODO: (DEV) to remove
            //System.out.println(" ///// ----- CTP Set Size: " + analyzer.getTransPatternSet().size()); // TODO: (DEV) to remove
            //System.out.println(" ///// ----- CTP Set Maximum Depth: " + analyzer.getTransPatternSet().getMaximumDepth()); // TODO: (DEV) to remove
            analyzer.updateMaximumInterpretation();
            //System.out.println(" ///// ----- Analyzer Update OK"); // TODO: (DEV) to remove
            //outputStatementSet = analyzer.getOutputStatementSet();
            outputStatementSet = analyzer.getBestOutputStatementSet();
            //System.out.println(" ///// -- OS Set: " + outputStatementSet); // TODO: (DEV) to remove

            // - WorkData Update
            workData.setInterpretation(interpretation);
            workData.setOutputStatementSet(outputStatementSet);
            workData.identifyOutputStatement(); // Add id to Output Statements

            // --- Work Data cleaning (removing duplicates of result statement)
            workData.removeResultStatementDuplicates();

        }

        return true;

    }



    //==================================================================
    // Main Methods
    //==================================================================

    /**
     * TODO: doc about this method
     */
    private static void learningProcess(String dataFileName) throws IOException {

        // --- Time Processing Evaluation (start)
        double startGlobalTime, startStepTime, endTime;
        startGlobalTime = System.currentTimeMillis();

        // --- Source Parsing
        startStepTime = System.currentTimeMillis();
        learningSourceParseStepValid = parseSource(dataFileName, trainingDataList);
        endTime = System.currentTimeMillis();
        dataStatistics.setLearningSourceParsingTime(endTime - startStepTime);

        // --- Symbol Tables Construction
        startStepTime = System.currentTimeMillis();
        if (learningSourceParseStepValid)
            learningSymbolTableBuildStepValid = buildSymbolTables(trainingDataList);
        endTime = System.currentTimeMillis();
        dataStatistics.setLearningSymbolTableConstructionTime(endTime - startStepTime);

        // --- Patterns Learning
        startStepTime = System.currentTimeMillis();
        if (learningSymbolTableBuildStepValid)
            patternLearnStepValid = patternLearn(trainingDataList);
        endTime = System.currentTimeMillis();
        dataStatistics.setLearningPatternLearningTime(endTime - startStepTime);

        // --- Time Processing Evaluation (end)
        endTime = System.currentTimeMillis();
        dataStatistics.setLearningGlobalTime(endTime - startGlobalTime);
        System.out.println("Computation (learning) Dev-30: " + Statistics.getTimeString(dataStatistics.getLearningGlobalTime())); // TODO: (DEV) to remove

    }


    /**
     * TODO: doc about this method
     */
    private static void analysisProcess(String dataFileName) throws IOException {

        // --- Time Processing Evaluation (start)
        double startGlobalTime, startStepTime, endTime;
        startGlobalTime = System.currentTimeMillis();

        // --- Source Parsing
        System.out.println(" ///// AnalysisProcess: Parsing (221)"); // TODO: (DEV) to remove
        startStepTime = System.currentTimeMillis();
        analysisSourceParseStepValid = parseSource(dataFileName, evalDataList);
        endTime = System.currentTimeMillis();
        dataStatistics.setProductionSourceParsingTime(endTime - startStepTime);

        // --- Symbol Tables Construction
        System.out.println(" ///// AnalysisProcess: Symbol Table Construction (222)"); // TODO: (DEV) to remove
        startStepTime = System.currentTimeMillis();
        if (analysisSourceParseStepValid)
            analysisSymbolTableBuildStepValid = buildSymbolTables(evalDataList);
        endTime = System.currentTimeMillis();
        dataStatistics.setProductionSymbolTableConstructionTime(endTime - startStepTime);

        // --- Semantic Analysis
        System.out.println(" ///// AnalysisProcess: Semantic Analysis (223)"); // TODO: (DEV) to remove
        startStepTime = System.currentTimeMillis();
        if (analysisSymbolTableBuildStepValid)
            semanticAnalyzeStepValid = semanticAnalyze(evalDataList);
        endTime = System.currentTimeMillis();
        dataStatistics.setProductionSemanticAnalysisTime(endTime - startStepTime);

        // --- Time Processing Evaluation (end)
        endTime = System.currentTimeMillis();
        dataStatistics.setProductionGlobalTime(endTime - startGlobalTime);

    }


    /**
     * Evaluation of result data by computing a F-Score (harmonic mean of the precision
     * and recall).
     *
     * @param dataList
     */
    private static void evaluationProcess(List<WorkData> dataList) throws IOException {

        // --- Time Processing Evaluation (start)
        double startGlobalTime, endTime;
        startGlobalTime = System.currentTimeMillis();

        // --- Output Statement Evaluations
        for(WorkData workData: dataList) workData.evaluateOutputStatement();

        // --- Relevant Sample and Produced Statement Lists
        List<String> relevantSampleList = new ArrayList<>();
        List<String> positiveStatementList = new ArrayList<>();
        List<String> negativeStatementList = new ArrayList<>();
        for(WorkData workData: dataList) {
            if (workData.isTransductionPerformed()) {
                relevantSampleList.addAll(workData.getRelevantSampleList());
                positiveStatementList.addAll((workData.getPositiveStatementList()));
                negativeStatementList.addAll(workData.getNegativeStatementList());
            }
        }

        // --- Relevant and positive numbers
        double relevantNumber = relevantSampleList.size();
        double truePositiveNumber = 0;
        double falsePositiveNumber = 0;
        for(String resultStatement: positiveStatementList)
            if(relevantSampleList.contains(resultStatement)) truePositiveNumber += 1;
            else falsePositiveNumber += 1;
        double allPositiveNumber = truePositiveNumber + falsePositiveNumber;
        double trueNegativeNumber = 0;
        for(String negStatement: negativeStatementList)
            if(relevantSampleList.contains(negStatement)) trueNegativeNumber += 1;

        // --- F-Score Computing
        double precision = truePositiveNumber / allPositiveNumber;
        double recall = (truePositiveNumber + trueNegativeNumber) / relevantNumber;
        double beta2 = Math.pow(Statistics.BETA_WEIGHT, 2);
        double fScore = (1 + beta2) * (precision * recall) / ((beta2 * precision) + recall);

/*        System.out.println("DEV DATA: "); // TODO: (DEV) to remove
        System.out.println(truePositiveNumber + ", " + allPositiveNumber +
                        ", " + trueNegativeNumber + ", " + relevantNumber + ".");*/

        // --- Statistics Update
        dataStatistics.setPrecision(precision);
        dataStatistics.setRecall(recall);
        dataStatistics.setFScore(fScore);

        // --- Time Processing Evaluation (end)
        endTime = System.currentTimeMillis();
        dataStatistics.setEvaluationGlobalTime(endTime - startGlobalTime);

    }


    /**
     * TODO: doc about this method
     */
    private static void reportingProcess(String dataFileName) throws FileNotFoundException {

        // --- Write Reporting (output files)
        Reporter reporter = new Reporter(dataFileName, evalDataList, dataStatistics);
        reporter.writeEntityFile();
        reporter.writePropertyFile();
        reporter.writeStatementFile();
        reporter.writeReportFile();

    }


    /**
     * Main Production Processing.
     */
    public static void main(String[] args) {

        try {

            // --- Initialize
            initialize();

            // --- Set file names from arguments
            switch(args.length) {

                case 0:
                    System.out.println("Module ATP called without parameter");
                    break;

                case 1:
                    System.out.println(" ///// Computation - Main case 1"); // TODO: (DEV) to remove
                    //transPatternSet.oldLoad(); // TODO: to remove
                    transPatternSet.newLoad(); // Load default pattern set
                    evalDataFileName = args[0];
                    analysisProcess(evalDataFileName);
                    evaluationProcess(evalDataList);
                    reportingProcess(evalDataFileName);
                    break;

                case 2:
                    System.out.println(" ///// Computation - Main case 2"); // TODO: (DEV) to remove
                    transPatternSet.clean();
                    trainingDataFileName = args[0];
                    System.out.println(" ///// Main: Learning Process (21)"); // TODO: (DEV) to remove
                    learningProcess(trainingDataFileName);
                    evalDataFileName = args[1];
                    System.out.println(" ///// Main: Analysis Process (22)"); // TODO: (DEV) to remove
                    analysisProcess(evalDataFileName);
                    System.out.println(" ///// Main: Eval Process (23)"); // TODO: (DEV) to remove
                    evaluationProcess(evalDataList);
                    System.out.println(" ///// Main: Report Process (24)"); // TODO: (DEV) to remove
                    reportingProcess(evalDataFileName);
                    break;

                default:
                    System.out.println("Module ATP called with wrong parameter(s)");
                    break;

            }

        } catch(IOException e) {

            e.printStackTrace();

        }


    }

}

