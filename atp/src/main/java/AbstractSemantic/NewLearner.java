package AbstractSemantic;

import AbstractSemantic.LearnerObject.*;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternObject.Token;
import ComputationObject.Statistics;
import LogicalQuery.Query;
import Parameters.Configuration;
import Utility.Tokenizer;
import Utility.VeruaGen;

import java.util.*;

import static Parameters.Configuration.OUTPUT_ABSTRACT_CONCEPT;

/**
 * A learner ...
 *
 * [...]
 *
 */
public class NewLearner {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Patterns for semantic transduction

    private final Set<TrainingData> trainingDataSet;
    private TransPatternSet patternSet;
    //private final Set<Archive> archiveSet; // TODO: to remove
    private final VeruaGen abstractConceptGen;

    // ---------- Learning Statistics
    // TODO


    //==================================================================
    // Constructor(s)
    //==================================================================

    public NewLearner(Set<TrainingData> trainingDataSet) {
        this.trainingDataSet = trainingDataSet;
        this.patternSet = new TransPatternSet();
        //this.archiveSet = new HashSet<>(); // TODO: to remove
        this.abstractConceptGen = new VeruaGen();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public TransPatternSet getTransductionPatternSet() {
        return patternSet;
    }

    public Set<TrainingData> getTrainingDataSet() {
        return trainingDataSet;
    }

    /*    public Set<Archive> getArchiveSet() { // TODO: to remove
        return archiveSet;
    }*/


    // ---------- Setters (memorize)

/*    public void memorize(Interpretation interpretation, String statement) { // TODO: to remove
        this.archiveSet.add(new Archive(interpretation, statement));
    }*/


    //==================================================================
    // Basic Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "NewLearner{" +
                "trainingDataSet=" + trainingDataSet +
                ", patternSet=" + patternSet +
                ", abstractConceptGen=" + abstractConceptGen +
                '}';
    }


    //==================================================================
    // Accuracy Method
    //==================================================================

    /**
     * TODO: doc about this method
     */
    private void estimatePatternAccuracy(TransPattern pattern) {

        pattern.initAccuracyParameters();

        for(TrainingData trainingData: this.trainingDataSet) {

            Interpretation interpretation =
                    trainingData.getInterpretationRun().getLastInterpretation();
            InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
            Query query = pattern.getQuery();

            if (query.satisfy(interpretation)) {
                String relevantStatement = trainingData.getNormalizedStatement();
                Boolean resultEval = (interpretationAnalyzer.evalPattern(pattern, relevantStatement));
                pattern.updatePrecisionFirstLine(resultEval);
            }

        }

        pattern.validatePrecision();

    }

    /**
     * TODO: doc about this method
     */
    private void estimatePatternAccuracies() {
        for(TransPattern pattern: this.patternSet.getSet()) {
            if (!pattern.isAssessedPrecision())
                this.estimatePatternAccuracy(pattern);
        }
    }


    /**
     * Keep only the patterns whose PPI (precision pattern indicator)
     * exceeds the expected minimum precision.
     */
    private TransPatternSet cleanInaccuratePatterns(TransPatternSet workSet) {
        TransPatternSet cleanSet = new TransPatternSet();
        double ppi1; // First Line PPI
        for(TransPattern tp: workSet.getSet()) {
            ppi1 = tp.getPrecisionFirstLine();
            if(ppi1 >= Configuration.MINIMAL_PRECISION) cleanSet.add(tp);
        }
        return cleanSet;
    }


    /**
     * Clean the pattern sets by removing patterns with worst accuracy (patterns with low
     * accuracy). Accuracy is recalculated at each cleaning.
     */
    public void cleanInaccuratePatterns() {
        //this.estimateAccuracies(); // TODO: to remove?
        this.estimatePatternAccuracies();
        this.patternSet = cleanInaccuratePatterns(this.patternSet);
        this.patternSet.cleanToKeepBestPatterns();
    }


    /**
     * Keep only the statements whose Second Line PPI (precision pattern indicator)
     * exceeds the expected minimum precision.
     */
    private TransPatternSet cleanWorstPatternsByPpi2(TransPatternSet workSet) { // TODO: to remove ?
        TransPatternSet cleanSet = new TransPatternSet();
        double ppi2; // Second Line PPI
        for(TransPattern tp: workSet.getSet()) {
            ppi2 = tp.getPrecisionSecondLine();
            if(ppi2 >= Configuration.MINIMAL_PRECISION) cleanSet.add(tp);
        }
        return cleanSet;
    }


    /**
     * Clean the pattern sets by removing the worst patterns (patterns with low
     * second line ppi). Accuracy is recalculated at each cleaning.
     * Ending process when the pattern set is stable.
     */
    public void cleanWorstPatternsByPpi2() { // TODO: to remove ?
        TransPatternSet workSet = this.patternSet;
        this.patternSet = cleanWorstPatternsByPpi2(this.patternSet);
        while (this.patternSet.size() < workSet.size()) {
            workSet = this.patternSet;
            this.estimateAccuraciesSecondLine();
            this.patternSet = cleanWorstPatternsByPpi2(this.patternSet);
        }
    }


    //==================================================================
    // Main Learning Method
    //==================================================================

    /**
     * Learn new templates from token list and target statement, using
     * interpretation analyzer.
     *
     * @param interpretationAnalyzer, tokenList, statement
     * @return set of templates
     */
    public static Set<Template> learnTemplates(
            InterpretationAnalyzer interpretationAnalyzer,
            String templateConcept,
            List<Token> tokenList,
            String statement) {

        // --- Find concordances between the definition and the statement
        Set<LearnAttribute> concordanceSet = interpretationAnalyzer.findConcordances(tokenList);

        // --- Infer templates from statement
        //String templateConcept = OUTPUT_ABSTRACT_CONCEPT; // TODO !!!
        TemplateInferenceFromTokenList templateInference =
                new TemplateInferenceFromTokenList(templateConcept, tokenList, concordanceSet);
        templateInference.inferTemplateSet();
        templateInference.cleanTemplateSet(interpretationAnalyzer.getInterpretation(), statement);

        return templateInference.getTemplateSet();

    }


    /**
     * Infer queries for all possible templates. Each pattern is tested
     * and added to pattern set only if the associated query is satisfied
     * by base interpretation.
     *
     * @param interpretationAnalyzer, statement, templateSet
     */
    public TransPatternSet inferQueries(
            InterpretationAnalyzer interpretationAnalyzer,
            String statement,
            Set<Template> templateSet) {

        TransPatternSet newPatternSet = new TransPatternSet();

        Query query;
        TransPattern pattern;
        for (Template template: templateSet) {
            query = interpretationAnalyzer.inferQuery(template);
            pattern = new TransPattern(query, template);
            //System.out.println("DEV 1: " + pattern.getQuery().toString()); // TODO: (DEV) to remove
            //System.out.println("DEV 2: " + pattern.getTemplate().getRawTemplateString()); // TODO: (DEV) to remove
            if (interpretationAnalyzer.testPattern(pattern, statement)) {
                //System.out.println("DEV 3: " + pattern.toString()); // TODO: (DEV) to remove
                newPatternSet.add(pattern);
            }
        }

        return newPatternSet;

    }


    /**
     * Learn new patterns from a list of tokens.
     *
     * @param interpretation, tokenList
     * @return New Pattern Set
     */
    public TransPatternSet learnPatternsFromTokenList(
            Interpretation interpretation, String templateConcept, List<Token> tokenList) {

        TransPatternSet newPatternSet;

        // --- Learning Preparation
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        String targetStatement = "";
        for(Token token: tokenList) targetStatement += token.getRawValue();

        // --- Template Learning
        Set<Template> templateSet =
                learnTemplates(interpretationAnalyzer, templateConcept, tokenList, targetStatement);

        // --- Queries inference and pattern set update
        newPatternSet = inferQueries(interpretationAnalyzer, targetStatement, templateSet);

        // --- Special case: identical template to the statement
/*        TransPattern specialPattern =
                new TransPattern(definitionAnalyzer.getMaximalQuery(), statement);
        this.patternSet.add(specialPattern);*/

        return newPatternSet;

    }


    /**
     * Learn new patterns from n-gram set of token list.
     *
     * @param trainingData
     */
    public void learnPatterns(int nGramSize, TrainingData trainingData) {

        Interpretation interpretation = trainingData.getInterpretationRun().getLastInterpretation();
        Tokenizer tokenizer = trainingData.getTokenizer();

        boolean isOutputPatterns = (tokenizer.getTokenListSize() == nGramSize);
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;

        TransPatternSet newPatternSet = new TransPatternSet();
        for (List<Token> subTokenList: tokenizer.getNGramTokenListSet(nGramSize)) {
            if (!isOutputPatterns) templateConcept = abstractConceptGen.getNewIoa();
            //System.out.println("Learner Dev-3A: " + templateConcept); // TODO: (DEV) to remove
            TransPatternSet learnPattern = learnPatternsFromTokenList(interpretation, templateConcept, subTokenList);
            newPatternSet.addAll(learnPattern);
            //System.out.println("Learner Dev-3C"); // TODO: (DEV) to remove
        }

        //int currentDepth = Math.max(nGramSize - 1, patternSet.getMinimalDepth()); // TODO: (DEV) to remove
        int currentDepth = Math.max(nGramSize, patternSet.getMinimalDepth());
        for (TransPattern p: newPatternSet.getSet()) p.setDepth(currentDepth);
        int testSize = currentDepth;
        for (TransPattern p: newPatternSet.getSet()) testSize = Math.min(testSize, p.getDepth());
        //System.out.println(" ----- Test Size 1: " + testSize); // TODO: (DEV) to remove

        this.patternSet.addAll(newPatternSet);
        for (TransPattern p: patternSet.getSet()) testSize = Math.max(testSize, p.getDepth());
        //System.out.println(" ----- Test Size 2: " + testSize); // TODO: (DEV) to remove

    }


    /**
     * Explore given definition and statement to learn new pattern(s).
     *
     * @param trainingData
     */
    public void learnPatterns(TrainingData trainingData) {

        // --- Learning Preparation
        //this.memorize(interpretation, statement);
        //Interpretation interpretation = trainingData.getInterpretation();
        String normalizedStatement = trainingData.getNormalizedStatement();
        Tokenizer tokenizer = new Tokenizer(normalizedStatement);

        // --- Pattern Learning from Token List
        int nGramSizeMax = tokenizer.getTokenListSize();
        //System.out.println("Learner Dev-1A: " + nGramSizeMax); // TODO: (DEV) to remove
        //for (int nGramSize = 1; nGramSize <= nGramSizeMax; nGramSize++) {
        for (int nGramSize = nGramSizeMax; nGramSize <= nGramSizeMax; nGramSize++) {

            //System.out.println("Learner Dev-2A: " + nGramSize); // TODO: (DEV) to remove
            //System.out.println("Learner Dev-2B: " + this.patternSet.size()); // TODO: (DEV) to remove

            // TODO: mise à jour interprétation en appliquant les patterns

            // -- Learn new patterns from n-gram set of token list
            this.learnPatterns(nGramSize, trainingData);

            // -- Cleaning inaccurate patterns
            this.cleanInaccuratePatterns();

            // TODO: fusion des patterns "équivalent"
            // TODO: calcul des profondeurs

        }

        //System.out.println("Learner Dev-4A: " + this.patternSet.size()); // TODO: (DEV) to remove

        patternSet.assessDepths();

    }


    public void learnPatterns(Interpretation interpretation, String statement) { // TODO (DEV): to remove
        TrainingData trainingData = new TrainingData(0, interpretation, statement);
        this.learnPatterns(trainingData);
    }

    /**
     * TODO: java doc
     */
    public void learnPatternsOld() {

        // -- Accuracies Initialization
        double startTime = System.currentTimeMillis(); // TODO (DEV): to remove
        for (TransPattern transPattern: this.patternSet.getSet())
            transPattern.initAccuracyParameters();
        System.out.println("NewLearner Timer-1: " + Statistics.getTimeString((System.currentTimeMillis() - startTime))); // TODO: (DEV) to remove

        // -- Main Learning Step
        startTime = System.currentTimeMillis(); // TODO (DEV): to remove
        for (TrainingData trainingData: this.trainingDataSet)
            this.learnPatterns(trainingData);
        System.out.println("NewLearner Timer-2: " + Statistics.getTimeString((System.currentTimeMillis() - startTime))); // TODO: (DEV) to remove

        // TODO: cleaning by equivalent query

        patternSet.assessDepths();

    }

    /**
     * TODO: to remove
     * Disappointing Old Algorithm
     */
    public void learnPatternsOld2() {

        // -- Accuracies Initialization
        double startTime = System.currentTimeMillis(); // TODO (DEV): to remove
        for (TransPattern transPattern: this.patternSet.getSet())
            transPattern.initAccuracyParameters();
        System.out.println("NewLearner Timer-1: " + Statistics.getTimeString((System.currentTimeMillis() - startTime))); // TODO: (DEV) to remove

        // -- Maximum Depth Estimation
        startTime = System.currentTimeMillis(); // TODO (DEV): to remove
        int trainingMaxDepth = 0;
        for (TrainingData trainingData: this.trainingDataSet)
            trainingMaxDepth = Math.max(trainingMaxDepth, trainingData.getMaxDepth());
        System.out.println("NewLearner Timer-2: " + Statistics.getTimeString((System.currentTimeMillis() - startTime))); // TODO: (DEV) to remove
        System.out.println("NewLearner Dev-1: " + trainingMaxDepth); // TODO: (DEV) to remove

        // -- Main Learning Step 1 - old
        startTime = System.currentTimeMillis(); // TODO (DEV): to remove
        for (int nGramSize = 1; nGramSize <= trainingMaxDepth; nGramSize++) {

            // --- Current Pattern Set Evaluation
            int currentDepth = nGramSize;
            Set<TransPattern> currentTransPatternSet = this.patternSet.getSet(currentDepth);
            System.out.println(" -- Current Depth: " + currentDepth); // TODO (DEV): to remove
            System.out.println(" -- Pattern Set: "); // TODO (DEV): to remove
            for (TransPattern p: patternSet.getSet(currentDepth)) // TODO (DEV): to remove
                System.out.println(p.getDepth() + " Q: " + p.getQuery().toString() + "  |  T: "
                        + "(" + p.getTemplate().getConcept() + ") -> "
                        + p.getTemplate().getRawTemplateString());

            for (TrainingData trainingData : this.trainingDataSet) {

                if (nGramSize <= trainingData.getMaxDepth()) {

                    // --- Learn new patterns from n-gram set of token list
                    System.out.println(" -- N Gram Size: " + nGramSize); // TODO (DEV): to remove
                    this.learnPatterns(nGramSize, trainingData);
                    //this.patternSet.assessDepths();

                    // --- Cleaning patterns
                    this.cleanInaccuratePatterns();

                    // TODO: fusion des patterns "équivalent"
                    // TODO: calcul des profondeurs

                }

            }

            System.out.println();
            System.out.println("<Training Step> " + nGramSize);
            System.out.println(" -- Pattern Set Size: " + this.patternSet.getSet().size());

        }
        System.out.println("NewLearner Timer-3: " + Statistics.getTimeString((System.currentTimeMillis() - startTime))); // TODO: (DEV) to remove

/*            // --- Update Interpretation Run
            InterpretationRun interRun = trainingData.getInterpretationRun();
            interRun.update(nGramSize, currentTransPatternSet);
            //System.out.println(" -- Last Interpretation Size(1): " +
            //interRun.getLastInterpretation().getInstanceSet().size()); // TODO (DEV): to remove
            //System.out.println(" -- Interpretation: " + interRun.getLastInterpretation().toString()); // TODO (DEV): to remove
            //System.out.println(" -- Instances: " + interRun.getLastInterpretation().getInstanceSet().toString()); // TODO (DEV): to remove
            interRun.alignInterpretationList();
            System.out.println(" -- Last Interpretation Size(2): " + interRun.getLastInterpretation().getInstanceSet().size()); // TODO (DEV): to remove
            System.out.println(" -- Interpretation (concepts): " + interRun.getLastInterpretation().getConceptSet().toString()); // TODO (DEV): to remove
            System.out.println(" -- Interpretation (instances): " + interRun.getLastInterpretation().getInstanceSet().toString()); // TODO (DEV): to remove
            System.out.println(" -- Interpretation (predicates): " + interRun.getLastInterpretation().getPredicateSet().toString()); // TODO (DEV): to remove*/

        // -- Assess Pattern Depths
        patternSet.assessDepths();

        // TODO: cleaning by equivalent query

    }


    /**
     * TODO: java doc
     */
    public void learnPatterns() {

        // -- Accuracies Initialization
        for (TransPattern transPattern: this.patternSet.getSet())
            transPattern.initAccuracyParameters();

        // -- Learning Step 1: Raw Output Pattern Learning
        for (TrainingData trainingData : this.trainingDataSet) {
            int nGramSize = trainingData.getMaxDepth();
            //System.out.println(" -- N Gram Size: " + nGramSize); // TODO (DEV): to remove
            this.learnPatterns(nGramSize, trainingData);
            this.cleanInaccuratePatterns();
        }

        // -- Learning Step 2: Segmentation (division into sub-concept)
        // TODO

        // -- Assess Pattern Depths
        patternSet.assessDepths();

    }


    //==================================================================
    // Old Accuracy Estimation Method(s):  // TODO: to remove?
    //==================================================================

    /**
     * TODO: doc about this method
     */
    private void estimateAccuraciesFirstLine() { // TODO: to remove?

        for(TransPattern pattern: this.patternSet.getSet()) {

            for(TrainingData trainingData: this.trainingDataSet) {

                Interpretation interpretation = trainingData.getInterpretationRun().getInitialInterpretation();
                InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
                Query query = pattern.getQuery();

                if (query.satisfy(interpretation)) {
                    String relevantStatement = trainingData.getNormalizedStatement();
                    Boolean resultEval = (interpretationAnalyzer.testPattern(pattern, relevantStatement));
                    pattern.updatePrecisionFirstLine(resultEval);
                }

            }
        }
    }


    /**
     * TODO: doc about this method
     */
    private void estimateAccuraciesSecondLine() { // TODO: to remove?

        for(TrainingData trainingData: this.trainingDataSet) {

            Interpretation interpretation = trainingData.getInterpretationRun().getInitialInterpretation();
            InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);

            // -- Find pattern which the request is verified by the statement
            Map<TransPattern, Double> firstLinePrecisionMap = new HashMap<>();
            for(TransPattern pattern: this.patternSet.getSet()) {
                Query query = pattern.getQuery();
                if (query.satisfy(interpretation))
                    firstLinePrecisionMap.put(pattern, pattern.getPrecisionFirstLine());
            }

            // -- Filter by precision (keep only the best)
            double bestPrecision = 0; // the initial best precision is null
            double toleranceLevel = 0.02; // TODO: move tolerance on parameters
            Set<TransPattern> bestPatternSet = new HashSet<>();
            for(Map.Entry<TransPattern, Double> e: firstLinePrecisionMap.entrySet()) {
                if(e.getValue() > (bestPrecision + toleranceLevel)) {
                    bestPatternSet.clear();
                    bestPatternSet.add(e.getKey());
                    bestPrecision = e.getValue();
                }
                else if(e.getValue() > bestPrecision) {
                    bestPatternSet.add(e.getKey());
                    bestPrecision = e.getValue();
                }
                else if(e.getValue() > (bestPrecision - toleranceLevel)) {
                    bestPatternSet.add(e.getKey());
                }
            }

            // -- Update precision second line for the best patterns
            for(TransPattern pattern: bestPatternSet) {
                Query query = pattern.getQuery();
                if (query.satisfy(interpretation)) {
                    String relevantStatement = trainingData.getNormalizedStatement();
                    Boolean resultEval = (interpretationAnalyzer.testPattern(pattern, relevantStatement));
                    pattern.updatePrecisionSecondLine(resultEval);
                }
            }

        }

    }


    /**
     * TODO: doc about this method
     */
    public void estimateAccuracies() { // TODO: to remove?
        estimateAccuraciesFirstLine();
        estimateAccuraciesSecondLine();
    }

}
