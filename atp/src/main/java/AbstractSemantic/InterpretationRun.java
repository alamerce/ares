package AbstractSemantic;

import LogicalQuery.Assignment;
import LogicalQuery.Atom;
import LogicalQuery.Query;
import Parameters.Configuration;
import Utility.IdCreator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static Parameters.Configuration.OUTPUT_ABSTRACT_CONCEPT;

public class InterpretationRun {

    //==================================================================
    // Attributes
    //==================================================================


    // ---------- Base Attribute

    private final String statementId;
    private final Interpretation initialInterpretation;
    private final List<Interpretation> interpretationList;
    private final IdCreator idCreator;


    //==================================================================
    // Construction Method
    //==================================================================

    public void initialize() {
        this.interpretationList.clear();
        this.interpretationList.add(this.initialInterpretation);
    }

    //==================================================================
    // Constructor
    //==================================================================

    public InterpretationRun(Interpretation initialInterpretation) {
        this.statementId = initialInterpretation.getStatementId();
        this.initialInterpretation = initialInterpretation;
        this.interpretationList = new ArrayList<>();
        this.initialize();
        this.idCreator = new IdCreator(initialInterpretation);
    }


    //==================================================================
    // Accessors
    //==================================================================

    // ---------- Getters

    public String getStatementId() {
        return statementId;
    }

    public Interpretation getInitialInterpretation() {
        return initialInterpretation;
    }

    public List<Interpretation> getInterpretationList() {
        return interpretationList;
    }

    public int getLastIndex() { return interpretationList.size() - 1; }

    public Interpretation getInterpretation(int index) {
        index = Math.max(0, Math.min(getLastIndex(), index));
        return interpretationList.get(index);
    }

    public Interpretation getLastInterpretation() {
        int lastIndex = this.getLastIndex();
        return this.getInterpretation(lastIndex);
    }


    //==================================================================
    // Interpretation Update Methods
    //==================================================================


    public void update(
            int round, TransPattern pattern, Assignment assignment) {

        // -- New interpretation by applying pattern
        String newId = idCreator.getNewId();
        Interpretation baseInterpretation = getInterpretation(round - 1);
        Interpretation newInterpretation = getInterpretation(round);
        Map<String, Instance> varMap = baseInterpretation.getInstanceMap(assignment);
        Interpretation interpretationFromPattern =
                pattern.getInterpretation(newId, varMap);

        //System.out.println(" ### DEV (NewAnalyzer-6A): " + round);  // TODO (DEV): to remove
        //System.out.println(" ### DEV (NewAnalyzer-6B): " + baseInterpretation.toString());  // TODO (DEV): to remove
        //System.out.println(" ### DEV (NewAnalyzer-6C): " + interpretationFromPattern.toString());  // TODO (DEV): to remove

        // -- Interpretation Extension (except for output abstract concept)
        List<String> valueList = assignment.getAssignVariableValueList();
        if (!pattern.getTemplate().getConcept().equals(OUTPUT_ABSTRACT_CONCEPT)) {
            //Set<Atom> newRelationSet = baseInterpretation.evalOutgoingRelation(valueList, newId);// TODO: to remove?
            //newInterpretation.addAllPredicate(newRelationSet);// TODO: to remove?
            newInterpretation.extendOutgoingRelation(valueList, newId);
        }

        // -- Union of interpretations
        newInterpretation.union(interpretationFromPattern);
        //newInterpretation.union(baseInterpretation);// TODO: to remove?

        //System.out.println(" ### DEV (NewAnalyzer-7A): " + newInterpretation.toString());  // TODO (DEV): to remove

    }


    /**
     * Update the interpretation by applying patterns whose depth
     * corresponds to the given round.
     *
     * @param round (equivalent to depth)
     */
    public void update(int round, Set<TransPattern> roundPatternSet) {

        // --- Update Initialization
        this.interpretationList.add(new Interpretation(getStatementId(), round));
        assert getLastIndex() == round:
                ("NewAnalyzer: Wrong size of interpretation list " +
                        "(" + round + ", " + getLastIndex() + ").");
        Interpretation baseInterpretation = getInterpretation(round - 1);

        Interpretation newInterpretation = getInterpretation(round);// TODO: to remove?
        newInterpretation.union(getInterpretation((round - 1)));// TODO: to remove?

        //System.out.println(" ### DEV (NewAnalyzer-2B): " + roundPatternSet.size());  // TODO (DEV): to remove

        // --- Analyze for each pattern
        //this.precisionLowLimit = 0; // use to keep the most precise requests // TODO: to remove
        int devCounter = 0; // TODO (DEV): to remove
        for (TransPattern pattern: roundPatternSet) {
            devCounter++; // TODO (DEV): to remove
            //System.out.println(" ### DEV (NewAnalyzer-3A): " + devCounter + " / " + roundPatternSet.size());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (NewAnalyzer-3B): " + baseInterpretation.getPredicateSet().size());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (NewAnalyzer-3C): " + baseInterpretation.getConceptSet());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (NewAnalyzer-3D): " + pattern.getQuery().toString());  // TODO (DEV): to remove

            Query query = pattern.getQuery();

            // Query evaluation only if the query precision valid the low limit // TODO (DEV): to remove
            //int queryPrecision = Math.min(query.getPrecision(), Configuration.LOW_LIMIT_MAX); // TODO (DEV): to remove
            //if (queryPrecision >= precisionLowLimit) { // TODO (DEV): to remove

            assert query.getVariablesNumber() <= Configuration.VAL_MAX:
                    ("Analyzer.generateFormattedStatementList: " +
                            "too variables on the query (limit imposed to keep proper performance).");

            // --- Search variable set which validates the query
            //System.out.println(" ### DEV (NewAnalyzer-4A): " + query.getVariablesNumber());  // TODO (DEV): to remove
            //System.out.println(" ### DEV (NewAnalyzer-4B): " + baseInterpretation.getInstanceIdSet());  // TODO (DEV): to remove

            // --- Update interpretation (if the query is satisfied)
            if (query.satisfy(baseInterpretation)) {

                for (Assignment assignment: query.getAssignmentList()) {
                    //System.out.println(" ### DEV (B7): " + query.getValuationString());  // TODO (DEV): to remove
                    this.update(round, pattern, assignment);
                }

            }

            //System.out.println(" ### DEV (NewAnalyzer-5A): END");  // TODO (DEV): to remove

        }

    }

    /**
     * Aligns all interpretations so that each new interpretation contains
     * only new instances.
     */
    public void alignInterpretationList() {
        int iMax = this.getLastIndex();
        //System.out.println(" ### DEV (InterpretationRun-10): -- iMax: " + iMax);  // TODO (DEV): to remove
        for (int i = 1 ; i < iMax ; i++) {
            Interpretation a = this.getInterpretation(i+1);
            //System.out.println(" ### DEV (InterpretationRun-10A): -- a: " + a.getInstanceIdSet());  // TODO (DEV): to remove
            Interpretation b = this.getInterpretation(i);
            //System.out.println(" ### DEV (InterpretationRun-10B): -- b: " + b.getInstanceIdSet());  // TODO (DEV): to remove
            a.difference(b);
            //System.out.println(" ### DEV (InterpretationRun-10A-B): -- a: " + a.getInstanceIdSet());  // TODO (DEV): to remove
        }
    }

}
