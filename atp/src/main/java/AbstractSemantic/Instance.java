package AbstractSemantic;

import SourceAnalyzer.SymbolTable.Symbol;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * An instance is a concrete occurrence of a concept.
 *
 * An entity is defined by an id and a concept, with some
 * additional features (modality, entity, property...).
 *
 */
public class Instance {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final String id;
    private final String concept;


    // ---------- Features

    private Map<String, String> featureMap = new HashMap<>();


    // ---------- Tracking Attribute

    private final boolean isAbstraction;
    private TransPattern producerPattern = new TransPattern();


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Instance(String id, String concept) {
        this.id = id;
        this.concept = concept;
        this.isAbstraction = false;
    }

    public Instance(Symbol symbol) {
        this.id = symbol.getIdent();
        this.concept = symbol.getConcept();
        this.isAbstraction = false;
    }

    public Instance(String id, String concept, TransPattern pattern) {
        this.id = id;
        this.concept = concept;
        this.isAbstraction = true;
        this.producerPattern = pattern;
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public String getId() {
        return this.id;
    }

    public String getConcept() {
        return this.concept;
    }

    public Set<String> getFeatureIdSet() {
        return this.featureMap.keySet();
    }

    public String getFeatureValue(String featureId) {
        String result = "";
        if (this.featureMap.containsKey(featureId)) {
            result = this.featureMap.get(featureId);
        }
        return result;
    }

    public double getPpi() {
        double ppi = -1;
        if (isAbstraction) ppi = producerPattern.getPrecisionFirstLine();
        return ppi;
    }

    public TransPattern getProducerPattern() {
        return producerPattern;
    }

    // ---------- Predicates

    public boolean hasId(String id) {
        return this.id.equals(id);
    }

    public boolean containsFeature(String featureId) {
        return this.featureMap.containsKey(featureId);
    }

    public boolean isAbstraction() { return this.isAbstraction; }


    // ---------- Setters (add)

    public void addFeature(String featureId, String featureValue) {
        if (this.featureMap.containsKey(featureId)) {
            this.featureMap.replace(featureId, featureValue);
        }
        else {
            this.featureMap.put(featureId, featureValue);
        }
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Instance{" +
                "id='" + id + '\'' +
                ", concept='" + concept + '\'' +
                ", featureMap=" + featureMap +
                '}';
    }

/*    @Override
    public String toString() {
        return "Instance{" +
                "id='" + id + '\'' +
                ", concept='" + concept + '\'' +
                ", featureMap=" + featureMap +
                ", isAbstraction=" + isAbstraction +
                ", producerPattern=" + producerPattern +
                '}';
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instance instance = (Instance) o;
        return getId().equals(instance.getId()) &&
                getConcept().equals(instance.getConcept());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getConcept());
    }

}
