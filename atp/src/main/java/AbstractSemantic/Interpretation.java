package AbstractSemantic;

import LogicalQuery.Assignment;
import LogicalQuery.Atom;
import LogicalQuery.Variable;

import java.util.*;

import static Parameters.Configuration.CONCEPT;
import static Parameters.Configuration.FEATURES;

/**
 * The AS Definition class is used to define the abstract semantics of a
 * statement. This definition is based on sets of concepts, concept
 * instances and predicates (atomic formula). Concepts, concept
 * instances and predicates are specified from an intermediate
 * representation of the statement.
 *
 */
public class Interpretation {

    //==================================================================
    // Attributes
    //==================================================================

    private final String statementId;
    private final int analysisDepth;
    private HashSet<String> conceptSet;
    private HashSet<Instance> instanceSet;
    private HashSet<Atom> predicateSet; // contains only predicates True


    //==================================================================
    // Construction Methods
    //==================================================================

    /**
     * Add concepts from data mapping.
     *
     * @param dataMap
     */
    public void initConcepts(Map<String, Map<String, String>> dataMap) {

        assert dataMap.containsKey(CONCEPT):
                ("Analyzer.addConcepts: No " + CONCEPT + " in dataMap");

        this.conceptSet = new HashSet<>();

        Map<String, String> conceptMap = dataMap.get(CONCEPT);
        for (Map.Entry<String, String> e : conceptMap.entrySet()) {
            this.conceptSet.add(e.getValue());
        }

    }

    /**
     * Characterize instance by adding features.
     *
     * @param instance (to characterize), dataMap
     * @return Characterized instance (with features)
     */
    public Instance characterizeInstance(
            Instance instance, Map<String, Map<String, String>> dataMap) {

        // Analyze each featureMap
        for (String featureId : FEATURES) {

            assert dataMap.containsKey(featureId):
                    ("Analyzer.characterizeInstance: No " + featureId + " in dataMap");

            String instanceId = instance.getId();
            Map<String, String> featureMap = dataMap.get(featureId);
            if (featureMap.containsKey(instanceId)) {
                String featureValue = featureMap.get(instanceId);
                instance.addFeature(featureId, featureValue);
            }

        }

        return instance;

    }

    /**
     * Extend interpretation by adding instances.
     *
     * @param dataMap
     */
    public void initInstances(Map<String, Map<String, String>> dataMap) {

        assert dataMap.containsKey(CONCEPT):
                ("Analyzer.addInstances: No " + CONCEPT + " in dataMap");

        this.instanceSet = new HashSet<>();

        Map<String, String> instanceMap = dataMap.get(CONCEPT);
        for (Map.Entry<String, String> e : instanceMap.entrySet()) {
            Instance instance = new Instance(e.getKey(), e.getValue());
            instance = characterizeInstance(instance, dataMap);
            if(!this.containInstance(instance)) this.instanceSet.add(instance);
        }

    }

    /**
     * Extend interpretation by adding predicates.
     *
     * @param atomSet
     */
    public void initPredicates(Set<Atom> atomSet) {
        this.predicateSet = new HashSet<>();
        for (Atom predicate: atomSet)
            if (!this.containPredicate(predicate))
                this.predicateSet.add(predicate);
    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Interpretation(String statementId, int analysisDepth) {
        this.statementId = statementId;
        this.analysisDepth = analysisDepth;
        this.conceptSet = new HashSet<String>();
        this.instanceSet = new HashSet<Instance>();
        this.predicateSet = new HashSet<Atom>();
    }

    public Interpretation(String statementId) {
        this.statementId = statementId;
        this.analysisDepth = 0;
        this.conceptSet = new HashSet<String>();
        this.instanceSet = new HashSet<Instance>();
        this.predicateSet = new HashSet<Atom>();
    }

    public Interpretation(String statementId, Set<Atom> atomSet) {
        this.statementId = statementId;
        this.analysisDepth = 0;
        this.conceptSet = new HashSet<String>();
        this.instanceSet = new HashSet<Instance>();
        this.initPredicates(atomSet);
    }

    public Interpretation(String statementId,
                          Map<String, Map<String, String>> dataMap,
                          Set<Atom> atomSet) {
        this.statementId = statementId;
        this.analysisDepth = 0;
        this.initConcepts(dataMap);
        this.initInstances(dataMap);
        this.initPredicates(atomSet);
    }


    //==================================================================
    // Accessors
    //==================================================================

    // ---------- Getters

    public String getStatementId() {
        return statementId;
    }

    public HashSet<String> getConceptSet() {
        return conceptSet;
    }

    public HashSet<Instance> getInstanceSet() {
        return instanceSet;
    }

    public HashSet<String> getInstanceIdSet() {
        HashSet<String> resultSet = new HashSet<String>();
        for (Instance instance : this.instanceSet) {
            resultSet.add(instance.getId());
        }
        return resultSet;
    }

    public Instance getInstance(String instanceId) {
        Instance instance = new Instance("-", "-");
        for(Iterator<Instance> e = instanceSet.iterator(); e.hasNext(); ) {
                 instance = e.next();
                 if (instance.getId().equals(instanceId)) break;
        }
        return instance;
    }

    public HashSet<Atom> getPredicateSet() {
        return predicateSet;
    }


    // ---------- Getters (predicate)

    public boolean containInstance(Instance i) {
        boolean result = false;
        for(String xId: this.getInstanceIdSet())
            if(xId.equals(i.getId())) {
                result = true;
                break;
            }
        return result;
    }

    public boolean containPredicate(Atom a) {
        boolean result = false;
        for(Atom x: this.getPredicateSet())
            if(x.equals(a)) {
                result = true;
                break;
            }
        return result;
    }


    // ---------- Setters (add)

    public void addConcept(String concept) {
        this.conceptSet.add(concept);
    }

    public void addInstance(Instance instance) {
        if(!this.containInstance(instance))
            this.instanceSet.add(instance);
    }

    public void addPredicate(Atom predicate) {
        if (!this.containPredicate(predicate))
            this.predicateSet.add(predicate);
    }

    public void addAllPredicate(Set<Atom> predicateSet) {
        for (Atom p: predicateSet) this.addPredicate(p);
    }

    public void removeInstance(Instance instance) { // TODO: test & validation
        this.instanceSet.remove(instance);
    }


    // ---------- Setters (align)

    /**
     * Align the different sets that make up the interpretation,
     * starting from the instances. Concepts or predicates
     * that do not refer to an existence instance are removed.
     */
    public void align() { // TODO: test & validation

        // -- Update Concept Set
        this.conceptSet.clear();
        for (Instance i: this.instanceSet) this.addConcept(i.getConcept());

        // -- Update Predicate Set
        HashSet<String> instanceIdSet = getInstanceIdSet();
        Set<Atom> updatePredicateSet = new HashSet<>();
        for (Atom p: this.predicateSet) {
            List<String> argList = p.getArgs();
            if (instanceIdSet.containsAll(argList))
                updatePredicateSet.add(p);
        }
        this.predicateSet.clear();
        this.predicateSet.addAll(updatePredicateSet);

    }


    // ---------- Operations

    /**
     * Gives relations relating to a variable group. Consider a group
     * of variables. These variables are grouped and replaced by a new
     * identifier. The group's outgoing relationships are duplicated
     * with the new identifier. A relation (predicate) is said to be outgoing
     * if it relates a group variable, and a variable outside the group.
     *
     * @param variableValueList (Variable Group), groupId (Unique id for the group)
     */
    public Set<Atom> evalOutgoingRelation(
            List<String> variableValueList, String groupId) {

        Set<Atom> newRelationSet = new HashSet<>();

        // -- Keep the outgoing predicates
        // (i.e. relating a variable on the given group and a variable
        // outside the group)
        Set<Atom> outgoingRelationSet = new HashSet<>();
        for(Atom p: this.getPredicateSet()) {
            boolean c1 = false;
            boolean c2 = false;
            for (String arg: p.getArgs()) {
                boolean c = false;
                for (String value: variableValueList) {
                    if (value.equals(arg)) c = true;
                }
                if (c) c1 = true;
                else c2 = true;
            }
            if (c1 && c2) outgoingRelationSet.add(p);
        }

        // -- Extend outgoing relation with new id
        for (String oldId: variableValueList) {
            for(Atom p: outgoingRelationSet) {
                Atom extendP = new Atom(p);
                extendP.substitute(oldId, groupId);
                if (!extendP.equals(p)) newRelationSet.add(extendP);
            }
        }

        return newRelationSet;

    }


    /**
     * Extend relations relating to a variable group. Consider a group
     * of variables. These variables are grouped and replaced by a new
     * identifier. The group's outgoing relationships are duplicated
     * with the new identifier. A relation (predicate) is said to be outgoing
     * if it relates a group variable, and a variable outside the group.
     *
     * @param variableValueList (Variable Group), groupId (Unique id for the group)
     */
    public void extendOutgoingRelation(
            List<String> variableValueList, String groupId) {

        Set<Atom> newRelationSet =
                evalOutgoingRelation(variableValueList, groupId);
        if (!newRelationSet.isEmpty())
            this.addAllPredicate(newRelationSet);

    }


    public void union(Interpretation op) {
        for(String c: op.getConceptSet()) this.addConcept(c);
        for(Instance i: op.getInstanceSet()) this.addInstance(i);
        for(Atom p: op.getPredicateSet()) this.addPredicate(p);
    }

    public void difference(Interpretation op) { // TODO: test & validation
        for(Instance i: op.getInstanceSet()) this.removeInstance(i);
        this.align();
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(" <Interpretation ");
        result.append(this.statementId + ">:\n");
        result.append("  ----- concepts = " + this.conceptSet.toString() + "\n");
        result.append("  ----- instances = " + this.getInstanceIdSet().toString() + "\n");
        result.append("  ----- predicates = " + this.predicateSet.toString() + "\n");
        return result.toString();
    }


    /**
     * Gives a default instance mapping where the keys are the instance id and
     * the values are the instance.
     *
     * @return Instance Map (key: id, value: instance)
     */
    public Map<String, Instance> getDefaultInstanceMap() {

        Map<String, Instance> result = new HashMap<>();
        Instance instance;

        // Browse of instance id list
        for (String instanceId : this.getInstanceIdSet()) {
            instance = this.getInstance(instanceId);
            result.put(instanceId, instance);
        }

        return result;

    }


    /**
     * Gives an instance mapping where the keys are the variable id and
     * the values are the instance.
     *
     * @param variableList
     * @return Instance Map (key: id, value: instance)
     */
    public Map<String, Instance> getInstanceMap(List<Variable> variableList) {

        Map<String, Instance> result = new HashMap<>();
        Instance instance;

        // Browse of instance id list
        for (Variable v : variableList) {

            String id = v.getIdent();
            String instanceId = v.getValue();

            assert this.getInstanceIdSet().contains(instanceId) :
                    ("Definition.getInstanceMap: No instance " + instanceId + " in instanceSet");

            instance = this.getInstance(instanceId);
            result.put(id, instance);

        }

        return result;

    }


    /**
     * Gives an instance mapping where the keys are the variable id and
     * the values are the instance.
     *
     * @param assignment (variable assignment)
     * @return Instance Map (key: id, value: instance)
     */
    public Map<String, Instance> getInstanceMap(Assignment assignment) {

        Map<String, Instance> result = new HashMap<>();
        Instance instance;

        // Browse of instance id list
        for (Map.Entry<String, String> e: assignment.getAssignMap().entrySet()) {

            String id = e.getKey();
            String instanceId = e.getValue();

            assert this.getInstanceIdSet().contains(instanceId) :
                    ("Definition.getInstanceMap: No instance " + instanceId + " in instanceSet");

            instance = this.getInstance(instanceId);
            result.put(id, instance);

        }

        return result;

    }

}
