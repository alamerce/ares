package AbstractSemantic;

import AbstractSemantic.LearnerObject.OldArchive;
import AbstractSemantic.LearnerObject.InterpretationAnalyzer;
import AbstractSemantic.LearnerObject.LearnAttribute;
import AbstractSemantic.LearnerObject.TemplateInferenceFromTokenList;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternObject.Token;
import LogicalQuery.Query;
import Parameters.Configuration;
import Utility.Tokenizer;
import Utility.VeruaGen;

import java.util.*;

import static Parameters.Configuration.NORMALIZER;
import static Parameters.Configuration.OUTPUT_ABSTRACT_CONCEPT;

/**
 * A learner ...
 *
 * [...]
 *
 */
public class OldLearner {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Patterns for semantic transduction

    private TransPatternSet patternSet;
    private final Set<OldArchive> archiveSet;
    private final VeruaGen abstractConceptGen;

    // ---------- Learning Statistics
    // TODO


    //==================================================================
    // Constructor(s)
    //==================================================================

    public OldLearner() {
        this.patternSet = new TransPatternSet();
        this.archiveSet = new HashSet<>();
        this.abstractConceptGen = new VeruaGen();
    }

    public OldLearner(TransPatternSet patternSet) {
        this.patternSet = patternSet;
        this.archiveSet = new HashSet<>();
        this.abstractConceptGen = new VeruaGen();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public TransPatternSet getTransductionPatternSet() {
        return patternSet;
    }

    public Set<OldArchive> getArchiveSet() {
        return archiveSet;
    }


    // ---------- Setters (memorize)

    public void memorize(Interpretation interpretation, String statement) {
        this.archiveSet.add(new OldArchive(interpretation, statement));
    }


    //==================================================================
    // Basic Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Learner{" +
                "patternSet=" + patternSet +
                '}';
    }


    //==================================================================
    // Main Learning Method(s)
    //==================================================================

    /**
     * Learn new templates from token list and target statement, using
     * interpretation analyzer.
     *
     * @param interpretationAnalyzer, tokenList, statement
     * @return set of templates
     */
    public static Set<Template> learnTemplates(
            InterpretationAnalyzer interpretationAnalyzer,
            String templateConcept,
            List<Token> tokenList,
            String statement) {

        // --- Find concordances between the definition and the statement
        Set<LearnAttribute> concordanceSet = interpretationAnalyzer.findConcordances(tokenList);

        // --- Infer templates from statement
        //String templateConcept = OUTPUT_ABSTRACT_CONCEPT; // TODO !!!
        TemplateInferenceFromTokenList templateInference =
                new TemplateInferenceFromTokenList(templateConcept, tokenList, concordanceSet);
        templateInference.inferTemplateSet();
        templateInference.cleanTemplateSet(interpretationAnalyzer.getInterpretation(), statement);

        return templateInference.getTemplateSet();

    }


    /**
     * Infer queries for all possible templates. Each pattern is tested
     * and added to pattern set only if the associated query is satisfied
     * by base interpretation.
     *
     * @param interpretationAnalyzer, statement, templateSet
     */
    public TransPatternSet inferQueries(
            InterpretationAnalyzer interpretationAnalyzer,
            String statement,
            Set<Template> templateSet) {

        TransPatternSet newPatternSet = new TransPatternSet();

        Query query;
        TransPattern pattern;
        for (Template template: templateSet) {
            query = interpretationAnalyzer.inferQuery(template);
            pattern = new TransPattern(query, template);
            //System.out.println("DEV 1: " + pattern.getQuery().toString()); // TODO: (DEV) to remove
            //System.out.println("DEV 2: " + pattern.getTemplate().getRawTemplateString()); // TODO: (DEV) to remove
            if (interpretationAnalyzer.testPattern(pattern, statement)) {
                //System.out.println("DEV 3: " + pattern.toString()); // TODO: (DEV) to remove
                this.patternSet.add(pattern);
            }
        }

        return newPatternSet;

    }


    /**
     * Learn new patterns from a list of tokens.
     *
     * @param interpretation, tokenList
     */
    public void learnPatternsFromTokenList(
            Interpretation interpretation, String templateConcept, List<Token> tokenList) {

        // --- Learning Preparation
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        String targetStatement = "";
        for(Token token: tokenList) targetStatement += token.getRawValue();

        // --- Template Learning
        Set<Template> templateSet =
                learnTemplates(interpretationAnalyzer, templateConcept, tokenList, targetStatement);

        // --- Queries inference and pattern set update
        TransPatternSet newPatternSet =
                this.inferQueries(interpretationAnalyzer, targetStatement, templateSet);
        this.patternSet.addAll(newPatternSet);

        // --- Special case: identical template to the statement
/*        TransPattern specialPattern =
                new TransPattern(definitionAnalyzer.getMaximalQuery(), statement);
        this.patternSet.add(specialPattern);*/

    }


    /**
     * Learn new patterns from n-gram set of token list.
     *
     * @param interpretation, statement
     */
    public void learnPatterns(int nGramSize, Interpretation interpretation, List<Token> tokenList) {

        int iMax = tokenList.size() - nGramSize;
        boolean isOutputPatterns = (tokenList.size() == nGramSize);
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;
        for (int i = 0; i <= iMax; i++) {
            if (!isOutputPatterns) templateConcept = abstractConceptGen.getNewIoa();
            System.out.println("Learner Dev-3A: " + i + ", " + templateConcept); // TODO: (DEV) to remove
            List<Token> subTokenList = tokenList.subList(i, (i + nGramSize));
            System.out.println("Learner Dev-3B: " + subTokenList.toString()); // TODO: (DEV) to remove
            this.learnPatternsFromTokenList(interpretation, templateConcept, subTokenList);
            System.out.println("Learner Dev-3C"); // TODO: (DEV) to remove
        }

    }


    /**
     * Explore given definition and statement to learn new pattern(s).
     *
     * @param interpretation, statement
     */
    public void learnPatterns(Interpretation interpretation, String statement) {

        // --- Learning Preparation
        this.memorize(interpretation, statement);
        String normalizedStatement = NORMALIZER.normalize(statement);
        Tokenizer tokenizer = new Tokenizer(normalizedStatement);
        List<Token> statementTokenList = tokenizer.getTokenList();

        // --- Pattern Learning from Token List
        int nGramSizeMax = statementTokenList.size();
        this.learnPatterns(nGramSizeMax, interpretation, statementTokenList);

        System.out.println("Learner Dev-4A: " + this.patternSet.size()); // TODO: (DEV) to remove

    }


    //==================================================================
    // Accuracy Estimation Method(s)
    //==================================================================


    /**
     * TODO: doc about this method
     */
    private void estimateAccuraciesFirstLine() {

        for(TransPattern pattern: this.patternSet.getSet()) {

            for(OldArchive archive: this.archiveSet) {

                Interpretation interpretation = archive.getInterpretation();
                InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
                Query query = pattern.getQuery();

                if (query.satisfy(interpretation)) {
                    String relevantStatement = archive.getNormalizedStatement();
                    Boolean resultEval = (interpretationAnalyzer.testPattern(pattern, relevantStatement));
                    pattern.updatePrecisionFirstLine(resultEval);
                }

            }
        }
    }


    /**
     * TODO: doc about this method
     */
    private void estimateAccuraciesSecondLine() {

        for(OldArchive archive: this.archiveSet) {

            Interpretation interpretation = archive.getInterpretation();
            InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);

            // -- Find pattern which the request is verified by the statement
            Map<TransPattern, Double> firstLinePrecisionMap = new HashMap<>();
            for(TransPattern pattern: this.patternSet.getSet()) {
                Query query = pattern.getQuery();
                if (query.satisfy(interpretation))
                    firstLinePrecisionMap.put(pattern, pattern.getPrecisionFirstLine());
            }

            // -- Filter by precision (keep only the best)
            double bestPrecision = 0; // the initial best precision is null
            double toleranceLevel = 0.02; // TODO: move tolerance on parameters
            Set<TransPattern> bestPatternSet = new HashSet<>();
            for(Map.Entry<TransPattern, Double> e: firstLinePrecisionMap.entrySet()) {
                if(e.getValue() > (bestPrecision + toleranceLevel)) {
                    bestPatternSet.clear();
                    bestPatternSet.add(e.getKey());
                    bestPrecision = e.getValue();
                }
                else if(e.getValue() > bestPrecision) {
                    bestPatternSet.add(e.getKey());
                    bestPrecision = e.getValue();
                }
                else if(e.getValue() > (bestPrecision - toleranceLevel)) {
                    bestPatternSet.add(e.getKey());
                }
            }

            // -- Update precision second line for the best patterns
            for(TransPattern pattern: bestPatternSet) {
                Query query = pattern.getQuery();
                if (query.satisfy(interpretation)) {
                    String relevantStatement = archive.getNormalizedStatement();
                    Boolean resultEval = (interpretationAnalyzer.testPattern(pattern, relevantStatement));
                    pattern.updatePrecisionSecondLine(resultEval);
                }
            }

        }

    }


    /**
     * TODO: doc about this method
     */
    public void estimateAccuracies() {
        estimateAccuraciesFirstLine();
        estimateAccuraciesSecondLine();
    }


    //==================================================================
    // Cleaning Method(s)
    //==================================================================

    /**
     * Keep only the statements whose Second Line PPI (precision pattern indicator)
     * exceeds the expected minimum precision.
     */
    private TransPatternSet cleanWorstPatterns(TransPatternSet workSet) {
        TransPatternSet cleanSet = new TransPatternSet();
        double ppi2; // Second Line PPI
        for(TransPattern tp: workSet.getSet()) {
            ppi2 = tp.getPrecisionSecondLine();
            if(ppi2 >= Configuration.MINIMAL_PRECISION) cleanSet.add(tp);
        }
        return cleanSet;
    }

    /**
     * Clean the pattern sets by removing the worst patterns (patterns with low
     * second line ppi). Accuracy is recalculated at each cleaning.
     * Ending process when the pattern set is stable.
     */
    public void cleanWorstPatterns() {
        TransPatternSet workSet = this.patternSet;
        this.patternSet = cleanWorstPatterns(this.patternSet);
        while (this.patternSet.size() < workSet.size()) {
            workSet = this.patternSet;
            this.estimateAccuraciesSecondLine();
            this.patternSet = cleanWorstPatterns(this.patternSet);
        }
    }



}
