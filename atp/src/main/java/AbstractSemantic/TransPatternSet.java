package AbstractSemantic;

import AbstractSemantic.TransPatternObject.QueryMap;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternObject.TemplateMap;
import LogicalQuery.Atom;
import LogicalQuery.Query;
import Parameters.Configuration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static Parameters.Configuration.TRANS_PATTERN_SET_DEPTH_LIMIT;

/**
 * Set of trans(duction) pattern. ...
 *
 * [...]
 *
 */
public class TransPatternSet {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Patterns for semantic transduction
    private HashSet<TransPattern> set;
    private static final int MINIMAL_DEPTH = 1;
    private int maximumDepth;


    // ---------- Learning Statistics
    // TODO


    //==================================================================
    // Construction Method(s)
    //==================================================================

    private void initSet() {
        this.set = new HashSet<>();
        this.maximumDepth = MINIMAL_DEPTH;
    }

    public void assessMaximumDepth() {;
        this.maximumDepth = MINIMAL_DEPTH;
        for (TransPattern p: this.set)
            this.maximumDepth = Math.max(p.getDepth(), maximumDepth);
    }

    public void assessDepths() {

        this.maximumDepth = MINIMAL_DEPTH;
        Set<Atom> workAtomSet1 = new HashSet<>();
        Set<TransPattern> workPatternSet1 = new HashSet<>();
        //Set<TransPattern> resultPatternSet = new HashSet<>();  // TODO: to remove?

        //System.out.println(" ### DEV (TransPatternSet-1A+): " + maximumDepth);  // TODO (DEV): to remove
        //System.out.println(" ### DEV (TransPatternSet-1B): " + workAtomSet1.toString());  // TODO (DEV): to remove

        for (TransPattern pattern: this.set) {
            pattern.setDepth(MINIMAL_DEPTH);
            if (pattern.isAbstract()) {
                pattern.incDepth();
                workPatternSet1.add(pattern);
            }
            else {
                workAtomSet1.add(pattern.getTemplate().getPredicate());
                //resultPatternSet.add(pattern);  // TODO: to remove?
            }
        }

        if (!workPatternSet1.isEmpty()) this.maximumDepth++;

        //System.out.println(" ### DEV (TransPatternSet-2A): " + maximumDepth);  // TODO (DEV): to remove
        //System.out.println(" ### DEV (TransPatternSet-2B): " + workAtomSet1.toString());  // TODO (DEV): to remove

        Set<Atom> workAtomSet2 = new HashSet<>();
        Set<TransPattern> workPatternSet2 = new HashSet<>();

        boolean fixpoint = false;
        while ((!fixpoint) &&
        //while((!workPatternSet1.isEmpty()) &&
                (maximumDepth <= TRANS_PATTERN_SET_DEPTH_LIMIT)) {

            workAtomSet2.clear();
            workPatternSet2.clear();

            for (TransPattern pattern: workPatternSet1) {
                if (pattern.isDeeperAbstract(workAtomSet1)) {
                    pattern.incDepth();
                    workPatternSet2.add(pattern);
                }
                else {
                    workAtomSet2.add(pattern.getTemplate().getPredicate());
                }
            }

            if (workPatternSet1.equals(workPatternSet2)) fixpoint = true;

            if (!workPatternSet2.isEmpty()) this.maximumDepth++;

            workAtomSet1.addAll(workAtomSet2);
            workPatternSet1.clear();
            workPatternSet1.addAll(workPatternSet2);

            //System.out.println(" ### DEV (TransPatternSet-3A): " + maximumDepth);  // TODO (DEV): to remove
            //System.out.println(" ### DEV (TransPatternSet-3B): " + workAtomSet1.toString());  // TODO (DEV): to remove

        }

        //System.out.println(" ### DEV (TransPatternSet-4): " + maximumDepth);  // TODO (DEV): to remove

        assert this.maximumDepth <= TRANS_PATTERN_SET_DEPTH_LIMIT:
                ("AS.TransPatternSet: " +
                        "depth limit exceeded " +
                        "(" + this.maximumDepth + "/" +
                        TRANS_PATTERN_SET_DEPTH_LIMIT + ").");

    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public TransPatternSet() {
        this.initSet();
        this.assessDepths();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getter(s)

    public HashSet<TransPattern> getSet() {
        return this.set;
    }


    public HashSet<TransPattern> getSet(int depth) {
        this.assessMaximumDepth();
        int targetDepth = Math.min(maximumDepth, depth);
        HashSet<TransPattern> result = new HashSet<>();
        for (TransPattern p: this.set)
            if (p.getDepth() == targetDepth) result.add(p);
        return result;
    }


    public HashSet<TransPattern> getSet(Query query) {
        HashSet<TransPattern> result = new HashSet<>();
        for (TransPattern p: this.set)
            if (p.getQuery().equals(query)) result.add(p);
        return result;
    }


    public int getMinimalDepth() {
        return MINIMAL_DEPTH;
    }

    public int getMaximumDepth() {
        return maximumDepth;
    }


    // ---------- Predicate(s)

    public boolean isFinalRound(int round) {
        return (round >= this.maximumDepth);
    }

    public boolean contains(Query query) {
        boolean result = false;
        for (TransPattern p: this.getSet()) {
            if (p.getQuery().equals(query)) {
                result = true;
                break;
            }
        }
        return result;
    }


    // ---------- Getters/Setters (extraction)

    public HashSet<TransPattern> extractSet(Query query) {
        HashSet<TransPattern> originSet = new HashSet<>();
        originSet.addAll(this.set);
        HashSet<TransPattern> result = new HashSet<>();
        this.set.clear();
        for (TransPattern p: originSet)
            if (p.getQuery().equals(query)) result.add(p);
            else this.set.add(p);
        return result;
    }


    // ---------- Setters (add)

    public void add(TransPattern pattern) {
        // TODO: analysis/optimization for pattern adding
        //  (avoid duplicates, merging of patterns, ...)
        this.set.add(pattern);
        //this.assessDepths(); // TODO: to remove?
    }

    public void addAll(TransPatternSet set) {
        this.set.addAll(set.getSet());
        //this.assessDepths(); // TODO: to remove?
    }

    public void addAll(HashSet<TransPattern> set) {
        this.set.addAll(set);
        //this.assessDepths(); // TODO: to remove?
    }


    // ---------- Setters (set)

    public void setSet(HashSet<TransPattern> set) {
        this.set = set;
        //this.assessDepths(); // TODO: to remove?
    }


    //==================================================================
    // Basic Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "TransPatternSet{" +
                "size=" + set.size() + '\'' +
                ", set=" + set +
                '}';
    }


    public int size() {
        return this.set.size();
    }

    public void clean() {
        this.initSet();
        this.assessDepths();
    }


    //==================================================================
    // Clean Method(s)
    //==================================================================

    public HashSet<TransPattern> getBestQuerySet(HashSet<TransPattern> originSet) {

        HashSet<TransPattern> resultSet = new HashSet<>();

        // -- Filter by precision (keep only the best)
        double bestPrecision = 0; // the initial best precision is null
        double toleranceLevel = 0.02; // TODO: move tolerance on parameters
        for(TransPattern tp: originSet) {
            double ppi = tp.getPrecisionFirstLine();
            if(ppi > (bestPrecision + toleranceLevel)) {
                resultSet.clear();
                resultSet.add(tp);
                bestPrecision = ppi;
            }
            else if(ppi > bestPrecision) {
                resultSet.add(tp);
                bestPrecision = ppi;
            }
            else if(ppi > (bestPrecision - toleranceLevel)) {
                resultSet.add(tp);
            }
        }

        return resultSet;

    }

    /**
     * Keep only, for patterns with the same requests, the pattern whose PPI
     * (precision pattern indicator) is the best.
     */
    public void cleanToKeepBestPatterns() {

        //System.out.println(" ///// TransPatternSet  "); // TODO: (DEV) to remove
        //System.out.println(" ///// -- cleanToKeepBestPatterns "); // TODO: (DEV) to remove
        HashSet<TransPattern> cleanPatternSet = new HashSet<>();

        // -- Get Query Set
        Set<Query> querySet = new HashSet<>();
        for (TransPattern tp: this.set) querySet.add(tp.getQuery());
        //System.out.println(" ///// ----- Query Set Size: " + querySet.size()); // TODO: (DEV) to remove

        // -- Browse queries to keep patterns with best ppi
        //System.out.println(" ///// ----- TP Set Size (before clean): " + set.size()); // TODO: (DEV) to remove
        for(Query query: querySet) {
            HashSet<TransPattern> queryPatternSet = this.getSet(query);
            queryPatternSet = getBestQuerySet(queryPatternSet);
            cleanPatternSet.addAll(queryPatternSet);
        }

        // -- Clear/update object set
        this.set.clear();
        this.set.addAll(cleanPatternSet);
        //System.out.println(" ///// ----- TP Set Size (after clean): " + set.size()); // TODO: (DEV) to remove

    }


    //==================================================================
    // Loan Processes
    //==================================================================

    public void initialize() {
        this.set = new HashSet<>();
    }

    public void newLoad() {

        // Initialize set
        this.set.clear();

        // -- Query and template definitions
        QueryMap queryMap = new QueryMap();
        TemplateMap templateMap = new TemplateMap();

        // -- Add Compositional Pattern

        Query query = queryMap.get("C-Q01"); // Hight Level Query (2)
        Template template = templateMap.get("C-T01");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q02"); // Hight Level Query (2)
        template = templateMap.get("C-T02");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q03"); // Hight Level Query (2)
        template = templateMap.get("C-T01");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q04"); // Hight Level Query (2)
        template = templateMap.get("C-T03");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q05"); // Hight Level Query (2)
        template = templateMap.get("C-T03");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q06"); // Hight Level Query (2)
        template = templateMap.get("C-T01");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q07"); // Hight Level Query (2)
        template = templateMap.get("C-T03");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q08"); // Hight Level Query (2)
        template = templateMap.get("C-T01");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q09"); // Hight Level Query (2)
        template = templateMap.get("C-T01");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q09b"); // Hight Level Query (2)
        template = templateMap.get("C-T01");
        this.set.add(new TransPattern(query, template));

        query = queryMap.get("C-Q11"); // Context Query (1)
        template = templateMap.get("C-T11");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q12"); // Context Query (1)
        template = templateMap.get("C-T11");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q12m"); // Context Query (1)
        template = templateMap.get("C-T11");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q13"); // Context Query (1)
        template = templateMap.get("C-T12");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q14"); // Context Query (1)
        template = templateMap.get("C-T13");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q15"); // Context Query (1)
        template = templateMap.get("C-T13");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q16"); // Context Query (1)
        template = templateMap.get("C-T14");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q17"); // Context Query (1)
        template = templateMap.get("C-T12");
        this.set.add(new TransPattern(query, template));

        query = queryMap.get("C-Q21"); // Modality Query (0)
        template = templateMap.get("C-T21");
        this.set.add(new TransPattern(query, template));
        //query = queryMap.get("C-Q22"); // Modality Query (0)
        //template = templateMap.get("C-T22");
        //this.set.add(new TransPattern(query, template));

        query = queryMap.get("C-Q31"); // Property Query (0)
        template = templateMap.get("C-T31");
        this.set.add(new TransPattern(query, template));
        query = queryMap.get("C-Q32"); // Property Query (0)
        template = templateMap.get("C-T33");
        this.set.add(new TransPattern(query, template));

        query = queryMap.get("C-Q41"); // Property Query (0)
        template = templateMap.get("C-T41");
        this.set.add(new TransPattern(query, template));


        // Depth Evaluation
        this.assessDepths();

    }

    public void oldLoad() {

        // Initialize set
        this.set.clear();

        // Query and template definitions
        QueryMap queryMap = new QueryMap();
        TemplateMap templateMap = new TemplateMap();

        // Mapping between query et template for pattern definition
        Map<String, String> patternRefMap = new HashMap<>();
        patternRefMap.put("Q01", "T01");
        patternRefMap.put("Q02", "T01");
        patternRefMap.put("Q03", "T02");
        patternRefMap.put("Q04", "T07");
        patternRefMap.put("Q05", "T03");
        patternRefMap.put("Q06", "T03");
        patternRefMap.put("Q07", "T04");
        patternRefMap.put("Q08", "T05");
        patternRefMap.put("Q09", "T05");
        patternRefMap.put("Q10", "T06");
        patternRefMap.put("Q11", "T01");
        patternRefMap.put("Q12", "T05");
        patternRefMap.put("Q13", "T05");
        patternRefMap.put("Q14", "T01");
        patternRefMap.put("Q15", "T01");
        patternRefMap.put("Q16", "T09");
        patternRefMap.put("Q17", "T10");
        patternRefMap.put("Q18", "T10");
        patternRefMap.put("Q19", "T05");
        patternRefMap.put("Q20", "T03");
        patternRefMap.put("Q21", "T01");
        patternRefMap.put("Q22", "T11");
        patternRefMap.put("Q23", "T11");
        patternRefMap.put("Q24", "T11");
        patternRefMap.put("Q25", "T12");

        // Transduction pattern definition
        Query query;
        Template template;
        TransPattern pattern;
        for (Map.Entry<String, String> ref:
                patternRefMap.entrySet()) {
            query = queryMap.get(ref.getKey());
            template = templateMap.get(ref.getValue());
            pattern = new TransPattern(query, template);
            this.set.add(pattern);
        }

    }

}
