package AbstractSemantic.LearnerObject;

import AbstractSemantic.Interpretation;
import AbstractSemantic.InterpretationRun;
import Utility.Normalizer;
import Utility.Tokenizer;

import static Parameters.Configuration.NORMALIZER;

public class TrainingData {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final int refNumber;
    private final InterpretationRun interpretationRun;
    private final String rawStatement;
    private final String normalizedStatement;
    private final Tokenizer tokenizer;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public TrainingData(int refNumber,
                        Interpretation initialInterpretation, String statement) {

        this.refNumber = refNumber;
        this.interpretationRun = new InterpretationRun(initialInterpretation);
        this.rawStatement = statement;
        this.normalizedStatement = NORMALIZER.normalize(rawStatement);
        this.tokenizer = new Tokenizer(normalizedStatement);

    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public int getRefNumber() {
        return refNumber;
    }

    public String getRefString() {
        return "Training_Data_" + refNumber;
    }

    public InterpretationRun getInterpretationRun() {
        return interpretationRun;
    }

    public String getRawStatement() {
        return rawStatement;
    }

    public String getNormalizedStatement() {
        return this.normalizedStatement;
    }

    public Tokenizer getTokenizer() {
        return tokenizer;
    }

    public int getMaxDepth() {
        return tokenizer.getTokenListSize();
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "TrainingData{" +
                "refNumber=" + refNumber +
                ", interpretationRun=" + interpretationRun +
                ", rawStatement='" + rawStatement + '\'' +
                ", normalizedStatement='" + normalizedStatement + '\'' +
                ", tokenizer=" + tokenizer +
                '}';
    }

}
