package AbstractSemantic.LearnerObject;

import AbstractSemantic.Interpretation;
import AbstractSemantic.Instance;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternObject.Token;
import Parameters.Configuration;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TemplateInferenceFromTokenList {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final String concept;
    private final List<Token> tokenList;
    private final Set<LearnAttribute> learnAttributeSet;
    private final Set<String> templateStringSet;
    private Set<Template> templateSet;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public TemplateInferenceFromTokenList(
            String concept,
            List<Token> tokenList,
            Set<LearnAttribute> attSet) {

        this.concept = concept;
        this.tokenList = tokenList;
        this.learnAttributeSet = attSet;
        this.templateStringSet = new HashSet<>();
        this.templateSet = new HashSet<>();

    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public List<Token> getTokenList() {
        return tokenList;
    }

    public Set<LearnAttribute> getLearnAttributeSet() {
        return learnAttributeSet;
    }

    public Set<String> getTemplateStringSet() {
        return templateStringSet;
    }

    public Set<Template> getTemplateSet() {
        return templateSet;
    }


    //==================================================================
    // Useful Method(s)
    //==================================================================

    /**
     * Give the power set of a set S, namely the set of all subsets of S,
     * including the empty set and S itself.
     *
     * @param originalSet
     * @return power set
     */
    public static Set<Set<LearnAttribute>> powerSet(Set<LearnAttribute> originalSet) {

        Set<Set<LearnAttribute>> sets = new HashSet<Set<LearnAttribute>>();

        if (originalSet.isEmpty()) {
            sets.add(new HashSet<LearnAttribute>());
            return sets;
        }

        List<LearnAttribute> list = new ArrayList<LearnAttribute>(originalSet);
        LearnAttribute head = list.get(0);
        Set<LearnAttribute> rest = new HashSet<LearnAttribute>(list.subList(1, list.size()));
        for (Set<LearnAttribute> set : powerSet(rest)) {
            Set<LearnAttribute> newSet = new HashSet<LearnAttribute>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }

        return sets;

    }


    //==================================================================
    // Methods to infer templates as string
    //==================================================================

    /**
     * Infer the tokens from a given attribute.
     *
     * @param tokenList, attribute
     * @return updated token list
     */
    public static List<Token> inferTokenList(
            List<Token> tokenList, LearnAttribute learnAttribute) {

        List<Token> resultTokenList = new ArrayList<>();

        for(Token token: tokenList) {

            if (token.isType(Configuration.TokenType.VARIANT)) {

                // Find attribute value using Matcher class
                Pattern markPattern = Pattern.compile(learnAttribute.getNormalizedValue());
                Matcher matcher = markPattern.matcher(token.getRawValue());

                if (matcher.matches()) {

                    // Define feature pointer corresponding to attribute
                    String featurePointer = Token.evalPointer(
                            learnAttribute.getInstanceId(), learnAttribute.getFeatureId());

                    resultTokenList.add(new Token(featurePointer));

                }
                else resultTokenList.add(token);

            }
            else resultTokenList.add(token);

        }

        return resultTokenList;

    }

    /**
     * Infer a template from the a token list and an attribute set.
     *
     * @param tokenList, attributeSet
     * @return set of templates
     */
    public Template inferTemplate(
            List<Token> tokenList, Set<LearnAttribute> learnAttributeSet) {
        for(LearnAttribute learnAttribute : learnAttributeSet)
            tokenList = inferTokenList(tokenList, learnAttribute);
        return new Template(this.concept, tokenList);
    }


    //==================================================================
    // Main Method(s)
    //==================================================================

    /**
     * Infer all templates from statement and attributes.
     *
     * @return set of templates
     */
    public void inferTemplateSet() {
        this.templateSet.clear();
        Set<Set<LearnAttribute>> powerSet = powerSet(learnAttributeSet);
        for (Set<LearnAttribute> set: powerSet) {
            if (!set.isEmpty()) {
                Template newTemplate = inferTemplate(this.tokenList, set);
                //if(!templateStringSet.contains(newTemplate))
                this.templateSet.add(newTemplate);
            }
        }
    }


    //==================================================================
    // Post-inference Process(es)
    //==================================================================

    public void cleanTemplateSet(Interpretation def, String statement) {

        Set<Template> newSet = new HashSet<>();

        Map<String, Instance> varMap = def.getDefaultInstanceMap();
        String newStatement = "";
        for (Template template: this.templateSet) {
            template.setVariableMap(varMap);
            newStatement = template.getStatement();
            if (newStatement.equals(statement)) newSet.add(template);
        }

        this.templateSet = newSet;

    }

}
