package AbstractSemantic.LearnerObject;

import AbstractSemantic.Interpretation;
import Utility.Normalizer;

public class OldArchive {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final Interpretation interpretation;
    private final String rawStatement;


    // ---------- Calculation Attributes

    private static final Normalizer NORMALIZER = new Normalizer();


    //==================================================================
    // Constructor(s)
    //==================================================================

    public OldArchive(Interpretation interpretation, String statement) {
        this.interpretation = interpretation;
        this.rawStatement = statement;
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public Interpretation getInterpretation() {
        return interpretation;
    }

    public String getRawStatement() {
        return rawStatement;
    }

    public String getNormalizedStatement() {
        return NORMALIZER.normalize(rawStatement);
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Archive{\n" +
                "  definition=" + interpretation.toString() +
                "  raw statement='" + rawStatement + '\'' + "\n" +
                "  normalized statement='" + this.getNormalizedStatement() + '\'' + "\n" +
                '}';
    }

}
