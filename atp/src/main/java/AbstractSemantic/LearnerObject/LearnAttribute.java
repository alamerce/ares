package AbstractSemantic.LearnerObject;

import Parameters.Configuration;

public class LearnAttribute {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base Attributes

    private final String instanceId;
    private final String featureId;
    private final String value;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public LearnAttribute(String instanceId, String featureId,
                          String value) {
        this.instanceId = instanceId;
        this.featureId = featureId;
        this.value = value;
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public String getInstanceId() {
        return instanceId;
    }

    public String getFeatureId() {
        return featureId;
    }

    public String getValue() {
        return value;
    }

    public String getNormalizedValue() {
        return Configuration.NORMALIZER.normalize(value);
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Attribute{" +
                "instanceId='" + instanceId + '\'' +
                ", featureId='" + featureId + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

}
