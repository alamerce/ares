package AbstractSemantic.LearnerObject;

import AbstractSemantic.Instance;
import AbstractSemantic.Interpretation;
import AbstractSemantic.TransPattern;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternObject.Token;
import LogicalQuery.Atom;
import LogicalQuery.Query;
import LogicalQuery.ValuesSet;
import LogicalQuery.Variable;
import Parameters.Configuration;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Parameters.Configuration.NORMALIZER;
import static Parameters.Configuration.UNDEFINED_TOKEN_STRING;

public class OldInterpretationAnalyzer {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Constant attributes

    private static final int VAL_MAX = 5; // size limit for variable set


    // ---------- Base attributes

    private final Interpretation interpretation;
    private final Set<LearnAttribute> learnAttributeSet;


    //==================================================================
    // Construction Methods
    //==================================================================


    /**
     * Extract attributes from a definition.
     *
     * @return set of attributes
     */
    private void extractAttributesFromDefinition() {
        LearnAttribute learnAttribute;
        String instanceId, featureValue;
        for (Instance instance: this.interpretation.getInstanceSet()) {
            instanceId = instance.getId();
            for (String featureId: instance.getFeatureIdSet()) {
                featureValue = instance.getFeatureValue(featureId);
                learnAttribute = new LearnAttribute(instanceId, featureId, featureValue);
                learnAttributeSet.add(learnAttribute);
            }
        }
    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public OldInterpretationAnalyzer(Interpretation interpretation) {
        this.interpretation = interpretation;
        this.learnAttributeSet = new HashSet<>();
        this.extractAttributesFromDefinition();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public Interpretation getInterpretation() {
        return interpretation;
    }

    public Set<LearnAttribute> getLearnAttributeSet() {
        return learnAttributeSet;
    }


    //==================================================================
    // Useful Method(s)
    //==================================================================

    public List<String> getVariableIdList(Template template) {
        List<String> varIdList = new ArrayList<>();
        for(String varId: template.getVariableIdList()) {
            if (!varId.equals(UNDEFINED_TOKEN_STRING)) varIdList.add(varId);
        }
        return varIdList;
    }


    /**
     * Filter definition predicate set by retaining the atoms whose
     * arguments belong to the given list of variables.
     *
     * @param variableIdList
     * @return Filtered atom set
     */
    public Set<Atom> filterAtomSet(List<String> variableIdList) {

        Set<Atom> resultAtomList = new HashSet<>();

        // --- Add atom whose arguments belong to the variable id list
        Set<Atom> predicateSet = this.interpretation.getPredicateSet();
        List<String> atomArgs;
        for(Atom atom: predicateSet) {
            atomArgs = atom.getArgs();
            if (variableIdList.containsAll(atomArgs)) resultAtomList.add(atom);
        }

        return resultAtomList;
    }


    /**
     * Complete atom set by adding some relations that define a path
     * between two instances (ex.: arg0(x,y) and arg0(y,z) define
     * a path between x and z).
     *
     * @param atomSet
     * @return Completed atom set
     */
    public Set<Atom> completeAtomSet(Set<Atom> atomSet) {

        Set<Atom> resultAtomSet = atomSet;

        // --- List of variables which are atom argument
        List<String> variableIdList = new ArrayList<>();
        for (Atom atom: atomSet) variableIdList.addAll(atom.getArgs());

        // --- Obtain the predicate set filtered following some conditions
        Set<Atom> predicateSet = this.interpretation.getPredicateSet();
        Set<Atom> filteredPredicateSet = new HashSet<>();
        List<String> atomArgs1, atomArgs2;
        Boolean c1, c2, c3;
        for(String varId: variableIdList) {
            for(Atom atom: predicateSet) {
                atomArgs1 =  atom.getArgs();
                c1 = (atomArgs1.size() == 2); // atom with 2 arguments
                c2 = atomArgs1.contains(varId); // atom with one variable of list
                c3 = !variableIdList.containsAll(atomArgs1); /// atom without all variables of list
                if(c1 && c2 && c3) filteredPredicateSet.add(atom);
            }
        }

        // Add relations that define a path between two instances
        // (ex.: arg0(x,y) and arg0(y,z) define a path between x and z).
        for(Atom atom1: filteredPredicateSet) {
            atomArgs1 =  atom1.getArgs(); // ex.: atom1 = arg(x,y)
            if (variableIdList.contains(atomArgs1.get(0))) {
                for(Atom atom2: filteredPredicateSet) { // ex.: atom2 = arg(y,z)
                    atomArgs2 = atom2.getArgs();
                    if (atomArgs1.get(1).equals(atomArgs2.get(0))) {
                        resultAtomSet.add(atom1);
                        resultAtomSet.add(atom2);
                    }
                }
            }
        }

        return resultAtomSet;

    }


    //==================================================================
    // Main Method(s)
    //==================================================================

    /**
     * Find concordances between the definition and a statement.
     *
     * @param statement
     * @return set of concordances
     */
    public Set<LearnAttribute> findConcordances(String statement) {

        Set<LearnAttribute> concordanceSet = new HashSet<>();
        String normalizedStatement = NORMALIZER.normalize(statement);

        for(LearnAttribute learnAttribute : learnAttributeSet) {
            Pattern markPattern = Pattern.compile(learnAttribute.getNormalizedValue());
            Matcher matcher = markPattern.matcher(normalizedStatement);
            if(matcher.find()) concordanceSet.add(learnAttribute);
        }

        return concordanceSet;

    }

    /**
     * Find concordances between the definition and a list of tokens.
     *
     * @param tokenList (list of tokens)
     * @return set of concordances
     */
    public Set<LearnAttribute> findConcordances(List<Token> tokenList) {

        Set<LearnAttribute> concordanceSet = new HashSet<>();

        for(Token token: tokenList) {

            if (token.isType(Configuration.TokenType.VARIANT)) {

                String normalizedTokenValue = NORMALIZER.normalize(token.getRawValue());

                for(LearnAttribute learnAttribute : learnAttributeSet) {
                    Pattern markPattern = Pattern.compile(learnAttribute.getNormalizedValue());
                    Matcher matcher = markPattern.matcher(normalizedTokenValue);
                    if(matcher.matches()) concordanceSet.add(learnAttribute);
                }

            }

        }




        return concordanceSet;

    }


    /**
     * Infer the query corresponding to the definition and a given
     * template.
     *
     * @param template
     * @return query
     */
    public Query inferQuery(Template template) {
        List<String> variableIdList = this.getVariableIdList(template);
        Set<Atom> atomSet = this.filterAtomSet(variableIdList);
        atomSet = this.completeAtomSet(atomSet);
        Query query = new Query(variableIdList, atomSet);
        return query;
    }


    /**
     * TODO: doc about this method
     */
    public Query getMaximalQuery() {
        List<String> variableIdList = new ArrayList<>();
        for(LearnAttribute a: this.learnAttributeSet) variableIdList.add(a.getInstanceId());
        Set<Atom> atomSet = this.filterAtomSet(variableIdList);
        atomSet = this.completeAtomSet(atomSet);
        Query query = new Query(variableIdList, atomSet);
        return query;
    }


    /**
     * TODO: doc about this method
     */
    public Boolean isQueryVerified(TransPattern pattern) {

        Boolean isQueryVerified = false;

        // --- Search variable set which validates the query
        Query query = pattern.getQuery();
        ValuesSet vs = new ValuesSet(interpretation.getInstanceIdSet());
        int valuationsNumber = Integer.min(query.getVariablesNumber(), VAL_MAX);
        List<List<String>> valuationsList = vs.getValuations(valuationsNumber);
        for (List<String> valuation : valuationsList) {
            query.affectVariables(valuation);
            if (query.isSatisfied(interpretation.getPredicateSet())) {
                isQueryVerified = true;
                break;
            }
        }

        return (isQueryVerified);

    }


    /**
     * Test if the application of the definition to the pattern produces
     * the expected statement, and only the expected statement.
     *
     * @param pattern, expectedStatement
     * @return True if produced statement is correct
     */
    public Boolean testPattern(TransPattern pattern, String expectedStatement) {

        Boolean isQueryVerified = false;
        Boolean isAllProducedStatementCorrect = true;
        String normalizedExpectedStatement = NORMALIZER.normalize(expectedStatement);
        //System.out.println("DEV 1 - A: " + normalizedExpectedStatement); // TODO: (DEV) to remove

        // --- Search variable set which validates the query
        Query query = pattern.getQuery();
        ValuesSet vs = new ValuesSet(interpretation.getInstanceIdSet());
        int valuationsNumber = Integer.min(query.getVariablesNumber(), VAL_MAX);
        List<List<String>> valuationsList = vs.getValuations(valuationsNumber);
        for (List<String> valuation : valuationsList) {
            query.affectVariables(valuation);
            //System.out.println("DEV 1 - B1: " + query.getValuationString()); // TODO: (DEV) to remove
            //System.out.println("DEV 1 - B2: " + definition.getPredicateSet()); // TODO: (DEV) to remove
            if (query.isSatisfied(interpretation.getPredicateSet())) {
                List<Variable> vList = query.getVariableList();
                //System.out.println("DEV 1 - C: " + vList.toString()); // TODO: (DEV) to remove
                Map<String, Instance> varMap = interpretation.getInstanceMap(vList);
                //System.out.println("DEV 1 - D: " + varMap.toString()); // TODO: (DEV) to remove
                String producedStatement = pattern.getStatement(varMap);
                isQueryVerified = true;
                //System.out.println("DEV 1 - E: " + producedStatement); // TODO: (DEV) to remove
                if (!producedStatement.equals(normalizedExpectedStatement))
                    isAllProducedStatementCorrect = false;
            }
            if (!isAllProducedStatementCorrect) break;
        }

        //System.out.println("DEV 1 - D: " + isQueryVerified + " / " + isAllProducedStatementCorrect); // TODO: (DEV) to remove
        return (isQueryVerified && isAllProducedStatementCorrect);

    }

}
