package AbstractSemantic;

import AbstractSemantic.TransPatternObject.Template;
import LogicalQuery.Atom;
import LogicalQuery.Query;
import Parameters.Configuration;

import java.util.*;

/**
 * The AS Pattern class associates a logical formula and a template
 * (formatting structure). The logical formula is used to capture a
 * statement expressed with the initial formalism, while the template
 * is used to generate data in the target format.
 *
 */
public class TransPattern {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base Attributes

    private final Query query;
    private final Template template;
    private int depth;


    // ---------- Accuracy Attributes

    private boolean assessedPrecision;
    private double truePositiveFirstLineNumber;
    private double falsePositiveFirstLineNumber;
    private double truePositiveSecondLineNumber;
    private double falsePositiveSecondLineNumber;



    //==================================================================
    // Construction Method(s)
    //==================================================================

    public void initAccuracyParameters() {
        this.assessedPrecision = false;
        this.truePositiveFirstLineNumber = 1; // TODO: to change init to 0 after update of loadPattern function
        this.falsePositiveFirstLineNumber = 0;
        this.truePositiveSecondLineNumber = 1; // TODO: to change init to 0 after update of loadPattern function
        this.falsePositiveFirstLineNumber = 0;
    }



    //==================================================================
    // Constructor(s)
    //==================================================================

    public TransPattern(Query query, Template template) {
        this.query = query;
        this.template = template;
        this.depth = 0;
        this.initAccuracyParameters();
    }

    public TransPattern() {
        this.query = new Query();
        this.template = new Template();
        this.depth = 0;
        this.initAccuracyParameters();
    }


    //==================================================================
    // Accessors
    //==================================================================

    // ---------- Getters (Base)

    public Query getQuery() {
        return query;
    }

    public Template getTemplate() {
        return template;
    }

    public int getDepth() {
        return depth;
    }


    // ---------- Getters (Precision)

    private double getPrecision(double trueNb, double falseNb) {
        double allPositiveNumber = trueNb + falseNb;
        return trueNb / allPositiveNumber;
    }

    public double getPrecisionFirstLine() {
        double trueNb = truePositiveFirstLineNumber;
        double falseNb = falsePositiveFirstLineNumber;
        return this.getPrecision(trueNb, falseNb);
    }

    public double getPrecisionSecondLine() {
        double trueNb = truePositiveSecondLineNumber;
        double falseNb = falsePositiveSecondLineNumber;
        return this.getPrecision(trueNb, falseNb);
    }


    // ---------- Predicates (Base)

    /**
     * A transduction pattern is abstract if its query is abstract.
     */
    public boolean isAbstract() {
        return this.query.isAbstract();
    }

    /**
     * A transduction pattern is relative to a set of atoms if its
     * query is relative to the given atom set.
     */
    public boolean isRelativeTo(Set<Atom> atomSet) {
        return this.query.isRelativeTo(atomSet);
    }


    /**
     * A transduction pattern is deeper abstract relative to a set of
     * atoms if its query is deeper abstract.
     */
    public boolean isDeeperAbstract(Set<Atom> atomSet) {
        return this.query.isDeeperAbstract(atomSet);
    }


    // ---------- Predicates (Precision)

    public boolean isAssessedPrecision() {
        return this.assessedPrecision;
    }


        // ---------- Setters (Base)

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void incDepth() {
        this.depth++;
    }


    // ---------- Setters (Precision)

    public void validatePrecision() {
        this.assessedPrecision = true;
    }

    public void updatePrecisionFirstLine(Boolean resultEval) {
        if(resultEval) this.truePositiveFirstLineNumber += 1;
        else this.falsePositiveFirstLineNumber += 1;
    }

    public void updatePrecisionSecondLine(Boolean resultEval) {
        if(resultEval) this.truePositiveSecondLineNumber += 1;
        else this.falsePositiveSecondLineNumber += 1;
    }

    //==================================================================
    // Base Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "TransPattern{" +
                "query=" + query +
                ", template=" + template +
                ", depth=" + depth +
                ", truePositiveFirstLineNumber=" + truePositiveFirstLineNumber +
                ", falsePositiveFirstLineNumber=" + falsePositiveFirstLineNumber +
                ", truePositiveSecondLineNumber=" + truePositiveSecondLineNumber +
                ", falsePositiveSecondLineNumber=" + falsePositiveSecondLineNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransPattern that = (TransPattern) o;
        return Objects.equals(getQuery(), that.getQuery()) &&
                Objects.equals(getTemplate(), that.getTemplate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuery(), getTemplate());
    }

    //==================================================================
    // Normalizing Method
    //==================================================================

    /**
     * Normalize pattern in two passes (necessary to avoid errors when
     * changing variable ids).
     */
    public void normalize() {

        // Normalize Pass 1
        Map<String, String> normIdMap;
        normIdMap = this.query.normalizePass1();
        this.template.changeVariableId(normIdMap);

        // Normalize Pass 2
        normIdMap = this.query.normalizePass2();
        this.template.changeVariableId(normIdMap);

    }

    //==================================================================
    // Main Methods
    //==================================================================

    /**
     * Gives the formatted statement following the formatting structure
     * with instantiated variables
     *
     * @param varMap (mapping between marks and instances)
     * @return Statement as string
     */
    public String getStatement(Map<String, Instance> varMap) {
        String result = "";
        template.setVariableMap(varMap);
        result = template.getStatement();
        result = Configuration.NORMALIZER.normalize(result);
        return result;
    }


    /**
     * Give the interpretation corresponding to pattern application with
     * given variables.
     *
     * @param newInstanceId (id for interpretation instance), varMap (variables)
     * @return new interpretation
     */
    public Interpretation getInterpretation(
            String newInstanceId, Map<String, Instance> varMap) {
        template.setVariableMap(varMap);
        //double ppi = this.getPrecisionFirstLine();
        return template.getInterpretation(newInstanceId, this);
    }

}
