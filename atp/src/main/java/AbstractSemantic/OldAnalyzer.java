package AbstractSemantic;


import ComputationObject.OutputStatement;
import LogicalQuery.Atom;
import LogicalQuery.Query;
import LogicalQuery.ValuesSet;
import LogicalQuery.Variable;
import Parameters.Configuration;

import java.util.*;

/**
 * The AS Analyzer class is the core of the AS package. It makes it
 * possible to analyze the semantic representation of a statement to
 * define an abstract semantics. This definition is then used to
 * produce the semantic data in the expected format.
 *
 */
public class OldAnalyzer {

    //==================================================================
    // Attributes
    //==================================================================


    // ---------- Base attributes

    private final String statementId;
    private Map<String, Map<String, String>> dataMap;
    private Set<Atom> atomSet;//TODO: create a dataMap class and merge dataMap and atomSet


    // ---------- References for concept and instance definitions

    private static final String CONCEPT = "concept"; //TODO: to review
    private static final String[] FEATURES = {
            "modality", "temporality", "logicConnector",
            "frequency", "adverb",
            "property", "entity"
    };


    // ---------- Templates for predicate definitions

    //private HashSet<Atom> predicateTemplateSet;


    // ---------- Patterns for semantic transduction

    private TransPatternSet transPatternSet;


    // ---------- Result Computing Attribute(s)
    int precisionLowLimit = 0; // use to keep the most precise requests


    //==================================================================
    // Constructor(s)
    //==================================================================

    public OldAnalyzer(String statementId,
                       Map<String, Map<String, String>> dataMap,
                       Set<Atom> atomSet,
                       TransPatternSet patternSet) {

        this.statementId = statementId;
        this.dataMap = dataMap;
        this.atomSet = atomSet;
        //this.predicateTemplateSet = new HashSet<Atom>();
        this.transPatternSet = patternSet;

    }


    //==================================================================
    // Accessors
    //==================================================================

    public String getStatementId() {
        return statementId;
    }

    public Map<String, Map<String, String>> getDataMap() {
        return dataMap;
    }


    //==================================================================
    // Method(s) to produce AS interpretation from SymbolTable
    //==================================================================

    /**
     * Extend interpretation by adding concepts.
     *
     * @param interpretation (to extend), dataMap
     * @return Extended interpretation (with concepts)
     */
    public Interpretation addConcepts(Interpretation interpretation) {

        assert dataMap.containsKey(CONCEPT):
                ("Analyzer.addConcepts: No " + CONCEPT + " in dataMap");

        Map<String, String> conceptMap = dataMap.get(CONCEPT);
        for (Map.Entry<String, String> e : conceptMap.entrySet()) {
            interpretation.addConcept(e.getValue());
        }

        return interpretation;
    }

    /**
     * Characterize instance by adding features.
     *
     * @param instance (to characterize)
     * @return Characterized instance (with features)
     */
    public Instance characterizeInstance(Instance instance) {

        // Analyze each featureMap
        for (String featureId : FEATURES) {
            assert dataMap.containsKey(featureId):
                    ("Analyzer.characterizeInstance: No " + featureId + " in dataMap");
            String instanceId = instance.getId();
            Map<String, String> featureMap = dataMap.get(featureId);
            if (featureMap.containsKey(instanceId)) {
                String featureValue = featureMap.get(instanceId);
                instance.addFeature(featureId, featureValue);
            }
        }

        return instance;

    }

    /**
     * Extend interpretation by adding instances.
     *
     * @param interpretation (to extend), dataMap
     * @return Extended interpretation (with instances)
     */
    public Interpretation addInstances(Interpretation interpretation) {

        assert dataMap.containsKey(CONCEPT):
                ("Analyzer.addInstances: No " + CONCEPT + " in dataMap");

        Map<String, String> instanceMap = dataMap.get(CONCEPT);
        for (Map.Entry<String, String> e : instanceMap.entrySet()) {
            Instance instance = new Instance(e.getKey(), e.getValue());
            instance = characterizeInstance(instance);
            interpretation.addInstance(instance);
        }

        return interpretation;

    }

    /**
     * Extend interpretation by adding predicates.
     *
     * @param interpretation (to extend), dataMap
     * @return Extended interpretation (with instances)
     */
    public Interpretation addPredicates(Interpretation interpretation) {
        for (Atom p: this.atomSet) interpretation.addPredicate(p);
        return interpretation;
    }

    /**
     * Gives the definition corresponding to dataMap analysis.
     *
     * @return AS Interpretation
     */
    public Interpretation computeInterpretation() {
        Interpretation interpretation = new Interpretation(this.statementId);
        interpretation = addConcepts(interpretation);
        interpretation = addInstances(interpretation);
        interpretation = addPredicates(interpretation);
        return interpretation;
    }


    //==================================================================
    // Method(s) to generate the output statement using pattern set
    //==================================================================

    /**
     * Add the formatted statement and its PPI (Precision Pattern Indicator)
     * to the collection (map).
     *
     * @param statementMap, interpretation, pattern
     * @return Updated Statement Map
     */
    public Map<String, Double> addFormattedStatementPPI(
            Map<String, Double>  statementMap,
            Interpretation interpretation, TransPattern pattern) {

        // --- Comparing query precision and low limit
        Query query = pattern.getQuery();
        int queryPrecision = Math.min(
                query.getPrecision(), Configuration.LOW_LIMIT_MAX);
        if (queryPrecision > precisionLowLimit) {
            statementMap.clear();
            precisionLowLimit = queryPrecision;
        }

        // Add statement corresponding to the validated query
        List<Variable> vList = query.getVariableList();
        Map<String, Instance> varMap = interpretation.getInstanceMap(vList);
        String statement = pattern.getStatement(varMap);
        Double ppi = pattern.getPrecisionFirstLine(); // PPI = Precision Pattern Indicator
        statementMap.put(statement, ppi);

        return statementMap;

    }

    /**
     * Add the formatted statement to the collection (map).
     *
     * @param outSet, definition, pattern
     * @return Updated Statement Set
     */
    public Set<OutputStatement> addFormattedStatement(
            Set<OutputStatement>  outSet, Interpretation interpretation, TransPattern pattern) {

        // --- Comparing query precision and low limit
        Query query = pattern.getQuery();
/*        int queryPrecision = Math.min( // TODO: to delete or to keep?
                query.getPrecision(), ComputationConfig.LOW_LIMIT_MAX);
        if (queryPrecision > precisionLowLimit) {
            outSet.clear();
            precisionLowLimit = queryPrecision;
        }*/

        // Add statement corresponding to the validated query
        List<Variable> vList = query.getVariableList();
        Map<String, Instance> varMap = interpretation.getInstanceMap(vList);
        String statement = pattern.getStatement(varMap);
        OutputStatement newOS = new OutputStatement(statement, pattern);
        double newPPI = newOS.getProducerPattern().getPrecisionFirstLine();
        OutputStatement osToRemove;
        for (OutputStatement os: outSet) { // Browse statements already selected
            if (os.equals(newOS) && // new OS already selected
                    os.getProducerPattern().getPrecisionFirstLine() < newPPI) { // but new PPI is better
                outSet.remove(os);
                break;
            }
        }
        //if (osToRemove != null)
        outSet.add(newOS);

        return outSet;

    }


    /**
     * Generates a set with all output statements corresponding to the
     * definition.
     *
     * @param interpretation
     * @return Set of statements
     */
    public Set<OutputStatement> produceOutputStatementSet(Interpretation interpretation) {

        Set<OutputStatement> outSet1 = new HashSet<>();
        List<Integer> analyzedPatternsNumberList = new ArrayList<>();

        // --- Analyze for each pattern
        this.precisionLowLimit = 0; // use to keep the most precise requests
        for (TransPattern pattern : this.transPatternSet.getSet()) {

            Query query = pattern.getQuery();

            // Query evaluation only if the query precision valid the low limit
            if (query.getPrecision() >= precisionLowLimit) {

                assert query.getVariablesNumber() <= Configuration.VAL_MAX:
                        ("Analyzer.generateFormattedStatementList: " +
                                " too variables on the query" +
                                " (limit imposed to keep proper performance).");

                // --- Search variable set which validates the query
                ValuesSet vs = new ValuesSet(interpretation.getInstanceIdSet());
                int valuationsNumber = Integer.min(query.getVariablesNumber(), Configuration.VAL_MAX);
                List<List<String>> valuationsList = vs.getValuations(valuationsNumber);
                for (List<String> valuation : valuationsList) {

                    // --- Affect variables to query
                    query.affectVariables(valuation);

                    // --- Evaluate the query
                    Boolean isQueryVerify = query.isSatisfied(interpretation.getPredicateSet());
                    if (isQueryVerify) {
                        System.out.println(" -- Old Analyzer -- ");

                        // --- Add formatted statement to result list
                        outSet1 = addFormattedStatement(outSet1, interpretation, pattern);

                    }
                }

            }

        }

/*        System.out.println("DEV A");
        for(Map.Entry<String, TransPattern> e: statementMap.entrySet()) { // TODO: (DEV) to remove
            System.out.println(e.getKey());
            System.out.println(" -- query: " + e.getValue().getQuery().toString());
            System.out.println(" -- template: " + e.getValue().getTemplate().getRawTemplateString());
            System.out.println(" -- precision: " + e.getValue().getPrecisionFirstLine() +
                    " / " + e.getValue().getPrecisionSecondLine());
        }*/

        // --- Keep only the statements associated with the best PPI (precision pattern indicator)
        Set<OutputStatement> resultSet = new HashSet<>();
        double bestPrecision = 0;
        double toleranceLevel = 0.02; // TODO: move tolerance on parameters
        double ppi1; // First Line PPI
        for(OutputStatement os: outSet1) {
            ppi1 = os.getProducerPattern().getPrecisionFirstLine();
            if(ppi1 > (bestPrecision + toleranceLevel)) {
                resultSet.clear();
                resultSet.add(os);
                bestPrecision = ppi1;
            }
            else if(ppi1 > bestPrecision) {
                resultSet.add(os);
                bestPrecision = ppi1;
            }
            else if(ppi1 > (bestPrecision - toleranceLevel)) {
                resultSet.add(os);
            }
        }

        // --- Default Statement if No Matching
        if (resultSet.isEmpty()) resultSet.add(new OutputStatement());

        // --- Update Statistic
        analyzedPatternsNumberList.add(this.transPatternSet.getSet().size());
        analyzedPatternsNumberList.add(outSet1.size());
        analyzedPatternsNumberList.add(resultSet.size());
        for(OutputStatement os: resultSet) {
            os.setAnalyzedPatternsNumberList(analyzedPatternsNumberList);
        }

        return resultSet;

    }


    // ---------- Output as single string

    /**
     * Generates the best precise statement corresponding to the
     * definition.
     *
     * @param interpretation
     * @return a statement corresponding to the definition
     */
    public String provideBestStatement(Interpretation interpretation) {
        String result = "-";
        Set<OutputStatement> osSet = produceOutputStatementSet(interpretation);
        double bestPrecision = -1;
        boolean c1, c2;
        for(OutputStatement os: osSet) {
            c1 = bestPrecision == -1;
            c2 = os.getProducerPattern().getPrecisionFirstLine() >= bestPrecision;
            if (c1 || c2) {
                result = os.getStatement();
                bestPrecision = os.getProducerPattern().getPrecisionFirstLine();
            }
        }
        return result;
    }

}
