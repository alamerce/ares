package AbstractSemantic.TransPatternObject;

import LogicalQuery.Atom;
import LogicalQuery.Query;

import java.util.*;

/**
 * Class to define the query used for transduction pattern.
 *
 * This class is temporary, pending templates definition in files.
 *
 */
public class QueryMap {

    //==================================================================
    // Attributes
    //==================================================================

    private Map<String, Query> queryMap;

    //==================================================================
    // Methods to define formulas
    //==================================================================

    private Query defineFormula(String[] vars, Atom[] atoms) {
        List<String> boundVariablesList = new ArrayList<>(Arrays.asList(vars));
        List<Atom> atomList = new ArrayList<>(Arrays.asList(atoms));
        return new Query(boundVariablesList, atomList);
    }



    //==================================================================
    // Old Query -> to remove (TODO)
    //==================================================================

    private void addQuery1() {
        String queryId = "Q01";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("condition", "a", "c")

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery2() {
        String queryId = "Q02";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("condition", "b", "c")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery3() {
        String queryId = "Q03";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("arg+", "a", "b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery4() {
        String queryId = "Q04";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "c"),
                new Atom("domain", "a", "b"),
                new Atom("time", "a", "c")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery5() {
        String queryId = "Q05";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("isProperty", "a"),
                new Atom("isProperty", "b"),
                new Atom("time", "a", "b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery6() {
        String queryId = "Q06";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("isProperty", "a"),
                new Atom("isProperty", "b"),
                new Atom("condition", "a", "b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery7() {
        String queryId = "Q07";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isProperty", "a"),
                new Atom("isTemporality", "b"),
                //new Atom("after", "b"),
                new Atom("isProperty", "c"),
                new Atom("time", "a", "b"),
                new Atom("op1", "b", "c")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery8() {
        String queryId = "Q08";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isTemporality", "c"),
                //new Atom("after", "c"),
                new Atom("isProperty", "d"),
                new Atom("time", "b", "c"),
                new Atom("op1", "c", "d")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery9() {
        String queryId = "Q09";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isTemporality", "c"),
                //new Atom("after", "c"),
                new Atom("isProperty", "d"),
                new Atom("domain", "a", "b"),
                new Atom("time", "a", "c"),
                new Atom("op1", "c", "d")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery10() {
        String queryId = "Q10";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "d"),
                new Atom("arg1", "a", "b"),
                new Atom("arg1", "b", "c"),
                new Atom("condition", "c", "d")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery11() {
        String queryId = "Q11";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("time", "a", "c")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery12() {
        String queryId = "Q12";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "d"),
                new Atom("isTemporality", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("time", "a", "c"),
                new Atom("op1", "c", "d"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery13() {
        String queryId = "Q13";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "d"),
                new Atom("isTemporality", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("location", "a", "c"),
                new Atom("op1", "c", "d"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery14() {
        String queryId = "Q14";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("manner", "b", "c")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery15() {
        String queryId = "Q15";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("time", "b", "c")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery16() {
        String queryId = "Q16";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isProperty", "a"),
                new Atom("isTemporality", "b"),
                new Atom("isProperty", "c"),
                new Atom("isOnly", "d"),
                new Atom("mod", "a", "d"),
                new Atom("time", "a", "b"),
                new Atom("op1", "b", "c")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery17() {
        String queryId = "Q17";
        String[] vars = {"a", "b", "c", "d", "e"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "d"),
                new Atom("isTemporality", "c"),
                new Atom("isOnly", "e"),
                new Atom("mod", "a", "e"),
                new Atom("arg+", "a", "b"),
                new Atom("time", "a", "c"),
                new Atom("op1", "c", "d"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery18() {
        String queryId = "Q18";
        String[] vars = {"a", "b", "c", "d", "e"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "d"),
                new Atom("isTemporality", "c"),
                new Atom("isOnly", "e"),
                new Atom("arg+", "a", "b"),
                new Atom("mod", "b", "e"),
                new Atom("time", "a", "c"),
                new Atom("op1", "c", "d"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery19() {
        String queryId = "Q19";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "d"),
                new Atom("isTemporality", "c"),
                new Atom("arg1", "a", "b"),
                new Atom("arg2", "a", "c"),
                new Atom("op1", "c", "d"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery20() {
        String queryId = "Q20";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("isProperty", "a"),
                new Atom("isProperty", "b"),
                new Atom("isRecurringEvent", "c"),
                new Atom("frequency", "a", "c"),
                new Atom("arg4", "c", "b"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery21() {
        String queryId = "Q21";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isProperty", "c"),
                new Atom("isRecurringEvent", "d"),
                new Atom("arg1", "a", "b"),
                new Atom("frequency", "b", "d"),
                new Atom("arg4", "d", "c"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery22() {
        String queryId = "Q22";
        String[] vars = {"a", "b", "c", "d", "e"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isLogicConnector", "c"),
                new Atom("isProperty", "d"),
                new Atom("isProperty", "e"),
                new Atom("arg+", "a", "b"),
                new Atom("arg+", "b", "c"),
                new Atom("op1", "c", "d"),
                new Atom("op2", "c", "e"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery23() {
        String queryId = "Q23";
        String[] vars = {"a", "b", "c", "d", "e"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isLogicConnector", "c"),
                new Atom("isProperty", "d"),
                new Atom("isProperty", "e"),
                new Atom("arg+", "a", "b"),
                new Atom("arg+", "a", "c"),
                new Atom("op1", "c", "d"),
                new Atom("op2", "c", "e"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery24() {
        String queryId = "Q24";
        String[] vars = {"a", "b", "c", "d", "e"};
        Atom[] atoms = {
                new Atom("isModality", "a"),
                new Atom("isProperty", "b"),
                new Atom("isLogicConnector", "c"),
                new Atom("isProperty", "d"),
                new Atom("isProperty", "e"),
                new Atom("arg+", "a", "b"),
                new Atom("condition", "a", "c"),
                new Atom("op1", "c", "d"),
                new Atom("op2", "c", "e"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery25() {
        String queryId = "Q25";
        String[] vars = {"a", "b", "c", "d"};
        Atom[] atoms = {
                new Atom("isProperty", "a"),
                new Atom("isLogicConnector", "b"),
                new Atom("isProperty", "c"),
                new Atom("isProperty", "d"),
                new Atom("arg+", "a", "b"),
                new Atom("op1", "b", "c"),
                new Atom("op2", "b", "d"),

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }


    //==================================================================
    // Compositional Query
    //==================================================================

    // ------ High Level Query

    private void addQuery101() {
        String queryId = "C-Q01";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("domain", "a", "b"),
                new Atom("time", "a", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery102() {
        String queryId = "C-Q02";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("is#property", "b"),
                new Atom("arg+", "a", "b"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery103() {
        String queryId = "C-Q03";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("time", "a", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery104() {
        String queryId = "C-Q04";
        String[] vars = {"b", "c"};
        Atom[] atoms = {
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("time", "b", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery105() {
        String queryId = "C-Q05";
        String[] vars = {"b", "c"};
        Atom[] atoms = {
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("frequency", "b", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery106() {
        String queryId = "C-Q06";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("condition", "a", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery107() {
        String queryId = "C-Q07";
        String[] vars = {"b", "c"};
        Atom[] atoms = {
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("condition", "b", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery108() {
        String queryId = "C-Q08";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("arg+", "a", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery109() {
        String queryId = "C-Q09";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("manner", "b", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery109b() {
        String queryId = "C-Q09b";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("is#property", "b"),
                new Atom("is#context", "c"),
                new Atom("arg+", "a", "b"),
                new Atom("manner", "a", "c"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }


    // ------ Context Query

    private void addQuery111() {
        String queryId = "C-Q11";
        String[] vars = {"a", "x"};
        Atom[] atoms = {
                new Atom("is#property", "a"),
                new Atom("time", "x", "a"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery112() {
        String queryId = "C-Q12";
        String[] vars = {"a", "x"};
        Atom[] atoms = {
                new Atom("is#property", "a"),
                new Atom("condition", "x", "a"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery112m() {
        String queryId = "C-Q12m";
        String[] vars = {"a", "x"};
        Atom[] atoms = {
                new Atom("is#property", "a"),
                new Atom("manner", "x", "a"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery113() {
        String queryId = "C-Q13";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("is#temporality", "a"),
                new Atom("is#property", "b"),
                new Atom("op1", "a","b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery114() {
        String queryId = "C-Q14";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("isRecurringEvent", "a"),
                new Atom("is#context", "b"),
                new Atom("arg+", "a","b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery115() {
        String queryId = "C-Q15";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("isRecurringEvent", "a"),
                new Atom("is#property", "b"),
                new Atom("arg+", "a","b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery116() {
        String queryId = "C-Q16";
        String[] vars = {"a", "b", "c"};
        Atom[] atoms = {
                new Atom("is#property", "a"),
                new Atom("is#property", "b"),
                new Atom("isLogicConnector", "c"),
                new Atom("op1", "c", "a"),
                new Atom("op2", "c", "b"),
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery117() {
        String queryId = "C-Q17";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("is#temporality", "a"),
                new Atom("is#context", "b"),
                new Atom("op1", "a","b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }



    // ------ Modality Query

    private void addQuery121() {
        String queryId = "C-Q21";
        String[] vars = {"a"};
        Atom[] atoms = {
                new Atom("isModality", "a")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery122() {
        String queryId = "C-Q22";
        String[] vars = {"a"};
        Atom[] atoms = {
                new Atom("is#modality", "a"),
                new Atom("polarity", "a", "-")

        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }


    // ------ Property Query

    private void addQuery131() {
        String queryId = "C-Q31";
        String[] vars = {"a"};
        Atom[] atoms = {
                new Atom("isProperty", "a")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }

    private void addQuery132() {
        String queryId = "C-Q32";
        String[] vars = {"a", "b"};
        Atom[] atoms = {
                new Atom("is#property", "a"),
                new Atom("isEntity", "b"),
                new Atom("name", "a", "b")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }


    // ------ Temporality Query

    private void addQuery141() {
        String queryId = "C-Q41";
        String[] vars = {"a"};
        Atom[] atoms = {
                new Atom("isTemporality", "a")
        };
        Query query = defineFormula(vars, atoms);
        this.queryMap.put(queryId, query);
    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public QueryMap() {
        this.queryMap = new HashMap<>();
        this.addQuery1();
        this.addQuery2();
        this.addQuery3();
        this.addQuery4();
        this.addQuery5();
        this.addQuery6();
        this.addQuery7();
        this.addQuery8();
        this.addQuery9();
        this.addQuery10();
        this.addQuery11();
        this.addQuery12();
        this.addQuery13();
        this.addQuery14();
        this.addQuery15();
        this.addQuery16();
        this.addQuery17();
        this.addQuery18();
        this.addQuery19();
        this.addQuery20();
        this.addQuery21();
        this.addQuery22();
        this.addQuery23();
        this.addQuery24();
        this.addQuery25();
        // --- Compositional Query
        this.addQuery101();
        this.addQuery102();
        this.addQuery103();
        this.addQuery104();
        this.addQuery105();
        this.addQuery106();
        this.addQuery107();
        this.addQuery108();
        this.addQuery109();
        this.addQuery109b();
        this.addQuery111();
        this.addQuery112();
        this.addQuery112m();
        this.addQuery113();
        this.addQuery114();
        this.addQuery115();
        this.addQuery116();
        this.addQuery117();
        this.addQuery121();
        this.addQuery122();
        this.addQuery131();
        this.addQuery132();
        this.addQuery141();
    }

    //==================================================================
    // Accessor(s)
    //==================================================================

    public Query get(String queryId) {
        assert this.queryMap.containsKey(queryId):
                ("No query " + queryId + " in queryMap");
        return this.queryMap.get(queryId);
    }

}
