package AbstractSemantic.TransPatternObject;

import AbstractSemantic.Instance;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Parameters.Configuration.*;

/**
 * A token (statement mark) is an element of a statement template. It is
 * structured as a pair consisting of a token name (type) and a token
 * value (raw value or pointer).
 *
 * There are several types of token. It is used to give  character
 * string according to its type. Separator token and keyword token will
 * produce a constant string, while a feature token will produce the string
 * corresponding to the pointed feature. A variant token is associated
 * with a constant string, but the variant type indicates that this 
 * token could be replaced by a feature token.
 *
 */
public class Token {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final TokenType tokenType;
    private final String rawValue;
    private String pointerStr;
    private String variableId;
    private final String featureId;


    //==================================================================
    // Construction Methods
    //==================================================================

    /**
     * Evaluate if the raw string correspond to a given type, specified
     * by a reference table.
     *
     * @param rawStr (raw string), refTab (reference table)
     * @return true if rawStr correspond to the type specified by refTab
     */
    private static boolean isType(String rawStr, String[] refTab) {
        boolean result = false;
        for (String kw: refTab)
            if (rawStr.equals(kw)) result = true;
        return result;
    }

    /**
     * Evaluate if the raw string correspond to a white space.
     *
     * @param rawStr (raw string)
     * @return true if rawStr is a white space.
     */
    private static boolean isWhiteSpace(String rawStr) {
        return rawStr.matches(WHITE_SPACE_REGEX);
    }

    /**
     * Evaluate if the raw string correspond to a separator.
     *
     * @param rawStr (raw string)
     * @return true if rawStr is a separator.
     */
    private static boolean isSeparator(String rawStr) {
        return isType(rawStr, SEPARATOR_TOKEN_TAB);
    }

    /**
     * Evaluate if the raw string correspond to a keyword.
     *
     * @param rawStr (raw string)
     * @return true if rawStr is a keyword.
     */
    private static boolean isKeyword(String rawStr) {
        return isType(rawStr, KEYWORD_TOKEN_TAB);
    }

    /**
     * Evaluate the marker type by raw string analyzing.
     *
     * @param rawStr (raw string)
     * @return pointerStr (pointer as a string)
     */
    private static TokenType evalType(String rawStr) {

        // --- Result initialization with default value (undefined)
        TokenType result = TokenType.UNDEFINED;

        // --- Matcher for feature regex
        Pattern markPattern = Pattern.compile(FEATURE_REGEX);
        Matcher matcher = markPattern.matcher(rawStr);

        // --- Type evaluation
        if (rawStr.startsWith(ABSTRACTION_MARK)) {
            if (matcher.find()) result = TokenType.FEATURE;
        }
        else {
            if (isWhiteSpace(rawStr)) result = TokenType.WHITESPACE;
            else if (isSeparator(rawStr)) result = TokenType.SEPARATOR;
            else if (isKeyword(rawStr)) result = TokenType.KEYWORD;
            else result = TokenType.VARIANT;
        }

        return result;

    }

    /**
     * Gives pointer as string by raw string analyzing.
     *
     * @param rawStr (raw string)
     * @return pointerStr (pointer as a string)
     */
    private static String evalPointer(String rawStr) {
        String result = "";
        Pattern markPattern = Pattern.compile(FEATURE_REGEX);
        Matcher matcher = markPattern.matcher(rawStr);
        if (matcher.find())
            result = matcher.toMatchResult().group();
        return result;
    }

    /**
     * Gives pointer as string defined with variable id
     * and feature id.
     *
     * @param variableId, featureId
     * @return pointerStr (pointer as a string)
     */
    public static String evalPointer(String variableId,
                                      String featureId) {
        return ABSTRACTION_MARK +
                variableId + "." +
                featureId;
    }

    /**
     * Gives variable id from pointer string.
     *
     * @param pointerStr (pointer string)
     * @return Variable Id
     */
    private static String evalVariableId(String pointerStr) {
        String result = UNDEFINED_TOKEN_STRING;
        if (pointerStr.contains(".")) {
            String target = pointerStr.substring(0, pointerStr.indexOf("."));
            Matcher matcher = Pattern.compile("[a-zA-Z0-9]+").matcher(target);
            if (matcher.find()) result = matcher.toMatchResult().group();
        }
        return result;
    }

    /**
     * Gives feature id from pointer string.
     *
     * @param pointerStr (pointer string)
     * @return Feature Id
     */
    private static String evalFeatureId(String pointerStr) {
        String result = UNDEFINED_TOKEN_STRING;
        if (pointerStr.contains(".")) {
            String target = pointerStr.substring(pointerStr.indexOf("."));
            Matcher matcher = Pattern.compile("[a-zA-Z]+").matcher(target);
            if (matcher.find()) result = matcher.toMatchResult().group();
        }
        return result;
    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Token(String rawValue) {

        this.tokenType = evalType(rawValue);

        switch (this.tokenType) {

            case WHITESPACE:

            case SEPARATOR:

            case KEYWORD:

            case VARIANT:
                this.rawValue = rawValue;
                this.pointerStr = UNDEFINED_TOKEN_STRING;
                this.variableId = UNDEFINED_TOKEN_STRING;
                this.featureId = UNDEFINED_TOKEN_STRING;
                break;

            case FEATURE:
                this.rawValue = UNDEFINED_TOKEN_STRING;
                this.pointerStr = evalPointer(rawValue);
                this.variableId = evalVariableId(this.pointerStr);
                this.featureId = evalFeatureId(this.pointerStr);
                break;
            
            default:
                this.rawValue = UNDEFINED_TOKEN_STRING;
                this.pointerStr = UNDEFINED_TOKEN_STRING;
                this.variableId = UNDEFINED_TOKEN_STRING;
                this.featureId = UNDEFINED_TOKEN_STRING;
                break;

        }

    }


    public Token(String variableId, String featureId) {
        this.tokenType = TokenType.FEATURE;
        this.rawValue = UNDEFINED_TOKEN_STRING;
        this.pointerStr = evalPointer(variableId, featureId);
        this.variableId = variableId;
        this.featureId = featureId;
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public TokenType getTokenType() {
        return tokenType;
    }


    public String getRawValue() {
        return this.rawValue;
    }


    public String getVariableId() {
        return variableId;
    }


    public String getPointerStatement() {
        return this.pointerStr;
    }


    public String getFeatureStatement(Map<String, Instance> variableMap) {

        String resultStatement = UNDEFINED_TOKEN_STRING;

        if (variableMap.containsKey(this.variableId)) {
            Instance instance = variableMap.get(this.variableId);
            resultStatement = instance.getFeatureValue(this.featureId);
        }

        return resultStatement;

    }

    public String getRawMarkString() {

        String rawMarkString;

        switch (this.tokenType) {

            case WHITESPACE:

            case SEPARATOR:

            case KEYWORD:

            case VARIANT:
                rawMarkString = this.getRawValue();
                break;

            case FEATURE:
                rawMarkString = this.getPointerStatement();
                break;

            default:
                rawMarkString = UNDEFINED_TOKEN_STRING;
                break;

        }

        return rawMarkString;

    }


    // ---------- Predicate(s)

    public boolean isType(TokenType tokenType) {
        return this.getTokenType().equals(tokenType);
    }


    // ---------- Setter

    public void setVariableId(String variableId) {
        this.variableId = variableId;
        this.pointerStr = evalPointer(this.variableId, this.featureId);
    }


    //==================================================================
    // Basic Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Token{" +
                "name=" + tokenType +
                ", rawValue='" + rawValue + '\'' +
                ", pointerStr='" + pointerStr + '\'' +
                ", variableId='" + variableId + '\'' +
                ", featureId='" + featureId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token = (Token) o;
        return getTokenType() == token.getTokenType() &&
                Objects.equals(getRawValue(), token.getRawValue()) &&
                Objects.equals(pointerStr, token.pointerStr) &&
                Objects.equals(getVariableId(), token.getVariableId()) &&
                Objects.equals(featureId, token.featureId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTokenType(), getRawValue(), pointerStr, getVariableId(), featureId);
    }


    //==================================================================
    // Main Method(s)
    //==================================================================

    /**
     * Give the marker statement using variable map if necessary.
     *
     * @param variableMap (map of variables)
     * @return Marker statement
     */
    public String getStatement(Map<String, Instance> variableMap) {

        String resultStatement;

        switch (this.tokenType) {

            case WHITESPACE:

            case SEPARATOR:

            case KEYWORD:

            case VARIANT:
                resultStatement = this.getRawValue();
                break;

            case FEATURE:
                resultStatement = this.getFeatureStatement(variableMap);
                break;

            default:
                resultStatement = UNDEFINED_TOKEN_STRING;
                break;

        }

        return resultStatement;

    }


    /**
     * Give the mark statement only for KEYWORD OR VARIANT
     * (undefined statement else).
     *
     * @return Mark statement
     */
    public String getStatement() {

        String resultStatement;

        switch (this.tokenType) {

            case WHITESPACE:

            case SEPARATOR:

            case KEYWORD:

            case VARIANT:
                resultStatement = this.getRawValue();
                break;

            default:
                resultStatement = UNDEFINED_TOKEN_STRING;
                break;

        }

        return resultStatement;

    }


}
