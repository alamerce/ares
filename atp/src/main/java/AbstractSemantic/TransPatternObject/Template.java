package AbstractSemantic.TransPatternObject;

import AbstractSemantic.Instance;
import AbstractSemantic.Interpretation;
import AbstractSemantic.TransPattern;
import LogicalQuery.Atom;
import Parameters.Configuration;
import Parameters.DefaultValues;
import Utility.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Parameters.Configuration.*;

public class Template {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final String concept;
    private final List<Token> tokenList;
    private Map<String, Instance> variableMap;


    //==================================================================
    // Construction Method(s)
    //==================================================================

    /**
     * Gives a set of marks from a template.
     *
     * @param rawStr
     * @return Marker list
     */
    public static List<Token> evalMarkerList(String rawStr) {

        List<Token> resultList = new ArrayList<>();

        // Find feature marker using Matcher class
        Pattern markPattern = Pattern.compile(FEATURE_REGEX);
        Matcher matcher = markPattern.matcher(rawStr);

        // Browse feature marker find by matching
        String markerStr;
        int beginIndex = 0; // Use to select sub-string
        int endIndex;
        while(matcher.find()) {

            // (1) Add constant type marker
            endIndex = matcher.start();
            markerStr = rawStr.substring(beginIndex, endIndex);
            resultList.add(new Token(markerStr));

            // (2) Add feature type marker
            beginIndex = matcher.start();
            endIndex = matcher.end();
            markerStr = rawStr.substring(beginIndex, endIndex);
            resultList.add(new Token(markerStr));

            // New begin index next to matcher end
            beginIndex = matcher.end();

        }

        if(beginIndex < rawStr.length()) {
            // Add final marker (constant type)
            markerStr = rawStr.substring(beginIndex);
            resultList.add(new Token(markerStr));
        }

        return resultList;

    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Template(String concept, List<Token> tokenList) {
        this.concept = concept;
        this.tokenList = tokenList;
        this.variableMap = new HashMap<>();
    }

    public Template(String concept, String rawStr) {
        this.concept = concept;
        this.tokenList = evalMarkerList(rawStr);
        this.variableMap = new HashMap<>();
    }

    public Template() {
        this.concept = DefaultValues.NO_CONCEPT;
        this.tokenList = new ArrayList<>();
        this.variableMap = new HashMap<>();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public String getConcept() {
        return concept;
    }

    public List<Token> getTokenList() {
        return tokenList;
    }

    public Map<String, Instance> getVariableMap() {
        return variableMap;
    }

    public List<String> getVariableIdList() {
        List<String> variableIdList = new ArrayList<>();
        for(Token token : this.tokenList)
            variableIdList.add(token.getVariableId());
        return variableIdList;
    }

    // ---------- Setters

    public void setVariableMap(Map<String, Instance> variableMap) {
        this.variableMap = variableMap;
    }


    /**
     * Changes variable id according to the given mapping.
     *
     * @param changeIdMap (variable id mapping)
     */
    public void changeVariableId(Map<String, String> changeIdMap) {

        for(Map.Entry<String, String> e: changeIdMap.entrySet()) {
            String targetId = e.getKey();
            String newId = e.getValue();
            for (Token t: this.tokenList) {
                if (t.getVariableId().equals(targetId))
                    t.setVariableId(newId);
            }
        }

    }



    //==================================================================
    // Basic Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Template{" +
                "markerList=" + tokenList +
                ", variableMap=" + variableMap +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Template template = (Template) o;
        return Objects.equals(this.getRawTemplateString(), template.getRawTemplateString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTokenList());
    }

    //==================================================================
    // Main Method
    //==================================================================

    public static String getRawTemplateString(List<Token> tokenList) {
        String result = "";
        for(Token token: tokenList) result += token.getRawMarkString();
        return result;
    }

    /**
     * Gives the raw template as string, namely a string obtained by
     * concatenation of raw marker string.
     */
    public String getRawTemplateString() {
        return getRawTemplateString(this.tokenList);
    }


    /**
     * Gives the formatted statement following the template and assigned
     * variables.
     *
     * @return Statement as string
     */
    public String getStatement() {
        String resultStm = "";
        for(Token token : this.tokenList)
            resultStm += token.getStatement(this.variableMap);
        resultStm = Configuration.NORMALIZER.normalize(resultStm);
        return resultStm;
    }


    /**
     * Gives a new instance corresponding to the template concept,
     * with a statement feature.
     *
     * @return Instance
     */
    public Instance getInstance(String variableId, TransPattern pattern) {
        Instance instance = new Instance(variableId, this.concept, pattern);
        instance.addFeature(FEATURE_STATEMENT_ID, this.getStatement());
        return instance;
    }


    /**
     * Gives the predicate indicating that there is an instance of the
     * concept associated with the model.
     *
     * @return Atom
     */
    public Atom getPredicate(String variableId) {
        String relation = "is" + StringUtils.capitalizeFirstLetter(this.concept);
        return new Atom(relation, variableId);
    }

    public Atom getPredicate() {
        return getPredicate("x");
    }

    /**
     * Gives a new interpretation, with concept, instance and predicate
     * defined following the template.
     *
     * @param variableId (id to concept instance), pattern
     * @return Interpretation
     */
    public Interpretation getInterpretation(String variableId, TransPattern pattern) {
        Interpretation interpretation = new Interpretation(this.concept);
        interpretation.addConcept(this.getConcept());
        interpretation.addInstance(this.getInstance(variableId, pattern));
        interpretation.addPredicate(this.getPredicate(variableId));
        return interpretation;
    }

}