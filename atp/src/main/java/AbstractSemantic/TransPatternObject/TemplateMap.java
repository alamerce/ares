package AbstractSemantic.TransPatternObject;

import java.util.HashMap;
import java.util.Map;

import static Parameters.Configuration.OUTPUT_ABSTRACT_CONCEPT;

/**
 * Class to define the templates used for transduction pattern.
 *
 * This class is temporary, pending templates definition in files.
 *
 */
public class TemplateMap {

    //==================================================================
    // Attributes
    //==================================================================

    private Map<String, Template> templateMap;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public TemplateMap() {
        this.templateMap = new HashMap<>();
        this.templateMap.put(
                "T01",
                new Template("old","<#a.modality, #b.property, #c.property>")
        );
        this.templateMap.put(
                "T02",
                new Template("old","<#a.modality, #b.property, True>")
        );
        this.templateMap.put(
                "T03",
                new Template("old","<necessary, #a.property, #b.property>")
        );
        this.templateMap.put(
                "T04",
                new Template("old","<necessary, #a.property, #b.temporality #c.property>")
        );
        this.templateMap.put(
                "T05",
                new Template("old","<#a.modality, #b.property, #c.temporality #d.property>")
        );
        this.templateMap.put(
                "T06",
                new Template("old","<#a.modality, #b.property, #d.property>")
        );
        this.templateMap.put(
                "T07",
                new Template("old","<#a.modality, not #b.property, not #c.property>")
        );
        this.templateMap.put(
                "T08",
                new Template("old","<#a.modality, #b.property, #c.property and #d.property>")
        );
        this.templateMap.put(
                "T09",
                new Template("old","<necessary, not #a.property, not #b.temporality #c.property>")
        );
        this.templateMap.put(
                "T10",
                new Template("old","<#a.modality, not #b.property, not #c.temporality #d.property>")
        );
        this.templateMap.put(
                "T11",
                new Template("old","<#a.modality, #b.property, #d.property #c.logicConnector #e.property>")
        );
        this.templateMap.put(
                "T12",
                new Template("old","<necessary, #a.property, #c.property #b.logicConnector #d.property>")
        );

        // --- Compositional Template

        // ------ Out

        this.templateMap.put(
                "C-T01",
                new Template( OUTPUT_ABSTRACT_CONCEPT ,"<#a.statement, #b.statement, #c.statement>")
        );

        this.templateMap.put(
                "C-T02",
                new Template( OUTPUT_ABSTRACT_CONCEPT, "<#a.statement, #b.statement, True>")
        );

        this.templateMap.put(
                "C-T03",
                new Template( OUTPUT_ABSTRACT_CONCEPT, "<necessary, #b.statement, #c.statement>")
        );


        // ------ Context

        this.templateMap.put(
                "C-T11",
                new Template("#context", "#a.statement")
        );

        this.templateMap.put(
                "C-T12",
                new Template("#context", "#a.statement #b.statement")
        );

        this.templateMap.put(
                "C-T13",
                new Template("#context", "#b.statement")
        );

        this.templateMap.put(
                "C-T14",
                new Template("#context", "#a.statement #c.logicConnector #b.statement")
        );


        // ------ Modality

        this.templateMap.put(
                "C-T21",
                new Template("#modality", "#a.modality")
        );

        this.templateMap.put(
                "C-T22",
                new Template("#modality", "not #a.statement")
        );


        // ------ Property

        this.templateMap.put(
                "C-T31",
                new Template("#property", "#a.property")
        );

        this.templateMap.put(
                "C-T32",
                new Template("#property", "not #a.property")
        );

        this.templateMap.put(
                "C-T33",
                new Template("#property", "#a.statement_#b.entity")
        );



        // ------ Temporality

        this.templateMap.put(
                "C-T41",
                new Template("#temporality", "#a.temporality")
        );

    }

    //==================================================================
    // Accessor(s)
    //==================================================================

    public Template get(String templateId) {
        assert this.templateMap.containsKey(templateId):
                ("No template " + templateId + " in templateMap");
        return this.templateMap.get(templateId);
    }

}
