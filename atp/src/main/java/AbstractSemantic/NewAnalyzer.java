package AbstractSemantic;

import ComputationObject.OutputStatement;
import LogicalQuery.Query;
import Parameters.Configuration;

import java.util.*;

import static Parameters.Configuration.FEATURE_STATEMENT_ID;
import static Parameters.Configuration.OUTPUT_ABSTRACT_CONCEPT;

/**
 * The AS Analyzer class is the core of the AS package. It makes it
 * possible to analyze the semantic representation of a statement to
 * define an abstract semantics. This definition is then used to
 * produce the semantic data in the expected format.
 *
 */
public class NewAnalyzer {

    //==================================================================
    // Attributes
    //==================================================================


    // ---------- Base Attribute

    private final String statementId;
    private final InterpretationRun interpretationRun;
    private final TransPatternSet transPatternSet;


    // ---------- Tracking Attribute

    // TODO: interpretRound (number of interpret round)
    // TODO: interpretTrace (list: interpretation/patternSet)


    // ---------- Result Computing Attribute(s)
    int precisionLowLimit = 0; // use to keep the most precise requests


    //==================================================================
    // Constructor
    //==================================================================

    public NewAnalyzer(Interpretation interpretation, TransPatternSet pSet) {
        this.statementId = interpretation.getStatementId();
        this.interpretationRun = new InterpretationRun(interpretation);
        this.transPatternSet = pSet;
    }


    //==================================================================
    // Accessors
    //==================================================================

    // ---------- Getters

    public String getStatementId() {
        return statementId;
    }

    public InterpretationRun getInterpretationRun() {
        return interpretationRun;
    }

    public TransPatternSet getTransPatternSet() {
        return transPatternSet;
    }

    public Set<Instance> getStatementInstanceSet(
            int interpretationIndex, String targetConcept) {

        Set<Instance> resultSet = new HashSet<>();

        Interpretation interpretation =
                this.interpretationRun.getInterpretation(interpretationIndex);
        for(Instance instance: interpretation.getInstanceSet()) {
            if ((instance.getConcept().equals(targetConcept)) &&
                    (instance.containsFeature(FEATURE_STATEMENT_ID)))
                resultSet.add(instance);
        }

        return resultSet;

    }

    public Set<Instance> getStatementInstanceSet(String targetConcept) {
        int lastIndex = this.interpretationRun.getLastIndex();
        return getStatementInstanceSet(lastIndex, targetConcept);

    }

    public Set<Instance> getOutputStatementInstanceSet() {
        return getStatementInstanceSet(OUTPUT_ABSTRACT_CONCEPT);
    }


    //==================================================================
    // Interpretation Update Method
    //==================================================================

    /**
     * Update the interpretation by applying all interpretation round,
     * i.e. by applying patterns round by round.
     */
    public void updateMaximumInterpretation() {

        //System.out.println(" ///// NewAnalyzer: updateMaximumInterpretation"); // TODO: (DEV) to remove
        this.interpretationRun.initialize();

        int roundMin = transPatternSet.getMinimalDepth();
        int roundMax = transPatternSet.getMaximumDepth() + 1;
        if (transPatternSet.getMaximumDepth() == 1)  roundMax = 1; // Special Case
        //System.out.println(" ///// -- Min/Max Depth: " + roundMin + " / " + roundMax); // TODO: (DEV) to remove

        for(int round = roundMin; round <= roundMax ; round++) {
            //System.out.println(" ///// ----- Round: " + round); // TODO: (DEV) to remove
            Set<TransPattern> roundPatternSet = transPatternSet.getSet(round);
            //System.out.println(" ///// ----- Round Set Size: " + roundPatternSet.size()); // TODO: (DEV) to remove
            this.interpretationRun.update(round, roundPatternSet);
        }

    }


    //==================================================================
    // OutputStatement Giving Method(s)
    //==================================================================

    private List<OutputStatement> getBaseOutputStatementList() {

        List<OutputStatement> resultSet = new ArrayList<>();
        Set<Instance> osInstanceSet = getOutputStatementInstanceSet();
        Interpretation interpretation = interpretationRun.getLastInterpretation();

        for (Instance osInstance: osInstanceSet) {
            OutputStatement os = new OutputStatement(osInstance);
            os.addAnalyzedPatternsNumber(transPatternSet.size());
            os.addAnalyzedPatternsNumber(interpretation.getInstanceSet().size());
            os.addAnalyzedPatternsNumber(osInstanceSet.size());
            resultSet.add(os);
        }

        if (resultSet.isEmpty()) resultSet.add(new OutputStatement());

        for(OutputStatement os: resultSet) os.addAnalyzedPatternsNumber(resultSet.size());

        return resultSet;

    }


    /**
     * Keep only the statements obtained by the deepest analyzes.
     */
    private List<OutputStatement> filterOutputByDeepestAnalyzes(
            List<OutputStatement> oss) {

        List<OutputStatement> resultSet = new ArrayList<>();
        int maxDepth = 0;

        for(OutputStatement os: oss)
            maxDepth = Math.max(maxDepth, os.getAnalysisDepth());
        maxDepth = Math.min(getTransPatternSet().getMaximumDepth(), maxDepth); // TODO: to remove?

        for(OutputStatement os: oss) {
            if (os.getAnalysisDepth() >= maxDepth) resultSet.add(os);
        }

        for(OutputStatement os: resultSet) os.addAnalyzedPatternsNumber(resultSet.size());

        return resultSet;

    }

    /**
     * Keep only the statements obtained by the deepest analyzes.
     */
    private List<OutputStatement> filterOutputByQueryPrecision(
            List<OutputStatement> oss) {

        List<OutputStatement> resultSet = new ArrayList<>();

        //this.precisionLowLimit = 0; // use to keep the most precise requests // TODO: to remove
        int maxPrecision = 0;

        for(OutputStatement os: oss) {
            Query query = os.getProducerPattern().getQuery();
            maxPrecision = Math.max(maxPrecision, query.getPrecision());
        }
        maxPrecision = Math.min(maxPrecision, Configuration.LOW_LIMIT_MAX);

        for(OutputStatement os: oss) {
            Query query = os.getProducerPattern().getQuery();
            if (query.getPrecision() >= maxPrecision) resultSet.add(os);
        }

        for(OutputStatement os: resultSet) os.addAnalyzedPatternsNumber(resultSet.size());

        return resultSet;

    }


    /**
     * TODO
     */
    public List<OutputStatement> getOutputStatementList() {
        List<OutputStatement> resultList;
        resultList = getBaseOutputStatementList();
        resultList = filterOutputByDeepestAnalyzes(resultList);
        resultList = filterOutputByQueryPrecision(resultList);
        return resultList;
    }


    /**
     * TODO
     */
    public Set<OutputStatement> getBestOutputStatementSet() {

        List<OutputStatement> osList = getOutputStatementList();
        Set<OutputStatement> resultSet = new HashSet<>(); // Set to remove duplicates

        // --- Keep only the statements associated with the best PPI (precision pattern indicator)
        double bestPrecision = 0;
        double toleranceLevel = 0.02; // TODO: move tolerance on parameters
        double ppi1; // First Line PPI
        for(OutputStatement os: osList) {
            ppi1 = os.getProducerPattern().getPrecisionFirstLine();
            if(ppi1 > (bestPrecision + toleranceLevel)) {
                resultSet.clear();
                resultSet.add(os);
                bestPrecision = ppi1;
            }
            else if(ppi1 > bestPrecision) {
                resultSet.add(os);
                bestPrecision = ppi1;
            }
            else if(ppi1 > (bestPrecision - toleranceLevel)) {
                resultSet.add(os);
            }
        }

        // --- Default Statement if No Matching
        if (resultSet.isEmpty()) resultSet.add(new OutputStatement());

        // --- Update Statistic
        for(OutputStatement os: resultSet)
            os.addAnalyzedPatternsNumber(resultSet.size());

        return resultSet;

    }



    /**
     * Generates the best precise statement corresponding to the
     * interpretation.
     *
     * @return a statement corresponding to the definition
     */
    public String getBestOutputStatement() {
        String result = "-";
        double bestPrecision = -1;
        boolean c1, c2;
        for(Instance instance: getOutputStatementInstanceSet()) {
            c1 = bestPrecision == -1;
            c2 = instance.getPpi() >= bestPrecision;
            if (c1 || c2) {
                result = instance.getFeatureValue(FEATURE_STATEMENT_ID);
                bestPrecision = instance.getPpi();
            }
        }
        return result;
    }


}
