package SourceAnalyzer;

import LogicalQuery.Atom;
import SourceAnalyzer.Amr.*;
import SourceAnalyzer.Amr.Element.Entity;
import SourceAnalyzer.Amr.Element.Modality;
import SourceAnalyzer.Amr.Element.Property;
import SourceAnalyzer.Amr.Element.Temporality;
import SourceAnalyzer.SymbolTable.Role;
import SourceAnalyzer.SymbolTable.Symbol;
import SourceAnalyzer.SymbolTable.Table;

import java.util.*;

public class CapturedData {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final SourceType sourceType;
    private Classifier classifier;
    private Map<String, Map<String, String>> dataMap;
    private Set<Atom> atomSet;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public CapturedData(SourceType sourceType) {
        this.sourceType = sourceType;
        this.classifier = new Classifier();
        this.dataMap = new HashMap<>();
        this.atomSet = new HashSet<>();
    }


    //==================================================================
    // Accessors
    //==================================================================

    public Map<String, Map<String, String>> getDataMap() {
        return dataMap;
    }

    public Set<Atom> getAtomSet() {
        return atomSet;
    }


    //==================================================================
    // Analyze Method(s)
    //==================================================================

    /**
     * Construct Concept map from Symbol Table using classifier.
     * Ex. : ("x9","gate")
     *
     * @param symbolTable
     * @return Concept Map
     */
    public Map<String, String>  consConceptMap(Table symbolTable) {
        Map<String, String> conceptMap = new HashMap<>();
        for (Symbol symbol: symbolTable.getSymbolList())
            conceptMap.put(symbol.getIdent(), symbol.getConcept());
        return conceptMap;
    }

    // TODO: consXxxMap Methods for each XxxMap


    /**
     * Construct Entity Map from Symbol Table, using classifier.
     * Ex. : ("x2","winter-coat")
     *
     * @param symbolTable
     * @return Entity Map
     */
    public Map<String, String>  consEntityMap(Table symbolTable) {

        Map<String, String> entityMap = new HashMap<>();

        Set<Entity> entitySet = new HashSet<>();

        // --- Update Entity Set from Symbol Table using classifier
        Entity amrEntity;
        for (Symbol symbol: symbolTable.getSymbolList())
            if (classifier.isEntity(symbol)) {
                amrEntity = new Entity(symbol);
                amrEntity.assignNamesToArgument(symbolTable);
                entitySet.add(amrEntity);
            }

        // --- Update Entity Map
        for (Entity p: entitySet)
            entityMap.put(p.getId(), p.getName());

        return entityMap;

    }

    /**
     * Construct Property Map from Symbol Table, using classifier.
     * Ex. : ("x10","open_gate")
     *
     * @param symbolTable
     * @return Property Map
     */
    public Map<String, String>  consPropertyMap(Table symbolTable) {

        Map<String, String> propertyMap = new HashMap<>();

        Set<Entity> entitySet = new HashSet<>();
        Set<Property> propertySet = new HashSet<>();

        // --- Update Entity Set from Symbol Table using classifier
        Entity amrEntity;
        for (Symbol symbol: symbolTable.getSymbolList())
            if (classifier.isEntity(symbol)) {
                amrEntity = new Entity(symbol);
                amrEntity.assignNamesToArgument(symbolTable);
                entitySet.add(amrEntity);
            }

        // --- Update Property Set from Symbol Table using classifier
        Property amrProperty;
        for (Symbol symbol: symbolTable.getSymbolList())
            if (classifier.isProperty(symbol)) {
                amrProperty = new Property(symbol);
                amrProperty.assignNamesToArgument(symbolTable);
                propertySet.add(amrProperty);
            }

        // --- Update name of properties
        for (Property p: propertySet) p.updateEntityArgNames(entitySet);
        for (Property p: propertySet) p.updatePropertyArgNames(propertySet);

        // --- Update Property Map
        for (Property p: propertySet)
            propertyMap.put(p.getId(), p.getName());

        return propertyMap;

    }


    /**
     * TODO
     */
    public void analyze(Table symbolTable) {

        // Init all map
        //Map<String, String> conceptMap = new HashMap<>();
        Map<String, String> modalityMap = new HashMap<>();// TODO: consXxxMap Methods for each XxxMap
        Map<String, String> temporalityMap = new HashMap<>();
        Map<String, String> logicConnectorMap = new HashMap<>();
        Map<String, String> frequencyMap = new HashMap<>();
        Map<String, String> adverbMap = new HashMap<>();

        // Init all AMR Reference classes
        Modality amrModality;
        Temporality amrTemporality;

        // Browse the symbols and update map using classifier
        String ident, concept, featureName;
        for (Symbol symbol: symbolTable.getSymbolList()) { // TODO: to remove after consXxxMap Methods for each XxxMap

            ident = symbol.getIdent();
            concept = symbol.getConcept();

            // Update conceptMap
            //conceptMap.put(ident, concept);//ex.: ("x9","gate");

            // Update modalityMap (conditional)
            if (classifier.isModality(symbol)) {
                amrModality = new Modality(symbol);
                featureName = amrModality.getName();
                modalityMap.put(ident, featureName);//ex.: ("x6", "necessary")
            }

            // Update temporalityMap (conditional)
            if (classifier.isTemporality(symbol)) {
                amrTemporality = new Temporality(symbol);
                featureName = amrTemporality.getName();
                temporalityMap.put(ident, featureName);//ex.: ("x6", "before")
            }

            // Update logicConnectorMap (conditional)
            if (classifier.isLogicConnector(symbol)) {
                featureName = classifier.evalBasicFeature(symbol);
                logicConnectorMap.put(ident, featureName);//ex.: ("x6", "and")
            }

            // Update logicConnectorMap (conditional)
            if (classifier.isFrequency(symbol)) {
                featureName = classifier.evalBasicFeature(symbol);
                frequencyMap.put(ident, featureName);//ex.: ("x6", "and")
            }

            // Update logicConnectorMap (conditional)
            if (classifier.isAdverb(symbol)) {
                featureName = classifier.evalBasicFeature(symbol);
                adverbMap.put(ident, featureName);//ex.: ("x6", "and")
            }

        }

        // Update dataMap
        dataMap.put("concept", consConceptMap(symbolTable)); // TODO: consXxxMap Methods for each XxxMap
        dataMap.put("modality", modalityMap);
        dataMap.put("temporality", temporalityMap);
        dataMap.put("logicConnector", logicConnectorMap);
        dataMap.put("frequency", frequencyMap);
        dataMap.put("adverb", adverbMap);
        dataMap.put("entity", consEntityMap(symbolTable));
        dataMap.put("property", consPropertyMap(symbolTable));

        // Browse the symbols and update atom set
        String roleName, roleValue;
        List<String> tagList;
        for (Symbol symbol: symbolTable.getSymbolList()) {

            ident = symbol.getIdent();
            //concept = symbol.getConcept();

            // Add isModality (conditional)
            if (classifier.isModality(symbol)) {
                atomSet.add(new Atom("isModality", ident));
            }

            // Add isEntity (conditional)
            if (classifier.isEntity(symbol)) {
                atomSet.add(new Atom("isEntity", ident));
            }

            // Add isProperty (conditional)
            if (classifier.isProperty(symbol)) {
                atomSet.add(new Atom("isProperty", ident));
            }

            // Add isTemporality (conditional)
            if (classifier.isTemporality(symbol)) {
                atomSet.add(new Atom("isTemporality", ident));
            }

            // Add isAdverb (conditional)
            if (classifier.isAdverb(symbol)) {
                atomSet.add(new Atom("isAdverb", ident));
            }

            // Add isLogicConnector (conditional)
            if (classifier.isLogicConnector(symbol)) {
                atomSet.add(new Atom("isLogicConnector", ident));
            }

            // Add tag(s)
            tagList = classifier.evalTags(symbol);
            for (String tag: tagList) atomSet.add(new Atom(tag, ident));

            // Browse roles and add relations (conditional)
            for (Role role: symbol.getRoleList()) {
                if (role.getRoleName().matches("ARG[1-9]+(-of)+")) { // inverse core roles +
                    roleName = "arg+";
                    roleValue = role.getInstanceId();
                    atomSet.add(new Atom(roleName, roleValue, ident));
                }
                else if (role.getRoleName().matches("ARG[1-9]+")) { // core roles +
                    roleName = "arg+";
                    roleValue = role.getInstanceId();
                    atomSet.add(new Atom(roleName, ident, roleValue));
                }
                else if (role.getRoleName().matches("ARG0(-of)+")) { // inverse core role 0
                    roleName = "arg0";
                    roleValue = role.getInstanceId();
                    atomSet.add(new Atom(roleName, roleValue, ident));
                }
                //if (!role.getRoleName().matches("arg[1-9]")) {
                else { // other roles
                    roleName = role.getRoleName();
                    roleValue = role.getInstanceId();
                    atomSet.add(new Atom(roleName, ident, roleValue));
                }
            }

        }

    }

}
