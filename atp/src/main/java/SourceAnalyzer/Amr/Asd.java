package SourceAnalyzer.Amr;

import Parameters.Category;
import Parameters.DefaultValues;
import SourceAnalyzer.SymbolTable.Symbol;
import SourceAnalyzer.SymbolTable.Table;
import java.util.List;
import java.util.ArrayList;

/**
 *  ASD of AMR
 */
public class Asd {

    /**
     *  Type/variant Representation
     */
    static public class Representation {

        String id;
        String snt;
        List<String> outList;
        Node node;

        /**
         * Constructor with default id
         */
        public Representation(Node node) {
            this.id = DefaultValues.NO_ID;
            this.snt = DefaultValues.NO_VALUE;
            this.outList = new ArrayList<>();
            this.node = node;
        }

        /**
         * Constructor with id
         */
        public Representation(String id, Node node) {
            this.id = id;
            this.snt = DefaultValues.NO_VALUE;
            this.outList = new ArrayList<>();
            this.node = node;
        }

        /**
         * Constructor with id and snt
         */
        public Representation(String id, String snt, Node node) {
            this.id = id;
            this.snt = snt;
            this.outList = new ArrayList<>();
            this.node = node;
        }

        /**
         * Constructor with id, snt and out
         */
        public Representation(String id, String snt, List<String> outList, Node node) {
            this.id = id;
            this.snt = snt;
            this.outList = outList;
            this.node = node;
        }

        /**
         * Default Representation
         */
        public Representation() {
            this.node = new Node(new Instance(""), new Concept(""), new ArrayList<>());
        }

        public String getId() {
            return id;
        }

        public String getSentence() {
            return snt;
        }

        public List<String> getOutputList() {
            List<String> resultList = new ArrayList<>();
            for(String out: this.outList)
                if (out.length() > 0 && out.lastIndexOf(" ") == (out.length() - 1))
                    resultList.add(out.substring(0, (out.length() - 1)));
            return resultList;
        }

        /**
         * Method to get a string corresponding to the type
         */
        public String toString() {
            return node.toString();
        }

        /**
         * Method to analyse the representation and construct a symbol table
         */
        public Table analyse() {
            Table symbolTable = new Table(this.id);
            return node.analyse(symbolTable);
        }

    }

    /**
     *  Type/variant Node
     */
    static public class Node {

        Instance instance;
        Concept concept;
        List<Edge> edgeList;

        /**
         * Constructor
         */
        public Node(Instance instance, Concept concept,
                    List<Edge> edgeList) {
            this.instance = instance;
            this.concept = concept;
            this.edgeList = edgeList;
        }

        /**
         * Method to get a string corresponding to the type
         */
        public String toString() {
            StringBuilder r = new StringBuilder();
            r.append("(");
            r.append(instance.toString()).append("/ ").append(concept.toString());
            edgeList.forEach((s) -> r.append("\n  ").append(s.toString()));
            r.append(")");
            return r.toString();
        }

        /**
         * Method to analyse the representation and construct a symbol table
         */
        public Table analyse(Table symbolTable) {
            List<SourceAnalyzer.SymbolTable.Role> nodeRoleList = new ArrayList<>();
            edgeList.forEach((e) -> nodeRoleList.add(e.getRole()));
            Symbol v = new Symbol(instance.getId(),
                    Category.UNKNOWN,
                    concept.getId(),
                    nodeRoleList);
            symbolTable.add(v);
            edgeList.forEach((e) -> e.analyse(symbolTable));
            return symbolTable;
        }

        /**
         * Getter for ID
         *
         * @return id
         */
        public String getId() {
            return this.instance.getId();
        }

    }

    /**
     *  Type/variant Edge
     */
    static public class Edge {

        Role role;
        Node node;

        /**
         * Constructor
         */
        public Edge(Role role, Node node) {
            this.role = role;
            this.node = node;
        }

        /**
         * Method to get a string corresponding to the type
         */
        public String toString() {
            return role.toString() + node.toString();
        }

        /**
         * Method to analyse the representation and construct a symbol table
         */
        public Table analyse(Table symbolTable) {
            return node.analyse(symbolTable);
        }

        /**
         * Getter for Role(roleId, nodeId)
         *
         * @return role
         */
        public SourceAnalyzer.SymbolTable.Role getRole() {
            return new SourceAnalyzer.SymbolTable.Role(role.getId(), node.getId());
        }

    }


    /**
     *  Type/variant Instance
     */
    static public class Instance {

        String id;

        /**
         * Constructor
         */
        public Instance(String value) {
            this.id = value;
        }

        /**
         * Method to get a string corresponding to the type
         */
        public String toString() {
            return id + " ";
        }

        /**
         * Getter for ID
         *
         * @return id
         */
        public String getId() {
            return this.id;
        }

    }


    /**
     *  Type/variant Concept
     */
    static public class Concept {

        String value;

        /**
         * Constructor
         */
        public Concept(String value) {
            this.value = value;
        }

        /**
         * Method to get a string corresponding to the type
         */
        public String toString() {
            return value + " ";
        }

        /**
         * Getter for ID
         *
         * @return id
         */
        public String getId() {
            return this.value;
        }

    }


    /**
     *  Type/variant Role
     */
    static public class Role {

        String value;

        /**
         * Constructor
         */
        public Role(String value) {
            this.value = value;
        }

        /**
         * Get a string corresponding to the type
         *
         * @return String corresponding to the type
         */
        public String toString() {
            return this.value + " ";
        }

        /**
         * Getter for ID
         *
         * @return id
         */
        public String getId() {
            return this.value;
        }

    }

}
