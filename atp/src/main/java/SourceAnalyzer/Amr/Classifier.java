package SourceAnalyzer.Amr;

import Parameters.Category;
import SourceAnalyzer.SymbolTable.Symbol;

import java.util.*;

/**
 *  SymbolTable.Classifier: this class provides various methods for categorizing or classifying symbol table entries.
 */
public class Classifier {

    //==================================================================
    // Attribute(s)
    //==================================================================

    private OntoMap ontoMap;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Classifier() {
        this.ontoMap = new OntoMap();
    }


    //==================================================================
    // Method(s) for categories evaluating
    //==================================================================

    /**
     * Test if a symbol is a modality.
     *
     * @param symbol (entry of the symbol table)
     * @return true if the symbol is a modality
     */
    public boolean isModality(Symbol symbol) {
        Set<String> refSet = this.ontoMap.getModalitySet();
        String mainConcept = symbol.getConcept();
        return refSet.contains(mainConcept);
    }

    /**
     * Test if a symbol is a temporality.
     *
     * @param symbol (entry of the symbol table)
     * @return true if the symbol is a modality
     */
    public boolean isTemporality(Symbol symbol) {
        Set<String> refSet = this.ontoMap.getTemporalitySet();
        String mainConcept = symbol.getConcept();
        return refSet.contains(mainConcept);
    }

    /**
     * Test if a symbol is a logic connector.
     *
     * @param symbol (entry of the symbol table)
     * @return true if the symbol is a logic connector
     */
    public boolean isLogicConnector(Symbol symbol) {
        Set<String> refSet = this.ontoMap.getLogicConnectorSet();
        String mainConcept = symbol.getConcept();
        return refSet.contains(mainConcept);
    }

    /**
     * Test if a symbol is a frequency indicator.
     *
     * @param symbol (entry of the symbol table)
     * @return true if the symbol is a logic connector
     */
    public boolean isFrequency(Symbol symbol) {
        Set<String> refSet = this.ontoMap.getFrequencySet();
        String mainConcept = symbol.getConcept();
        return refSet.contains(mainConcept);
    }

    /**
     * Test if a symbol is an adverb.
     *
     * @param symbol (entry of the symbol table)
     * @return true if the symbol is a logic connector
     */
    public boolean isAdverb(Symbol symbol) {
        Set<String> refSet = this.ontoMap.getAdverbSet();
        String mainConcept = symbol.getConcept();
        return refSet.contains(mainConcept);
    }

    /**
     * Test if a symbol is a logic connector.
     *
     * @param symbol (entry of the symbol table)
     * @return true if the symbol is a logic connector
     */
    public boolean isProperty(Symbol symbol) {
        Boolean condSet = true;
        condSet = condSet && !(isModality(symbol));
        condSet = condSet && !(isTemporality(symbol));
        condSet = condSet && !(isLogicConnector(symbol));
        condSet = condSet && !(isFrequency(symbol));
        condSet = condSet && !(isAdverb(symbol));
        Boolean c1 = !(symbol.getCoreRoleList().isEmpty()); // at least one role
        Boolean c2 = symbol.getConcept().matches("[a-zA-Z]*-[0-9]+");
        condSet = condSet && (c1 || c2);
        return condSet;
    }

    /**
     * Test if a symbol is a logic connector.
     *
     * @param symbol (entry of the symbol table)
     * @return true if the symbol is a logic connector
     */
    public boolean isEntity(Symbol symbol) {
        Boolean condSet = true;
        condSet = condSet && !(isModality(symbol));
        condSet = condSet && !(isTemporality(symbol));
        condSet = condSet && !(isLogicConnector(symbol));
        condSet = condSet && !(isFrequency(symbol));
        condSet = condSet && !(isAdverb(symbol));
        condSet = condSet && !(isProperty(symbol));
        Boolean c1 = (symbol.getCoreRoleList().isEmpty()); // no core role
        condSet = condSet && (c1);
        return condSet;
    }


    //==================================================================
    // Method(s) for tags evaluating
    //==================================================================

    public boolean isOnly(Symbol symbol) {
        Set<String> refSet =
                this.ontoMap.getAdverbRef("only");
        String targetConcept = symbol.getConcept();
        return refSet.contains(targetConcept);
    }

    public boolean isRecurringEvent(Symbol symbol) {
        Set<String> refSet =
                this.ontoMap.getFrequencyRef("recurring_event");
        String targetConcept = symbol.getConcept();
        return refSet.contains(targetConcept);
    }


    //==================================================================
    // Method(s) for general evaluation (category and tag)
    //==================================================================

    /**
     * Get the category corresponding to the symbol given
     * as input.
     *
     * @param symbol (entry of the symbol table)
     * @return category
     */
    public Category evalCategory(Symbol symbol) {

        Category result = Category.UNKNOWN;

        if (isModality(symbol)) {
            result = Category.MODALITY;
        } else if (isTemporality(symbol)) {
            result = Category.TEMPORALITY;
        } else if (isLogicConnector(symbol)) {
            result = Category.LOGIC_CONNECTOR;
        } else if (isFrequency(symbol)) {
            result = Category.FREQUENCY;
        } else if (isAdverb(symbol)) {
            result = Category.ADVERB;
        } else if (isProperty(symbol)) {
            result = Category.PROPERTY;
        } else if (isEntity(symbol)) {
            result = Category.ENTITY;
        }

        return result;

    }

    public List<String> evalTags(Symbol symbol) {
        List<String> tagList = new ArrayList<>();
        if (isOnly(symbol)) tagList.add("isOnly");
        if (isRecurringEvent(symbol))
            tagList.add("isRecurringEvent");
        return tagList;
    }

    public String evalBasicFeature(Symbol symbol) {
        String baseFeature =
                this.ontoMap.getReferenceName(symbol.getConcept());
        return baseFeature;
    }
}
