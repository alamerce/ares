package SourceAnalyzer.Amr;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *  Class to manage the reference ontology.
 */
public class OntoMap {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Map for modality

    private Map<String, Set<String>> modalityMap;

    private Set<String> possibleRef = new HashSet<>();
    private Set<String> necessaryRef = new HashSet<>();
    private Set<String> contingentRef = new HashSet<>();
    private Set<String> impossibleRef = new HashSet<>();


    // ---------- Map for temporality

    private Map<String, Set<String>> temporalityMap;

    private Set<String> afterRef = new HashSet<>();
    private Set<String> beforeRef = new HashSet<>();


    // ---------- Map for logic connector

    private Map<String, Set<String>> logicConnectorMap;

    private Set<String> conjunctionRef = new HashSet<>();
    private Set<String> disjunctionRef = new HashSet<>();
    private Set<String> polarityRef = new HashSet<>();


    // ---------- Map for frequency

    private Map<String, Set<String>> frequencyMap;

    private Set<String> recurringEventRef = new HashSet<>();


    // ---------- Map for adverb

    private Map<String, Set<String>> adverbMap;

    private Set<String> onlyRef = new HashSet<>();


    //==================================================================
    // Methods to prepare modality map
    //==================================================================

    private void prepareModality() {
        possibleRef.add("possible");
        possibleRef.add("possible-01");
        possibleRef.add("possible-02");
        possibleRef.add("allow-01");
        possibleRef.add("allow-02");
        possibleRef.add("authorize-01");
        possibleRef.add("recommend-01");
        this.modalityMap.put("possible", possibleRef);
        necessaryRef.add("necessary");
        necessaryRef.add("necessary-01");
        necessaryRef.add("necessary-02");
        necessaryRef.add("obligatory");
        necessaryRef.add("obligate-01");
        necessaryRef.add("need-01");
        this.modalityMap.put("necessary", necessaryRef);
        contingentRef.add("contingent");
        contingentRef.add("contingent-01");
        contingentRef.add("contingent-02");
        this.modalityMap.put("contingent", contingentRef);
        impossibleRef.add("impossible");
        impossibleRef.add("impossible-01");
        impossibleRef.add("impossible-02");
        impossibleRef.add("prohibit-01");
        impossibleRef.add("forbid-01");
        this.modalityMap.put("impossible", impossibleRef);
    }


    private void prepareTemporality() {
        afterRef.add("after");
        afterRef.add("after-01");
        afterRef.add("next");
        this.temporalityMap.put("after", afterRef);
        beforeRef.add("before");
        beforeRef.add("before-01");
        this.temporalityMap.put("before", beforeRef);
    }


    private void prepareLogicConnector() {
        conjunctionRef.add("and");
        this.logicConnectorMap.put("and", conjunctionRef);
        disjunctionRef.add("or");
        this.logicConnectorMap.put("or", disjunctionRef);
        polarityRef.add("-");
        polarityRef.add("+");
        this.logicConnectorMap.put("polarity", polarityRef);
    }


    private void prepareFrequency() {
        recurringEventRef.add("rate-entity-91");
        this.frequencyMap.put("recurring_event", recurringEventRef);
    }


    private void prepareAdverb() {
        onlyRef.add("only");
        this.adverbMap.put("only", onlyRef);
    }


    private void prepare() {
        prepareModality();
        prepareTemporality();
        prepareLogicConnector();
        prepareFrequency();
        prepareAdverb();
    }

    //==================================================================
    // Constructor(s)
    //==================================================================

    public OntoMap() {
        this.modalityMap = new HashMap<>();
        this.temporalityMap = new HashMap<>();
        this.logicConnectorMap = new HashMap<>();
        this.frequencyMap = new HashMap<>();
        this.adverbMap = new HashMap<>();
        this.prepare();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    private Set<String> getReferenceSet(
            Map<String, Set<String>> referenceMap) {
        Set<String> refSet = new HashSet<>();
        for (Set<String> s: referenceMap.values()) refSet.addAll(s);
        return refSet;
    }

    public Set<String> getModalitySet() {
        return getReferenceSet(this.modalityMap);
    }

    public Set<String> getTemporalitySet() {
        return getReferenceSet(this.temporalityMap);
    }

    public Set<String> getLogicConnectorSet() {
        return getReferenceSet(this.logicConnectorMap);
    }

    public Set<String> getFrequencySet() {
        return getReferenceSet(this.frequencyMap);
    }

    public Set<String> getFrequencyRef(String refKey) {
        Set<String> refSet = new HashSet<>();
        if (this.frequencyMap.containsKey(refKey))
            refSet.addAll(this.frequencyMap.get(refKey));
        return refSet;
    }

    public Set<String> getAdverbSet() {
        return getReferenceSet(this.adverbMap);
    }

    public Set<String> getAdverbRef(String refKey) {
        Set<String> refSet = new HashSet<>();
        if (this.adverbMap.containsKey(refKey))
            refSet.addAll(this.adverbMap.get(refKey));
        return refSet;
    }

    //==================================================================
    // Method(s)
    //==================================================================

    /**
     * Gives the reference name corresponding to an AMR concept.
     *
     * @param amrConcept concept
     * @return reference corresponding to the concept
     */
    private String getReferenceName (
            Map<String, Set<String>> referenceMap,
            String amrConcept) {
        String result = "-";
        for (Map.Entry<String, Set<String>> e : referenceMap.entrySet()) {
            if (e.getValue().contains(amrConcept)) {
                result = e.getKey();
                break;
            }
        }
        return result;
    }


    /**
     * Gives the modality name corresponding to an AMR concept.
     *
     * @param amrConcept concept
     * @return reference corresponding to the concept
     */
    public String getModalityName (String amrConcept) {
        return getReferenceName(this.modalityMap, amrConcept);
    }


    /**
     * Gives the temporality name corresponding to an AMR concept.
     *
     * @param amrConcept concept
     * @return reference corresponding to the concept
     */
    public String getTemporalityName (String amrConcept) {
        return getReferenceName(this.temporalityMap, amrConcept);
    }


    /**
     * Gives the logic connector name corresponding to an AMR concept.
     *
     * @param amrConcept concept
     * @return reference corresponding to the concept
     */
    public String getLogicConnectorName (String amrConcept) {
        return getReferenceName(this.logicConnectorMap, amrConcept);
    }


    /**
     * Gives the frequency name corresponding to an AMR concept.
     *
     * @param amrConcept concept
     * @return reference corresponding to the concept
     */
    public String getFrequencyName (String amrConcept) {
        return getReferenceName(this.frequencyMap, amrConcept);
    }


    /**
     * Gives the adverb name corresponding to an AMR concept.
     *
     * @param amrConcept concept
     * @return reference corresponding to the concept
     */
    public String getAdverbName (String amrConcept) {
        return getReferenceName(this.adverbMap, amrConcept);
    }


    /**
     * Gives the reference name corresponding to an AMR concept.
     *
     * @param amrConcept concept
     * @return reference corresponding to the concept
     */
    public String getReferenceName (String amrConcept) {
        String result = "-";
        result = getModalityName(amrConcept);
        if (result.equals("-")) result = getModalityName(amrConcept);
        if (result.equals("-")) result = getTemporalityName(amrConcept);
        if (result.equals("-")) result = getLogicConnectorName(amrConcept);
        if (result.equals("-")) result = getFrequencyName(amrConcept);
        if (result.equals("-")) result = getAdverbName(amrConcept);
        return result;
    }

}
