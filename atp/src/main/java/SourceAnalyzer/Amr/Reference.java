package SourceAnalyzer.Amr;

import java.util.ArrayList;
import java.util.List;

public class Reference {

    //==================================================================
    // Modality
    //==================================================================

    public static final List<String> MODALITY = new ArrayList<String>() {
        {
            add("possible");
            add("possible-01");//modality possible
            add("possible-02");
            add("allow-01");
            add("allow-02");
            add("necessary");
            add("necessary-01");// modality necessary
            add("necessary-02");
            add("obligatory");
            add("contingent");
            add("contingent-01");// modality contingent
            add("contingent-02");
            add("impossible");
            add("impossible-01");// modality contingent
            add("impossible-02");
            add("prohibit-01");
        }
    };

    public static final List<String> MODALITY_POSSIBLE = new ArrayList<String>() {
        {
            add("possible");
            add("possible-01");
            add("possible-02");
            add("allow-01");
            add("allow-02");
        }
    };

    public static final List<String> MODALITY_NECESSARY = new ArrayList<String>() {
        {
            add("necessary");
            add("necessary-01");
            add("necessary-02");
            add("obligatory");
        }
    };

    public static final List<String> MODALITY_CONTINGENT = new ArrayList<String>() {
        {
            add("contingent");
            add("contingent-01");
            add("contingent-02");
        }
    };

    public static final List<String> MODALITY_IMPOSSIBLE = new ArrayList<String>() {
        {
            add("impossible");
            add("impossible-01");
            add("impossible-02");
            add("prohibit-01");
            add("forbid-01");
        }
    };

    //==================================================================
    // Modality
    //==================================================================

    public static final List<String> TEMPORALITY = new ArrayList<String>() {
        {
            add("after");
            add("after-01");
            add("before");
            add("before-01");
        }
    };

    public static final List<String> TEMPORALITY_AFTER = new ArrayList<String>() {
        {
            add("after");
            add("after-01");
        }
    };

    public static final List<String> TEMPORALITY_BEFORE = new ArrayList<String>() {
        {
            add("before");
            add("before-01");
        }
    };

    //==================================================================
    // Logic Connector
    //==================================================================

    public static final List<String> LOGIC_CONNECTOR = new ArrayList<String>() {
        {
            add("or");// disjunction
            add("and");// conjunction
            add("-");// polarity
            add("+");
        }
    };
}
