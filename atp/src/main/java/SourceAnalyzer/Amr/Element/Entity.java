package SourceAnalyzer.Amr.Element;

import Parameters.DefaultValues;
import SourceAnalyzer.SymbolTable.Role;
import SourceAnalyzer.SymbolTable.Symbol;
import SourceAnalyzer.SymbolTable.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * An entity is a basic concept, ie concept without core arguement.
 *
 * An entity is defined by an ident and a concept, with possibly some
 * additional features (name, mod).
 *
 */
public class Entity {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private String id;
    private String mainConcept;
    private List<Arg> featureList;


    //==================================================================
    // Construction Method(s)
    //==================================================================

    private void updateFeatureList(Symbol symbol) {
        for(Role role: symbol.getSpecificRole("mod"))
            this.featureList.add(new Arg(role));
        for(Role role: symbol.getSpecificRole("name"))
            this.featureList.add(new Arg(role));
    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    /**
     * Construction of an entity from an element of the symbol table.
     *
     * @param symbol (element of symbol table)
     */
    public Entity(Symbol symbol) {
        this.id = symbol.getIdent();
        this.mainConcept = symbol.getConcept();
        this.featureList = new ArrayList<>();
        this.updateFeatureList(symbol);
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    public String getId() {
        return this.id;
    }

    public String getMainConcept() {
        return this.mainConcept;
    }

    public String getName() {
        String featureStr;
        String conceptStr = this.getMainConcept();
        featureStr = "";
        for(Arg arg: featureList) featureStr = arg.getName() + "-" + featureStr;
        return featureStr + conceptStr;
    }

    public String getFullName() {
        return this.getName();
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Entity{" +
                "id='" + id + '\'' +
                ", mainConcept='" + mainConcept + '\'' +
                ", featureList=" + featureList +
                '}';
    }

    /**
     * Assign the name to each argument of the entity.
     *
     * The name is the main concept of the symbol corresponding
     * to the argument id (found in the symbol table).
     *
     * @param symbolTable (table of symbols)
     */
    public void assignNamesToArgument(Table symbolTable) {
        String currentId;
        for(Arg arg: featureList) {
            currentId = arg.getId();
            Symbol currentSymb = symbolTable.lookup(currentId);
            if (currentSymb != null) arg.setName(currentSymb.getConcept());
        }
    }

    /**
     * Update the Arg Names from property set. this is important
     * to take into account the full name associated with the property
     * (when the argument is a property), and not only the concept
     * (associated after the first assignment).
     *
     * @param propertySet
     */
    public void updateArgNames(Set<Property> propertySet) {
        for(Arg arg: featureList) {
            for (Property p: propertySet)
                if (p.getId().equals(arg.getId()))
                    arg.setName(p.getName());
        }
    }

}