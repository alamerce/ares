package SourceAnalyzer.Amr.Element;

import Parameters.Configuration;
import SourceAnalyzer.SymbolTable.Symbol;

import java.util.Set;

/**
 *  AMR Property
 */
public class Temporality {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private String id;
    private String mainConcept;
    //TODO: mod (ex.: only)


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Temporality(Symbol symbol) {
        this.id = symbol.getIdent();
        this.mainConcept = symbol.getConcept();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public String getId() {
        return this.id;
    }

    public String getMainConcept() {
        return this.mainConcept;
    }

    public String getName() {
        return Configuration.ONTO_MAP.getTemporalityName(mainConcept);
    }


    // ---------- Predicate(s)

    public Boolean isTemporality(String concept) {
        Set<String> tempoSet =
                Configuration.ONTO_MAP.getTemporalitySet();
        return tempoSet.contains(concept);
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Temporality{" +
                "id='" + id + '\'' +
                ", mainConcept='" + mainConcept + '\'' +
                '}';
    }

}
