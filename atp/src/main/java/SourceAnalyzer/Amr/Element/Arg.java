package SourceAnalyzer.Amr.Element;

import Parameters.DefaultValues;
import SourceAnalyzer.SymbolTable.Role;

public class Arg {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base Attributes

    private final String id;
    private String name;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Arg() {
        this.id = DefaultValues.NO_ID;
        this.name = DefaultValues.NO_NAME;
    }

    public Arg(String id) {
        this.id = id;
        this.name = DefaultValues.NO_NAME;
    }

    public Arg(Role role) {
        if (role.getRoleId().matches("ARG[0-9]+(-of)?"))
            this.id = role.getInstanceId();
        else if (role.getRoleId().matches("mod|domain|name"))
            this.id = role.getInstanceId();
        else this.id = DefaultValues.NO_ID;
        this.name = DefaultValues.NO_NAME;
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public String getId() {
        return id;
    }

    public boolean isEmpty() {
        return id.equals(DefaultValues.NO_ID);
    }

    public boolean hasName() {
        return !(name.equals(DefaultValues.NO_NAME));
    }

    public String getName() {
        String result;
        if (hasName()) result = this.name;
        else result = this.id;
        return result;
    }


    // ---------- Setters

    public void setName(String name) {
        this.name = name;
    }



    //==================================================================
    // Method(s)
    //==================================================================


    @Override
    public String toString() {
        return "Arg{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", isEmpty=" + isEmpty() +
                ", hasName=" + hasName() +
                '}';
    }

}
