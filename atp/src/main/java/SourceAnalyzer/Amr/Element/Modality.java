package SourceAnalyzer.Amr.Element;

import SourceAnalyzer.Amr.OntoMap;
import SourceAnalyzer.SymbolTable.Symbol;

/**
 * An entity is a basic concept, ie concept without core arguement.
 *
 * An entity is defined by an ident and a concept, with possibly some
 * additional features (name).
 *
 */
public class Modality {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final OntoMap ontoMap;
    private String id;
    private String mainConcept;
    private boolean negation;


    //==================================================================
    // Constructor(s)
    //==================================================================

    /**
     * Construction of an entity from an element of the symbol table.
     *
     * @param symbol (element of symbol table)
     */
    public Modality(Symbol symbol) {
        this.ontoMap = new OntoMap();
        this.id = symbol.getIdent();
        this.mainConcept = symbol.getConcept();
        this.negation = false;
        if (symbol.hasNegation()) {
            this.negation = true;
        }
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    public String getId() {
        return this.id;
    }

    public String getMainConcept() {
        return this.mainConcept;
    }

    public boolean isNegation() {
        return negation;
    }

    public String getName() {
        String neg = "";
        if (isNegation()) { neg = "not "; }
        String modName = ontoMap.getModalityName(this.mainConcept);
        return neg + modName;
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Modality{" +
                "id='" + id + '\'' +
                ", mainConcept='" + mainConcept + '\'' +
                ", negation=" + negation +
                '}';
    }

}