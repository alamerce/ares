package SourceAnalyzer.Amr.Element;

import SourceAnalyzer.SymbolTable.Role;
import SourceAnalyzer.SymbolTable.Symbol;
import SourceAnalyzer.SymbolTable.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *  AMR Property
 */
public class Property {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final String id;
    private String mainConcept;
    private List<Arg> preArg;
    private List<Arg> postArg;
    private boolean negation;


    //==================================================================
    // Construction Methods
    //==================================================================

    private void initArg() {
        this.preArg = new ArrayList<>();
        this.postArg = new ArrayList<>();
        this.negation = false;
    }

    private void updatePreArg(Symbol symbol) {
        for(Role role: symbol.getSpecificRole("mod"))
            this.preArg.add(new Arg(role));
        for(Role role: symbol.getPreCoreRole(0))
            this.preArg.add(new Arg(role));
        for (int i = 1 ; i < 10 ; i++)
            for(Role role: symbol.getPreCoreRole(i))
                this.preArg.add(new Arg(role));
    }

    private void updatePostArg(Symbol symbol) {
        for(Role role: symbol.getSpecificRole("domain"))
            this.postArg.add(new Arg(role));
        for (int i = 1 ; i < 10 ; i++)
            for(Role role: symbol.getPostCoreRole(i))
                this.postArg.add(new Arg(role));
    }

    private void updateNegation(Symbol symbol) {
        if (symbol.hasNegation()) this.negation = true;
        else this.negation = false;
    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Property(Symbol symbol) {
        this.id = symbol.getIdent();
        this.mainConcept = symbol.getConcept();
        initArg();
        this.updatePreArg(symbol);
        this.updatePostArg(symbol);
        this.updateNegation(symbol);
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    public String getId() {
        return this.id;
    }

    public String getMainConcept() {
        return this.mainConcept;
    }

    public boolean isNegation() {
        return negation;
    }

    public String getName() {
        String negStr, preArgStr, postArgStr;
        String conceptStr = this.getMainConcept();
        if (isNegation()) negStr = "not ";
        else negStr = "";
        preArgStr = "";
        for(Arg arg: preArg) preArgStr = arg.getName() + "_" + preArgStr;
        postArgStr = "";
        for(Arg arg: postArg) postArgStr += "_" + arg.getName();
        return negStr + preArgStr + conceptStr + postArgStr;
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "Property{" +
                "id='" + id + '\'' +
                ", mainConcept='" + mainConcept + '\'' +
                ", preArg=" + preArg +
                ", postArg=" + postArg +
                ", negation=" + negation +
                '}';
    }

    /**
     * Assign the name to each argument of the property.
     *
     * The name is the main concept of the symbol corresponding
     * to the argument id (found in the symbol table).
     *
     * @param symbolTable (table of symbols)
     */
    public void assignNamesToArgument(Table symbolTable) {
        String currentId;
        for(Arg arg: preArg) {
            currentId = arg.getId();
            Symbol currentSymb = symbolTable.lookup(currentId);
            if (currentSymb != null) arg.setName(currentSymb.getConcept());
        }
        for(Arg arg: postArg) {
            currentId = arg.getId();
            Symbol currentSymb = symbolTable.lookup(currentId);
            if (currentSymb != null) arg.setName(currentSymb.getConcept());
        }
    }

    /**
     * Update the Arg Names from property set. this is important
     * to take into account the full name associated with the property
     * (when the argument is a property), and not only the concept
     * (associated after the first assignment).
     *
     * @param propertySet
     */
    public void updatePropertyArgNames(Set<Property> propertySet) {
        for(Arg arg: preArg) {
            for (Property p: propertySet)
                if (p.getId().equals(arg.getId()))
                    arg.setName(p.getName());
        }
        for(Arg arg: postArg) {
            for (Property p: propertySet)
                if (p.getId().equals(arg.getId()))
                    arg.setName(p.getName());
        }
    }

    /**
     * Update the Arg Names from entity set. this is important
     * to take into account the full name associated with the entity
     * (when the argument is an entity), and not only the concept
     * (associated after the first assignment).
     *
     * @param entitySet
     */
    public void updateEntityArgNames(Set<Entity> entitySet) {
        for(Arg arg: preArg) {
            for (Entity p: entitySet)
                if (p.getId().equals(arg.getId()))
                    arg.setName(p.getName());
        }
        for(Arg arg: postArg) {
            for (Entity p: entitySet)
                if (p.getId().equals(arg.getId()))
                    arg.setName(p.getName());
        }
    }

}
