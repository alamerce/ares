package SourceAnalyzer.SymbolTable;

import Parameters.*;

import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;

/**
 *  --
 */
public class Symbol {

    //==================================================================
    // Attributes
    //==================================================================

    // --- Parameters

    private final static String CORE_ROLE_STR = "ARG[0-9]+(-of)?";
    private final static String PRE_CORE_ROLE_STR = "ARG[0-9]+(-of)";
    private final static String POST_CORE_ROLE_STR = "ARG[0-9]+";

    // --- Main attributes

    private String ident;
    private String concept;
    private List<Role> roleList;
    private Category category;
    private List<String> tagList;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Symbol(String ident, Category category) {
        this.ident = ident;
        this.concept = DefaultValues.NO_CONCEPT;
        this.roleList = new ArrayList<>();
        this.category = category;
        this.tagList = new ArrayList<>();
    }

    public Symbol(String ident,
                  Category category,
                  String concept) {
        this.ident = ident;
        this.concept = concept;
        this.roleList = new ArrayList<>();
        this.category = category;
        this.tagList = new ArrayList<>();
    }

    public Symbol(String ident,
                  Category category,
                  String concept,
                  List<Role> roleList) {
        this.ident = ident;
        this.concept = concept;
        this.roleList = roleList;
        this.category = category;
        this.tagList = new ArrayList<>();
    }


    //==================================================================
    // Accessor(s) for Identification
    //==================================================================

    public String getIdent() {
        return this.ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }


    //==================================================================
    // Accessor(s) for Concept and Roles
    //==================================================================

    // ---------- Getters

    public String getConcept() {
        return this.concept;
    }

    public List<Role> getRoleList() {
        return this.roleList;
    }

    public List<Role> getRole(String roleId) {
        List<Role> result = new ArrayList<>();
        for (Role r : this.getRoleList()) {
            if (r.getRoleId().matches(roleId)) {
                result.add(r);
            }
        }
        return result;
    }

    public List<Role> getCoreRoleList() {
        List<Role> result = new ArrayList<>();
        List<Role> roleList = this.getRoleList();
        for (Role r : roleList) {
            if (r.getRoleId().matches(CORE_ROLE_STR)) {
                result.add(r);
            }
        }
        return result;
    }

    public List<Role> getPreCoreRoleList() {
        return this.getRole(PRE_CORE_ROLE_STR);
    }

    public List<Role> getPostCoreRoleList() {
        return this.getRole(POST_CORE_ROLE_STR);
    }

    public List<Role> getPreCoreRole(int number) {
        String targetRoleId;
        if (number == 0) targetRoleId = "ARG0(-of)?";
        else targetRoleId = "ARG" + number + "-of";
        return this.getRole(targetRoleId);
    }

    public List<Role> getPostCoreRole(int number) {
        String targetRoleId = "ARG" + number;
        return this.getRole(targetRoleId);
    }

    public List<Role> getSpecificRoleList() {
        List<Role> result = new ArrayList<>();
        List<Role> roleList = this.getRoleList();
        for (Role r : roleList) {
            if (!r.getRoleId().matches(CORE_ROLE_STR)) {
                result.add(r);
            }
        }
        return result;
    }

    public List<Role> getSpecificRole(String roleId) {
        return this.getRole(roleId);
    }


    // ---------- Predicates

    /**
     * Test if the symbol has the requested core role.
     *
     * @param number (identification for the requested role)
     * @return true if the symbol has the requested core role
     */
    public boolean hasPreCoreRole(int number) {
        return !this.getPreCoreRole(number).isEmpty();
    }

    /**
     * Test if the symbol has the requested core role.
     *
     * @param number (identification for the requested role)
     * @return true if the symbol has the requested core role
     */
    public boolean hasPostCoreRole(int number) {
        return !this.getPostCoreRole(number).isEmpty();
    }

    /**
     * Test if the symbol has the requested specific role.
     *
     * @param roleId (identification for the requested role)
     * @return true if the symbol has the requested specific role
     */
    public boolean hasSpecificRole(String roleId) {
        return !this.getSpecificRole(roleId).isEmpty();
    }

    public boolean hasNegation() {
        boolean result = false;
        for(Role r : this.getRoleList()) {
            if (r.getRoleId().equals("polarity") && r.getInstanceId().equals("-")) {
                result = true;
                break;
            }
        }
        return result;
    }


    // ---------- Setters

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    //==================================================================
    // Accessor(s) for Category and Tags
    //==================================================================

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isCategory(Category category) {
        return (this.category == category);
    }


    public List<String> getTagList() {
        return this.tagList;
    }

    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

    public boolean hasTag() {return !this.tagList.isEmpty(); }

    public void addTag(String tag) {
        this.tagList.add(tag);
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("(" + this.ident);
        result.append(", ").append(this.concept);
        result.append(", " + "[");
        boolean first;
        first = true;
        for (Role r : roleList) {
            if (first) {
                result.append(r.toString());
                first = false;
            }
            else result.append(", ").append(r.toString());
        }
        result.append("]");
        result.append(", ").append(this.category.toString());
        if (this.hasTag()) {
            result.append(", " + "[");
            first = true;
            for (String t : tagList) {
                if (first) {
                    result.append(t);
                    first = false;
                }
                else result.append(", ").append(t);
            }
            result.append("]");
        }
        result.append(")");
        return result.toString();
    }

}
