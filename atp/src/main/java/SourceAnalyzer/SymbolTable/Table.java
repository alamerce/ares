package SourceAnalyzer.SymbolTable;

import LogicalQuery.Atom;
import Parameters.Category;
import SourceAnalyzer.Amr.Classifier;

import java.util.*;
import java.util.Map.Entry;

/**
 * This file contains the symbol table definition.
 * A symbol table contains a set of ident and the
 * corresponding symbols.
 * It can have a parent, containing itself other
 * symbols. If a symbol is not found, the request
 * is forwarded to the parent.
 */
public class Table {

    //==================================================================
    // Attributes
    //==================================================================

    // Requirement identification
    private final String reqId;

    // Store the table as a map
    private HashMap<String, Symbol> table;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Table(String reqId) {
        this.reqId = reqId;
        this.table = new HashMap<>();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public String getReqId() {
        return reqId;
    }

    /**
     * Give the set of identifiers (all symbol table entries.
     * @return Set of identifier (string)
     */
    public Set<String> getIdSet() {
        return this.table.keySet();
    }

    public List<Symbol> getSymbolList() {
        List<Symbol> result = new ArrayList<>();
        for (Map.Entry<String, Symbol> entry : table.entrySet()) {
            result.add(entry.getValue());
        }
        return result;
    }

    /**
     * Give the list of identifier corresponding to a symbol whose categoy
     * matches with the category given in parameter.
     * @param cat (category)
     * @return List of identifier (string)
     */
    public List<String> getIdListByCategory(Category cat) {
        List<String> result = new ArrayList<>();
        for (Map.Entry<String, Symbol> entry : table.entrySet()) {
            if (entry.getValue().isCategory(cat)) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    /**
     * Give the list of symbol corresponding to a symbol whose categoy
     * matches with the category given in parameter.
     * @param cat (category)
     * @return List of symbol
     */
    public List<Symbol> getSymbolListByCategory(Category cat) {
        List<Symbol> result = new ArrayList<>();
        for (Map.Entry<String, Symbol> entry : table.entrySet()) {
            if (entry.getValue().isCategory(cat)) {
                result.add(entry.getValue());
            }
        }
        return result;
    }


    //==================================================================
    // Method(s)
    //==================================================================

    /**
     * Add a new symbol.
     *
     * @param var to add
     * @return false if the symbol cannot be added (already in scope)
     */
    public boolean add(Symbol var) {
        Symbol res = table.get(var.getIdent());
        if(res != null) {
            return false;
        }
        table.put(var.getIdent(), var);
        return true;
    }

    /**
     * Remove a symbol.
     *
     * @param ident of the symbol to remove
     * @return false if the symbol is not in the table
     */
    public boolean remove(String ident) {
        return table.remove(ident) != null;
    }

    /**
     * Find the symbol (abstract variable) corresponding to a given
     * ident.
     *
     * @param ident of the searched variable
     * @return target symbol if found, default symbol otherwise
     */
    public Symbol lookup(String ident, Symbol defaultSymbol) {
        Symbol result = defaultSymbol;
        if (table.containsKey(ident)) {
            result = table.get(ident);
        }
        return result;
    }

    /**
     * Find the symbol (abstract variable) corresponding to a given
     * ident.
     *
     * @param ident of the searched variable
     * @return target symbol if found, null otherwise
     */
    public Symbol lookup(String ident) {
        return lookup(ident, null);
    }

    /**
     * Update thye category of each symbol table entry.
     */
    public void categorize(Classifier classifier) {

        // Useful variable
        Entry<String, Symbol> entry;
        Set<Entry<String, Symbol>> tableSet = table.entrySet();
        Symbol workingSymbol;
        Category newCategory;
        List<String> newTagList;

        // Categorize each entry of the symbol table
        for (Entry<String, Symbol> stringSymbolEntry : tableSet) {
            entry = stringSymbolEntry;
            workingSymbol = entry.getValue();
            newCategory = classifier.evalCategory(workingSymbol);
            workingSymbol.setCategory(newCategory);
        }

        // Evaluate tags associated with the symbol
        for (Entry<String, Symbol> stringSymbolEntry : tableSet) {
            entry = stringSymbolEntry;
            workingSymbol = entry.getValue();
            newTagList = classifier.evalTags(workingSymbol);
            workingSymbol.setTagList(newTagList);
        }

    }

    /**
     * Give the list of atomic formula (atom) corresponding
     * to each entry of symbol table.
     * @return List of atom (atomic formula)
     */
    public List<Atom> getAtomList(Classifier classifier) {

        // Init result
        List<Atom> result = new ArrayList<>();

        // Useful variable
        Entry<String, Symbol> entry;
        String x, y;
        Symbol s;
        Category c;
        List<String> tl;

        // Table scan
        for (Entry<String, Symbol> e : table.entrySet()) {
            x = e.getKey();
            s = e.getValue();
            for (Role r : s.getRoleList()) {
                result.add(new Atom(r.getRoleId(), x, r.getInstanceId()));
            }
            c = classifier.evalCategory(s);
            result.add(new Atom(c.toString(), x));
            tl = classifier.evalTags(s);
            for (String t : tl) {
                result.add(new Atom(t, x));
            }
        }

        // Result
        return result;

    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("<" + this.reqId + "> ");
        result.append("Symbol Table:" + "\n");
        Set<Entry<String, Symbol>> tableSet = table.entrySet();
        Iterator<Entry<String, Symbol>> it = tableSet.iterator();
        boolean first = true;
        while(it.hasNext()){
            Entry<String, Symbol> e = it.next();
            if (first) {
                result.append("  ");
                result.append(e.getValue().toString());
                result.append("\n");
                first = false;
            }
            else {
                result.append("  ");
                result.append(e.getValue().toString());
                result.append("\n");
            }
        }
        return result.toString();
    }

}
