package SourceAnalyzer.SymbolTable;

/**
 *  Class Role define a relation between a semantic role (RoleId) and a
 *  concept instance (an identifier of symbol table entry).
 *  For example, the role (A1, x1) associates the semantic role A1 and
 *  the instance x1.
 */
public class Role {

    //==================================================================
    // Attributes
    //==================================================================

    // Main attributes
    private String roleId; // semantic role (ex: A1, condition)
    private String instanceId; // concept instance id (ex: x1)


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Role(String roleId, String instanceId) {
        this.roleId = roleId;
        this.instanceId = instanceId;
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    /**
     * Get the identifier of a semantic role (ex: ARG1, time).
     *
     * @return roleId
     */
    public String getRoleId() {
        return this.roleId;
    }

    /**
     * Get the name of a semantic role (ex: arg1, time).
     *
     * @return roleId
     */
    public String getRoleName() {
        return this.roleId;
    }

    /**
     * Get the identifier of a concept instance role (ex: x1).
     *
     * @return instanceId
     */
    public String getInstanceId() {
        return this.instanceId;
    }


    // ---------- Setters

    /**
     * Set the identifier of a semantic role (ex: A1, condition).
     *
     * @param roleId
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     * Set the identifier of a concept instance role (ex: x1).
     *
     * @param instanceId
     */
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }


    //==================================================================
    // Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "(" + roleId + ", " + instanceId + ")";
    }

}
