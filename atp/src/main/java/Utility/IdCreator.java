package Utility;

import AbstractSemantic.Interpretation;
import LogicalQuery.Atom;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IdCreator {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base Attribute

    private char[] alphabet;
    private Set<String> idSet;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public IdCreator() {
        this.alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        this.idSet = new HashSet<>();
    }

    public IdCreator(Interpretation interpretation) {
        this.alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        this.idSet = new HashSet<>();
        this.idSet.add(interpretation.getStatementId());
        this.idSet.addAll(interpretation.getConceptSet());
        this.idSet.addAll(interpretation.getInstanceIdSet());
        for(Atom a: interpretation.getPredicateSet()) {
            this.idSet.add(a.getRelation());
            this.idSet.addAll(a.getArgs());
        }
    }

    public IdCreator(Set<String> baseIdSet) {
        this.alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        this.idSet = new HashSet<>();
        this.idSet.addAll(baseIdSet);
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public char[] getAlphabet() {
        return alphabet;
    }

    public Set<String> getIdSet() {
        return idSet;
    }


    //==================================================================
    // Base Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "IdCreator{" +
                "alphabet=" + Arrays.toString(alphabet) +
                ", idSet=" + idSet +
                '}';
    }


    //==================================================================
    // Main Method(s)
    //==================================================================

    public String getNewId() {

        String newId = "";

        boolean newIdFound = false;
        while(!newIdFound) {
            int n = (int)(Math.random() * alphabet.length);
            newId = newId + alphabet[n];
            for (int i = 1; i < 100; i++) {
                String idProp = newId + i;
                if (!idSet.contains(idProp)) {
                    newIdFound = true;
                    newId = idProp;
                    break;
                }
            }
        }

        idSet.add(newId);

        return newId;

    }

}
