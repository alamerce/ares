package Utility;

import AbstractSemantic.TransPatternObject.Token;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Parameters.Configuration.*;

public class Tokenizer {

    private final String baseStatement;
    private final List<Token> tokenList;

    //==================================================================
    // Construction Method(s)
    //==================================================================

    private static List<Token> refineToken(String tokenStr) {

        List<Token> resultList = new ArrayList<>();

        // Find white space using Matcher class
        Pattern markPattern = Pattern.compile(SEPARATOR_TOKEN_REGEX);
        Matcher matcher = markPattern.matcher(tokenStr);

        // Browse white space found by matching
        String newTokenStr;
        int beginIndex = 0; // Use to select sub-string
        int endIndex;
        while(matcher.find()) {

            // (1) Add keyword/variant token
            endIndex = matcher.start();
            if (beginIndex < endIndex) {
                newTokenStr = tokenStr.substring(beginIndex, endIndex);
                resultList.add(new Token(newTokenStr));
            }

            // (2) Add separator token
            beginIndex = matcher.start();
            endIndex = matcher.end();
            if (beginIndex < endIndex) {
                newTokenStr = tokenStr.substring(beginIndex, endIndex);
                resultList.add(new Token(newTokenStr));
            }

            // New begin index next to matcher end
            beginIndex = matcher.end();

        }

        if(beginIndex < tokenStr.length()) {
            // Add final token
            newTokenStr = tokenStr.substring(beginIndex);
            resultList.add(new Token(newTokenStr));
        }

        return resultList;

    }

    /**
     * Evaluate the token list from the raw string.
     */
    private void evalTokenList() {

        // Find white space using Matcher class
        Pattern markPattern = Pattern.compile(WHITE_SPACE_REGEX);
        Matcher matcher = markPattern.matcher(baseStatement);

        // Browse white space found by matching
        String tokenStr;
        int beginIndex = 0; // Use to select sub-string
        int endIndex;
        while(matcher.find()) {

            // (1) Add constant type marker
            endIndex = matcher.start();
            tokenStr = baseStatement.substring(beginIndex, endIndex);
            tokenList.addAll(refineToken(tokenStr));

            // (2) Add white space token
            beginIndex = matcher.start();
            endIndex = matcher.end();
            tokenStr = baseStatement.substring(beginIndex, endIndex);
            tokenList.add(new Token(tokenStr));

            // New begin index next to matcher end
            beginIndex = matcher.end();

        }

        if(beginIndex < baseStatement.length()) {
            // Add final marker (constant type)
            tokenStr = baseStatement.substring(beginIndex);
            tokenList.addAll(refineToken(tokenStr));
        }

    }


    //==================================================================
    // Constructor
    //==================================================================

    public Tokenizer(String baseStatement) {
        this.baseStatement = baseStatement;
        this.tokenList = new ArrayList<>();
        this.evalTokenList();
    }


    //==================================================================
    // Accessor
    //==================================================================

    public String getBaseStatement() {
        return baseStatement;
    }

    public List<Token> getTokenList() {
        return tokenList;
    }

    public int getTokenListSize() {
        return tokenList.size();
    }


    //==================================================================
    // N-Gram Method
    //==================================================================

    public Set<List<Token>> getNGramTokenListSet(int nGramSize) {
        Set<List<Token>> nGramSet = new HashSet<>();
        int iMax = tokenList.size() - nGramSize;
        for (int i = 0; i <= iMax; i++) {
            List<Token> subTokenList = tokenList.subList(i, (i + nGramSize));
            nGramSet.add(subTokenList);
        }
        return nGramSet;
    }

    public Set<String> getNGramStringSet(int nGramSize) {
        Set<List<Token>> nGramSet = getNGramTokenListSet(nGramSize);
        Set<String> nGramStringSet = new HashSet<>();
        for(List<Token> nGram: nGramSet) {
            String nGramStr = "";
            for (Token t: nGram) nGramStr += t.getRawValue();
            nGramStringSet.add(nGramStr);
        }
        return nGramStringSet;
    }

    public boolean nGramMatches(String targetStatement) {
        Tokenizer targetTokenizer = new Tokenizer(targetStatement);
        int n = targetTokenizer.getTokenListSize();
        Set<String> nGramStringSet = getNGramStringSet(n);
        return nGramStringSet.contains(targetStatement);
    }

}
