package Utility;

import AbstractSemantic.Interpretation;
import LogicalQuery.Atom;
import org.antlr.v4.runtime.misc.Array2DHashSet;

import java.util.*;

public class VeruaGen {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base Attribute

    private final List<String> tonaList; // alphabet
    private Set<String> ioaSet; // concept set


    //==================================================================
    // Construction Method
    //==================================================================

    private void initTonaSet() {
        this.tonaList.addAll(Arrays.asList("me", "ha", "nae", "hu", "na"));
        this.tonaList.addAll(Arrays.asList("ni", "hu", "hi", "nu", "mu"));
        this.tonaList.addAll(Arrays.asList("te", "va", "tea", "kea", "vi"));
        this.tonaList.addAll(Arrays.asList("va", "nau", "ri", "mi", "ti"));
        this.tonaList.addAll(Arrays.asList("ma", "ta", "re", "rii", "mae"));
        this.tonaList.addAll(Arrays.asList("kao", "ki", "hau", "hei", "moa"));
        this.tonaList.addAll(Arrays.asList("wai", "wa", "wo", "te", "tie"));
    }


    //==================================================================
    // Constructor
    //==================================================================

    public VeruaGen() {
        this.tonaList = new ArrayList<>();
        this.initTonaSet();
        this.ioaSet = new HashSet<>();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters

    public List<String> getTonaList() {
        return tonaList;
    }

    public Set<String> getIoaSet() {
        return ioaSet;
    }


    //==================================================================
    // Base Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "VeruaGen{" +
                "tonaList=" + tonaList +
                ", ioaSet=" + ioaSet +
                '}';
    }


    //==================================================================
    // Main Method(s)
    //==================================================================

    public String getNewIoa() {

        String newIoa = "";

        int ioaLenghtMin = 2;
        int ioaLengthMax = 4;
        int ioaLength = (int)(Math.random() * ioaLengthMax);
        ioaLength = Math.max(ioaLength, ioaLenghtMin);

        boolean newIoaFound = false;
        int nMax = tonaList.size() - 1;
        int tryNb1 = 0;
        int tryNb2 = 0;
        while(!newIoaFound) {

            tryNb1++;
            if (tryNb1 > 100) {
                tryNb2++;
                if (tryNb2 > 100) {
                    ioaLengthMax++;
                    tryNb2 = 0;
                }
                ioaLength = (int)(Math.random() * ioaLengthMax);
                ioaLength = Math.max(ioaLength, ioaLenghtMin);
                tryNb1 = 0;
            }

            newIoa = "";
            for (int i = 1; i <= ioaLength ; i++) {
                int n = (int)(Math.random() * nMax);
                newIoa = newIoa + tonaList.get(n);
            }

            if (!ioaSet.contains(newIoa)) newIoaFound = true;

        }

        ioaSet.add(newIoa);

        return newIoa;

    }

}
