package Utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Normalizer {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Constant attribute(s)

    public enum Type { ENTITY, PREDICATE, FEATURE, UNDEFINED};
    public static final String NUMBER_REGEX = "-[0-9]+";


    //==================================================================
    // Constructor
    //==================================================================

    public Normalizer() {
    }


    //==================================================================
    // Methods
    //==================================================================

    public String removeNumber(String rawStr) {
        String result = "";
        Pattern markPattern = Pattern.compile(NUMBER_REGEX);
        Matcher matcher = markPattern.matcher(rawStr);
        result = matcher.replaceAll("");
        return result;
    }


    public String normalize(String rawStr) {
        String result = "";
        result = this.removeNumber(rawStr);
        return result;
    }

}
