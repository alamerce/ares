package Parameters;

public class DefaultValues {

    // --- Basic Default value
    public static final String NO_NAME = "***No Name***";
    public static final String NO_ID = "***No Ident***";
    public static final String NO_VALUE = "***No Value***";
    public static final String NO_CONCEPT = "***No Concept***";

    // --- Default values for Role
    public static final String DEFAULT_ROLE_ID = "-1";
    public static final String DEFAULT_ROLE_INSTANCE = "-1";

    // --- Default Values for Work Data
    public static final String DEFAULT_STATEMENT = "-";

}
