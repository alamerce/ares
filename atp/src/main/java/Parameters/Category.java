package Parameters;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

/**
 *  Enumeration: Modality for ARD.
 */
public enum Category {

    MODALITY,
    TEMPORALITY,
    PROPERTY,
    ENTITY,
    LOGIC_CONNECTOR,
    FREQUENCY,
    ADVERB,
    UNKNOWN;

}
