package Parameters;

import Utility.Normalizer;
import SourceAnalyzer.Amr.OntoMap;

public class Configuration {

    //==================================================================
    // Abstract Semantic Processes
    //==================================================================

    // ---------- References for concept and instance interpretation

    public static final String CONCEPT = "concept"; //TODO: to review
    public static final String[] FEATURES = {
            "modality", "temporality", "logicConnector",
            "frequency", "adverb",
            "property", "entity"
    };

    // --- Token Types

    public enum TokenType {WHITESPACE, SEPARATOR, KEYWORD, VARIANT, FEATURE, UNDEFINED}

    public static final String WHITE_SPACE_TOKEN = " ";
    public static final String WHITE_SPACE_REGEX = "( )+";
    public static final String[] SEPARATOR_TOKEN_TAB = {"<", ">", ","};
    public static final String SEPARATOR_TOKEN_REGEX = "(<|>|,)";
    public static final String[] KEYWORD_TOKEN_TAB = {"or", "and"};


    // --- Transduction Pattern Configuration

    public static final String ABSTRACTION_MARK = "#";
    public static final String FEATURE_REGEX = ABSTRACTION_MARK + "[a-zA-Z0-9]+.[a-zA-Z]+";
    public static final String UNDEFINED_TOKEN_STRING = "-- undefined token --";
    public static final String OUTPUT_ABSTRACT_CONCEPT = ABSTRACTION_MARK + "out";
    public static final String FEATURE_STATEMENT_ID = "statement";

    public static final int TRANS_PATTERN_SET_DEPTH_LIMIT = 50;


    // --- Learning Configuration

    public static final double MINIMAL_PRECISION = 0.3; // learned statement rejected if less than expected minimum precision


    // --- Production Configuration
    public static final int VAL_MAX = 5; // size limit for variable set
    public static final int LOW_LIMIT_MAX = 4; // use to keep sufficiently precise requests


    // ---------- Calculation Attributes

    public static final OntoMap ONTO_MAP = new OntoMap();
    public static final Normalizer NORMALIZER = new Normalizer();

}
