package ComputationObject;

import AbstractSemantic.TransPattern;
import SourceAnalyzer.Amr.Asd;
import SourceAnalyzer.SymbolTable.Table;

import java.util.*;

public class Utility {

    //==================================================================
    // Data Collections from WorkDataList
    //==================================================================

    // ---------- AST and Symbol Table

    public static List<Asd.Representation> getAstList(List<WorkData> workDataList) {
        List<Asd.Representation> astList = new ArrayList<>();
        for (WorkData wd: workDataList)
            astList.add(wd.getAst());
        return astList;
    }

    public static List<Table> getSymbolTableList(List<WorkData> workDataList) {
        List<Table> symbolTableList = new ArrayList<>();
        for (WorkData wd: workDataList)
            symbolTableList.add(wd.getSymbolTable());
        return symbolTableList;
    }


    // ---------- Entity, Property and Statement

    public static Set<String> getEntitySet(List<WorkData> workDataList) {
        Set<String> resultSet = new HashSet<>();
        for (WorkData wd: workDataList)
            resultSet.addAll(wd.getEntitySet());
        return resultSet;
    }

    public static Set<String> getPropertySet(List<WorkData> workDataList) {
        Set<String> resultSet = new HashSet<>();
        for (WorkData wd: workDataList)
            resultSet.addAll(wd.getPropertySet());
        return resultSet;
    }

    public static List<String> getResultStatementList(List<WorkData> workDataList) {
        List<String> resultList = new ArrayList<>();
        for (WorkData wd: workDataList)
            for (String req: wd.getIdentifiedStatementList()) {
                resultList.add(req);
            }
        //Collections.sort(requirementList);
        return resultList;
    }

    public static Set<OutputStatement> getOutputStatementSet(List<WorkData> workDataList) {
        Set<OutputStatement> resultSet = new HashSet<>();
        for (WorkData wd: workDataList) resultSet.addAll(wd.getOutputStatementSet());
        return resultSet;
    }

    public static List<OutputStatement> getOutputStatementList(List<WorkData> workDataList) {
        List<OutputStatement> resultList = new ArrayList<>();
        for(OutputStatement os: getOutputStatementSet(workDataList)) resultList.add(os);
        Collections.sort(resultList);
        return resultList;
    }

}
