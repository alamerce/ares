package ComputationObject;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class Reporter {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final String baseName;
    private final List<WorkData> workDataList;
    private final Statistics statistics;


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Reporter(String baseName, List<WorkData> workDataList) {
        this.baseName = baseName;
        this.workDataList = workDataList;
        this.statistics = new Statistics();
    }

    public Reporter(String baseName, List<WorkData> workDataList, Statistics statistics) {
        this.baseName = baseName;
        this.workDataList = workDataList;
        this.statistics = statistics;
    }


    //==================================================================
    // Accessor(s) and Basic Method(s)
    //==================================================================

    // ---------- Getters

    public String getBaseName() {
        return baseName;
    }

    public List<WorkData> getWorkDataList() {
        return workDataList;
    }


    // ---------- Basic Method(s)

    @Override
    public String toString() {
        return "Reporter{" +
                "baseName='" + baseName + '\'' +
                ", workDataList=" + workDataList +
                '}';
    }


    //==================================================================
    // Reporting method(s)
    //==================================================================

    public void writeEntityFile() throws FileNotFoundException {
        String specificBaseName = this.baseName.substring(0,this.baseName.indexOf('.'));
        String fileName = specificBaseName + ".atp.entities";
        PrintWriter writer = new PrintWriter(fileName);
        writer.println("=============================================================");
        writer.println("== ENTITIES");
        writer.println("=============================================================");
        for (String eName : Utility.getEntitySet(this.workDataList)) {
            writer.println(eName);
        }
        writer.close();
    }

    public void writePropertyFile() throws FileNotFoundException {
        String specificBaseName = this.baseName.substring(0,this.baseName.indexOf('.'));
        String fileName = specificBaseName + ".atp.properties";
        PrintWriter writer = new PrintWriter(fileName);
        writer.println("=============================================================");
        writer.println("== PROPERTIES");
        writer.println("=============================================================");
        for (String eName : Utility.getPropertySet(this.workDataList)) {
            writer.println(eName);
        }
        writer.close();
    }

    public void writeStatementFile() throws FileNotFoundException {
        String specificBaseName = this.baseName.substring(0,this.baseName.indexOf('.'));
        String fileName = specificBaseName + ".atp.statements";
        PrintWriter writer = new PrintWriter(fileName);
        writer.println("=============================================================");
        writer.println("== STATEMENTS");
        writer.println("=============================================================");
        for (String eName : Utility.getResultStatementList(this.workDataList)) {
            writer.println(eName);
        }
        writer.close();
    }

    public void writeReportFile() throws FileNotFoundException {
        String specificBaseName = this.baseName.substring(0,this.baseName.indexOf('.'));
        String fileName = specificBaseName + ".atp.report";
        PrintWriter writer = new PrintWriter(fileName);
        writer.println("=============================================================");
        writer.println("== ATP TECHNICAL REPORT");
        writer.println("=============================================================");
        writer.println();
        writer.println("--------------------------------------------------------");
        writer.println("-- Statistics");
        writer.println("--------------------------------------------------------");
        writer.println();
        writer.println("Precision: " + statistics.getPrecisionStr());
        writer.println("Recall: " + statistics.getRecallStr());
        writer.println("F-Score (" + Statistics.BETA_WEIGHT + "): " + statistics.getFScoreStr());
        writer.println();
        writer.println("--------------------------------------------------------");
        writer.println("-- Result");
        writer.println("--------------------------------------------------------");
        writer.println();
        writer.println("     <Entities>");
        for (String eName : Utility.getEntitySet(this.workDataList)) { writer.println(eName); }
        writer.println();
        writer.println("     <Properties>");
        for (String pName : Utility.getPropertySet(this.workDataList)) { writer.println(pName); }
        writer.println();
        writer.println("     <Statements>");
        for (String req : Utility.getResultStatementList(this.workDataList)) { writer.println(req); }
        writer.println();
        writer.println("--------------------------------------------------------");
        writer.println("-- Symbol Table");
        writer.println("--------------------------------------------------------");
        writer.println();
        for (WorkData wd: workDataList) {
            writer.println(wd.getSymbolTable().toString());
        }
        writer.println();
        writer.close();
    }


}
