package ComputationObject;

import AbstractSemantic.Instance;
import AbstractSemantic.TransPattern;
import Parameters.DefaultValues;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static Parameters.Configuration.FEATURE_STATEMENT_ID;

public class OutputStatement implements Comparable {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base Attributes

    private String id = DefaultValues.NO_ID;
    private final String statement;


    // ---------- Tracking Attribute

    private final TransPattern producerPattern;


    // ---------- Analysis Statistics

    private final boolean consideredPositive;
    private List<Integer> analyzedPatternsNumberList;
    private boolean assessed;
    private boolean truePositive, trueNegative;


    //==================================================================
    // Construction Method(s)
    //==================================================================

    private void initStats() {
        this.analyzedPatternsNumberList = new ArrayList<>();
        this.assessed = false;
        this.truePositive = false; // default value until evaluation
        this.trueNegative = false; // default value until evaluation
    }

    //==================================================================
    // Constructor(s)
    //==================================================================

    public OutputStatement(String statement, TransPattern producerPattern) {
        this.statement = statement;
        this.producerPattern = producerPattern;
        this.consideredPositive = true;
        this.initStats();
    }

    public OutputStatement() {
        this.statement = DefaultValues.DEFAULT_STATEMENT;
        this.producerPattern = new TransPattern();
        this.consideredPositive = false;
        this.initStats();
    }

    public OutputStatement(Instance instance) {
        this.statement = instance.getFeatureValue(FEATURE_STATEMENT_ID);
        this.producerPattern = instance.getProducerPattern();
        this.consideredPositive = true;
        this.initStats();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters (Base)

    public String getId() {
        return id;
    }

    public String getStatement() {
        return statement;
    }

    public String getIdentifiedStatement() {
        return "<" + this.id + ">: " + statement;
    }


    // ---------- Getters (Tracking)

    public TransPattern getProducerPattern() {
        return producerPattern;
    }

    public int getAnalysisDepth() {
        return producerPattern.getDepth();
    }


    // ---------- Getters (Analysis Statistics)

    public List<Integer> getAnalyzedPatternsNumberList() {
        return analyzedPatternsNumberList;
    }

    public boolean isConsideredPositive() {
        return consideredPositive;
    }

    public boolean isAssessed() {
        return assessed;
    }

    public boolean isTruePositive() {
        return truePositive;
    }

    public boolean isTrueNegative() {
        return trueNegative;
    }

    public boolean isCorrect() {
        return (truePositive || trueNegative);
    }


    // ---------- Setters (Analysis Statistics)

    public void setId(String id) {
        this.id = id;
    }

    public void setAnalyzedPatternsNumberList(List<Integer> analyzedPatternsNumberList) {
        this.analyzedPatternsNumberList = analyzedPatternsNumberList;
    }

    public void addAnalyzedPatternsNumber(Integer number) {
        this.analyzedPatternsNumberList.add(number);
    }


    //==================================================================
    // Base Method(s)
    //==================================================================

    @Override
    public String toString() {
        return "OutputStatement{" +
                "statement='" + statement + '\'' +
                ", producerPattern=" + producerPattern +
                ", analyzedPatternsNumberList=" + analyzedPatternsNumberList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutputStatement that = (OutputStatement) o;
        return Objects.equals(getId(), that.getId()) &&
                getStatement().equals(that.getStatement());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStatement());
    }

    @Override
    public int compareTo(Object o) {
        OutputStatement os = (OutputStatement) o;
        return this.getId().compareTo(os.getId());
    }


    //==================================================================
    // Evaluation Method(s)
    //==================================================================

    private void positiveEval(Set<String> expectedStatementSet) {
        for(String es: expectedStatementSet)
            if (es.equals(statement)) {
                truePositive = true;
                break;
            }
    }

    private void negativeEval(Set<String> expectedStatementSet) {
        trueNegative = expectedStatementSet.contains(statement);
    }

    /**
     * Evaluate the produced statement by comparison with expected
     * statement, and update analysis attributes.
     */
    public void evaluate(Set<String> expectedStatementSet) {
        if (consideredPositive)
            positiveEval(expectedStatementSet);
        else negativeEval(expectedStatementSet);
        this.assessed = true;
    }

}
