package ComputationObject;

import Parameters.DefaultValues;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * WordData class is used to regroup all data working during the main
 * computation processes, including input (ASD representation) and
 * output (formatted statement).
 *
 */
public class Statistics {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final String dataId;


    // ---------- F-Score Evaluation

    public static final double BETA_WEIGHT = 1;
    private double precision;
    private double recall;
    private double fScore;


    // ---------- Time Processing Statistics

    private double learningGlobalTime;
    private double learningSourceParsingTime;
    private double learningSymbolTableConstructionTime;
    private double learningPatternLearningTime;
    private double productionGlobalTime;
    private double productionSourceParsingTime;
    private double productionSymbolTableConstructionTime;
    private double productionSemanticAnalysisTime;
    private double evaluationGlobalTime;

    //==================================================================
    // Construction Method(s)
    //==================================================================

    /**
    * TODO: doc about this method
    */
    private void initializeEvaluation() {
        this.precision = 0;
        this.recall = 0;
        this.fScore = 0;
    }

    /**
     * TODO: doc about this method
     */
    private void initializeStatistics() {
        this.learningGlobalTime = 0;
        this.learningSourceParsingTime = 0;
        this.learningSymbolTableConstructionTime = 0;
        this.learningPatternLearningTime = 0;
        this.productionGlobalTime = 0;
        this.productionSourceParsingTime = 0;
        this.productionSymbolTableConstructionTime = 0;
        this.productionSemanticAnalysisTime = 0;
        this.evaluationGlobalTime = 0;
    }


    //==================================================================
    // Constructor(s)
    //==================================================================

    public Statistics() {
        this.dataId = DefaultValues.NO_ID;
        this.initializeEvaluation();
        this.initializeStatistics();
    }

    public Statistics(String dataId) {
        this.dataId = dataId;
        this.initializeEvaluation();
        this.initializeStatistics();
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters (Base)

    public String getDataId() {
        return this.dataId;
    }


    // ---------- Getters (F-Score Evaluation)

    public double getPrecision() {
        return precision;
    }

    public String getPrecisionStr() {
        NumberFormat nf = new DecimalFormat("0.##");
        return nf.format(precision);
    }

    public double getRecall() {
        return recall;
    }

    public String getRecallStr() {
        NumberFormat nf = new DecimalFormat("0.##");
        return nf.format(recall);
    }

    public double getFScore() {
        return fScore;
    }

    public String getFScoreStr() {
        NumberFormat nf = new DecimalFormat("0.##");
        return nf.format(fScore);
    }


    // ---------- Getters (Time Processing Statistics)

    public double getLearningGlobalTime() {
        return learningGlobalTime;
    }

    public double getLearningSourceParsingTime() {
        return learningSourceParsingTime;
    }

    public double getLearningSymbolTableConstructionTime() {
        return learningSymbolTableConstructionTime;
    }

    public double getLearningPatternLearningTime() {
        return learningPatternLearningTime;
    }

    public double getProductionGlobalTime() {
        return productionGlobalTime;
    }

    public double getProductionSourceParsingTime() {
        return productionSourceParsingTime;
    }

    public double getProductionSymbolTableConstructionTime() {
        return productionSymbolTableConstructionTime;
    }

    public double getProductionSemanticAnalysisTime() {
        return productionSemanticAnalysisTime;
    }

    public double getEvaluationGlobalTime() {
        return evaluationGlobalTime;
    }


    // ---------- Setters (F-Score Evaluation)

    public void setPrecision(double precision) {
        this.precision = precision;
    }

    public void setRecall(double recall) {
        this.recall = recall;
    }

    public void setFScore(double fScore) {
        this.fScore = fScore;
    }


    // ---------- Setters (Time Processing Statistics)

    public void setLearningGlobalTime(double learningGlobalTime) {
        this.learningGlobalTime = learningGlobalTime;
    }

    public void setLearningSourceParsingTime(double learningSourceParsingTime) {
        this.learningSourceParsingTime = learningSourceParsingTime;
    }

    public void setLearningSymbolTableConstructionTime(double learningSymbolTableConstructionTime) {
        this.learningSymbolTableConstructionTime = learningSymbolTableConstructionTime;
    }

    public void setLearningPatternLearningTime(double learningPatternLearningTime) {
        this.learningPatternLearningTime = learningPatternLearningTime;
    }

    public void setProductionGlobalTime(double productionGlobalTime) {
        this.productionGlobalTime = productionGlobalTime;
    }

    public void setProductionSourceParsingTime(double productionSourceParsingTime) {
        this.productionSourceParsingTime = productionSourceParsingTime;
    }

    public void setProductionSymbolTableConstructionTime(double productionSymbolTableConstructionTime) {
        this.productionSymbolTableConstructionTime = productionSymbolTableConstructionTime;
    }

    public void setProductionSemanticAnalysisTime(double productionSemanticAnalysisTime) {
        this.productionSemanticAnalysisTime = productionSemanticAnalysisTime;
    }

    public void setEvaluationGlobalTime(double evaluationGlobalTime) {
        this.evaluationGlobalTime = evaluationGlobalTime;
    }


    // ---------- Basic Method(s)

    @Override
    public String toString() {
        return "Statistics{" +
                "dataId='" + dataId + '\'' +
                ", precision=" + precision +
                ", recall=" + recall +
                ", fScore=" + fScore +
                '}';
    }


    //==================================================================
    // Time Print Method(s)
    //==================================================================

    public static String getTimeString(double time) {
        String result = "";
        int miliseconds = (int) time % 1000;
        int seconds = (int) time / 1000;
        int minutes = (int) seconds / 60;
        seconds %= 60;
        if (minutes >= 1) result += minutes + " m ";
        if (seconds >= 1) result += seconds + " s ";
        result += miliseconds + " ms";
        return  result;
    }

}
