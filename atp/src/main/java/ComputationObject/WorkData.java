package ComputationObject;

import AbstractSemantic.Interpretation;
import Parameters.Configuration;
import Parameters.DefaultValues;
import SourceAnalyzer.Amr.Asd;
import SourceAnalyzer.CapturedData;
import SourceAnalyzer.SymbolTable.Table;

import java.util.*;

/**
 * WordData class is used to regroup all data working during the main
 * computation processes, including input (ASD representation) and
 * output (formatted statement).
 *
 */
public class WorkData {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Base attributes

    private final Asd.Representation ast;
    private final String id;
    private final String sentence;
    private final List<String> expectedOutputStatementList;


    // ---------- Source Analyzing Attributes

    private Table symbolTable;
    private CapturedData capturedData;
    private Boolean sourceAnalysisPerformed = false;


    // ---------- Semantic Transduction Attributes

    private Interpretation interpretation;
    //private Map<String, TransPattern> resultStatementMap; // TODO: to remove
    private Boolean transductionPerformed = false;


    // ---------- Output Attributes

    //private static final String DEFAULT_REQ = "-"; // TODO: to remove
    private Set<OutputStatement> outputStatementSet;


    // ---------- Time Processing Statistics

    // TODO


    //==================================================================
    // Constructor(s)
    //==================================================================

    public WorkData(Asd.Representation ast) {
        this.ast = ast;
        this.id = ast.getId();
        this.sentence = ast.getSentence();
        this.expectedOutputStatementList = new ArrayList<>();
        for (String rawExpectedOutput: ast.getOutputList()) {
            String normalizedOutput =
                    Configuration.NORMALIZER.normalize(rawExpectedOutput);
            expectedOutputStatementList.add(normalizedOutput);
        }
    }


    //==================================================================
    // Accessor(s)
    //==================================================================

    // ---------- Getters (base)

    public Asd.Representation getAst() {
        return ast;
    }

    public String getId() {
        return this.id;
    }

    public String getSentence() {
        return sentence;
    }

    public List<String> getExpectedOutputStatementList() {
        return this.expectedOutputStatementList;
    }


    // ---------- Getters (source analyzing)

    public Table getSymbolTable() {
        return symbolTable;
    }

    public CapturedData getCapturedData() {
        return capturedData;
    }


    // ---------- Getters (semantic transduction)

    public Interpretation getInterpretation() {
        return interpretation;
    }

/*    public Map<String, TransPattern> getResultStatementMap() {
        return resultStatementMap;
    }*/ // TODO: to remove

    /*public Set<String> getResultStatementSet() {
        return resultStatementMap.keySet();
    }*/ // TODO: to remove


    // ---------- Getters (output)

    public Set<String> getEntitySet() {
        Set<String> entitySet = new HashSet<>();
        Map<String, Map<String, String>> dataMap = capturedData.getDataMap();
        Collection propValues = dataMap.get("entity").values();
        entitySet.addAll(propValues);
        return entitySet;
    }

    public Set<String> getPropertySet() {
        Set<String> propertySet = new HashSet<>();
        Map<String, Map<String, String>> dataMap = capturedData.getDataMap();
        Collection propValues = dataMap.get("property").values();
        propertySet.addAll(propValues);
        return propertySet;
    }

    public Set<OutputStatement> getOutputStatementSet() {
        Set<OutputStatement> resultSet = new HashSet<>();
        if (this.outputStatementSet.isEmpty()) {
            OutputStatement os = new OutputStatement();
            os.setId(this.getId());
            resultSet.add(os);
        }
        else resultSet.addAll(outputStatementSet);
        return resultSet;
    }

    public String getIdentifiedStatement(String statement) {
        return "<" + this.id + ">: " + statement;
    }

    public List<String> getIdentifiedStatementList() {
        List<String> reqList = new ArrayList<>();
        if (this.outputStatementSet.isEmpty())
            reqList.add(getIdentifiedStatement(DefaultValues.DEFAULT_STATEMENT));
        else {
            for(OutputStatement os: outputStatementSet)
                reqList.add(getIdentifiedStatement(os.getStatement()));
        }
        //Collections.sort(reqList); // TODO: to remove
        return reqList;

    }


    // ---------- Getters (evaluation and statistics)

    public List<String> getRelevantSampleList() {
        List<String> sampleList = new ArrayList<>();
        String normalizedOS;
        for (String os: this.expectedOutputStatementList) {
            normalizedOS = Configuration.NORMALIZER.normalize(os);
            sampleList.add(getIdentifiedStatement(normalizedOS));
        }
        return sampleList;
    }

    public List<String> getPositiveStatementList() {
        List<String> positiveList = new ArrayList<>();
        if (!this.outputStatementSet.isEmpty()) {
            for(OutputStatement os: outputStatementSet) {
                if (os.isConsideredPositive())
                    positiveList.add(getIdentifiedStatement(os.getStatement()));
            }
        }
        return positiveList;
    }

    public List<String> getNegativeStatementList() {
        List<String> negativeList = new ArrayList<>();
        if (this.outputStatementSet.isEmpty()) {
            negativeList.add(getIdentifiedStatement(DefaultValues.DEFAULT_STATEMENT));
        }
        else {
            for (OutputStatement os: outputStatementSet)
                if (!os.isConsideredPositive())
                    negativeList.add(os.getIdentifiedStatement());
        }
        return negativeList;
    }


    // ---------- Predicates

    public boolean hasId(String id) {
        return this.id.equals(id);
    }

    public Boolean isSourceAnalysisPerformed() {
        return sourceAnalysisPerformed;
    }

    public Boolean isTransductionPerformed() {
        return transductionPerformed;
    }


    // ---------- Setters (source analyzing)

    public void setSymbolTable(Table symbolTable) {
        this.symbolTable = symbolTable;
    }

    public void setCapturedData(CapturedData capturedData) {
        this.capturedData = capturedData;
        this.sourceAnalysisPerformed = true;
    }


    // ---------- Setters (semantic transduction)

    public void setInterpretation(Interpretation interpretation) {
        this.interpretation = interpretation;
    }

/*    public void setResultStatementMap(Map<String, TransPattern> resultStatementMap) {
        this.resultStatementMap = resultStatementMap;
        this.transductionPerformed = true;
    }*/ // TODO: to remove

    public void setOutputStatementSet(Set<OutputStatement> outputStatementSet) {
        this.outputStatementSet = outputStatementSet;
        this.transductionPerformed = true;
    }

    // ---------- Basic Method(s)

    @Override
    public String toString() {
        return "WorkData{" +
                "ast=" + ast +
                ", id='" + id + '\'' +
                ", sentence='" + sentence + '\'' +
                ", expectedOutputStatementList=" + expectedOutputStatementList +
                ", symbolTable=" + symbolTable +
                ", capturedData=" + capturedData +
                ", sourceAnalysisPerformed=" + sourceAnalysisPerformed +
                ", definition=" + interpretation +
                ", transductionPerformed=" + transductionPerformed +
                ", outputStatementSet=" + outputStatementSet +
                '}';
    }


    //==================================================================
    // Handling Method(s)
    //==================================================================

    /**
     * TODO: doc about this method
     */
    public void removeResultStatementDuplicates() {
        // TODO: if necessary
    }

    /**
     * Give an id to all output statements.
     */
    public void identifyOutputStatement() {
        for(OutputStatement os: outputStatementSet) os.setId(this.id);
    }

    /**
     * Evaluate output statements by comparison with expected statements.
     */
    public void evaluateOutputStatement() {
        Set<String> esSet = new HashSet<>();
        if (expectedOutputStatementList.isEmpty())
            esSet.add(DefaultValues.DEFAULT_STATEMENT);
        else esSet.addAll(expectedOutputStatementList);
        for(OutputStatement os: outputStatementSet) os.evaluate(esSet);
    }

}
