# ::id corpus_vending_machine_01_01
# ::snt The classic vending machine revisited.
# ::out -
(vv1 / revisit-01
      :ARG1 (vv2 / machine
            :mod (vv4 / classic)))

# ::id corpus_vending_machine_01_02
# ::snt Initially, a coin can be inserted.
# ::out <possible, insert_coin, time initial>
(vv1 / possible-01
      :ARG1 (vv2 / insert-01
            :ARG1 (vv3 / coin))
      :time (vv4 / initial))

# ::id corpus_vending_machine_01_03
# ::snt Initially, the cancel button is disabled.
# ::out <necessary, disable_cancel-button, time initial>
(vv1 / disable-01
      :ARG1 (vv2 / button
            :mod (vv3 / cancel-01))
      :ARG1-of (vv4 / appear-02))

# ::id corpus_vending_machine_01_04
# ::snt Cancel cannot be pressed after the cancel button has already been pressed.
# ::out <impossible, press_cancel, after press_cancel>
(vv1 / possible-01
      :ARG1 (vv2 / press-01
            :ARG1 (vv3 / cancel-01)
            :time (vv4 / after
                  :op1 (vv5 / out-01
                        :ARG1 (vv6 / button
                              :mod (vv7 / cancel-01))
                        :time (vv8 / already))))
      :polarity -)

# ::id corpus_vending_machine_01_05
# ::snt No drink is ordered.
# ::out -
(vv1 / order-01
      :ARG2 (vv2 / drink-01
            :polarity -))

# ::id corpus_vending_machine_01_06
# ::snt After a cancellation, a coin can be inserted.
# ::out <possible, insert-coin, cancel>
(vv1 / possible-01
      :ARG1 (vv2 / insert-01
            :ARG1 (vv3 / coin)))

# ::id corpus_vending_machine_01_07
# ::snt After inserting a coin, cancel can be pressed.
# ::out <possible, press-01_cancel-01, after insert-01_coin>
(vv1 / possible-01
      :ARG1 (vv2 / press-01
            :ARG1 (vv3 / cancel-01))
      :time (vv4 / after
            :op1 (vv5 / insert-01
                  :ARG1 (vv6 / coin))))

# ::id corpus_vending_machine_01_08
# ::snt After pressing request, we must get drink.
# ::out <necessary, get-03_we_drink-01, after press_request-01>
(vv1 / obligate-01
      :ARG2 (vv2 / get-03
            :ARG1 (vv3 / we)
            :ARG2 (vv4 / drink-01
                  :ARG0 vv3)
            :time (vv6 / after
                  :op1 (vv7 / request-01
                        :ARG0 (vv8 / press)))))

# ::id corpus_vending_machine_01_09
# ::snt After obtaining drink, we must be allowed to insert a coin.
# ::out <possible, we_insert-coin, after we_obtain-drink-01>
(vv1 / obligate-01
      :ARG2 (vv2 / allow-01
            :ARG1 (vv3 / insert-01
                  :ARG0 (vv4 / we)
                  :ARG1 (vv5 / coin))
            :time (vv6 / after
                  :op1 (vv7 / obtain-01
                        :ARG0 vv4
                        :ARG1 (vv9 / drink-01)))))

# ::id corpus_vending_machine_01_10
# ::snt After inserting a coin, we must be allowed to order drink.
# ::out <possible, we_order-02_drink-01, after insert_coin>
(vv1 / obligate-01
      :ARG2 (vv2 / allow-01
            :ARG1 (vv3 / order-02
                  :ARG0 (vv4 / we)
                  :ARG1 (vv5 / drink-01
                        :ARG0 vv4)))
      :time (vv7 / after
            :op1 (vv8 / insert-01
                  :ARG0 vv4
                  :ARG1 (vv10 / coin))))