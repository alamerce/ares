import LogicalQuery.Atom;
import LogicalQuery.Query;
import LogicalQuery.ValuesSet;
import LogicalQuery.Variable;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConjunctiveQueryTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Test Data Constructor(s)

    public Query getTestQuery1() {
        List<String> variables = new ArrayList<>();
        variables.add("x4");
        variables.add("x6");
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("ARG1", "x4", "x2"));
        atoms.add(new Atom("MODALITY", "x6"));
        Query query = new Query(variables, atoms);
        System.out.println(" -- Test Query 1: " + query.toString());
        return query;
    }

    public Query getTestQuery2() {
        List<String> variables = new ArrayList<>();
        variables.add("x1");
        variables.add("x2");
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("ARG1", "x1", "x2"));
        atoms.add(new Atom("MODALITY", "x1"));
        Query query = new Query(variables, atoms);
        System.out.println(" -- Test Query 1: " + query.toString());
        return query;
    }

    public Set<Atom> getTestAtomSet1() {
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("ARG1", "x6", "x3"));
        atomSet.add(new Atom("MODALITY", "x6"));
        atomSet.add(new Atom("TEST", "zzzz"));
        System.out.println(" -- Atom List: " + atomSet.toString());
        return atomSet;
    }



    //==================================================================
    // Tests: class Variable
    //==================================================================

    @Test
    public void VariableTest1() {
        Variable v1 = new Variable("x", "1");
        System.out.println("v1: " + v1.toString());
        Variable v2 = new Variable("y", "2");
        System.out.println("v2: " + v2.toString());
        Assert.assertEquals("(x, 1)", v1.toString());
        Assert.assertEquals("(y, 2)", v2.toString());
    }

    @Test
    public void VariableTest2() {
        Variable v1 = new Variable("x", "1");
        System.out.println(" -- Variable v1: " + v1.toString());
        System.out.println(" ----- ident: " + v1.getIdent());
        System.out.println(" ----- init value: " + v1.getValue());
        v1.setValue("3");
        System.out.println(" ------ new value (1): " + v1.getValue());
        v1.setValue("2");
        System.out.println(" ------ new value (2): " + v1.getValue());
        Assert.assertEquals("(x, 2)", v1.toString());
        Assert.assertEquals("x", v1.getIdent());
        Assert.assertEquals("2", v1.getValue());
    }


    //==================================================================
    // Tests: class ValuesSet
    //==================================================================

    @Test
    public void ValuesSetTest1() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        List<String> a, b, c, d;
        List<String> s1, s2, s3, s4, s5;
        ValuesSet vs;

        // Test Code
        a = new ArrayList<>();
        a.add("a");
        a.add("b");
        a.add("c");
        a.add("d");
        a.add("e");
        b = new ArrayList<>();
        b.add("a");
        c = new ArrayList<>();
        c.add("b");
        c.add("e");
        d = new ArrayList<>();
        d.add("f");
        vs = new ValuesSet("test");
        System.out.println("a: " + a.toString());
        System.out.println("b: " + b.toString());
        System.out.println("c: " + c.toString());
        System.out.println("d: " + d.toString());
        s1 = vs.substractList(a, b);
        System.out.println("substract(a, b): " + s1.toString());
        s2 = vs.substractList(a, c);
        System.out.println("substract(a, b): " + s2.toString());
        s3 = vs.substractList(a, d);
        System.out.println("substract(a, b): " + s3.toString());
        s4 = vs.substractList(b, d);
        System.out.println("substract(a, b): " + s4.toString());
        s5 = vs.substractList(b, a);
        System.out.println("substract(b, a): " + s5.toString());

        // Asserts
        Assert.assertEquals("[b, c, d, e]", s1.toString());
        Assert.assertEquals("[a, c, d]", s2.toString());
        Assert.assertEquals("[a, b, c, d, e]", s3.toString());
        Assert.assertEquals("[a]", s4.toString());
        Assert.assertEquals("[]", s5.toString());

    }

    @Test
    public void ValuesSetTest2() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ValuesSet vs1, vs2;
        List<List<String>> valuations;

        // Test Code
        vs1 = new ValuesSet("a", "b", "c");
        System.out.println(vs1.getBaseList().toString());
        valuations = vs1.getValuations(2);
        System.out.println(valuations.toString());
        vs2 = new ValuesSet("a", "b", "c", "d", "e");
        System.out.println(vs2.getBaseList().toString());
        valuations = vs2.getValuations(1);
        System.out.println(valuations.toString());
        valuations = vs2.getValuations(2);
        System.out.println(valuations.toString());
        valuations = vs2.getValuations(3);
        System.out.println(valuations.toString());
        valuations = vs2.getValuations(4);
        System.out.println(valuations.toString());
        valuations = vs2.getValuations(5);
        System.out.println(valuations.toString());
        valuations = vs2.getValuations(6);
        System.out.println(valuations.toString());

        // Asserts
        Assert.assertEquals("[[a, b], [a, c], [b, a], [b, c], [c, a], [c, b]]",
                vs1.getValuations(2).toString());
        Assert.assertEquals("[]",
                vs1.getValuations(5).toString());

    }

    @Test
    public void ValuesSetPerfTest1() {
        System.out.println(" *** Performance Test 1 ***");
        ValuesSet vs = new ValuesSet("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "x", "y", "z");
        vs.getValuations(3);
        System.out.println("Done");
    }

    @Test
    public void ValuesSetPerfTest2() {
        System.out.println(" *** Performance Test 2 ***");
        ValuesSet vs = new ValuesSet("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "x", "y", "z");
        vs.getValuations(4);
        System.out.println("Done");
    }

    @Test
    public void ValuesSetPerfTest3() {
        System.out.println(" *** Performance Test 3 ***");
        ValuesSet vs = new ValuesSet("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "x", "y", "z");
        vs.getValuations(5);
        System.out.println("Done");
    }


    //==================================================================
    // Tests: class Atom
    //==================================================================

    @Test
    public void AtomTest1() {
        Atom a1 = new Atom("ARG1", "x4", "x2");
        System.out.println("a1: " + a1.toString());
        Atom a2 = new Atom("MODALITY", "x6");
        System.out.println("a2: " + a2.toString());
        Assert.assertEquals("ARG1(x4, x2)", a1.toString());
        Assert.assertEquals("MODALITY(x6)", a2.toString());
    }

    @Test
    public void AtomTest2() {
        Atom a1 = new Atom("ARG1", "x4", "x2");
        System.out.println("a1: " + a1.toString());
        System.out.println("relation(a1)= " + a1.getRelation());
        System.out.println("args(a1)= " + a1.getArgs().toString());
        Atom a2 = new Atom("MODALITY", "x6");
        System.out.println("a2: " + a2.toString());
        System.out.println("relation(a2)= " + a2.getRelation());
        System.out.println("args(a2)= " + a2.getArgs().toString());
        Atom a3 = new Atom("TEST");
        System.out.println("a3: " + a3.toString());
        System.out.println("relation(a3)= " + a3.getRelation());
        System.out.println("args(a3)= " + a3.getArgs().toString());
        Assert.assertEquals("ARG1", a1.getRelation());
        Assert.assertEquals("[x4, x2]", a1.getArgs().toString());
        Assert.assertEquals("MODALITY", a2.getRelation());
        Assert.assertEquals("[x6]", a2.getArgs().toString());
        Assert.assertEquals("TEST", a3.getRelation());
        Assert.assertEquals("[]", a3.getArgs().toString());
    }

    @Test
    public void AtomTest3() {
        Atom a1 = new Atom("ARG1", "x4", "x2");
        Atom a2 = new Atom("ARG1", "x4", "x2");
        Atom a3 = new Atom("ARG1", "y", "z");
        Assert.assertTrue(a1.equals(a2));
        Assert.assertFalse(a1.equals(a3));
        Assert.assertFalse(a2.equals(a3));
    }

    //==================================================================
    // Tests: class Formula
    //==================================================================

    @Test
    public void FormulaTest1() {
        System.out.println(" **** Formula Test 1 *** ");
        Query f1 = getTestQuery1();
        System.out.println(" -- Query f1: " + f1.toString());
        String expectedRawQuery = "lambda x4. lambda x6. ARG1(x4, x2) AND MODALITY(x6)";
        Assert.assertEquals(expectedRawQuery, f1.toString());
    }

    @Test
    public void FormulaTest2() {

        System.out.println(" **** Formula Test 2 *** ");

        Query f1 = getTestQuery1();
        System.out.println(" -- Query f1: " + f1.toString());
        System.out.println(" ----- <formula> " + f1.toString());
        System.out.println(" ----- <valuation> " + f1.getValuationString());

        // Affectation
        f1.affectVariable("x6", "test_err");
        System.out.println(" -- Query f1 after affectation (1): " + f1.toString());
        System.out.println(" ----- <formula> " + f1.toString());
        System.out.println(" ----- <valuation> " + f1.getValuationString());

        // Affectation
        f1.affectVariable("x6", "x99");
        System.out.println(" -- Query f1 after affectation (2): " + f1.toString());
        System.out.println(" ----- <formula> " + f1.toString());
        System.out.println(" ----- <valuation> " + f1.getValuationString());

        // Asserts
        String expectedRawQuery1 = "lambda x4. lambda x6. ARG1(x4, x2) AND MODALITY(x6)";
        String expectedRawQuery2 = "lambda x4. lambda x99. ARG1(x4, x2) AND MODALITY(x99)";
        Assert.assertEquals(expectedRawQuery1, f1.toString());
        Assert.assertEquals(expectedRawQuery2, f1.getValuationString());

    }

    @Test
    public void FormulaTest3() {

        System.out.println(" **** Formula Test 3 *** ");

        Query f1 = getTestQuery2();
        Set<Atom> atomSet = getTestAtomSet1();

        f1.affectVariable("x1", "x6");
        f1.affectVariable("x2", "test_err");
        f1.affectVariable("x2", "y3");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation 1: " + f1.isSatisfied(atomSet));
        f1.affectVariable("x2", "test_err");
        f1.affectVariable("x2", "z3");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation 2: " + f1.isSatisfied(atomSet));
        f1.affectVariable("x2", "test_err");
        f1.affectVariable("x2", "x3");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation 3: " + f1.isSatisfied(atomSet));

        Assert.assertTrue(f1.isSatisfied(atomSet));

    }

    @Test
    public void FormulaTest4() {

        System.out.println(" **** Formula Test 4 *** ");

        Query f1 = getTestQuery2();
        Set<Atom> atomSet = getTestAtomSet1();

        f1.affectVariables("x6", "x3");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation: " + f1.isSatisfied(atomSet));

        Assert.assertTrue(f1.isSatisfied(atomSet));

    }

    @Test
    public void FormulaTest5() {

        System.out.println(" **** Formula Test 5 *** ");

        Query f1 = getTestQuery2();
        Set<Atom> atomSet = getTestAtomSet1();

        List<String> newValues = new ArrayList<>();
        newValues.add("x6");
        newValues.add("x3");
        f1.affectVariables(newValues);
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation: " + f1.isSatisfied(atomSet));

        Assert.assertTrue(f1.isSatisfied(atomSet));

    }

    @Test
    public void FormulaTest6() {

        System.out.println(" **** Formula Test 6 *** ");

        Query f1 = getTestQuery2();
        Set<Atom> atomSet = getTestAtomSet1();


        f1.affectVariables("x6", "x3");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation 1: " + f1.isSatisfied(atomSet));
        f1.affectVariables("x6", "test_err");
        f1.affectVariables("x6", "z3");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation 2: " + f1.isSatisfied(atomSet));
        f1.affectVariable("x3", "x6");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation 3: " + f1.isSatisfied(atomSet));
        f1.affectVariables("x6", "test_err");
        f1.affectVariables("x6", "x3");
        System.out.println(" -- Updated Query: : " + f1.toString());
        System.out.println(" -- Query Evaluation 4: " + f1.isSatisfied(atomSet));

        Assert.assertTrue(f1.isSatisfied(atomSet));

    }


}
