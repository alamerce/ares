import AbstractSemantic.TransPattern;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternSet;
import ComputationObject.OutputStatement;
import ComputationObject.Statistics;
import ComputationObject.Utility;
import ComputationObject.WorkData;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class ParkingCorpusTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    static private String TEST_RESOURCES_PATH = "src/test/resources/";
    static private String CORPUS_FILE_01 = "corpus_parking_01.amrep";
    static private String CORPUS_FILE_02 = "corpus_parking_02.amrep";
    static private String CORPUS_FILE_03 = "corpus_parking_03.amrep";
    static private String CORPUS_FILE_ONE_CASE = "corpus_parking_one_case.amrep";


    // ---------- Run Method(s)

    public void runComputation(String corpus) {
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + corpus;
        Computation.main(args);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
    }


    // ---------- Print Method(s)

    public void printStatistics() {
        Statistics statistics = Computation.getDataStatistics();
        System.out.println("");
        System.out.println("======================================================================");
        System.out.println("== ");
        System.out.println("== Evaluation (data: " + statistics.getDataId() + ")");
        System.out.println("==   Precision: " + statistics.getPrecisionStr());
        System.out.println("==   Recall: " + statistics.getRecallStr());
        System.out.println("==   F-Score (" + Statistics.BETA_WEIGHT + "): " + statistics.getFScoreStr());
        System.out.println("== ");
        System.out.println("== --------------------------------------");
        System.out.println("== ");
        System.out.println("== Time Processing Statistics ");
        System.out.print("==   Learning: " + Statistics.getTimeString(statistics.getLearningGlobalTime()));
        System.out.println("   (  " + Statistics.getTimeString(statistics.getLearningSourceParsingTime()) +
                "  |  " + Statistics.getTimeString(statistics.getLearningSymbolTableConstructionTime()) +
                "  |  " + Statistics.getTimeString(statistics.getLearningPatternLearningTime()) +
                "  )");
        System.out.print("==   Production: " + Statistics.getTimeString(statistics.getProductionGlobalTime()));
        System.out.println("   ( " + Statistics.getTimeString(statistics.getProductionSourceParsingTime()) +
                "  |  " + Statistics.getTimeString(statistics.getProductionSymbolTableConstructionTime()) +
                "  |  " + Statistics.getTimeString(statistics.getProductionSemanticAnalysisTime()) +
                "  )");
        System.out.println("==   Evaluation: " + Statistics.getTimeString(statistics.getEvaluationGlobalTime()));
        System.out.println("== ");
        System.out.println("======================================================================");
    }

    public void printProducedStatements() {

        List<WorkData> evalDataList = Computation.getEvalDataList();
        List<OutputStatement> statementList = Utility.getOutputStatementList(evalDataList);

        System.out.println("");
        System.out.println(" -- Produced Statements (" + statementList.size() + "): ");

        for(OutputStatement os: statementList) {

            String oss = String.format("%1$-100s", new String[]{os.getIdentifiedStatement()});

            String statString = "";
            int count = 0;
            for(Integer stat: os.getAnalyzedPatternsNumberList()) {
                statString += " | " + stat;
                count++;
            }
            for (int i = count; i < 5 ; i++) statString += " | " + "-";
            statString += " | ";
            statString = String.format("%1$-20s", new String[]{"" + statString});

            String osEvalString = "   ----->   ";
            if(os.isCorrect()) osEvalString += String.format("%1$-8s", new String[]{"True"});
            else osEvalString += String.format("%1$-8s", new String[]{"False"});
            String detailedEvalString = "(" + os.isConsideredPositive() +
                    ", " + os.isAssessed() +
                    ", " + os.isTruePositive() +
                    ", " + os.isTrueNegative() +
                    ")";
            osEvalString += String.format("%1$-30s", new String[]{detailedEvalString});

            String producedPatternString = "[Produced Pattern: ";
            producedPatternString += " depth = " + os.getProducerPattern().getDepth();
            producedPatternString += " ; ppi1 = " + os.getProducerPattern().getPrecisionFirstLine();
            producedPatternString += " ; ppi2 = " + os.getProducerPattern().getPrecisionSecondLine();
            producedPatternString += "]";
            producedPatternString = String.format("%1$-100s", new String[]{producedPatternString});

            System.out.print(oss + statString + osEvalString + producedPatternString);
            System.out.println();

        }
    }


    public void printDetailedData() {
        for (WorkData wd: Computation.getEvalDataList()) {
            Set<OutputStatement> osSet = wd.getOutputStatementSet();
            Iterator<OutputStatement> osSetIt = osSet.iterator();
            while (osSetIt.hasNext()) {
                osSetIt.next();
                System.out.println();
                System.out.println(" -- Detailed Data for " + wd.getId());
                System.out.println(wd.getSymbolTable().toString());
                System.out.println(wd.getInterpretation().toString());
                System.out.println(wd.getIdentifiedStatementList());
                System.out.println();
            }
        }
    }

    public void printPatternSet() {

        TransPatternSet patternSet = Computation.getTransPatternSet();

        System.out.println("");
        System.out.println(" -- Pattern Set (" + patternSet.size() + "): ");
        System.out.println(" --- Maximum Depth = " + patternSet.getMaximumDepth());

        for(TransPattern pattern: patternSet.getSet()) {
            System.out.println("");
            System.out.println(pattern.toString());
            System.out.println("   ----- query: " + pattern.getQuery().toString());
            Template t = pattern.getTemplate();
            System.out.println("   ----- template (" + t.getConcept() +"): " + t.getRawTemplateString());
            System.out.println("   ----- depth: " + pattern.getDepth());
            System.out.println("   ----- isAbstract? " + pattern.isAbstract());
            System.out.println("   ----- precision: " + pattern.getPrecisionFirstLine() +
                    " / " + pattern.getPrecisionSecondLine());
        }
    }


    //==================================================================
    // Development Test
    //==================================================================

    @Test
    public void DevelopmentTest() {
        System.out.println();
        System.out.println(" **** Development Test **** ");
        String corpus = CORPUS_FILE_ONE_CASE;
        runComputation(corpus);
        printStatistics();
        printProducedStatements();
        printDetailedData();
        printPatternSet();
    }


    //==================================================================
    // Parking Corpus Tests
    //==================================================================

    @Test
    public void ParkingCorpusTest1() {
        System.out.println();
        System.out.println(" **** Parking Corpus Test 1 **** ");
        String corpus = CORPUS_FILE_01;
        runComputation(corpus);
        printStatistics();
        printProducedStatements();
        printDetailedData();
        printPatternSet();
    }

    @Test
    public void ParkingCorpusTest2() {
        System.out.println();
        System.out.println(" **** Parking Corpus Test 2 **** ");
        String corpus = CORPUS_FILE_02;
        runComputation(corpus);
        printStatistics();
        printProducedStatements();
        printDetailedData();
        printPatternSet();
    }
    @Test
    public void ParkingCorpusTest3() {
        System.out.println();
        System.out.println(" **** Parking Corpus Test 3 **** ");
        String corpus = CORPUS_FILE_03;
        runComputation(corpus);
        printStatistics();
        printProducedStatements();
        printDetailedData();
        printPatternSet();
    }

}
