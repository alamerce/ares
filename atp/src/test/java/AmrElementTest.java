import Parameters.Category;
import SourceAnalyzer.Amr.Element.*;
import SourceAnalyzer.SymbolTable.Role;
import SourceAnalyzer.SymbolTable.Symbol;
import SourceAnalyzer.SymbolTable.Table;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AmrElementTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Test Data Constructor(s)

    public Symbol genSymbol1() {
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("ARG1-of", "y2"));
        rList0.add(new Role("ARG1", "x3"));
        rList0.add(new Role("type", "test"));
        return new Symbol("x", Category.UNKNOWN, "sky", rList0);
    }

    public Symbol genSymbol2() {
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("ARG1-of", "y2"));
        rList0.add(new Role("ARG1", "x3"));
        rList0.add(new Role("type", "test"));
        rList0.add(new Role("name", "Red"));
        return new Symbol("x", Category.UNKNOWN, "test", rList0);
    }

    public Symbol genSymbol3() {
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("ARG1-of", "y2"));
        rList0.add(new Role("ARG1", "x3"));
        rList0.add(new Role("name", "Red"));
        rList0.add(new Role("name", "Black"));
        rList0.add(new Role("type", "Ciseaux"));
        return new Symbol("x", Category.UNKNOWN, "car", rList0);
    }

    public Symbol genSymbol4() {
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("name", "Red"));
        return new Symbol("m", Category.MODALITY, "possible-01", rList0);
    }

    public Symbol genSymbol5() {
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("name", "Red"));
        return new Symbol("m", Category.MODALITY, "obligatory", rList0);
    }

    public Symbol genSymbol6() {
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("name", "Red"));
        return new Symbol("t", Category.TEMPORALITY, "next", rList0);
    }

    public Symbol genSymbol7() {
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("name", "Red"));
        return new Symbol("t", Category.TEMPORALITY, "before-01", rList0);
    }

    public Symbol genSymbol8() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG0", "x0"));
        rList.add(new Role("ARG1", "x1"));
        rList.add(new Role("condition", "x5"));
        return new Symbol("p", Category.PROPERTY, "focus", rList);
    }

    public Symbol genSymbol9() {
        return new Symbol("x0", Category.ENTITY, "reflector");
    }

    public Symbol genSymbol10() {
        return new Symbol("x1", Category.ENTITY, "sunlight");
    }

    public Symbol genSymbol11() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG1", "vv6"));
        rList.add(new Role("ARG1", "vv7"));
        return new Symbol("vv5", Category.PROPERTY, "pass", rList);
    }

    public Symbol genSymbol12() {
        return new Symbol("vv6", Category.ENTITY, "vehicle");
    }

    public Symbol genSymbol13() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG1", "vv6"));
        return new Symbol("vv7", Category.PROPERTY, "dock-01", rList);
    }

    public Symbol genSymbol14() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("op1", "vv5"));
        return new Symbol("vv4", Category.TEMPORALITY, "after", rList);
    }

    public Symbol genSymbol15() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG0", "x1"));
        rList.add(new Role("condition", "x8"));
        rList.add(new Role("polarity", "-"));
        return new Symbol("x4", Category.PROPERTY, "pass-01", rList);
    }

    public Symbol genSymbol16() {
        return new Symbol("x1", Category.ENTITY, "vehicles");
    }

    public Symbol genSymbol17() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG1", "x6"));
        return new Symbol("x8", Category.PROPERTY, "close-01", rList);
    }

    public Symbol genSymbol18() {
        return new Symbol("-", Category.ENTITY);
    }

    public Symbol genSymbol19() {
        return new Symbol("x6", Category.ENTITY, "gate");
    }

    public Symbol genSymbol20() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG1", "vv2"));
        rList.add(new Role("condition", "vv4"));
        return new Symbol("vv1", Category.MODALITY, "recommend-01", rList);
    }

    public Symbol genSymbol21() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("domain", "vv3"));
        return new Symbol("vv2", Category.PROPERTY, "thaw", rList);
    }

    public Symbol genSymbol22() {
        return new Symbol("vv3", Category.ENTITY, "snow");
    }

    public Symbol genSymbol23() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG1", "vv5"));
        return new Symbol("vv4", Category.PROPERTY, "appear-01");
    }

    public Symbol genSymbol24() {
        return new Symbol("vv5", Category.ENTITY, "sun");
    }

    public Symbol genSymbol25() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("mod", "vv5"));
        rList.add(new Role("domain", "vv3"));
        return new Symbol("vv2", Category.PROPERTY, "thaw", rList);
    }

    public Symbol genSymbol26() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("mod", "enter"));
        return new Symbol("x2", Category.ENTITY, "gate", rList);
    }

    public Symbol genSymbol27() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("mod", "vv8"));
        return new Symbol("vv7", Category.ENTITY, "coat", rList);
    }

    public Symbol genSymbol28() {
        return new Symbol("vv8", Category.ENTITY, "winter");
    }

    public Symbol genSymbol29() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("mod", "vv5"));
        return new Symbol("vv4", Category.ENTITY, "crisis", rList);
    }

    public Symbol genSymbol30() {
        List<Role> rList = new ArrayList<>();
        rList.add(new Role("ARG1", "vv6"));
        return new Symbol("vv5", Category.ENTITY, "storm-01", rList);
    }

    public Symbol genSymbol31() {
        return new Symbol("vv6", Category.ENTITY, "ice");
    }

    public Table genTable1() {
        Table table = new Table("T1");
        table.add(genSymbol4());
        table.add(genSymbol8());
        table.add(genSymbol9());
        table.add(genSymbol10());
        return table;
    }

    public Table genTable2() {
        Table table = new Table("Parking_1.05");
        table.add(genSymbol11());
        table.add(genSymbol12());
        table.add(genSymbol13());
        table.add(genSymbol14());
        return table;
    }

    public Table genTable3() {
        Table table = new Table("ParkingTest3");
        table.add(genSymbol15());
        table.add(genSymbol16());
        table.add(genSymbol17());
        table.add(genSymbol18());
        table.add(genSymbol19());
        return table;
    }

    public Table genTable4() {
        Table table = new Table("WeatherTest1");
        table.add(genSymbol20());
        table.add(genSymbol21());
        table.add(genSymbol22());
        table.add(genSymbol23());
        table.add(genSymbol24());
        return table;
    }

    public Table genTable5() {
        Table table = new Table("WeatherTest2");
        table.add(genSymbol27());
        table.add(genSymbol28());
        return table;
    }

    public Table genTable6() {
        Table table = new Table("WeatherTest3");
        table.add(genSymbol29());
        table.add(genSymbol30());
        table.add(genSymbol31());
        return table;
    }


    //==================================================================
    // Arg Tests
    //==================================================================

    @Test
    public void ArgTest1() {
        System.out.println(" *** Arg Test 1 *** ");
        Role r1 = new Role("ARG0", "x0");
        Arg a1 = new Arg(r1);
        System.out.println(" -- role: " + r1.toString());
        System.out.println(" -- arg: " + a1.toString());
        String expectedArg1 = "Arg{id='x0', name='***No Name***', isEmpty=false, hasName=false}";
        Assert.assertEquals(expectedArg1, a1.toString());
        Assert.assertEquals("x0", a1.getId());
        Assert.assertEquals("x0", a1.getName());
    }

    @Test
    public void ArgTest2() {
        System.out.println(" *** Arg Test 2 *** ");
        Role r1 = new Role("ARG0", "x0");
        Arg a1 = new Arg(r1);
        System.out.println(" -- role: " + r1.toString());
        System.out.println(" -- arg (1): " + a1.toString());
        a1.setName("updated-name");
        System.out.println(" -- arg (2): " + a1.toString());
        String expectedArg1 = "Arg{id='x0', name='updated-name', isEmpty=false, hasName=true}";
        Assert.assertEquals(expectedArg1, a1.toString());
        Assert.assertEquals("x0", a1.getId());
        Assert.assertEquals("updated-name", a1.getName());
    }


    //==================================================================
    // Entity Tests
    //==================================================================

    @Test
    public void EntityTest1() {
        System.out.println(" *** Entity Test 1 *** ");
        Symbol s = genSymbol1();
        Entity e = new Entity(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- entity: " + e.toString());
        System.out.println(" -- entity full name: " + e.getFullName());
        String expectedEntity = "Entity{id='x', mainConcept='sky', featureList=[]}";
        Assert.assertEquals(expectedEntity, e.toString());
        Assert.assertEquals("x", e.getId());
        Assert.assertEquals("sky", e.getMainConcept());
        Assert.assertEquals("sky", e.getFullName());
    }

    @Test
    public void EntityTest2() {
        System.out.println(" *** Entity Test 2 *** ");
        Symbol s = genSymbol2();
        Entity e = new Entity(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- entity: " + e.toString());
        System.out.println(" -- entity full name: " + e.getFullName());
        String expectedEntity = "Entity{id='x', mainConcept='test', featureList=[Arg{id='Red', name='***No Name***', isEmpty=false, hasName=false}]}";
        Assert.assertEquals(expectedEntity, e.toString());
        Assert.assertEquals("x", e.getId());
        Assert.assertEquals("test", e.getMainConcept());
        Assert.assertEquals("Red-test", e.getFullName());
    }

    @Test
    public void EntityTest3() {
        System.out.println(" *** Entity Test 3 *** ");
        Symbol s = genSymbol3();
        Entity e = new Entity(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- entity: " + e.toString());
        System.out.println(" -- entity full name: " + e.getFullName());
        Assert.assertEquals("x", e.getId());
        Assert.assertEquals("car", e.getMainConcept());
        Assert.assertEquals("Black-Red-car", e.getFullName());
    }

    @Test
    public void EntityTest4() {
        System.out.println(" *** Entity Test 4 *** ");
        Symbol s = genSymbol26();
        Entity e = new Entity(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- entity: " + e.toString());
        System.out.println(" -- entity full name: " + e.getFullName());
        String expectedEntity = "Entity{id='x2', mainConcept='gate', featureList=[Arg{id='enter', name='***No Name***', isEmpty=false, hasName=false}]}";
        Assert.assertEquals(expectedEntity, e.toString());
        Assert.assertEquals("x2", e.getId());
        Assert.assertEquals("gate", e.getMainConcept());
        Assert.assertEquals("enter-gate", e.getFullName());
    }

    @Test
    public void EntityTest5() {
        System.out.println(" *** Entity Test 5 *** ");
        Symbol s = genSymbol27();
        Entity e = new Entity(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- entity: " + e.toString());
        System.out.println(" -- entity name: " + e.getName());
        Assert.assertEquals("vv8-coat", e.getName());
        Table t = genTable5();
        System.out.print(" -- table: " + t.toString());
        e.assignNamesToArgument(t);
        System.out.println(" -- entity name after assignment: " + e.getName());
        Assert.assertEquals("winter-coat", e.getName());
    }

    @Test
    public void EntityTest6() {
        System.out.println(" *** Entity Test 6 *** ");
        Symbol s = genSymbol29();
        Entity e = new Entity(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- entity: " + e.toString());
        System.out.println(" -- entity full name: " + e.getFullName());
        Assert.assertEquals("vv5-crisis", e.getFullName());
        Table t = genTable6();
        System.out.print(" -- table: " + t.toString());
        e.assignNamesToArgument(t);
        System.out.println(" -- entity name after assignment: " + e.getName());
        Assert.assertEquals("storm-01-crisis", e.getName());
    }


    //==================================================================
    // Modality Tests
    //==================================================================

    @Test
    public void ModalityTest1() {
        System.out.println(" *** Modality Test 1 *** ");
        Symbol s = genSymbol4();
        Modality m = new Modality(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- modality: " + m.toString());
        System.out.println(" -- modality name: " + m.getName());
        String expectedEntity = "Modality{id='m', mainConcept='possible-01', negation=false}";
        Assert.assertEquals(expectedEntity, m.toString());
        Assert.assertEquals("m", m.getId());
        Assert.assertEquals("possible-01", m.getMainConcept());
        Assert.assertFalse(m.isNegation());
        Assert.assertEquals("possible", m.getName());
    }

    @Test
    public void ModalityTest2() {
        System.out.println(" *** Modality Test 2 *** ");
        Symbol s = genSymbol5();
        Modality m = new Modality(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- modality: " + m.toString());
        System.out.println(" -- modality name: " + m.getName());
        String expectedEntity = "Modality{id='m', mainConcept='obligatory', negation=false}";
        Assert.assertEquals(expectedEntity, m.toString());
        Assert.assertEquals("m", m.getId());
        Assert.assertEquals("obligatory", m.getMainConcept());
        Assert.assertFalse(m.isNegation());
        Assert.assertEquals("necessary", m.getName());
    }


    //==================================================================
    // Property Tests
    //==================================================================

    @Test
    public void PropertyTest1() {
        System.out.println(" *** Property Test 1 *** ");
        Symbol s = genSymbol1();
        Property p = new Property(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- property: " + p.toString());
        System.out.println(" -- property name: " + p.getName());
        String expectedEntity = "Property{id='x', mainConcept='sky', " +
                "preArg=[Arg{id='x0', name='***No Name***', isEmpty=false, hasName=false}, Arg{id='y2', name='***No Name***', isEmpty=false, hasName=false}], " +
                "postArg=[Arg{id='x2', name='***No Name***', isEmpty=false, hasName=false}, Arg{id='x3', name='***No Name***', isEmpty=false, hasName=false}], " +
                "negation=false" +
                "}";
        Assert.assertEquals(expectedEntity, p.toString());
        Assert.assertEquals("x", p.getId());
        Assert.assertEquals("sky", p.getMainConcept());
        Assert.assertFalse(p.isNegation());
        Assert.assertEquals("y2_x0_sky_x2_x3", p.getName());
    }

    @Test
    public void PropertyTest2() {
        System.out.println(" *** Property Test 2 *** ");
        Symbol s = genSymbol15();
        Property p = new Property(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- property name before assignment: " + p.getName());
        Assert.assertEquals("x4", p.getId());
        Assert.assertTrue(p.isNegation());
        Assert.assertEquals("not x1_pass-01", p.getName());
        Table t = genTable3();
        System.out.print(" -- table: " + t.toString());
        p.assignNamesToArgument(t);
        System.out.println(" -- property name after assignment: " + p.getName());
        Assert.assertEquals("x4", p.getId());
        Assert.assertEquals("not vehicles_pass-01", p.getName());
    }

    @Test
    public void PropertyTest3() {
        System.out.println(" *** Property Test 3 *** ");
        Symbol s = genSymbol8();
        Property p = new Property(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- property name before assignment: " + p.getName());
        Assert.assertEquals("p", p.getId());
        Assert.assertEquals("x0_focus_x1", p.getName());
        Table t = genTable1();
        System.out.print(" -- table: " + t.toString());
        p.assignNamesToArgument(t);
        System.out.println(" -- property name after assignment: " + p.getName());
        Assert.assertEquals("p", p.getId());
        Assert.assertEquals("reflector_focus_sunlight", p.getName());
    }

    @Test
    public void PropertyTest4() {
        System.out.println(" *** Property Test 4 *** ");
        Symbol s = genSymbol11();
        Property p = new Property(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- property name before assignment: " + p.getName());
        Assert.assertEquals("vv5", p.getId());
        Assert.assertEquals("pass_vv6_vv7", p.getName());
        Table t = genTable2();
        System.out.print(" -- table: " + t.toString());
        p.assignNamesToArgument(t);
        System.out.println(" -- property name after assignment: " + p.getName());
        Assert.assertEquals("vv5", p.getId());
        Assert.assertEquals("pass_vehicle_dock-01", p.getName());
        Set<Property> pSet = new HashSet<>();
        Property argP = new Property(genSymbol13());
        argP.assignNamesToArgument(t);
        pSet.add(argP);
        p.updatePropertyArgNames(pSet);
        System.out.println(" -- property name after update: " + p.getName());
        Assert.assertEquals("vv5", p.getId());
        Assert.assertEquals("pass_vehicle_dock-01_vehicle", p.getName());
    }

    @Test
    public void PropertyTest5() {
        System.out.println(" *** Property Test 5 *** ");
        Symbol s = genSymbol21();
        Property p = new Property(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- property name before assignment: " + p.getName());
        Assert.assertEquals("vv2", p.getId());
        Assert.assertEquals("thaw_vv3", p.getName());
        Table t = genTable4();
        System.out.print(" -- table: " + t.toString());
        p.assignNamesToArgument(t);
        System.out.println(" -- property name after assignment: " + p.getName());
        Assert.assertEquals("vv2", p.getId());
        Assert.assertEquals("thaw_snow", p.getName());
    }

    @Test
    public void PropertyTest6() {
        System.out.println(" *** Property Test 6 *** ");
        Symbol s = genSymbol25();
        Property p = new Property(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- property name before assignment: " + p.getName());
        Assert.assertEquals("vv2", p.getId());
        Assert.assertEquals("vv5_thaw_vv3", p.getName());
        Table t = genTable4();
        System.out.print(" -- table: " + t.toString());
        p.assignNamesToArgument(t);
        System.out.println(" -- property name after assignment: " + p.getName());
        Assert.assertEquals("vv2", p.getId());
        Assert.assertEquals("sun_thaw_snow", p.getName());
    }


    //==================================================================
    // Temporality Tests
    //==================================================================

    @Test
    public void TemporalityTest1() {
        System.out.println(" *** Temporality Test 1 *** ");
        Symbol s = genSymbol4();
        Temporality t = new Temporality(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- temporality: " + t.toString());
        System.out.println(" -- temporality name: " + t.getName());
        String expectedEntity = "Temporality{id='m', mainConcept='possible-01'}";
        Assert.assertEquals(expectedEntity, t.toString());
        Assert.assertEquals("m", t.getId());
        Assert.assertEquals("possible-01", t.getMainConcept());
        Assert.assertEquals("-", t.getName());
    }

    @Test
    public void TemporalityTest2() {
        System.out.println(" *** Temporality Test 2 *** ");
        Symbol s = genSymbol6();
        Temporality t = new Temporality(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- temporality: " + t.toString());
        System.out.println(" -- temporality name: " + t.getName());
        String expectedEntity = "Temporality{id='t', mainConcept='next'}";
        Assert.assertEquals(expectedEntity, t.toString());
        Assert.assertEquals("t", t.getId());
        Assert.assertEquals("next", t.getMainConcept());
        Assert.assertEquals("after", t.getName());
    }

    @Test
    public void TemporalityTest3() {
        System.out.println(" *** Temporality Test 3 *** ");
        Symbol s = genSymbol7();
        Temporality t = new Temporality(s);
        System.out.println(" -- symbol: " + s.toString());
        System.out.println(" -- temporality: " + t.toString());
        System.out.println(" -- temporality name: " + t.getName());
        String expectedEntity = "Temporality{id='t', mainConcept='before-01'}";
        Assert.assertEquals(expectedEntity, t.toString());
        Assert.assertEquals("t", t.getId());
        Assert.assertEquals("before-01", t.getMainConcept());
        Assert.assertEquals("before", t.getName());
    }


}
