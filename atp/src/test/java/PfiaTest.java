import AbstractSemantic.TransPattern;
import AbstractSemantic.TransPatternSet;
import ComputationObject.OutputStatement;
import ComputationObject.Statistics;
import ComputationObject.Utility;
import ComputationObject.WorkData;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Set;

public class PfiaTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    static private String TEST_RESOURCES_PATH = "src/test/resources/";
    static private String MPC_COLING_PATH = TEST_RESOURCES_PATH + "experiment/pfia-mpc/";
    static private String TRAINING_DATA_WEATHER = "mpc_training_data_weather.amrep";
    static private String EVAL_DATA_SYSTEM_01 = "mpc_eval_data_system_01.amrep";
    static private String EVAL_DATA_SYSTEM_01_A = "mpc_eval_data_system_01a.amrep";
    static private String EVAL_DATA_SYSTEM_01_B = "mpc_eval_data_system_01b.amrep";
    static private String EVAL_DATA_SYSTEM_01_C = "mpc_eval_data_system_01c.amrep";
    static private String EVAL_DATA_SYSTEM_01_D = "mpc_eval_data_system_01d.amrep";
    static private String EVAL_DATA_SYSTEM_01_E = "mpc_eval_data_system_01e.amrep";
    static private String DEV_DATA_1 = "mpc_dev_data.amrep";
    static private String DEV_DATA_2 = "mpc_dev_data_2.amrep";


    // ---------- Test Data Constructor(s)

    public void runComputationLearning(String trainingFile, String evalFile) {
        String[] args = new String[2];
        args[0] = MPC_COLING_PATH + trainingFile;
        args[1] = MPC_COLING_PATH + evalFile;
        Computation.main(args);
        Assert.assertTrue(Computation.isPatternLearnStepValid());
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
    }

    public void printPatternSet(TransPatternSet pSet, String comment) {
        System.out.println();
        System.out.println("-- Patterns (" + comment + "): " +
                "size: " + pSet.size());
    }


    public void printProducedStatements() {

        List<WorkData> evalDataList = Computation.getEvalDataList();
        List<OutputStatement> statementList = Utility.getOutputStatementList(evalDataList);

        System.out.println();
        System.out.println(" -- Produced Statements (" + statementList.size() + "): ");

        for(OutputStatement os: statementList) {

            String oss = String.format("%1$-100s", new String[]{os.getIdentifiedStatement()});

            String statString = "";
            int count = 0;
            for(Integer stat: os.getAnalyzedPatternsNumberList()) {
                statString += " | " + stat;
                count++;
            }
            for (int i = count; i < 5 ; i++) statString += " | " + "-";
            statString += " | ";
            statString = String.format("%1$-20s", new String[]{"" + statString});

            String osEvalString = "   ----->   ";
            if(os.isCorrect()) osEvalString += String.format("%1$-8s", new String[]{"True"});
            else osEvalString += String.format("%1$-8s", new String[]{"False"});
            String detailedEvalString = "(" + os.isConsideredPositive() +
                    ", " + os.isAssessed() +
                    ", " + os.isTruePositive() +
                    ", " + os.isTrueNegative() +
                    ")";
            osEvalString += String.format("%1$-30s", new String[]{detailedEvalString});

            String producedPatternString = "[Produced Pattern: ";
            producedPatternString += " depth = " + os.getProducerPattern().getDepth();
            producedPatternString += " ; ppi1 = " + os.getProducerPattern().getPrecisionFirstLine();
            producedPatternString += " ; ppi2 = " + os.getProducerPattern().getPrecisionSecondLine();
            producedPatternString += "]";
            producedPatternString = String.format("%1$-100s", new String[]{producedPatternString});

            System.out.print(oss + statString + osEvalString + producedPatternString);
            System.out.println();

        }
    }


    public void printStatistics() {
        Statistics statistics = Computation.getDataStatistics();
        System.out.println("");
        System.out.println("======================================================================");
        System.out.println("== ");
        System.out.println("== Evaluation (data: " + statistics.getDataId() + ")");
        System.out.println("==   Precision: " + statistics.getPrecisionStr());
        System.out.println("==   Recall: " + statistics.getRecallStr());
        System.out.println("==   F-Score (" + Statistics.BETA_WEIGHT + "): " + statistics.getFScoreStr());
        System.out.println("== ");
        System.out.println("== --------------------------------------");
        System.out.println("== ");
        System.out.println("== Time Processing Statistics ");
        System.out.print("==   Learning: " + Statistics.getTimeString(statistics.getLearningGlobalTime()));
        System.out.println("   (  " + Statistics.getTimeString(statistics.getLearningSourceParsingTime()) +
                "  |  " + Statistics.getTimeString(statistics.getLearningSymbolTableConstructionTime()) +
                "  |  " + Statistics.getTimeString(statistics.getLearningPatternLearningTime()) +
                "  )");
        System.out.print("==   Production: " + Statistics.getTimeString(statistics.getProductionGlobalTime()));
        System.out.println("   ( " + Statistics.getTimeString(statistics.getProductionSourceParsingTime()) +
                "  |  " + Statistics.getTimeString(statistics.getProductionSymbolTableConstructionTime()) +
                "  |  " + Statistics.getTimeString(statistics.getProductionSemanticAnalysisTime()) +
                "  )");
        System.out.println("==   Evaluation: " + Statistics.getTimeString(statistics.getEvaluationGlobalTime()));
        System.out.println("== ");
        System.out.println("======================================================================");
    }

    public void printPrecisions() {
        List<WorkData> evalDataList = Computation.getEvalDataList();
        Set<OutputStatement> osSet = Utility.getOutputStatementSet(evalDataList);
        System.out.println("");
        System.out.println("Produced Statements with pattern (" + osSet.size() + "): ");
        for(OutputStatement os: osSet) {
            String statement = os.getStatement();
            TransPattern pattern = os.getProducerPattern();
            System.out.println(statement);
            System.out.println(" -- depth: " + pattern.getDepth());
            System.out.println(" -- query: " + pattern.getQuery().toString());
            System.out.println(" -- template: " + pattern.getTemplate().getRawTemplateString());
            System.out.println(" -- precision: " + pattern.getPrecisionFirstLine() +
                    " / " + pattern.getPrecisionSecondLine());
        }
        TransPatternSet pSet = Computation.getTransPatternSet();
        System.out.println("");
        System.out.println("Precision / pattern: ");
        int i = 0;
        //int j = 0;
        for(TransPattern p: pSet.getSet()) {
            i++;
            //if (i > 1) System.out.print("  ");
            String rawQuery = p.getQuery().toString();
            String rawTemplate = p.getTemplate().getRawTemplateString();
            System.out.println("P-" + i + "(" + p.getDepth() + ")" + ": " + rawQuery + ", " + rawTemplate);
            System.out.println("            -- First Line Precision ="+ p.getPrecisionFirstLine() +
                    "            -- Second Line Precision ="+ p.getPrecisionSecondLine() +
                    "");
/*            if (i == j + 5) {
                j = i;
                System.out.println("");
            }*/
        }
    }

    //==================================================================
    // Global Learning Process Tests
    //==================================================================

    @Test
    public void Experiment1() {
        TransPatternSet pSet1 = Computation.getTransPatternSet();
        runComputationLearning(TRAINING_DATA_WEATHER, EVAL_DATA_SYSTEM_01);
        TransPatternSet pSet2 = Computation.getTransPatternSet();
        printStatistics();
        printProducedStatements();
        printPrecisions();
        printPatternSet(pSet1, "before learning");
        printPatternSet(pSet2, "after learning");
    }

    @Test
    public void Experiment2() {
        TransPatternSet pSet1 = Computation.getTransPatternSet();
        runComputationLearning(TRAINING_DATA_WEATHER, TRAINING_DATA_WEATHER);
        TransPatternSet pSet2 = Computation.getTransPatternSet();
        printStatistics();
        printProducedStatements();
        printPrecisions();
        printPatternSet(pSet1, "before learning");
        printPatternSet(pSet2, "after learning");
    }

    @Test
    public void Experiment3() {
        TransPatternSet pSet1 = Computation.getTransPatternSet();
        runComputationLearning(TRAINING_DATA_WEATHER, EVAL_DATA_SYSTEM_01_A);
        TransPatternSet pSet2 = Computation.getTransPatternSet();
        printStatistics();
        printProducedStatements();
        //printPrecisions();
        printPatternSet(pSet1, "before learning");
        printPatternSet(pSet2, "after learning");
        runComputationLearning(TRAINING_DATA_WEATHER, EVAL_DATA_SYSTEM_01_B);
        TransPatternSet pSet3 = Computation.getTransPatternSet();
        printStatistics();
        printProducedStatements();
        //printPrecisions();
        printPatternSet(pSet2, "before learning");
        printPatternSet(pSet3, "after learning");
    }

    @Test
    public void Experiment4() {
        TransPatternSet pSet1 = Computation.getTransPatternSet();
        runComputationLearning(TRAINING_DATA_WEATHER, DEV_DATA_1);
        //runComputationLearning(TRAINING_DATA_WEATHER, DEV_DATA_2);
        TransPatternSet pSet2 = Computation.getTransPatternSet();
        printStatistics();
        printProducedStatements();
        printPrecisions();
        printPatternSet(pSet1, "before learning");
        printPatternSet(pSet2, "after learning");
    }

    @Test
    public void Experiment5() {
        TransPatternSet pSet1 = Computation.getTransPatternSet();
        runComputationLearning(DEV_DATA_1, DEV_DATA_1);
        TransPatternSet pSet2 = Computation.getTransPatternSet();
        printStatistics();
        printProducedStatements();
        printPrecisions();
        printPatternSet(pSet1, "before learning");
        printPatternSet(pSet2, "after learning");
    }
    
}
