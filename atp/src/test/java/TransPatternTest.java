import AbstractSemantic.Instance;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPattern;
import AbstractSemantic.TransPatternObject.QueryMap;
import AbstractSemantic.TransPatternObject.TemplateMap;
import LogicalQuery.Atom;
import LogicalQuery.Query;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class TransPatternTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Test Data Constructor(s)

    public Query genQuery1() {
        List<String> variables = new ArrayList<>();
        variables.add("x4");
        variables.add("x6");
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("ARG1", "x4", "x2"));
        atoms.add(new Atom("MODALITY", "x6"));
        return new Query(variables, atoms);
    }

    public Query genQuery2() {
        List<String> variables = new ArrayList<>();
        variables.add("x1");
        variables.add("x2");
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("ARG1", "x1", "x2"));
        atoms.add(new Atom("MODALITY", "x1"));
        return new Query(variables, atoms);
    }

    public Query genQuery3() {
        List<String> variables = new ArrayList<>(Arrays.asList("a", "b", "c"));
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("isModality", "a"));
        atoms.add(new Atom("isProperty", "b"));
        atoms.add(new Atom("isProperty", "c"));
        atoms.add(new Atom("arg+", "a", "b"));
        atoms.add(new Atom("condition", "a", "c"));
        return new Query(variables, atoms);
    }

    public Template genTemplate1() {
        String rawStr = "(#a.modality, #b.property, #c.property)";
        return new Template("testTemplate1", rawStr);
    }


    //==================================================================
    // TemplateMap Class Tests
    //==================================================================

    @Test
    public void TemplateMapTest1() {
        TemplateMap templateMap = new TemplateMap();
        System.out.println(templateMap);
        Template template = templateMap.get("T01");
        System.out.println(template);
        Assert.assertEquals(7, template.getTokenList().size());
    }


    //==================================================================
    // Construction Tests
    //==================================================================

    @Test
    public void ConstructorTest1() {
        QueryMap queryMap = new QueryMap();
        TemplateMap templateMap = new TemplateMap();
        Query query = queryMap.get("Q01");
        System.out.println(query);
        Template template = templateMap.get("T01");
        System.out.println(template);
        TransPattern pattern = new TransPattern(query, template);
        System.out.println(pattern);
    }

    @Test
    public void ConstructorTest2() {

        // Test Data
        List<String> boundVariables = new ArrayList<>();
        List<Atom> atoms = new ArrayList<>();
        Query query;
        String formatting, statement;
        TransPattern pattern;

        // Complement data: Instances
        Instance i1 = new Instance("x4", "possible");
        i1.addFeature("modality", "POSSIBLE");
        i1.addFeature("property", "possible-01_x1");
        Instance i2 = new Instance("x6", "concept");
        i2.addFeature("property", "concept_i1");
        Instance i3 = new Instance("x2", "test");
        i3.addFeature("entity", "test-01");
        i3.addFeature("property", "test-01_x1");

        // Test Code
        boundVariables.add("x4");
        boundVariables.add("x6");
        boundVariables.add("x2");
        atoms.add(new Atom("MODALITY", "x4"));
        atoms.add(new Atom("ARG1", "x6", "x2"));
        query = new Query(boundVariables, atoms);
        formatting = "(#x4.modality, #x6.property, #x2.property)";
        Template template = new Template("test", formatting);
        pattern = new TransPattern(query, template);
        System.out.println(pattern.toString());
        Map<String, Instance> varMap = new HashMap<>();
        varMap.put("x4", i1);
        varMap.put("x6", i2);
        varMap.put("x2", i3);
        statement = pattern.getStatement(varMap);
        System.out.println("statement: " + statement);

        // Asserts
        Assert.assertEquals(
                "(POSSIBLE, concept_i1, test_x1)",
                statement);

    }

    @Test
    public void ConstructorTest3() {

        System.out.println();
        System.out.println(" *** Constructor Test 3 *** ");

        Query query = genQuery3();
        System.out.println(" -- Query: " + query.toString());
        Template template = genTemplate1();
        System.out.println(" -- Template: " + template.toString());
        TransPattern pattern = new TransPattern(query, template);
        System.out.println(" -- Transduction Pattern: ");
        System.out.println(" ----- (" + pattern.getDepth() + "): " + pattern.getQuery().toString() +
                "  |  " + pattern.getTemplate().getRawTemplateString());
        System.out.println(" ----- " + pattern.toString());

    }


    //==================================================================
    // Accessor Tests
    //==================================================================

    // TODO


    //==================================================================
    // Normalizing Method Tests
    //==================================================================

    // TODO: NormalizeTest1 & NormalizeTest2 (to complete)

    @Test
    public void NormalizeTest3() {

        System.out.println();
        System.out.println(" *** Normalize Test 3 *** ");

        Query query = genQuery3();
        System.out.println(" -- Query: " + query.toString());
        Template template = genTemplate1();
        System.out.println(" -- Template: " + template.toString());
        TransPattern pattern = new TransPattern(query, template);
        System.out.println(" -- Transduction Pattern (before normalize): ");
        System.out.println(" ----- (" + pattern.getDepth() + "): " + pattern.getQuery().toString() +
                "  |  " + pattern.getTemplate().getRawTemplateString());
        System.out.println(" ----- " + pattern.toString());

        pattern.normalize();
        System.out.println(" -- Transduction Pattern (before normalize): ");
        System.out.println(" ----- (" + pattern.getDepth() + "): " + pattern.getQuery().toString() +
                "  |  " + pattern.getTemplate().getRawTemplateString());
        System.out.println(" ----- " + pattern.toString());

    }


    //==================================================================
    // Main Methods Tests
    //==================================================================

    // TODO

}
