import AbstractSemantic.*;
import LogicalQuery.Atom;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class OldAnalyzerTest {

    //==================================================================
    // Global variable(s)
    //==================================================================

    private static final String TEST_RESOURCES_PATH = "src/test/resources/";

    //==================================================================
    // Tests: Analyzer
    //==================================================================

    @Test
    public void PrepareTest1() {
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();
        HashSet<TransPattern> patternSet = transPatternSet.getSet();
        System.out.println(patternSet.toString());
        Assert.assertFalse(patternSet.isEmpty());
    }

    @Test
    public void ComputeInterpretationTest1() {

        // Test Data
        OldAnalyzer analyzer;
        Interpretation interpretation;
        HashSet<Instance> instanceSet;

        // Pattern Set Loading
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();

        // Analyzer creation for definition and output statement computing
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("x9","gate");
        conceptMap.put("x10","open-01");
        conceptMap.put("x12","obligatory");
        conceptMap.put("x3","ticket");
        conceptMap.put("x6","issue-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","necessary");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("x9","gate");
        entityMap.put("x3","ticket");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("x10","open-01_gate");
        propertyMap.put("x6","issue-01_ticket");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "x9"));
        atomSet.add(new Atom("isEntity", "x3"));
        atomSet.add(new Atom("isProperty", "x10"));
        atomSet.add(new Atom("isProperty", "x3"));
        atomSet.add(new Atom("isModality", "x12"));
        atomSet.add(new Atom("arg1", "x10", "x9"));
        atomSet.add(new Atom("arg1", "x6", "x3"));
        atomSet.add(new Atom("time", "x12", "x6"));
        atomSet.add(new Atom("domain", "x12", "x10"));
        analyzer = new OldAnalyzer("T1", dataMap, atomSet, transPatternSet);

        // Definition computing
        interpretation = analyzer.computeInterpretation();
        System.out.println(interpretation);
        instanceSet = interpretation.getInstanceSet();
        System.out.println(instanceSet);

        // Asserts
        Assert.assertTrue(interpretation.getConceptSet().contains("open-01"));
        Assert.assertTrue(interpretation.getConceptSet().contains("issue-01"));
        Assert.assertTrue(interpretation.getConceptSet().contains("obligatory"));
        Assert.assertTrue(interpretation.getConceptSet().contains("ticket"));
        Assert.assertTrue(interpretation.getConceptSet().contains("gate"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x9"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x10"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x12"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x3"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x6"));

    }

    @Test
    public void generateFormattedStatement1() {

        // Test Data
        OldAnalyzer analyzer;
        Interpretation interpretation;
        String formattedStatement;

        // Pattern Set Loading
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();

        // Analyzer creation for definition and output statement computing
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("x9","gate");
        conceptMap.put("x10","open-01");
        conceptMap.put("x12","obligatory");
        conceptMap.put("x3","ticket");
        conceptMap.put("x6","issue-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","necessary");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("x9","gate");
        entityMap.put("x3","ticket");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("x10","open-01_gate");
        propertyMap.put("x6","issue-01_ticket");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "x9"));
        atomSet.add(new Atom("isEntity", "x3"));
        atomSet.add(new Atom("isProperty", "x10"));
        atomSet.add(new Atom("isProperty", "x6"));
        atomSet.add(new Atom("isModality", "x12"));
        atomSet.add(new Atom("arg+", "x10", "x9"));
        atomSet.add(new Atom("arg+", "x6", "x3"));
        atomSet.add(new Atom("time", "x12", "x6"));
        atomSet.add(new Atom("domain", "x12", "x10"));
        analyzer = new OldAnalyzer("T1", dataMap, atomSet, transPatternSet);

        // Definition computing
        interpretation = analyzer.computeInterpretation();
        System.out.println(interpretation);

        // Generate formatted statement
        formattedStatement = analyzer.provideBestStatement(interpretation);
        System.out.println(formattedStatement);

        // Asserts
        Assert.assertEquals(
                "<necessary, not open_gate, not issue_ticket>",
                formattedStatement);

    }

    @Test
    public void generateFormattedStatement2() {

        // Test Data
        OldAnalyzer analyzer;
        Interpretation interpretation;
        String formattedStatement;

        // Pattern Set Loading
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();

        // Analyzer creation for definition and output statement computing
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("x9","gate");
        conceptMap.put("x10","open-01");
        conceptMap.put("x12","obligatory");
        conceptMap.put("x3","ticket");
        conceptMap.put("x6","issue-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","necessary");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("x9","gate");
        entityMap.put("x3","ticket");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("x10","open-01_gate");
        propertyMap.put("x6","issue-01_ticket");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "x9"));
        atomSet.add(new Atom("isEntity", "x3"));
        atomSet.add(new Atom("isProperty", "x10"));
        atomSet.add(new Atom("isProperty", "x6"));
        atomSet.add(new Atom("isModality", "x12"));
        atomSet.add(new Atom("arg+", "x10", "x9"));
        atomSet.add(new Atom("arg+", "x6", "x3"));
        atomSet.add(new Atom("time", "x12", "x6"));
        atomSet.add(new Atom("domain", "x12", "x10"));
        analyzer = new OldAnalyzer("T1", dataMap, atomSet, transPatternSet);

        // Test
        interpretation = analyzer.computeInterpretation();
        formattedStatement = analyzer.provideBestStatement(interpretation);
        System.out.println(formattedStatement);

        // Asserts
        Assert.assertEquals(
                "<necessary, not open_gate, not issue_ticket>",
                formattedStatement);

    }


}