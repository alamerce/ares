import AbstractSemantic.OldAnalyzer;
import AbstractSemantic.Interpretation;
import AbstractSemantic.TransPatternSet;
import LogicalQuery.Atom;
import SourceAnalyzer.CapturedData;
import SourceAnalyzer.SourceType;
import SourceAnalyzer.SymbolTable.Table;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class SourceAnalyzerTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    static private String TEST_RESOURCES_PATH = "src/test/resources/";
    static private String TEST_FILE_01 = "amr_req_parking_02.txt";
    static private String TEST_FILE_02 = "amr_req_parking_09.txt";
    static private String TEST_FILE_03 = "mpc_req_training_1_05.txt";
    static private String TEST_FILE_04 = "mpc_req_training_5_02.txt";


    // ---------- Test Data Constructor(s)

    public void runComputationProducer(String evalFile) {
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + evalFile;
        Computation.main(args);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
    }


    //==================================================================
    // Tests: AMR references
    //==================================================================

    // TODO


    //==================================================================
    // Tests: CapturedData
    //==================================================================

    @Test
    public void CapturedDataTest1() {

        runComputationProducer(TEST_FILE_01);
        List<Table> tList = Computation.getSymbolTableList();

        // Pattern Set Loading
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();

        // Browse tables for printing
        CapturedData capturedData = new CapturedData(SourceType.AMR);
        for (Table table : tList) {

            // Capture data from symbol table
            System.out.println(table.toString());
            capturedData.analyze(table);

            // Analyzer creation for definition and output statement computing
            Map<String, Map<String, String>> dataMap = capturedData.getDataMap();
            System.out.println(dataMap.toString());
            Set<Atom> atomSet = capturedData.getAtomSet();
            OldAnalyzer analyzer = new OldAnalyzer("T1", dataMap, atomSet, transPatternSet);

            // Definition computing
            Interpretation interpretation = analyzer.computeInterpretation();
            System.out.println(interpretation);

            // Generate formatted statement
            String formattedStatement = analyzer.provideBestStatement(interpretation);
            System.out.println(formattedStatement);

            // Asserts
            Assert.assertEquals(
                    "<necessary, not open_gate, not issue_ticket>",
                    formattedStatement);

        }

    }

    @Test
    public void CapturedDataTest2() {

        runComputationProducer(TEST_FILE_02);
        List<Table> tList = Computation.getSymbolTableList();

        // Pattern Set Loading
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();

        // Browse tables for printing
        CapturedData capturedData = new CapturedData(SourceType.AMR);
        for (Table table : tList) {

            // Capture data from symbol table
            System.out.println(table.toString());
            capturedData.analyze(table);

            // Analyzer creation for definition and output statement computing
            Map<String, Map<String, String>> dataMap = capturedData.getDataMap();
            System.out.println(dataMap.toString());
            Set<Atom> atomSet = capturedData.getAtomSet();
            OldAnalyzer analyzer = new OldAnalyzer("T1", dataMap, atomSet, transPatternSet);

            // Definition computing
            Interpretation interpretation = analyzer.computeInterpretation();
            System.out.println(interpretation);

            // Generate formatted statement
            String formattedStatement = analyzer.provideBestStatement(interpretation);
            System.out.println(formattedStatement);

            // Asserts
            Assert.assertEquals(
                    "<necessary, come_good_weather, after bad_weather>",
                    formattedStatement);

        }

    }

    @Test
    public void CapturedDataTest3() {

        runComputationProducer(TEST_FILE_03);
        List<Table> tList = Computation.getSymbolTableList();

        // Pattern Set Loading
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();

        // Browse tables for printing
        CapturedData capturedData = new CapturedData(SourceType.AMR);
        for (Table table : tList) {

            // Capture data from symbol table
            System.out.println(table.toString());
            capturedData.analyze(table);

            // Analyzer creation for definition and output statement computing
            Map<String, Map<String, String>> dataMap = capturedData.getDataMap();
            System.out.println(dataMap.toString());
            Set<Atom> atomSet = capturedData.getAtomSet();
            OldAnalyzer analyzer = new OldAnalyzer("T1", dataMap, atomSet, transPatternSet);

            // Definition computing
            Interpretation interpretation = analyzer.computeInterpretation();
            System.out.println(interpretation);

            // Generate formatted statement
            String formattedStatement = analyzer.provideBestStatement(interpretation);
            System.out.println(formattedStatement);

            // Asserts
            Assert.assertEquals(
                    "<necessary, close_exit-gate, after pass_vehicle_dock_vehicle>",
                    formattedStatement);

        }

    }

    @Test
    public void CapturedDataTest4() {

        runComputationProducer(TEST_FILE_04);
        List<Table> tList = Computation.getSymbolTableList();

        // Pattern Set Loading
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();

        // Browse tables for printing
        CapturedData capturedData = new CapturedData(SourceType.AMR);
        for (Table table : tList) {

            // Capture data from symbol table
            System.out.println(table.toString());
            capturedData.analyze(table);

            // Analyzer creation for definition and output statement computing
            Map<String, Map<String, String>> dataMap = capturedData.getDataMap();
            System.out.println(dataMap.toString());
            Set<Atom> atomSet = capturedData.getAtomSet();
            OldAnalyzer analyzer = new OldAnalyzer("T1", dataMap, atomSet, transPatternSet);

            // Definition computing
            Interpretation interpretation = analyzer.computeInterpretation();
            System.out.println(interpretation);

            // Generate formatted statement
            String formattedStatement = analyzer.provideBestStatement(interpretation);
            System.out.println(formattedStatement);

            // Asserts
            Assert.assertEquals(
                    "<necessary, fall_rain, after pass_cloud_mountain>",
                    formattedStatement);

        }

    }

}