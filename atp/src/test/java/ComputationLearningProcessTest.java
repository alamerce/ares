import AbstractSemantic.*;
import ComputationObject.Statistics;
import ComputationObject.Utility;
import ComputationObject.WorkData;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ComputationLearningProcessTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    static private String TEST_RESOURCES_PATH = "src/test/resources/";
    static private String TRAINING_DATA_01 = "mpc_training_data_01.amrep";
    static private String TRAINING_DATA_02 = "mpc_training_data_02.amrep";
    static private String EVAL_DATA_01 = "mpc_eval_data_01.amrep";
    static private String DEV_DATA_01 = "mpc_dev_data_01.amrep";


    // ---------- Test Data Constructor(s)

    public void runComputationLearning(String trainingFile, String evalFile) {
        String[] args = new String[2];
        args[0] = TEST_RESOURCES_PATH + trainingFile;
        args[1] = TEST_RESOURCES_PATH + evalFile;
        Computation.main(args);
        Assert.assertTrue(Computation.isPatternLearnStepValid());
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
    }

    public void printPatternSet() {
        TransPatternSet pSet = Computation.getTransPatternSet();
        System.out.println("-- Pattern Set (" + pSet.size() + ") " + pSet.toString());
    }

    public void printProducedStatements() {
        List<WorkData> evalDataList = Computation.getEvalDataList();
        TransPatternSet pSet = Computation.getTransPatternSet();
        List<String> statementList = Utility.getResultStatementList(evalDataList);
        System.out.println("");
        System.out.println("-- Produced Statements (" + pSet.size() + "): ");
        for(String s: statementList) System.out.println("     " + s);
    }

    public void printStatistics() {
        Statistics statistics = Computation.getDataStatistics();
        System.out.println("");
        System.out.println("======================================================================");
        System.out.println("== Statistics (data: " + statistics.getDataId() + ")");
        System.out.println("==   Precision: " + statistics.getPrecisionStr());
        System.out.println("==   Recall: " + statistics.getRecallStr());
        System.out.println("==   F-Score (" + Statistics.BETA_WEIGHT + "): " + statistics.getFScoreStr());
        System.out.println("======================================================================");
    }

    public void printPrecisions() {
        TransPatternSet pSet = Computation.getTransPatternSet();
        System.out.println("");
        System.out.println("Precision / pattern: ");
        int i = 0;
        //int j = 0;
        for(TransPattern p: pSet.getSet()) {
            i++;
            //if (i > 1) System.out.print("  ");
            String rawTemplate = p.getTemplate().getRawTemplateString();
            System.out.println("P-" + i + ":      " + rawTemplate +
                    "                           -- First Line Precision ="+ p.getPrecisionFirstLine() +
                    "                           -- Second Line Precision ="+ p.getPrecisionSecondLine() +
                    "");
/*            if (i == j + 5) {
                j = i;
                System.out.println("");
            }*/
        }
    }

    //==================================================================
    // Global Learning Process Tests
    //==================================================================

    @Test
    public void LearningProcessTest1() {
        System.out.println("Before learning... ");
        printPatternSet();
        runComputationLearning(EVAL_DATA_01, EVAL_DATA_01);
        System.out.println("After learning... ");
        printPatternSet();
        printProducedStatements();
        printStatistics();
        printPrecisions();
    }

    @Test
    public void LearningProcessTest2() {
        System.out.println("Before learning... ");
        printPatternSet();
        runComputationLearning(TRAINING_DATA_01, EVAL_DATA_01);
        System.out.println("After learning... ");
        printPatternSet();
        printProducedStatements();
        printStatistics();
        printPrecisions();
    }

    @Test
    public void LearningProcessTest3() {
        System.out.println("Before learning... ");
        printPatternSet();
        runComputationLearning(TRAINING_DATA_02, EVAL_DATA_01);
        System.out.println("After learning... ");
        printPatternSet();
        printProducedStatements();
        printStatistics();
        printPrecisions();
    }

    @Test
    public void LearningProcessTest4() {
        System.out.println("Before learning... ");
        printPatternSet();
        runComputationLearning(DEV_DATA_01, DEV_DATA_01);
        System.out.println("After learning... ");
        printPatternSet();
        printProducedStatements();
        printStatistics();
        printPrecisions();
    }


    
}
