import AbstractSemantic.Interpretation;
import AbstractSemantic.Instance;
import AbstractSemantic.LearnerObject.LearnAttribute;
import AbstractSemantic.LearnerObject.InterpretationAnalyzer;
import AbstractSemantic.TransPatternObject.Template;
import ComputationObject.WorkData;
import LogicalQuery.Query;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class InterpretationAnalyzerTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    static private String TEST_RESOURCES_PATH = "src/test/resources/";
    static private String CORPUS_FILE_ONE_CASE = "corpus_parking_one_case.amrep";
    static private String STATEMENT = "<necessary, not close_exit-gate, not after vehicle_down-02>";
    private enum VarMapContent { NULL, SIMPLE, PTP};


    // ---------- Test Data attribute(s)

    private Interpretation interpretation;


    // ---------- Test Data Constructor(s)

    private void runComputation(String corpus) {
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + corpus;
        Computation.main(args);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
    }

    private void generateDefinition(String corpus) {
        runComputation(corpus);
        List<WorkData> dataList = Computation.getEvalDataList();
        Assert.assertTrue(!dataList.isEmpty());
        this.interpretation = dataList.get(0).getInterpretation();
    }

    public Map<String, Instance> generateVariableMap(VarMapContent content) {
        Map<String, Instance> varMap = new HashMap<>();
        Instance i1 = new Instance("x", "test");
        i1.addFeature("feature", "feature-value-test");
        Instance i2 = new Instance("y", "test");
        Instance i3 = new Instance("vv1", "test");
        Instance p1 = new Instance("p1", "test");
        p1.addFeature("property", "prop1-value");
        Instance t1 = new Instance("t1", "test");
        t1.addFeature("temporality", "temp1-value");
        Instance p2 = new Instance("p2", "test");
        p2.addFeature("property", "prop2-value");
        switch (content) {
            case SIMPLE:
                varMap.put("x", i1);
                varMap.put("y", i2);
                varMap.put("z", i3);
                break;
            case PTP:
                varMap.put("a", p1);
                varMap.put("b", t1);
                varMap.put("c", p2);
                break;
            default: break;
        }
        return varMap;
    }

    //==================================================================
    // Constructor Tests
    //==================================================================

    @Test
    public void ConstructorTest() {
        generateDefinition(CORPUS_FILE_ONE_CASE);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        Set<LearnAttribute> learnAttributeSet = interpretationAnalyzer.getLearnAttributeSet();
        System.out.println(learnAttributeSet.size());
        System.out.println(learnAttributeSet.toString());
        Assert.assertTrue(learnAttributeSet.size() > 5);
    }


    //==================================================================
    // Useful Method(s)
    //==================================================================

    // TODO: tests for get variable, filter and complete atom set


    //==================================================================
    // Main Method Tests
    //==================================================================

    @Test
    public void FindConcordancesTest1() {
        generateDefinition(CORPUS_FILE_ONE_CASE);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        Set<LearnAttribute> concordanceSet = interpretationAnalyzer.findConcordances(STATEMENT);
        System.out.println("Concordances (" + concordanceSet.size() + "): " + concordanceSet.toString());
        Assert.assertTrue(concordanceSet.size() > 3);
    }

    @Test
    public void QueryInferenceTest1() {
        generateDefinition(CORPUS_FILE_ONE_CASE);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        String rawStr = "(necessary, #vv1.property, #vv6.temporality #vv2.property)";
        Template template = new Template("test", rawStr);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.PTP);
        template.setVariableMap(varMap);
        System.out.println(template);
        Query query = interpretationAnalyzer.inferQuery(template);
        System.out.println("Query: " + query.toString());
    }

    // TODO: tests for test pattern

}
