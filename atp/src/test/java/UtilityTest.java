import AbstractSemantic.Interpretation;
import AbstractSemantic.TransPatternObject.Token;
import LogicalQuery.Atom;
import Utility.IdCreator;
import Utility.Normalizer;
import Utility.Tokenizer;
import Utility.VeruaGen;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class UtilityTest {

    //==================================================================
    // Normalizer Class Tests
    //==================================================================

    @Test
    public void NormalizerTest1() {
        System.out.println(" *** Normalizer Test 1 *** ");
        String rawStr = "<test>";
        Normalizer normalizer = new Normalizer();
        String result = normalizer.normalize(rawStr);
        System.out.println("Normalize(" + rawStr + ") = " + result);
        Assert.assertEquals("<test>", result);
    }

    @Test
    public void NormalizerTest2() {
        System.out.println(" *** Normalizer Test 2 *** ");
        String rawStr = "open-01";
        Normalizer normalizer = new Normalizer();
        String result = normalizer.normalize(rawStr);
        System.out.println("Normalize(" + rawStr + ") = " + result);
        Assert.assertEquals("open", result);
    }

    @Test
    public void NormalizerTest3() {
        System.out.println(" *** Normalizer Test 3 *** ");
        String rawStr = "<necessary, open-01_gate, close-01_gate>";
        Normalizer normalizer = new Normalizer();
        String result = normalizer.normalize(rawStr);
        System.out.println("Normalize(" + rawStr + ") = " + result);
        Assert.assertEquals("<necessary, open_gate, close_gate>", result);
    }


    //==================================================================
    // Tokenizer Class Tests
    //==================================================================

    @Test
    public void TokenizerTest1() {
        System.out.println(" *** Tokenizer Test 1 ***");
        String rawStr = "context>>";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        System.out.println("Token List: " + tokenizer.getTokenList());
        System.out.println("Token List Size: " + tokenizer.getTokenListSize());
        Assert.assertEquals(3, tokenizer.getTokenListSize());
    }

    @Test
    public void TokenizerTest2() {
        System.out.println(" *** Tokenizer Test 2 ***");
        String rawStr = "<modality, property, context>";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        System.out.println("Token List: " + tokenizer.getTokenList());
        System.out.println("Token List Size: " + tokenizer.getTokenListSize());
        Assert.assertEquals(9, tokenizer.getTokenListSize());
    }

    @Test
    public void TokenizerTest3() {
        System.out.println(" *** Tokenizer Test 3 ***");
        String rawStr = "A,B,C,D,E";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        System.out.println("Token List: " + tokenizer.getTokenList());
        System.out.println("Token List Size: " + tokenizer.getTokenListSize());
        Assert.assertEquals(9, tokenizer.getTokenListSize());
    }

    @Test
    public void TokenNGramTest1() {
        System.out.println(" *** Token N-Gram Test 1 ***");
        String rawStr = "A,B,C,D,E";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        int n = 1;
        Set<List<Token>> nGramSet = tokenizer.getNGramTokenListSet(n);
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        System.out.println("N-Gram Set: " + nGramSet);
        System.out.println("N-Gram Set Size: " + nGramSet.size());
        Set<String> nGramStrSet = tokenizer.getNGramStringSet(n);
        System.out.println("N-Gram Set (string): " + nGramStrSet);
    }

    @Test
    public void TokenNGramTest2() {
        System.out.println(" *** Token N-Gram Test 2 ***");
        String rawStr = "A,B,C,D,E";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        int n = 2;
        Set<List<Token>> nGramSet = tokenizer.getNGramTokenListSet(n);
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        System.out.println("N-Gram Set: " + nGramSet);
        System.out.println("N-Gram Set Size: " + nGramSet.size());
        Set<String> nGramStrSet = tokenizer.getNGramStringSet(n);
        System.out.println("N-Gram Set (string): " + nGramStrSet);
    }

    @Test
    public void TokenNGramTest3() {
        System.out.println(" *** Token N-Gram Test 3 ***");
        String rawStr = "A,B,C,D,E";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        int n = 3;
        Set<List<Token>> nGramSet = tokenizer.getNGramTokenListSet(n);
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        System.out.println("N-Gram Set: " + nGramSet);
        System.out.println("N-Gram Set Size: " + nGramSet.size());
        Set<String> nGramStrSet = tokenizer.getNGramStringSet(n);
        System.out.println("N-Gram Set (string): " + nGramStrSet);
    }

    @Test
    public void TokenNGramTest4() {
        System.out.println(" *** Token N-Gram Test 4 ***");
        String rawStr = "A,B,C,D,E";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        int n = tokenizer.getTokenListSize();
        Set<List<Token>> nGramSet = tokenizer.getNGramTokenListSet(n);
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        System.out.println("N-Gram Set: " + nGramSet);
        System.out.println("N-Gram Set Size: " + nGramSet.size());
        Set<String> nGramStrSet = tokenizer.getNGramStringSet(n);
        System.out.println("N-Gram Set (string): " + nGramStrSet);
    }

    @Test
    public void TokenNGramMatchesTest1() {
        System.out.println(" *** Token N-Gram Matches Test 1 ***");
        String rawStr = "A,B,C,D,E";
        Tokenizer tokenizer = new Tokenizer(rawStr);
        int n = tokenizer.getTokenListSize();
        System.out.println("Base Statement: " + tokenizer.getBaseStatement());
        boolean nGramMatches = tokenizer.nGramMatches("A");
        System.out.println("Matches(A): " + nGramMatches);
        Assert.assertTrue(nGramMatches);
        nGramMatches = tokenizer.nGramMatches("A,C");
        System.out.println("Matches(A,C): " + nGramMatches);
        Assert.assertFalse(nGramMatches);
        nGramMatches = tokenizer.nGramMatches("A,B,C,D,E");
        System.out.println("Matches(A,B,C,D,E): " + nGramMatches);
        Assert.assertTrue(nGramMatches);
    }


    //==================================================================
    // IdCreator Class Tests
    //==================================================================

    @Test
    public void IdCreatorTest1() {
        System.out.println(" *** IdCreator Test 1 *** ");
        IdCreator idCreator = new IdCreator();
        System.out.println("<IC(1)> " + idCreator.toString());
        String id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        System.out.println("<IC(2)> " + idCreator.toString());
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        System.out.println("<IC(3)> " + idCreator.toString());
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        System.out.println("<IC(4)> " + idCreator.toString());
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        System.out.println("<IC(5)> " + idCreator.toString());
    }

    @Test
    public void IdCreatorTest2() {

        System.out.println(" *** IdCreator Test 2 *** ");

        // Interpretation
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("a1","gate");
        conceptMap.put("a2","open-01");
        conceptMap.put("x1","obligatory");
        conceptMap.put("e1","ticket");
        conceptMap.put("p1","issue-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","necessary");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("f1","gate");
        entityMap.put("b1","ticket");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("c1","open-01_gate");
        propertyMap.put("d1","issue-01_ticket");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "e1"));
        atomSet.add(new Atom("isEntity", "g1"));
        atomSet.add(new Atom("isProperty", "h1"));
        atomSet.add(new Atom("isProperty", "i1"));
        atomSet.add(new Atom("isModality", "l1"));
        atomSet.add(new Atom("arg1", "y1", "p1"));
        atomSet.add(new Atom("arg1", "v1", "t1"));
        atomSet.add(new Atom("time", "z1", "q1"));
        atomSet.add(new Atom("domain", "s1", "r1"));
        atomSet.add(new Atom("alphabet", "d1", "e2", "f1", "c1", "n1", "m1"));
        Interpretation interpretation = new Interpretation("TEST", dataMap, atomSet);

        // IdCreator
        IdCreator idCreator = new IdCreator(interpretation);
        System.out.println("<IC(1)> " + idCreator.toString());
        String id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        System.out.println("<IC(2)> " + idCreator.toString());
         id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);
        id = idCreator.getNewId();
        System.out.println("New Id: " + id);

    }

    // TODO: add test for IdCreator(idSet)


    //==================================================================
    // Verua Class Tests
    //==================================================================

    @Test
    public void VeruaGenTest1() {
        System.out.println(" *** Verua Generator Test 1 *** ");
        VeruaGen veruaGen = new VeruaGen();
        System.out.println("<start> " + veruaGen.toString());
        for (int i = 1; i < 10; i++) {
            String ioa = veruaGen.getNewIoa();
            System.out.println("New Ioa (concept): " + ioa);
            System.out.println("Ioa Set: " + veruaGen.getIoaSet().toString());
        }
        System.out.println("<end> " + veruaGen.toString());
    }

}
