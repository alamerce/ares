import AbstractSemantic.*;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternObject.Token;
import LogicalQuery.Atom;
import LogicalQuery.Query;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static Parameters.Configuration.*;

public class TemplateTest {

    //==================================================================
    // Attributes
    //==================================================================

    // ---------- Constant attribute(s)

    private enum VarMapContent { NULL, SIMPLE, PTP}


    //==================================================================
    // Useful method(s)
    //==================================================================

    public Map<String, Instance> generateVariableMap(VarMapContent content) {
        Map<String, Instance> varMap = new HashMap<>();
        Instance i1 = new Instance("x", "test");
        i1.addFeature("feature", "feature-value-test");
        Instance i2 = new Instance("y", "test");
        Instance i3 = new Instance("vv1", "test");
        Instance p1 = new Instance("p1", "test");
        p1.addFeature("property", "prop1-value");
        Instance t1 = new Instance("t1", "test");
        t1.addFeature("temporality", "temp1-value");
        Instance p2 = new Instance("p2", "test");
        p2.addFeature("property", "prop2-value");
        switch (content) {
            case SIMPLE:
                varMap.put("x", i1);
                varMap.put("y", i2);
                varMap.put("z", i3);
                break;
            case PTP:
                varMap.put("a", p1);
                varMap.put("b", t1);
                varMap.put("c", p2);
                break;
            default: break;
        }
        return varMap;
    }

    public void printTemplate(Template template) {
        System.out.println("-- Template");
        System.out.println("----- Template concept: " + template.getConcept());
        System.out.println("----- Raw Template String: " + template.getRawTemplateString());
        System.out.println("----- All Details: " + template);
    }


    //==================================================================
    // Token Class Tests
    //==================================================================

    @Test
    public void TokenTest1() {
        String rawStr = " ";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.SIMPLE);
        Assert.assertEquals(token.getTokenType(), TokenType.WHITESPACE);
        Assert.assertEquals(token.getRawValue(), rawStr);
        Assert.assertEquals(token.getStatement(), rawStr);
        Assert.assertEquals(token.getPointerStatement(), UNDEFINED_TOKEN_STRING);
        Assert.assertEquals(token.getFeatureStatement(varMap), UNDEFINED_TOKEN_STRING);
        Assert.assertEquals(token.getStatement(varMap), rawStr);
    }

    @Test
    public void TokenTest2() {
        String rawStr = "   ";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.SIMPLE);
        Assert.assertEquals(token.getTokenType(), TokenType.WHITESPACE);
        Assert.assertEquals(token.getRawValue(), rawStr);
        Assert.assertEquals(token.getStatement(), rawStr);
        Assert.assertEquals(token.getPointerStatement(), UNDEFINED_TOKEN_STRING);
        Assert.assertEquals(token.getFeatureStatement(varMap), UNDEFINED_TOKEN_STRING);
        Assert.assertEquals(token.getStatement(varMap), rawStr);
    }

    @Test
    public void TokenTest3() {
        String rawStr = "<";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.SIMPLE);
        Assert.assertTrue(token.getTokenType().equals(TokenType.SEPARATOR));
        Assert.assertTrue(token.getRawValue().equals(rawStr));
        Assert.assertTrue(token.getStatement().equals(rawStr));
        Assert.assertTrue(token.getPointerStatement().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getFeatureStatement(varMap).equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement(varMap).equals(rawStr));
    }

    @Test
    public void TokenTest4() {
        String rawStr = ",";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.SIMPLE);
        Assert.assertTrue(token.getTokenType().equals(TokenType.SEPARATOR));
        Assert.assertTrue(token.getRawValue().equals(rawStr));
        Assert.assertTrue(token.getStatement().equals(rawStr));
        Assert.assertTrue(token.getPointerStatement().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getFeatureStatement(varMap).equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement(varMap).equals(rawStr));
    }

    @Test
    public void TokenTest5() {
        String rawStr = "or";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.SIMPLE);
        Assert.assertTrue(token.getTokenType().equals(TokenType.KEYWORD));
        Assert.assertTrue(token.getRawValue().equals(rawStr));
        Assert.assertTrue(token.getStatement().equals(rawStr));
        Assert.assertTrue(token.getPointerStatement().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getFeatureStatement(varMap).equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement(varMap).equals(rawStr));
    }

    @Test
    public void TokenTest6() {
        String rawStr = "<test>";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.SIMPLE);
        Assert.assertTrue(token.getTokenType().equals(TokenType.VARIANT));
        Assert.assertTrue(token.getRawValue().equals(rawStr));
        Assert.assertTrue(token.getStatement().equals(rawStr));
        Assert.assertTrue(token.getPointerStatement().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getFeatureStatement(varMap).equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement(varMap).equals(rawStr));
    }

    @Test
    public void TokenTest7() {
        String rawStr = "#x.feature";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.NULL);
        Assert.assertTrue(token.getTokenType().equals(TokenType.FEATURE));
        Assert.assertTrue(token.getRawValue().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getPointerStatement().equals("#x.feature"));
        Assert.assertTrue(token.getFeatureStatement(varMap).equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement(varMap).equals(UNDEFINED_TOKEN_STRING));
    }

    @Test
    public void TokenTest8() {
        String rawStr = "#x.feature";
        Token token = new Token(rawStr);
        System.out.println(token);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.SIMPLE);
        Assert.assertTrue(token.getTokenType().equals(TokenType.FEATURE));
        Assert.assertTrue(token.getRawValue().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getPointerStatement().equals("#x.feature"));
        Assert.assertTrue(token.getFeatureStatement(varMap).equals("feature-value-test"));
        Assert.assertTrue(token.getStatement(varMap).equals("feature-value-test"));
    }

    @Test
    public void TokenTest9() {
        System.out.println(" *** Token Test 9 *** ");
        String rawStr = "#x.feature";
        Token token = new Token(rawStr);
        System.out.println(" -- Token (before change): " + token);
        token.setVariableId("y");
        System.out.println(" -- Token (after change): " + token);
        Assert.assertTrue(token.getTokenType().equals(TokenType.FEATURE));
        Assert.assertTrue(token.getRawValue().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getStatement().equals(UNDEFINED_TOKEN_STRING));
        Assert.assertTrue(token.getPointerStatement().equals("#y.feature"));
        Assert.assertTrue(token.getVariableId().equals("y"));
    }


    //==================================================================
    // Construction Tests
    //==================================================================

    @Test
    public void ConstructorTest1() {
        System.out.println(" *** Constructor Test 1 *** ");
        String rawStr = "test #x.feature >>>";
        Template template = new Template("test", rawStr);
        printTemplate(template);
        Assert.assertEquals(3, template.getTokenList().size());
    }

    @Test
    public void ConstructorTest2() {
        System.out.println(" *** Constructor Test 2 *** ");
        String rawStr = "(necessary, #1.property, #2.temporality #3.property)";
        Template template = new Template("test", rawStr);
        printTemplate(template);
        Assert.assertEquals(7, template.getTokenList().size());
    }


    //==================================================================
    // Accessor Tests
    //==================================================================

    @Test
    public void ChangeVariableIdTest1() {
        System.out.println(" *** Change Variable Id Test 1 *** ");
        String rawStr = "(necessary, #v1.property, #v2.temporality #v3.property)";
        Template template = new Template("test", rawStr);
        System.out.println(" ... before change");
        printTemplate(template);
        Map<String, String> changeIdMap = new HashMap<>();
        changeIdMap.put("v1", "x1");
        template.changeVariableId(changeIdMap);
        System.out.println(" ... after change");
        printTemplate(template);
    }

    @Test
    public void ChangeVariableIdTest2() {
        System.out.println(" *** Change Variable Id Test 2 *** ");
        String rawStr = "(necessary, #v1.property, #v2.temporality #v3.property)";
        Template template = new Template("test", rawStr);
        System.out.println(" ... before change");
        printTemplate(template);
        Map<String, String> changeIdMap = new HashMap<>();
        changeIdMap.put("v1", "x1");
        changeIdMap.put("v2", "x2");
        changeIdMap.put("v3", "x3");
        changeIdMap.put("v4", "x4");
        template.changeVariableId(changeIdMap);
        System.out.println(" ... after change");
        printTemplate(template);
    }


    //==================================================================
    // Main Method Tests
    //==================================================================

    @Test
    public void StatementGivingTest1() {
        System.out.println(" *** Statement Giving Test 1 *** ");
        String rawStr = "(necessary, #a.property, #b.temporality #c.property)";
        Template template = new Template("test", rawStr);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.PTP);
        template.setVariableMap(varMap);
        printTemplate(template);
        String statement = template.getStatement();
        System.out.println(statement);
        Assert.assertEquals(
                "(necessary, prop1-value, temp1-value prop2-value)",
                statement);
    }

    @Test
    public void InterpretationGivingTest1() {
        System.out.println(" *** Interpretation Giving Test 1 *** ");
        String rawStr = "(necessary, #a.property, #b.temporality #c.property)";
        Template template = new Template("#out", rawStr);
        Map<String, Instance> varMap = generateVariableMap(VarMapContent.PTP);
        template.setVariableMap(varMap);
        printTemplate(template);
        String varId = "x";
        TransPattern pattern = new TransPattern(new Query(), template);
        Instance instance = template.getInstance(varId, pattern);
        Atom predicate = template.getPredicate(varId);
        System.out.println("-- Concept: " + template.getConcept());
        System.out.println("-- Instance: " + instance.toString());
        System.out.println("-- Predicate: " + predicate.toString());
        Interpretation interpretation = template.getInterpretation(varId, pattern);
        System.out.println("-- Interpretation: " + interpretation.toString());
        Assert.assertEquals("#out", template.getConcept());
        Assert.assertEquals("is#out(x)", predicate.toString());
    }

}
