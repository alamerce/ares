import AbstractSemantic.*;
import LogicalQuery.Atom;
import LogicalQuery.Variable;
import Parameters.Category;
import SourceAnalyzer.SymbolTable.Role;
import SourceAnalyzer.SymbolTable.Symbol;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class InterpretationTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    public void printInterpretation(Interpretation interpretation) {
        System.out.println();
        System.out.println(interpretation);
        System.out.println("Instances:");
        int n = 0;
        for(Instance i: interpretation.getInstanceSet()) {
            n++;
            System.out.println(" -- I(" + n + "): " + i);
        }
    }


    public Interpretation getTestInterpretation1() {

        System.out.println();
        System.out.println(" -- Test Interpretation 1 (construction) ");

        // Map of data
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("x9","gate");
        conceptMap.put("x10","open-01");
        conceptMap.put("x12","obligatory");
        conceptMap.put("x3","ticket");
        conceptMap.put("x6","issue-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","necessary");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("x9","gate");
        entityMap.put("x3","ticket");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("x10","open-01_gate");
        propertyMap.put("x6","issue-01_ticket");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);

        // Set of atoms
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "x9"));
        atomSet.add(new Atom("isEntity", "x3"));
        atomSet.add(new Atom("isProperty", "x10"));
        atomSet.add(new Atom("isProperty", "x3"));
        atomSet.add(new Atom("isModality", "x12"));
        atomSet.add(new Atom("arg1", "x10", "x9"));
        atomSet.add(new Atom("arg1", "x6", "x3"));
        atomSet.add(new Atom("time", "x12", "x6"));
        atomSet.add(new Atom("domain", "x12", "x10"));

        // Interpretation
        return new Interpretation("TEST", dataMap, atomSet);

    }


    //==================================================================
    // Tests: Instance
    //==================================================================

    @Test
    public void InstanceTest1() {

        System.out.println(" *** Instance Test 1 *** ");

        // Test Data
        List<Role> rList;
        Symbol s;
        Instance i1, i2, i3;

        // Construction of Symbol Table

        // Test Code
        i1 = new Instance("instance1", "concept");
        System.out.println(i1.toString());
        rList = new ArrayList<>();
        rList.add(new Role("A1", "x2"));
        s = new Symbol("x1", Category.MODALITY, "possible-01", rList);
        i2 = new Instance(s);
        System.out.println(i2.toString());
        i3 = new Instance(s);
        i3.addFeature("entity", "possible-01");
        i3.addFeature("property", "possible-01_x1");
        System.out.println(i3.toString());

        // Asserts
        Assert.assertEquals("instance1", i1.getId());
        Assert.assertEquals("concept", i1.getConcept());
        Assert.assertEquals("x1", i2.getId());
        Assert.assertEquals("possible-01", i2.getConcept());
        Assert.assertEquals("x1", i3.getId());
        Assert.assertEquals("possible-01", i3.getConcept());
        Assert.assertEquals("possible-01_x1", i3.getFeatureValue("property"));
        Assert.assertEquals("possible-01", i3.getFeatureValue("entity"));

    }

    @Test
    public void InstanceTest2() {

        System.out.println(" *** Instance Test 2 *** ");
        TransPattern p = new TransPattern();
        Instance instance = new Instance("instance1", "concept", p);
        System.out.println(instance.toString());

        Assert.assertEquals("instance1", instance.getId());
        Assert.assertEquals("concept", instance.getConcept());
        Assert.assertTrue(instance.isAbstraction());

    }


    //==================================================================
    // Interpretation Class Tests (constructions)
    //==================================================================

    @Test
    public void ConstructionTest1() {

        System.out.println(" *** Construction Test 1 *** ");

        // Interpretations d1, d2, d3
        Interpretation d1 = new Interpretation("test1");
        printInterpretation(d1);
        d1.addConcept("open");
        d1.addConcept("close");
        Interpretation d2 = new Interpretation("test2");
        d2.addConcept("open");
        d2.addConcept("close");
        Instance i1 = new Instance("x", "test");
        Instance i2 = new Instance("y", "test");
        d2.addInstance(i1);
        d2.addInstance(i2);
        printInterpretation(d2);
        Interpretation d3 = new Interpretation("test3");
        d3.addConcept("open");
        d3.addConcept("close");
        d3.addInstance(i1);
        d3.addInstance(i2);
        Atom a1 = new Atom("open", "x");
        Atom a2 = new Atom("close", "y", "z");
        d3.addPredicate(a1);
        d3.addPredicate(a2);
        printInterpretation(d3);

        // Asserts
        Assert.assertTrue(d3.getConceptSet().contains("open"));
        Assert.assertTrue(d3.getConceptSet().contains("close"));
        Assert.assertTrue(d3.getInstanceSet().contains(i1));
        Assert.assertTrue(d3.getInstanceSet().contains(i2));
        Assert.assertTrue(d3.getPredicateSet().contains(a1));
        Assert.assertTrue(d3.getPredicateSet().contains(a2));

    }

    @Test
    public void ConstructionTest2() {

        System.out.println(" *** Construction Test 2 *** ");

        // Interpretation
        Interpretation interpretation = new Interpretation("test3");
        interpretation.addConcept("open");
        interpretation.addConcept("close");
        interpretation.addInstance(new Instance("x", "conceptX"));
        interpretation.addInstance(new Instance("y", "conceptY"));
        interpretation.addPredicate(new Atom("open", "x"));
        interpretation.addPredicate(new Atom("close", "y", "z"));
        printInterpretation(interpretation);

        // Asserts
        Assert.assertEquals("conceptX", interpretation.getInstance("x").getConcept());

    }

    @Test
    public void ConstructionTest3() {

        System.out.println(" *** Construction Test 3 *** ");

        // Test Data
        List<Variable> vList;
        Map<String, Instance> instanceMap;

        // Interpretation
        Interpretation interpretation = new Interpretation("test3");
        interpretation.addConcept("open");
        interpretation.addConcept("close");
        interpretation.addInstance(new Instance("x12", "conceptX"));
        interpretation.addInstance(new Instance("x6", "conceptY"));
        interpretation.addPredicate(new Atom("open", "x12"));
        interpretation.addPredicate(new Atom("close", "x6", "z"));
        printInterpretation(interpretation);
        Variable v1 = new Variable("x", "x12");
        Variable v2 = new Variable("y", "x6");
        vList = new ArrayList<>();
        vList.add(v1);
        vList.add(v2);
        instanceMap = interpretation.getInstanceMap(vList);
        System.out.println();
        System.out.println("Instance Map of " + vList.toString() + ": " + instanceMap.toString());

        // Asserts
        Assert.assertEquals("[x, y]", instanceMap.keySet().toString());
        Assert.assertEquals("conceptX", instanceMap.get("x").getConcept());

    }

    @Test
    public void ConstructionTest4() {

        System.out.println(" *** Construction Test 4 *** ");

        // Map of data
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("x9","gate");
        conceptMap.put("x10","open-01");
        conceptMap.put("x12","obligatory");
        conceptMap.put("x3","ticket");
        conceptMap.put("x6","issue-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","necessary");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("x9","gate");
        entityMap.put("x3","ticket");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("x10","open-01_gate");
        propertyMap.put("x6","issue-01_ticket");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);

        // Set of atoms
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "x9"));
        atomSet.add(new Atom("isEntity", "x3"));
        atomSet.add(new Atom("isProperty", "x10"));
        atomSet.add(new Atom("isProperty", "x3"));
        atomSet.add(new Atom("isModality", "x12"));
        atomSet.add(new Atom("arg1", "x10", "x9"));
        atomSet.add(new Atom("arg1", "x6", "x3"));
        atomSet.add(new Atom("time", "x12", "x6"));
        atomSet.add(new Atom("domain", "x12", "x10"));

        // Interpretation
        Interpretation interpretation = new Interpretation("TEST", dataMap, atomSet);
        printInterpretation(interpretation);

        // Asserts
        Assert.assertTrue(interpretation.getConceptSet().contains("open-01"));
        Assert.assertTrue(interpretation.getConceptSet().contains("issue-01"));
        Assert.assertTrue(interpretation.getConceptSet().contains("obligatory"));
        Assert.assertTrue(interpretation.getConceptSet().contains("ticket"));
        Assert.assertTrue(interpretation.getConceptSet().contains("gate"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x9"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x10"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x12"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x3"));
        Assert.assertTrue(interpretation.getInstanceIdSet().contains("x6"));

    }

    //==================================================================
    // Interpretation Class Test (Accessors)
    //==================================================================



    @Test
    public void RemoveInstanceTest1() {

        System.out.println(" *** Construction Test 4 *** ");

        Interpretation interpretation = getTestInterpretation1();
        printInterpretation(interpretation);

        System.out.println();
        System.out.println(" -- Remove instance x3 ");
        Instance instance = new Instance("x3", "ticket");
        interpretation.removeInstance(instance);
        printInterpretation(interpretation);

    }

    @Test
    public void AlignTest1() {

        System.out.println(" *** Construction Test 4 *** ");

        Interpretation interpretation = getTestInterpretation1();
        printInterpretation(interpretation);

        System.out.println();
        System.out.println(" -- Remove instance x3 ");
        Instance instance = new Instance("x3", "ticket");
        interpretation.removeInstance(instance);
        printInterpretation(interpretation);

        System.out.println();
        System.out.println(" -- Align interpretation ");
        interpretation.align();
        printInterpretation(interpretation);

    }

    // TODO: complete tests for others accessors




    //==================================================================
    // Interpretation Class Tests (Operation)
    //==================================================================

    // TODO: ExtendRelationsTest

    @Test
    public void UnionTest1() {

        System.out.println(" *** Union Test 1 *** ");

        Interpretation a = new Interpretation("A");
        a.addConcept("open");
        a.addConcept("close");
        a.addInstance(new Instance("x12", "conceptX"));
        a.addInstance(new Instance("x6", "conceptY"));
        a.addPredicate(new Atom("open", "x12"));
        a.addPredicate(new Atom("close", "x6", "z"));
        System.out.println("A: " + a.toString());

        Interpretation b = new Interpretation("B");
        b.addConcept("open");
        b.addConcept("close");
        b.addInstance(new Instance("x12", "conceptX"));
        b.addInstance(new Instance("x6", "conceptY"));
        b.addPredicate(new Atom("open", "x12"));
        b.addPredicate(new Atom("close", "x6", "z"));
        System.out.println("B: " + b.toString());

        a.union(b);
        System.out.println("A union B: " + a.toString());

    }

    @Test
    public void UnionTest2() {

        System.out.println(" *** Union Test 2 *** ");

        Interpretation a = new Interpretation("A");
        a.addConcept("open");
        a.addConcept("close");
        a.addInstance(new Instance("x1", "conceptX"));
        a.addInstance(new Instance("y1", "conceptY1"));
        a.addPredicate(new Atom("open", "x1"));
        a.addPredicate(new Atom("close", "y1", "z1"));
        System.out.println("<A> \n" + a.toString());

        Interpretation b = new Interpretation("B");
        b.addConcept("wear");
        b.addConcept("suit");
        b.addInstance(new Instance("x2", "conceptX"));
        b.addInstance(new Instance("y2", "conceptY2"));
        b.addPredicate(new Atom("wear", "x2"));
        b.addPredicate(new Atom("suit", "y2", "z2"));
        System.out.println("<B> \n" + b.toString());

        a.union(b);
        System.out.println("<A union B> \n" + a.toString());

        Assert.assertTrue(a.containInstance(new Instance("x1", "conceptX")));
        Assert.assertTrue(a.containInstance(new Instance("y1", "conceptY1")));
        Assert.assertTrue(a.containInstance(new Instance("x2", "conceptX")));
        Assert.assertTrue(a.containInstance(new Instance("y2", "conceptY2")));
        Assert.assertTrue(a.containPredicate(new Atom("open", "x1")));
        Assert.assertTrue(a.containPredicate(new Atom("wear", "x2")));
        Assert.assertTrue(a.containPredicate(new Atom("close", "y1", "z1")));
        Assert.assertTrue(a.containPredicate(new Atom("suit", "y2", "z2")));

    }

    @Test
    public void DifferenceTest1() {

        System.out.println(" *** Difference Test 1 *** ");

        Interpretation a = new Interpretation("A");
        a.addConcept("conceptX");
        a.addConcept("conceptY1");
        a.addConcept("conceptY2");
        a.addConcept("conceptZ");
        a.addInstance(new Instance("x1", "conceptX"));
        a.addInstance(new Instance("y1", "conceptY1"));
        a.addInstance(new Instance("y2", "conceptY2"));
        a.addInstance(new Instance("z1", "conceptZ"));
        a.addPredicate(new Atom("open", "x1"));
        a.addPredicate(new Atom("close", "y1", "z1"));
        a.addPredicate(new Atom("suit", "y2", "y1"));
        System.out.println("<A> \n" + a.toString());

        Interpretation b = new Interpretation("B");
        b.addConcept("conceptX");
        b.addConcept("conceptY2");
        b.addInstance(new Instance("x2", "conceptX"));
        b.addInstance(new Instance("y2", "conceptY2"));
        b.addPredicate(new Atom("wear", "x2"));
        b.addPredicate(new Atom("suit", "y2", "z2"));
        System.out.println("<B> \n" + b.toString());

        Assert.assertTrue(a.containInstance(new Instance("x1", "conceptX")));
        Assert.assertTrue(a.containInstance(new Instance("y1", "conceptY1")));
        Assert.assertTrue(a.containInstance(new Instance("y2", "conceptY2")));
        Assert.assertTrue(a.containInstance(new Instance("z1", "conceptZ")));
        Assert.assertTrue(a.containPredicate(new Atom("open", "x1")));
        Assert.assertTrue(a.containPredicate(new Atom("suit", "y2", "y1")));
        Assert.assertTrue(a.containPredicate(new Atom("close", "y1", "z1")));

        a.difference(b);
        System.out.println("<A difference B> \n" + a.toString());

        Assert.assertTrue(a.containInstance(new Instance("x1", "conceptX")));
        Assert.assertTrue(a.containInstance(new Instance("y1", "conceptY1")));
        Assert.assertFalse(a.containInstance(new Instance("y2", "conceptY2")));
        Assert.assertTrue(a.containInstance(new Instance("z1", "conceptZ")));
        Assert.assertTrue(a.containPredicate(new Atom("open", "x1")));
        Assert.assertFalse(a.containPredicate(new Atom("suit", "y2", "y1")));
        Assert.assertTrue(a.containPredicate(new Atom("close", "y1", "z1")));

    }

    @Test
    public void DifferenceTest2() {

        System.out.println(" *** Difference Test 2 *** ");

        Interpretation a = new Interpretation("A");
        a.addConcept("conceptX");
        a.addConcept("conceptY1");
        a.addConcept("conceptY2");
        a.addConcept("conceptZ");
        a.addInstance(new Instance("x1", "conceptX"));
        a.addInstance(new Instance("y1", "conceptY1"));
        a.addInstance(new Instance("y2", "conceptY2"));
        a.addInstance(new Instance("z1", "conceptZ"));
        a.addPredicate(new Atom("open", "x1"));
        a.addPredicate(new Atom("close", "y1", "z1"));
        a.addPredicate(new Atom("suit", "y2", "y1"));
        System.out.println("<A> \n" + a.toString());

        Interpretation b = new Interpretation("B");
        b.addConcept("conceptX");
        b.addConcept("conceptY2");
        b.addInstance(new Instance("x2", "conceptX"));
        b.addInstance(new Instance("y2", "conceptY2"));
        b.addPredicate(new Atom("wear", "x2"));
        b.addPredicate(new Atom("suit", "y2", "z2"));
        System.out.println("<B> \n" + b.toString());

        Assert.assertTrue(b.containInstance(new Instance("x2", "conceptX")));
        Assert.assertTrue(b.containInstance(new Instance("y2", "conceptY2")));

        b.difference(a);
        System.out.println("<B difference A> \n" + b.toString());

        Assert.assertTrue(b.containInstance(new Instance("x2", "conceptX")));
        Assert.assertFalse(b.containInstance(new Instance("y2", "conceptY2")));

    }

}