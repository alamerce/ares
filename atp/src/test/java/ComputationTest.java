import ComputationObject.OutputStatement;
import ComputationObject.Utility;
import ComputationObject.WorkData;
import SourceAnalyzer.Amr.Asd;
import SourceAnalyzer.SymbolTable.Role;
import SourceAnalyzer.SymbolTable.Symbol;
import SourceAnalyzer.SymbolTable.Table;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ComputationTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    static private String TEST_RESOURCES_PATH = "src/test/resources/";


    // ---------- Useful Methods

    private void printDataAboutAST() {
        System.out.println("");
        System.out.println(" -- Data from AST");
        System.out.println(" ----- AST List Size: " + Computation.getAstList().size());
        System.out.println(" ----- Sentence for the first AST: " + Computation.getAstList().get(0).getSentence());
        System.out.println(" ----- Output List for the first AST: " + Computation.getAstList().get(0).getOutputList());
    }

    private String printResult() {

        List<String> reqList = new ArrayList<>();
        String req = "-";
        OutputStatement outputStatement = new OutputStatement();

        List<WorkData> evalDataList = Computation.getEvalDataList();
        List<OutputStatement> osList = Utility.getOutputStatementList(evalDataList);
        for(OutputStatement os: osList) reqList.add(os.getIdentifiedStatement());
        //reqList = Computation.getResultStatementList();
        if (!reqList.isEmpty()) req = reqList.get(0);
        if (!osList.isEmpty()) outputStatement = osList.get(0);


        printDataAboutAST();
        System.out.println(" -- Transduction Pattern Number: " + Computation.getTransPatternSet().size());
        System.out.println(" -- Symbol Table List ");
        System.out.println(Computation.getSymbolTableList().toString());
        System.out.println();
        System.out.println(" -- Result ");
        System.out.println(" ----- reqList size: " + reqList.size());
        System.out.println(" ----- req: " + req);
        System.out.println(" ----- reqList: " + reqList.toString());
        System.out.println(" ----- produce query: " + outputStatement.getProducerPattern().getQuery().toString());
        System.out.println(" ----- produce analysis depth: " + outputStatement.getAnalysisDepth());

        return req;

    }

    //==================================================================
    // Basic Tests
    //==================================================================

    @Test
    public void AstAnalyzeTest1() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        List<Role> rList;
        Symbol v1, v2, v3, v4, v5, v6, v7;
        Table t1;

        // Nodes and Edges Declaration
        Asd.Node node1, node2, node3, node4, node5, node6, node7;
        Asd.Edge edge1, edge2, edge3, edge4, edge5, edge6;

        // Node 6 : (x9, date-entity, [])
        node6 = new Asd.Node(
                new Asd.Instance("x9"),
                new Asd.Concept("date-entity"),
                new ArrayList<>());

        // Node 7 : (x11, this, [])
        node7 = new Asd.Node(
                new Asd.Instance("x11"),
                new Asd.Concept("this"),
                new ArrayList<>());

        // List of edges (2): [node6, node7]
        List<Asd.Edge> edgeList3 = new ArrayList<>();
        edge5 = new Asd.Edge(new Asd.Role(":op1"), node6);
        edgeList3.add(edge5);
        edge6 = new Asd.Edge(new Asd.Role(":op2"), node7);
        edgeList3.add(edge6);

        // Node 3 : (x2, receiver, [])
        node3 = new Asd.Node(
                new Asd.Instance("x2"),
                new Asd.Concept("receiver"),
                new ArrayList<>());

        // Node 4 : (x4, only, [])
        node4 = new Asd.Node(
                new Asd.Instance("x4"),
                new Asd.Concept("only"),
                new ArrayList<>());

        // Node 5 : (x8, between, edgeList3)
        node5 = new Asd.Node(
                new Asd.Instance("x8"),
                new Asd.Concept("between"),
                edgeList3);

        // List of edges (2): [node3, node4, node5]
        List<Asd.Edge> edgeList2 = new ArrayList<>();
        edge2 = new Asd.Edge(new Asd.Role(":ARG1"), node3);
        edgeList2.add(edge2);
        edge3 = new Asd.Edge(new Asd.Role(":mod"), node4);
        edgeList2.add(edge3);
        edge4 = new Asd.Edge(new Asd.Role(":ARG2"), node5);
        edgeList2.add(edge4);

        // Node 2 : (x6, heat-01, edgeList2)
        node2 = new Asd.Node(
                new Asd.Instance("x6"),
                new Asd.Concept("heat-01"),
                edgeList2);

        // List of edges (1): [node2]
        List<Asd.Edge> edgeList1 = new ArrayList<>();
        edge1 = new Asd.Edge(new Asd.Role(":ARG1"), node2);
        edgeList1.add(edge1);

        // Node 1 : (testInstance, testConcept, edgeList1)
        node1 = new Asd.Node(
                new Asd.Instance("x3"),
                new Asd.Concept("possible-01"),
                edgeList1);

        // Print as Ntriples
        Asd.Representation ast = new Asd.Representation(node1);
        System.out.println(ast.toString());
        Table symbolTable = ast.analyse();
        System.out.println(symbolTable.toString());

        // Asserts
        Assert.assertEquals("<***No Ident***> Symbol Table:\n" +
                "  (x8, between, [(:op1, x9), (:op2, x11)], UNKNOWN)\n" +
                "  (x9, date-entity, [], UNKNOWN)\n" +
                "  (x11, this, [], UNKNOWN)\n" +
                "  (x2, receiver, [], UNKNOWN)\n" +
                "  (x3, possible-01, [(:ARG1, x6)], UNKNOWN)\n" +
                "  (x4, only, [], UNKNOWN)\n" +
                "  (x6, heat-01, [(:ARG1, x2), (:mod, x4), (:ARG2, x8)], UNKNOWN)\n", symbolTable.toString());

    }

    @Test
    public void EntityListComputationTest() {

        // Test Data
        String test_file_name = "amr_req_reflectors_01.txt";
        Set<String> eSet;

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        //Computation.init();
        Computation.main(args);

        // Extracting entities
        eSet = Computation.getEntitySet();
        for (String entity : eSet)
            System.out.println(entity);

        // Asserts
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        Assert.assertTrue(Computation.getEntitySet().contains("sky"));
        Assert.assertTrue(Computation.getEntitySet().contains("reflector"));
        Assert.assertTrue(Computation.getEntitySet().contains("sunlight"));

    }

    @Test
    public void PropertyListComputationTest() {

        // Test Data
        String test_file_name = "amr_req_reflectors_01.txt";
        Set<String> pSet;

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);

        // Extracting entities
        pSet = Computation.getPropertySet();
        for (String property : pSet)
            System.out.println(property);

        // Asserts
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        Assert.assertTrue(Computation.getPropertySet().contains("clear-06_sky"));
        Assert.assertTrue(Computation.getPropertySet().contains("reflector_focus-01_sunlight"));

    }

    //==================================================================
    // Tests for Reflector Case
    //==================================================================

    @Test
    public void ReflectorTest1() {

        // Test Data
        String test_file_name = "amr_req_reflectors_01.txt";
        String req = "-";
        Table t1;

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        List<String> reqList = Computation.getResultStatementList();
        if (!reqList.isEmpty())
            req = Computation.getResultStatementList().get(0);
        t1 = Computation.getSymbolTableList().get(0);
        System.out.println(Computation.getSymbolTableList().toString());

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        Assert.assertEquals("<***No Ident***> Symbol Table:\n" +
                "  (x9, sky, [], ENTITY)\n" +
                "  (x11, clear-06, [(ARG1, x9)], PROPERTY)\n" +
                "  (x2, reflector, [], ENTITY)\n" +
                "  (x3, possible-01, [(mod, x4), (ARG1, x5)], MODALITY)\n" +
                "  (x4, only, [], ADVERB, [isOnly])\n" +
                "  (x5, focus-01, [(ARG0, x2), (ARG1, x6), (condition, x11)], PROPERTY)\n" +
                "  (x6, sunlight, [], ENTITY)\n", t1.toString());

    }


    //==================================================================
    // Tests for some parking cases
    //==================================================================

    @Test
    public void ParkingTest1() {

        System.out.println();
        System.out.println(" *** Parking Test 1 *** ");

        String test_file_name = "amr_req_parking_01.txt";
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        String req = printResult();

        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<***No Ident***>: <impossible, pass_vehicle, close_gate>", req);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTest2() {

        // Test Data
        String test_file_name = "amr_req_parking_02.txt";

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        String req = printResult();

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<test>: <necessary, open_gate, issue_ticket>", req.toString());
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTest3() {

        // Test Data
        String test_file_name = "amr_req_parking_03.txt";

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        String req = printResult();

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<test>: <necessary, not vehicles_pass, close_gate>", req.toString());
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTest4() {

        // Test Data
        String test_file_name = "amr_req_parking_04.txt";
        String req = "-";

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        List<String> reqList = Computation.getResultStatementList();
        if (!reqList.isEmpty())
            req = Computation.getResultStatementList().get(0);
        System.out.println(Computation.getSymbolTableList().toString());
        System.out.println("reqList size: " + reqList.size());
        System.out.println("req: " + req);
        System.out.println("reqList: " + reqList.toString());

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<1>: <necessary, open_gate, issue_ticket>", req.toString());
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTest5() {

        // Test Data
        String test_file_name = "amr_req_parking_05.txt";
        String req = "-";

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        List<String> reqList = Computation.getResultStatementList();
        if (!reqList.isEmpty())
            req = Computation.getResultStatementList().get(0);
        System.out.println(Computation.getSymbolTableList().toString());
        System.out.println("reqList size: " + reqList.size());
        System.out.println("req: " + req);
        System.out.println("reqList: " + reqList.toString());

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<5>: <necessary, close_exit-gate, after vehicle_exit_parking>", req);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTest6() {

        // Test Data
        String test_file_name = "amr_req_parking_06.txt";
        String req = "-";

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        List<String> reqList = Computation.getResultStatementList();
        if (!reqList.isEmpty())
            req = Computation.getResultStatementList().get(0);
        System.out.println(Computation.getSymbolTableList().toString());
        System.out.println("reqList size: " + reqList.size());
        System.out.println("req: " + req);
        System.out.println("reqList: " + reqList.toString());

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<***No Ident***>: <necessary, open_exit-gate, after insert_exit-ticket>", req.toString());
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTest7() {

        // Test Data
        String test_file_name = "amr_req_parking_07.txt";
        String req = "-";

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        List<String> reqList = Computation.getResultStatementList();
        if (!reqList.isEmpty())
            req = Computation.getResultStatementList().get(0);
        System.out.println(Computation.getSymbolTableList().toString());
        System.out.println("reqList size: " + reqList.size());
        System.out.println("req: " + req);
        System.out.println("reqList: " + reqList.toString());

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<***No Ident***>: <necessary, open_exit_gate, after insert_exit-ticket>", req);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTest8() {

        // Test Data
        String test_file_name = "amr_req_parking_08.txt";
        String req = "-";

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        List<String> reqList = Computation.getResultStatementList();
        if (!reqList.isEmpty())
            req = Computation.getResultStatementList().get(0);
        System.out.println(Computation.getSymbolTableList().toString());
        System.out.println("reqList size: " + reqList.size());
        System.out.println("req: " + req);
        System.out.println("reqList: " + reqList.toString());

        // Asserts
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
        Assert.assertEquals("<***No Ident***>: <necessary, close_exit-gate, after vehicle_exit_parking>", req);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());

    }

    @Test
    public void ParkingTestAll() {

        // Test Data
        String test_file_name = "amr_req_parking.txt";
        List<String> reqList;

        // Construction of Symbol Table
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + test_file_name;
        Computation.main(args);
        printDataAboutAST();
        reqList = Computation.getResultStatementList();
        System.out.println("reqList size: " + reqList.size());
        for (String req: reqList) System.out.println(req);

        // Asserts
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        for (String req: reqList)
            Assert.assertFalse(req.endsWith("-"));

    }

}
