import AbstractSemantic.*;
import LogicalQuery.Atom;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class NewAnalyzerTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    public TransPatternSet genOldTransPatternSet1() {
        System.out.println(" -- Generation: Transduction Pattern Set 1 ");
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.oldLoad();
        return transPatternSet;
    }

    public TransPatternSet genNewTransPatternSet1() {
        System.out.println(" -- Generation: Transduction Pattern Set 1 ");
        TransPatternSet transPatternSet = new TransPatternSet();
        transPatternSet.newLoad();
        return transPatternSet;
    }

    public Interpretation genInterpretation1() {

        System.out.println(" -- Generation: Interpretation 1 ");

        // Map of data
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("x9","gate");
        conceptMap.put("x10","open-01");
        conceptMap.put("x12","obligatory");
        conceptMap.put("x3","ticket");
        conceptMap.put("x6","issue-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","necessary");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("x9","gate");
        entityMap.put("x3","ticket");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("x10","open-01_gate");
        propertyMap.put("x6","issue-01_ticket");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);

        // Set of atoms
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "x9"));
        atomSet.add(new Atom("isEntity", "x3"));
        atomSet.add(new Atom("isProperty", "x10"));
        atomSet.add(new Atom("isProperty", "x6"));
        atomSet.add(new Atom("isModality", "x12"));
        atomSet.add(new Atom("arg+", "x10", "x9"));
        atomSet.add(new Atom("arg+", "x6", "x3"));
        atomSet.add(new Atom("time", "x12", "x6"));
        atomSet.add(new Atom("domain", "x12", "x10"));

        // Interpretation
        Interpretation interpretation = new Interpretation("TEST", dataMap, atomSet);

        return interpretation;

    }

    public Interpretation genInterpretation2() {

        System.out.println(" -- Generation: Interpretation 2 ");

        // Map of data
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("x9","gate");
        conceptMap.put("x10","open-01");
        conceptMap.put("x12","obligate-01");
        conceptMap.put("x3","enter-01");
        Map<String, String> modalityMap = new HashMap<>();
        modalityMap.put("x12","obligate-01");
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("x9","gate");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("x10","open_enter-gate");
        propertyMap.put("x3","enter-01");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", defaultMap);
        dataMap.put("adverb", defaultMap);

        // Set of atoms
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isEntity", "x9"));
        atomSet.add(new Atom("isProperty", "x10"));
        atomSet.add(new Atom("isProperty", "x3"));
        atomSet.add(new Atom("isModality", "x12"));
        atomSet.add(new Atom("arg+", "x12", "x10"));
        atomSet.add(new Atom("mod", "x9", "x3"));
        atomSet.add(new Atom("arg+", "x10", "x9"));

        // Interpretation
        Interpretation interpretation = new Interpretation("TEST", dataMap, atomSet);

        return interpretation;

    }

    public Interpretation genInterpretation3() {

        System.out.println(" -- Generation: Interpretation 3 ");

        // Map of data
        Map<String, String> conceptMap = new HashMap<>();
        conceptMap.put("vv1","insert-01");
        conceptMap.put("vv3","use-01");
        conceptMap.put("vv2","person");
        conceptMap.put("vv5","rate-entity-91");
        conceptMap.put("vv4","coin");
        conceptMap.put("vv6","insert-01");
        Map<String, String> modalityMap = new HashMap<>();
        Map<String, String> entityMap = new HashMap<>();
        entityMap.put("vv4","coin");
        Map<String, String> propertyMap = new HashMap<>();
        propertyMap.put("vv1","insert_A");
        propertyMap.put("vv3","use-01");
        propertyMap.put("vv2","person");
        propertyMap.put("vv6","insert_B");
        Map<String, String> frequencyMap = new HashMap<>();
        frequencyMap.put("vv5","rate-entity-91");
        Map<String, String> defaultMap = new HashMap<>();
        Map<String, Map<String, String>> dataMap = new HashMap<>();
        dataMap.put("concept", conceptMap);
        dataMap.put("modality", modalityMap);
        dataMap.put("entity", entityMap);
        dataMap.put("property", propertyMap);
        dataMap.put("temporality", defaultMap);
        dataMap.put("logicConnector", defaultMap);
        dataMap.put("frequency", frequencyMap);
        dataMap.put("adverb", defaultMap);

        // Set of atoms
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isProperty", "vv1"));
        atomSet.add(new Atom("isProperty", "vv3"));
        atomSet.add(new Atom("isProperty", "vv2"));
        atomSet.add(new Atom("isRecurringEvent", "vv5"));
        atomSet.add(new Atom("isEntity", "vv4"));
        atomSet.add(new Atom("isProperty", "vv6"));
        atomSet.add(new Atom("arg0", "vv1", "vv2"));
        atomSet.add(new Atom("arg+", "vv1", "vv4"));
        atomSet.add(new Atom("frequency", "vv1", "vv5"));
        atomSet.add(new Atom("arg0", "vv3", "vv2"));
        atomSet.add(new Atom("arg+", "vv5", "vv6"));

        // Interpretation
        Interpretation interpretation = new Interpretation("TEST", dataMap, atomSet);

        return interpretation;

    }

    //==================================================================
    // Preparation Test
    //==================================================================

    @Test
    public void PreparationTest1() {

        Interpretation interpretation = genInterpretation1();
        System.out.println(interpretation);
        Assert.assertFalse(interpretation.getPredicateSet().isEmpty());

        TransPatternSet transPatternSet = genOldTransPatternSet1();
        for(TransPattern p: transPatternSet.getSet())
            System.out.println(p.toString());
        Assert.assertFalse(transPatternSet.getSet().isEmpty());

    }


    //==================================================================
    // Construction Test
    //==================================================================

    @Test
    public void ConstructorTest1() {
        System.out.println();
        System.out.println(" *** Construction Test 1 *** ");
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation1(), genOldTransPatternSet1());
        System.out.println(analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(analyzer.getTransPatternSet());
    }


    //==================================================================
    // Interpretation Update Test
    //==================================================================

    @Test
    public void InterpretationUpdateRounByRoundTest1() {
        System.out.println();
        System.out.println(" *** Interpretation Update (Round by Round) Test 1 *** ");
        System.out.println();
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation1(), genNewTransPatternSet1());
        System.out.println();
        System.out.println(" -- Base Interpretation: " + analyzer.getInterpretationRun().getLastInterpretation());
        int round = 1;
        Set<TransPattern> roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println();
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
        round = 2;
        roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println();
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
        round = 3;
        roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println();
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
    }

    @Test
    public void InterpretationUpdateRounByRoundTest2() {
        System.out.println();
        System.out.println(" *** Interpretation Update (Round by Round) Test 2 *** ");
        System.out.println();
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation3(), genNewTransPatternSet1());
        System.out.println();
        System.out.println(" -- Base Interpretation: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        int round = 1;
        Set<TransPattern> roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println();
        System.out.println(" -- Interpretation after updating(0): " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        round = 2;
        roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println();
        System.out.println(" -- Interpretation after updating(1): " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        round = 3;
        roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println();
        System.out.println(" -- Interpretation after updating(2): " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
    }

    @Test
    public void MaximumInterpretationUpdateTest1() {
        System.out.println();
        System.out.println(" *** Maximum Interpretation Update Test 1 *** ");
        System.out.println();
        //NewAnalyzer analyzer = new NewAnalyzer(genInterpretation1(), genTransPatternSet1());
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation1(), genNewTransPatternSet1());
        System.out.println();
        System.out.println(" -- Base Interpretation: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        analyzer.updateMaximumInterpretation();
        int maxDepth = analyzer.getTransPatternSet().getMaximumDepth();
        System.out.println();
        System.out.println(" -- Maximum Depth: " + maxDepth);
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
    }

    @Test
    public void MaximumInterpretationUpdateTest2() {
        System.out.println();
        System.out.println(" *** Maximum Interpretation Update Test 2 *** ");
        System.out.println();
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation3(), genNewTransPatternSet1());
        System.out.println();
        System.out.println(" -- Base Interpretation: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        analyzer.updateMaximumInterpretation();
        int maxDepth = analyzer.getTransPatternSet().getMaximumDepth();
        System.out.println();
        System.out.println(" -- Maximum Depth: " + maxDepth);
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
    }


    //==================================================================
    // OutputStatement Giving Test
    //==================================================================


    @Test
    public void OutputStatementGivingTest1() {
        System.out.println();
        System.out.println(" *** OutputStatement Giving Test 1 *** ");
        System.out.println();
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation1(), genNewTransPatternSet1());
        System.out.println();
        int round = 1;
        Set<TransPattern> roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        round = 2;
        roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        round = 3;
        roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        round = 4;
        roundPatternSet = analyzer.getTransPatternSet().getSet(round);
        analyzer.getInterpretationRun().update(round, roundPatternSet);
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        Assert.assertEquals(
                "<necessary, open_gate, issue_ticket>",
                analyzer.getBestOutputStatement());
    }

    @Test
    public void OutputStatementGivingTest2() {
        System.out.println();
        System.out.println(" *** OutputStatement Giving Test 2 *** ");
        System.out.println();
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation1(), genNewTransPatternSet1());
        System.out.println(" -- Base Interpretation: " + analyzer.getInterpretationRun().getLastInterpretation());
        analyzer.updateMaximumInterpretation();
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        Assert.assertEquals(
                "<necessary, open_gate, issue_ticket>",
                analyzer.getBestOutputStatement());
    }

    @Test
    public void OutputStatementGivingTest3() {
        System.out.println();
        System.out.println(" *** OutputStatement Giving Test 3 *** ");
        System.out.println();
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation2(), genNewTransPatternSet1());
        System.out.println(" -- Base Interpretation: " + analyzer.getInterpretationRun().getLastInterpretation());
        analyzer.updateMaximumInterpretation();
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        Assert.assertEquals(
                "<obligate, open_enter-gate, True>",
                analyzer.getBestOutputStatement());
    }

    @Test
    public void OutputStatementGivingTest4() {
        System.out.println();
        System.out.println(" *** OutputStatement Giving Test 4 *** ");
        System.out.println();
        NewAnalyzer analyzer = new NewAnalyzer(genInterpretation3(), genNewTransPatternSet1());
        System.out.println(" -- Base Interpretation: " + analyzer.getInterpretationRun().getLastInterpretation());
        analyzer.updateMaximumInterpretation();
        System.out.println(" -- Interpretation after updating: " + analyzer.getInterpretationRun().getLastInterpretation());
        System.out.println(" -- Output Statement: " + analyzer.getBestOutputStatement());
        Assert.assertEquals(
                "<necessary, insert_A, insert_B>",
                analyzer.getBestOutputStatement());
    }

}