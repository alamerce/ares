import AbstractSemantic.Interpretation;
import AbstractSemantic.OldLearner;
import AbstractSemantic.LearnerObject.OldArchive;
import AbstractSemantic.LearnerObject.InterpretationAnalyzer;
import AbstractSemantic.TransPattern;
import AbstractSemantic.TransPatternObject.Template;
import AbstractSemantic.TransPatternObject.Token;
import AbstractSemantic.TransPatternSet;
import ComputationObject.WorkData;
import Utility.Tokenizer;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static Parameters.Configuration.NORMALIZER;
import static Parameters.Configuration.OUTPUT_ABSTRACT_CONCEPT;

public class OldLearnerTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    private static final String TEST_RESOURCES_PATH = "src/test/resources/";
    private static final String LEARNING_TEST_DATA_1 = "learning_test_data_1.amrep";
    private static final String LEARNING_TEST_DATA_2 = "learning_test_data_2.amrep";
    private static final String LEARNING_TEST_DATA_3 = "learning_test_data_3.amrep";
    private static final String STATEMENT_1 = "<impossible, pass_vehicle, close_gate>";
    private static final String STATEMENT_2 = "<necessary, not close_exit-gate, after vehicle_down-02>";
    private static final String STATEMENT_3 = "<necessary, not_issue_enter-ticket, full_parking>";
    private static final String TRAINING_DATA_01 = "mpc_training_data_01.amrep";


    // ---------- Test Data attribute(s)

    private Interpretation interpretation;
    private OldLearner learner;


    // ---------- Test Data Constructor(s)

    private void runComputation(String corpus) {
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + corpus;
        Computation.main(args);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
    }

    private void generateDefinition(String corpus) {
        runComputation(corpus);
        List<WorkData> dataList = Computation.getEvalDataList();
        Assert.assertFalse(dataList.isEmpty());
        this.interpretation = dataList.get(0).getInterpretation();
    }

    public List<Interpretation> generateDefinitionList(String corpus) {
        List<Interpretation> defList = new ArrayList<>();
        runComputation(corpus);
        List<WorkData> dataList = Computation.getEvalDataList();
        Assert.assertFalse(dataList.isEmpty());
        for(WorkData wd: dataList) defList.add(wd.getInterpretation());
        return defList;
    }

    public void printPatternSet() {
        TransPatternSet pSet = learner.getTransductionPatternSet();
        System.out.println("-- Pattern Set (" + pSet.size() + ") " + pSet.toString());
        System.out.println();
        System.out.println("Precision / pattern: ");
        int i = 0;
        //int j = 0;
        for(TransPattern p: pSet.getSet()) {
            i++;
            //if (i > 1) System.out.print("  ");
            String rawQuery = p.getQuery().toString();
            String rawTemplate = p.getTemplate().getRawTemplateString();
            System.out.println("P-" + i + ": \n" +
                    "  -- " + rawQuery + ", \n" +
                    "  -- " + rawTemplate);
            System.out.println("  -- First Line Precision =" + p.getPrecisionFirstLine() +
                    "  /  Second Line Precision =" + p.getPrecisionSecondLine() +
                    "");
        }
    }

    public void printPreparationData(Set<OldArchive> aSet, InterpretationAnalyzer defA) {
        System.out.println("-- Archive Set:\n" + aSet.toString());
        System.out.println("-- Definition Analyzer: " + defA.getInterpretation().toString());
    }

    public void printTokenList(List<Token> tokenList) {
        System.out.println("-- Token Object List: " + tokenList.toString());
        System.out.print("-- Token String List: ");
        for(Token t: tokenList)
            System.out.print("(" + t.getTokenType() + ", " + t.getRawMarkString() + ")  ");
    }

    public void printTemplateSet(Set<Template> templateSet) {
        System.out.println("-- Template Set: ");
        int i = 0;
        for(Template t: templateSet) {
            i++;
            System.out.println("  T-" + i + ": " + t.getRawTemplateString());
        }
    }


    //==================================================================
    // Pattern Set Init Tests
    //==================================================================

    @Test
    public void TransPatternSetInitTest() {
        System.out.println(" *** Transduction Pattern Set Initialization Test 1 ***");
        TransPatternSet pSet = new TransPatternSet();
        System.out.println("Before loading: " + pSet.toString());
        pSet.oldLoad();
        System.out.println("After loading: " + pSet.toString());
        Assert.assertTrue(pSet.size() > 10);
    }


    //==================================================================
    // Learning Method Tests
    //==================================================================

    @Test
    public void PreparationTest1() {
        System.out.println(" *** Preparation Test 1 ***");
        generateDefinition(LEARNING_TEST_DATA_1);
        learner = new OldLearner();
        learner.memorize(interpretation, STATEMENT_1);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        printPreparationData(learner.getArchiveSet(), interpretationAnalyzer);
    }

    @Test
    public void TokenizationTest1() {
        System.out.println(" *** Tokenization Test 1 ***");
        Tokenizer tokenizer = new Tokenizer(STATEMENT_1);
        List<Token> statementTokenList = tokenizer.getTokenList();
        printTokenList(statementTokenList);
    }

    @Test
    public void TokenizationTest2() {
        System.out.println(" *** Tokenization Test 2 ***");
        Tokenizer tokenizer = new Tokenizer(STATEMENT_2);
        List<Token> statementTokenList = tokenizer.getTokenList();
        printTokenList(statementTokenList);
    }

    @Test
    public void TokenizationTest3() {
        System.out.println(" *** Tokenization Test 3 ***");
        Tokenizer tokenizer = new Tokenizer(STATEMENT_3);
        List<Token> statementTokenList = tokenizer.getTokenList();
        printTokenList(statementTokenList);
    }

    @Test
    public void TemplateLearningTest1() {
        System.out.println(" *** Template Learning Test 1 ***");
        generateDefinition(LEARNING_TEST_DATA_1);
        learner = new OldLearner(); // learner initialization
        learner.memorize(interpretation, STATEMENT_1); // preparation
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        System.out.println("-- Definition Analyzer: " + interpretationAnalyzer.getInterpretation().toString());
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;
        String normalizedStatement = NORMALIZER.normalize(STATEMENT_1);
        Tokenizer tokenizer = new Tokenizer(normalizedStatement); // tokenization
        List<Token> tokenList = tokenizer.getTokenList();
        printTokenList(tokenList);
        System.out.println("\n");
        Set<Template> templateSet =
                learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, normalizedStatement);
        printTemplateSet(templateSet);
        templateSet = learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, STATEMENT_1);
        printTemplateSet(templateSet);
        Assert.assertTrue(templateSet.size() > 5);
    }

    @Test
    public void TemplateLearningTest2() {
        System.out.println(" *** Template Learning Test 2 ***");
        generateDefinition(LEARNING_TEST_DATA_2);
        learner = new OldLearner(); // learner initialization
        learner.memorize(interpretation, STATEMENT_2); // preparation
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        System.out.println("-- Definition Analyzer: " + interpretationAnalyzer.getInterpretation().toString());
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;
        String normalizedStatement = NORMALIZER.normalize(STATEMENT_2);
        Tokenizer tokenizer = new Tokenizer(normalizedStatement); // tokenization
        List<Token> tokenList = tokenizer.getTokenList();
        printTokenList(tokenList);
        System.out.println("\n");
        Set<Template> templateSet =
                learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, normalizedStatement);
        printTemplateSet(templateSet);
        templateSet = learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, STATEMENT_2);
        printTemplateSet(templateSet);
        Assert.assertTrue(templateSet.size() >= 0);
    }

    @Test
    public void TemplateLearningTest3() {
        System.out.println(" *** Template Learning Test 3 ***");
        generateDefinition(LEARNING_TEST_DATA_3);
        learner = new OldLearner(); // learner initialization
        learner.memorize(interpretation, STATEMENT_3); // preparation
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        System.out.println("-- Definition Analyzer: " + interpretationAnalyzer.getInterpretation().toString());
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;
        String normalizedStatement = NORMALIZER.normalize(STATEMENT_3);
        Tokenizer tokenizer = new Tokenizer(normalizedStatement); // tokenization
        List<Token> tokenList = tokenizer.getTokenList();
        printTokenList(tokenList);
        System.out.println("\n");
        Set<Template> templateSet =
                learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, normalizedStatement);
        printTemplateSet(templateSet);
        templateSet = learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, STATEMENT_3);
        printTemplateSet(templateSet);
        Assert.assertTrue(templateSet.size() > 0);
    }

    // TODO: specific test for queries infering

    @Test
    public void MainLearningTest1() {
        System.out.println(" *** Main Learning Test 1 ***");
        generateDefinition(LEARNING_TEST_DATA_1);
        learner = new OldLearner();
        learner.learnPatterns(interpretation, STATEMENT_1);
        printPatternSet();
        int resultSize = learner.getTransductionPatternSet().getSet().size();
        Assert.assertTrue(resultSize > 0 && resultSize < 20 );
    }

    @Test
    public void MainLearningTest2() {
        System.out.println(" *** Main Learning Test 2 ***");
        generateDefinition(LEARNING_TEST_DATA_2);
        learner = new OldLearner();
        learner.learnPatterns(interpretation, STATEMENT_2);
        printPatternSet();
        int resultSize = learner.getTransductionPatternSet().getSet().size();
        Assert.assertTrue(resultSize > 0 && resultSize < 20 );
    }

    @Test
    public void MainLearningTest3() {
        System.out.println(" *** Main Learning Test 3 ***");
        generateDefinition(LEARNING_TEST_DATA_3);
        learner = new OldLearner();
        learner.learnPatterns(interpretation, STATEMENT_3);
        printPatternSet();
        int resultSize = learner.getTransductionPatternSet().getSet().size();
        Assert.assertTrue(resultSize > 0 && resultSize < 20 );
    }


    //==================================================================
    // Training Tests
    //==================================================================

    @Test
    public void TrainingTest1() {
        OldLearner learner = new OldLearner();
        List<Interpretation> defList = generateDefinitionList(TRAINING_DATA_01);
        List<String> statementList = new ArrayList<>();
        statementList.add("<necessary, open-01_gate, issue-01_ticket>");
        statementList.add("<necessary, open-01_gate, issue-01_ticket>");
        statementList.add("<necessary, open-01_gate, issue-01_ticket>");
        statementList.add("<necessary, not_open-01_gate, close-01_gate>");
        statementList.add("<necessary, close-01_gate, vehicle_pass-01_dock-01>");
        int limit = Math.min(defList.size(), statementList.size());
        TransPatternSet result = new TransPatternSet();
        for(int i = 0; i < limit; i++) {
            learner.learnPatterns(defList.get(i), statementList.get(i));
            result = learner.getTransductionPatternSet();
            System.out.println("Training Step " + i);
            System.out.println(result.toString());
            for(TransPattern p: result.getSet()) System.out.println(p.toString());
        }
        result = learner.getTransductionPatternSet();
        System.out.println("Training Results");
        System.out.println(result.toString());
        for(TransPattern p: result.getSet()) System.out.println(p.toString());
        Assert.assertTrue(result.getSet().size() > 0 );
    }

}
