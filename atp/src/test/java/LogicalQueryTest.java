import AbstractSemantic.Interpretation;
import LogicalQuery.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class LogicalQueryTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Test Data Constructor(s)

    public Query genQuery1() {
        List<String> variables = new ArrayList<>();
        variables.add("x4");
        variables.add("x6");
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("ARG1", "x4", "x2"));
        atoms.add(new Atom("MODALITY", "x6"));
        return new Query(variables, atoms);
    }

    public Query genQuery2() {
        List<String> variables = new ArrayList<>();
        variables.add("x1");
        variables.add("x2");
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("ARG1", "x1", "x2"));
        atoms.add(new Atom("MODALITY", "x1"));
        return new Query(variables, atoms);
    }

    public Query genQuery3() {
        List<String> variables = new ArrayList<>(Arrays.asList("a", "b", "c"));
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("isModality", "a"));
        atoms.add(new Atom("isProperty", "b"));
        atoms.add(new Atom("isProperty", "c"));
        atoms.add(new Atom("arg+", "a", "b"));
        atoms.add(new Atom("condition", "a", "c"));
        return new Query(variables, atoms);
    }

    public Query genQuery4() {
        List<String> variables = new ArrayList<>(Arrays.asList("a"));
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("is#property", "a"));
        return new Query(variables, atoms);
    }

    public Query genQuery5() {
        List<String> variables = new ArrayList<>(Arrays.asList("a", "b", "c"));
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("is#modality", "a"));
        atoms.add(new Atom("is#property", "b"));
        atoms.add(new Atom("is#context", "c"));
        atoms.add(new Atom("domain", "a", "b"));
        atoms.add(new Atom("time", "a", "c"));
        return new Query(variables, atoms);
    }

    public Query genQuery6() {
        List<String> variables = new ArrayList<>(Arrays.asList("x1", "x0", "x3"));
        List<Atom> atoms = new ArrayList<>();
        atoms.add(new Atom("isModality", "x1"));
        atoms.add(new Atom("isProperty", "x0"));
        atoms.add(new Atom("isProperty", "x3"));
        atoms.add(new Atom("arg+", "x1", "x0"));
        atoms.add(new Atom("condition", "x1", "x3"));
        return new Query(variables, atoms);
    }

    public Interpretation genInterpretation1() {
        //Map<String, Map<String, String>> dataMap = new HashMap<>();
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("ARG1", "y1", "y2"));
        atomSet.add(new Atom("MODALITY", "y1"));
        return new Interpretation("I-01", atomSet);
    }

    public Interpretation genInterpretation2() {
        //Map<String, Map<String, String>> dataMap = new HashMap<>();
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(new Atom("isModality", "prohibit"));
        atomSet.add(new Atom("isProperty", "close"));
        atomSet.add(new Atom("isEntity", "gate"));
        atomSet.add(new Atom("isProperty", "pass"));
        atomSet.add(new Atom("isEntity", "vehicle"));
        atomSet.add(new Atom("arg+", "prohibit", "close"));
        atomSet.add(new Atom("arg+", "close", "gate"));
        atomSet.add(new Atom("arg+", "pass", "vehicle"));
        atomSet.add(new Atom("condition", "prohibit", "pass"));
        return new Interpretation("I-02", atomSet);
    }


    //==================================================================
    // Variable Class Tests
    //==================================================================

    @Test
    public void VariableTest1() {

        System.out.println();
        System.out.println(" *** Variable Test 1 *** ");

        Variable v1 = new Variable("x", "1");
        System.out.println("v1: " + v1.toString());
        Variable v2 = new Variable("y", "2");
        System.out.println("v2: " + v2.toString());

        Assert.assertEquals("(x, 1)", v1.toString());
        Assert.assertEquals("(y, 2)", v2.toString());

    }

    @Test
    public void VariableTest2() {

        System.out.println();
        System.out.println(" *** Variable Test 2 *** ");

        Variable v1 = new Variable("x", "1");
        System.out.println(" -- Variable v1: " + v1.toString());
        System.out.println(" ----- ident: " + v1.getIdent());
        System.out.println(" ----- init value: " + v1.getValue());
        v1.setValue("3");
        System.out.println(" ------ new value (1): " + v1.getValue());
        v1.setValue("2");
        System.out.println(" ------ new value (2): " + v1.getValue());

        Assert.assertEquals("(x, 2)", v1.toString());
        Assert.assertEquals("x", v1.getIdent());
        Assert.assertEquals("2", v1.getValue());

    }


    //==================================================================
    // Atom Class Tests
    //==================================================================

    // ----- Construction Tests

    @Test
    public void AtomConstructionTest1() {
        System.out.println();
        System.out.println(" *** Atom Construction Test 1 *** ");
        Atom a2 = new Atom("MODALITY", "x6");
        System.out.println("a2: " + a2.toString());
        Assert.assertEquals("MODALITY(x6)", a2.toString());
    }

    @Test
    public void AtomConstructionTest2() {
        System.out.println();
        System.out.println(" *** Atom Construction Test 2 *** ");
        Atom a1 = new Atom("ARG1", "x4", "x2");
        System.out.println("a1: " + a1.toString());
        Assert.assertEquals("ARG1(x4, x2)", a1.toString());
    }


    // ----- Accessor Tests (Getters)

    @Test
    public void AtomAccessorTest1() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 1 *** ");
        Atom a1 = new Atom("ARG1", "x4", "x2");
        System.out.println("a1: " + a1.toString());
        System.out.println("relation(a1)= " + a1.getRelation());
        System.out.println("args(a1)= " + a1.getArgs().toString());
        Assert.assertEquals("ARG1", a1.getRelation());
        Assert.assertEquals("[x4, x2]", a1.getArgs().toString());
    }

    @Test
    public void AtomAccessorTest2() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 2 *** ");
        Atom a2 = new Atom("MODALITY", "x6");
        System.out.println("a2: " + a2.toString());
        System.out.println("relation(a2)= " + a2.getRelation());
        System.out.println("args(a2)= " + a2.getArgs().toString());
        Assert.assertEquals("MODALITY", a2.getRelation());
        Assert.assertEquals("[x6]", a2.getArgs().toString());
    }

    @Test
    public void AtomAccessorTest3() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 3 *** ");
        Atom a3 = new Atom("TEST");
        System.out.println("a3: " + a3.toString());
        System.out.println("relation(a3)= " + a3.getRelation());
        System.out.println("args(a3)= " + a3.getArgs().toString());
        Assert.assertEquals("TEST", a3.getRelation());
        Assert.assertEquals("[]", a3.getArgs().toString());
    }


    // ----- Accessor Tests (Predicates)

    @Test
    public void AtomAccessorTest4() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 4 *** ");
        Atom atom = new Atom("MODALITY", "x6");
        System.out.println("atom: " + atom.toString());
        System.out.println("isNegation(atom)= " + atom.isNegation());
        Assert.assertFalse(atom.isNegation());
    }

    @Test
    public void AtomAccessorTest5() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 5 *** ");
        Atom atom = new Atom("Not MODALITY", "x6");
        System.out.println("atom: " + atom.toString());
        System.out.println("isNegation(atom)= " + atom.isNegation());
        Assert.assertTrue(atom.isNegation());
    }

    @Test
    public void AtomAccessorTest6() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 6 *** ");
        Atom atom = new Atom("No MODALITY", "x6");
        System.out.println("atom: " + atom.toString());
        System.out.println("isNegation(atom)= " + atom.isNegation());
        Assert.assertTrue(atom.isNegation());
    }

    @Test
    public void AtomAccessorTest7() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 7 *** ");
        Atom atom = new Atom("No MODALITY", "x6");
        Set<Atom> atomSet1 = new HashSet<>();
        Set<Atom> atomSet2 = new HashSet<>();
        atomSet1.add(new Atom("test", "x4", "x2"));
        atomSet1.add(new Atom("modality", "x4", "x2"));
        atomSet2.add(new Atom("test2", "x4", "x2"));
        boolean c1 = atom.isAbstract();
        boolean c2 = atom.isRelativeTo(atomSet1);
        boolean c3 = atom.isRelativeTo(atomSet2);
        System.out.println("atom: " + atom.toString());
        System.out.println("isAbstract(atom)= " + c1);
        System.out.println("isRelativeTo(atom, conceptSet1)= " + c2);
        System.out.println("isRelativeTo(atom, conceptSet2)= " + c3);
        Assert.assertFalse(c1);
        Assert.assertTrue(c2);
        Assert.assertFalse(c3);
    }

    @Test
    public void AtomAccessorTest8() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 8 *** ");
        Atom atom = new Atom("No #MODALITY", "x6");
        Set<Atom> atomSet1 = new HashSet<>();
        Set<Atom> atomSet2 = new HashSet<>();
        atomSet1.add(new Atom("test", "x4", "x2"));
        atomSet1.add(new Atom("#modality", "x4", "x2"));
        atomSet2.add(new Atom("test2", "x4", "x2"));
        boolean c1 = atom.isAbstract();
        boolean c2 = atom.isRelativeTo(atomSet1);
        boolean c3 = atom.isRelativeTo(atomSet2);
        System.out.println("atom: " + atom.toString());
        System.out.println("isAbstract(atom)= " + c1);
        System.out.println("isRelativeTo(atom, conceptSet1)= " + c2);
        System.out.println("isRelativeTo(atom, conceptSet2)= " + c3);
        Assert.assertTrue(c1);
        Assert.assertTrue(c2);
        Assert.assertFalse(c3);
    }

    @Test
    public void AtomAccessorTest9() {
        System.out.println();
        System.out.println(" *** Atom Accessor Test 9 *** ");
        Atom atom = new Atom("is#Context", "x6");
        Set<Atom> atomSet1 = new HashSet<>();
        Set<Atom> atomSet2 = new HashSet<>();
        atomSet1.add(new Atom("test", "x4", "x2"));
        atomSet1.add(new Atom("#modality", "x4", "x2"));
        atomSet1.add(new Atom("is#Context", "x4", "x2"));
        atomSet2.add(new Atom("test2", "x4", "x2"));
        boolean c1 = atom.isAbstract();
        boolean c2 = atom.isRelativeTo(atomSet1);
        boolean c3 = atom.isRelativeTo(atomSet2);
        System.out.println("atom: " + atom.toString());
        System.out.println("isAbstract(atom)= " + c1);
        System.out.println("isRelativeTo(atom, conceptSet1)= " + c2);
        System.out.println("isRelativeTo(atom, conceptSet2)= " + c3);
        Assert.assertTrue(c1);
        Assert.assertTrue(c2);
        Assert.assertFalse(c3);
    }


    // ----- Base Method Tests (Substitution / Comparison)

    @Test
    public void AtomBasicTest1() {
        System.out.println();
        System.out.println(" *** Atom Basic Test 1 *** ");
        Atom atom = new Atom("ARG1", "x4", "x2");
        atom.substitute("x2", "good");
        System.out.println("atom: " + atom.toString());
        System.out.println("relation(atom)= " + atom.getRelation());
        System.out.println("args(atom)= " + atom.getArgs().toString());
        Assert.assertEquals("[x4, good]", atom.getArgs().toString());
    }

    @Test
    public void AtomBasicTest2() {
        System.out.println();
        System.out.println(" *** Atom Basic Test 2 *** ");
        Atom a1 = new Atom("ARG1", "x4", "x2");
        Atom a2 = new Atom("ARG1", "x4", "x2");
        Atom a3 = new Atom("ARG1", "y", "z");
        System.out.println("a1 == a2? " + a1.equals(a2));
        System.out.println("a1 == a3? " + a1.equals(a3));
        Assert.assertEquals(a1, a2);
        Assert.assertNotEquals(a1, a3);
        Assert.assertNotEquals(a2, a3);
    }


    // ----- Satisfaction Relation Tests

    @Test
    public void AtomSatTest1() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 1 *** ");
        Atom atom1 = new Atom("ARG1", "x1", "x2");
        Atom atom2 = new Atom("ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
    }

    @Test
    public void AtomSatTest2() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 2 *** ");
        Atom atom1 = new Atom("ARG1", "x1", "x2");
        Atom atom2 = new Atom("ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x1", "y1");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
    }

    @Test
    public void AtomSatTest3() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 3 *** ");
        Atom atom1 = new Atom("ARG1", "x1", "x2");
        Atom atom2 = new Atom("ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x2", "y2");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
    }

    @Test
    public void AtomSatTest4() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 4 *** ");
        Atom atom1 = new Atom("ARG1", "x1", "x2");
        Atom atom2 = new Atom("ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x2", "y1");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertFalse(result);
    }

    @Test
    public void AtomSatTest5() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 5 *** ");
        Atom atom1 = new Atom("ARG1", "x1", "x2");
        Atom atom2 = new Atom("not ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x2", "y2");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertFalse(result);
    }

    @Test
    public void AtomSatTest6() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 6 *** ");
        Atom atom1 = new Atom("not ARG1", "x1", "x2");
        Atom atom2 = new Atom("not ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x2", "y2");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
    }

    @Test
    public void AtomSatTest7() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 7 *** ");
        Atom atom1 = new Atom("not ARG1", "x1", "x2");
        Atom atom2 = new Atom("not ARG1", "y1");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x1", "y1");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertFalse(result);
    }

    @Test
    public void AtomSatTest8() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 8 *** ");
        Atom atom1 = new Atom("not ARG1", "x1", "x2");
        Atom atom2 = new Atom("not ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x1", "y1");
        assignment.assign("x2", "y2");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
    }

    @Test
    public void AtomSatTest9() {
        System.out.println();
        System.out.println(" *** Atom Sat Test 9 *** ");
        Atom atom1 = new Atom("not ARG1", "x1", "x2");
        Atom atom2 = new Atom("not ARG1", "y1", "y2");
        Set<String> variableIdSet = new HashSet<>(Arrays.asList("x1", "x2"));
        Assignment assignment = new Assignment(variableIdSet);
        assignment.assign("x2", "y2");
        assignment.assign("x1", "y1");
        boolean result = atom1.satisfy(atom2, assignment);
        System.out.println(" -- Atom 1: " + atom1.toString());
        System.out.println(" -- Atom 2: " + atom2.toString());
        System.out.println(" -- Assignment: " + assignment.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
    }


    //==================================================================
    // Query Class Tests
    //==================================================================

    // ----- Construction Tests

    @Test
    public void QueryConstructionTest1() {
        System.out.println();
        System.out.println(" **** Query Construction Test 1 *** ");
        Query query = genQuery1();
        System.out.println(" -- Query: " + query.toString());
        String expectedRawQuery = "lambda x4. lambda x6. ARG1(x4, x2) AND MODALITY(x6)";
        Assert.assertEquals(expectedRawQuery, query.toString());
    }


    // ----- Accessor Tests

    @Test
    public void QueryAccessorTest1() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 1 *** ");
        Query query = genQuery1();
        List<String> varIdList = query.getVariableIdList();
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Variable Id List: " + varIdList.toString());
        System.out.println(" -- Variables Number: " + query.getVariablesNumber());
        Assert.assertEquals("[x4, x6]", varIdList.toString());
        Assert.assertEquals(2, query.getVariablesNumber());
    }

    @Test
    public void QueryAccessorTest2() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 2 *** ");
        Query query = genQuery1();
        List<Atom> atomList = query.getAtomList();
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Atom List: " + atomList.toString());
        Assert.assertEquals("[ARG1(x4, x2), MODALITY(x6)]", atomList.toString());
    }

    @Test
    public void QueryAccessorTest3() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 3 *** ");
        Query query = genQuery1();
        boolean satResult = query.isSatisfiable();
        List<Assignment> assignmentList = query.getAssignmentList();
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Satisfiable? " + satResult);
        System.out.println(" -- Assignment List: " + assignmentList.toString());
        Assert.assertFalse(satResult);
        Assert.assertEquals("[]", assignmentList.toString());
    }

    @Test
    public void QueryAccessorTest4() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 4 *** ");
        Query query = genQuery3();
        boolean satResult = query.isAbstract();
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Abstract? " + satResult);
        Assert.assertFalse(satResult);
    }

    @Test
    public void QueryAccessorTest5() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 5 *** ");
        Query query = genQuery4();
        boolean satResult = query.isAbstract();
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Abstract? " + satResult);
        Assert.assertTrue(satResult);
    }

    @Test
    public void QueryAccessorTest6() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 6 *** ");
        Query query = genQuery5();
        boolean satResult = query.isAbstract();
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Abstract? " + satResult);
        Assert.assertTrue(satResult);
    }

    @Test
    public void QueryAccessorTest7() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 7 *** ");
        Query query = genQuery5();
        Set<Atom> workAtomSet = new HashSet<>();
        workAtomSet.add(new Atom("is#modality", "x"));
        workAtomSet.add(new Atom("is#property", "y"));
        boolean satResult = query.isDeeperAbstract(workAtomSet);
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Deeper Abstract? " + satResult);
        Assert.assertTrue(satResult);
    }

    @Test
    public void QueryAccessorTest8() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 8 *** ");
        Query query = genQuery5();
        Set<Atom> workAtomSet = new HashSet<>();
        workAtomSet.add(new Atom("is#modality", "x"));
        workAtomSet.add(new Atom("is#property", "y"));
        workAtomSet.add(new Atom("is#context", "z"));
        boolean satResult = query.isDeeperAbstract(workAtomSet);
        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Deeper Abstract? " + satResult);
        Assert.assertFalse(satResult);
    }

    @Test
    public void QueryAccessorTest9() {
        System.out.println();
        System.out.println(" **** Query Accessor Test 9 *** ");
        Query query = genQuery3();
        System.out.println(" -- Query (before change): " + query.toString());
        Assert.assertTrue(query.getVariableIdList().contains("a"));
        Assert.assertFalse(query.getVariableIdList().contains("x1"));
        query.changeVariableId("a", "x1");
        System.out.println(" -- Query (after change): " + query.toString());
        Assert.assertFalse(query.getVariableIdList().contains("a"));
        Assert.assertTrue(query.getVariableIdList().contains("x1"));
    }

    @Test
    public void QueryEqualityTest1() {
        System.out.println();
        System.out.println(" **** Query Equality Test 1 *** ");
        Query q1 = genQuery6();
        Query q2 = genQuery6();
        q1.normalize();
        q2.normalize();
        System.out.println(" -- Q1: " + q1.toString());
        System.out.println(" -- Q2: " + q2.toString());
        Assert.assertTrue(q1.equals(q2));
    }

    @Test
    public void QueryEqualityTest2() {
        System.out.println();
        System.out.println(" **** Query Equality Test 2 *** ");
        Query q1 = genQuery6();
        Query q2 = genQuery5();
        q1.normalize();
        q2.normalize();
        System.out.println(" -- Q1: " + q1.toString());
        System.out.println(" -- Q2: " + q2.toString());
        Assert.assertFalse(q1.equals(q2));
    }

    @Test
    public void QueryEqualityTest3a() {
        System.out.println();
        System.out.println(" **** Query Equality Test 3 (a) *** ");
        List<Query> qList1 = new ArrayList<>();
        qList1.add(genQuery6());
        qList1.add(genQuery5());
        qList1.add(genQuery6());
        for (Query q: qList1) q.normalize();
        System.out.println();
        System.out.println(" -- Query List 1: ");
        for (Query q: qList1) System.out.println(" ----- " + q.toString());
        List<Query> qList2 = new ArrayList<>();
        for (Query q: qList1) if (!qList2.contains(q)) qList2.add(q);
        System.out.println();
        System.out.println(" -- Query List 2: ");
        for (Query q: qList2) System.out.println(" ----- " + q.toString());
        Assert.assertEquals(3, qList1.size());
        Assert.assertEquals(2, qList2.size());
    }

    @Test
    public void QueryEqualityTest3b() {
        System.out.println();
        System.out.println(" **** Query Equality Test 3 (b) *** ");
        List<Query> qList = new ArrayList<>();
        qList.add(genQuery6());
        qList.add(genQuery5());
        qList.add(genQuery6());
        for (Query q: qList) q.normalize();
        System.out.println();
        System.out.println(" -- Query List: ");
        for (Query q: qList) System.out.println(" ----- " + q.toString());
        Set<Query> qSet = new HashSet<>();
        qSet.addAll(qList);
        System.out.println();
        System.out.println(" -- Query Set: ");
        for (Query q: qSet) System.out.println(" ----- " + q.toString());
        Assert.assertEquals(3, qList.size());
        Assert.assertEquals(2, qSet.size());
    }


    // ----- Normalizing Method Tests

    @Test
    public void NormalizeMethodTest1() {
        System.out.println();
        System.out.println(" **** Normalize Method Test 1 *** ");
        Query query = genQuery1();
        System.out.println(" -- Query (before change): " + query.toString());
        Assert.assertTrue(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x4", "x6"})));
        Assert.assertFalse(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x1", "x2"})));
        query.normalizePass1();
        System.out.println(" -- Query (after normalizing pass 1): " + query.toString());
        query.normalizePass2();
        System.out.println(" -- Query (after normalizing pass 2): " + query.toString());
        Assert.assertTrue(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x0", "x1"})));
    }

    @Test
    public void NormalizeMethodTest2() {
        System.out.println();
        System.out.println(" **** Normalize Method Test 2 *** ");
        Query query = genQuery2();
        System.out.println(" -- Query (before change): " + query.toString());
        Assert.assertTrue(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x1", "x2"})));
        query.normalizePass1();
        System.out.println(" -- Query (after normalizing pass 1): " + query.toString());
        query.normalizePass2();
        System.out.println(" -- Query (after normalizing pass 2): " + query.toString());
        Assert.assertTrue(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x0", "x1"})));
    }

    @Test
    public void NormalizeMethodTest3() {
        System.out.println();
        System.out.println(" **** Normalize Method Test 3 *** ");
        Query query = genQuery3();
        System.out.println(" -- Query (before change): " + query.toString());
        Assert.assertTrue(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"a", "b", "c"})));
        Assert.assertFalse(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x0", "x1", "x2"})));
        query.normalizePass1();
        System.out.println(" -- Query (after normalizing pass 1): " + query.toString());
        query.normalizePass2();
        System.out.println(" -- Query (after normalizing pass 2): " + query.toString());
        Assert.assertFalse(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"a", "b", "c"})));
        Assert.assertTrue(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x0", "x1", "x2"})));
    }

    @Test
    public void NormalizeMethodTest4() {
        System.out.println();
        System.out.println(" **** Normalize Method Test 4 *** ");
        Query query = genQuery6();
        Interpretation interpretation = genInterpretation2();
        System.out.println(" -- Query (before change): " + query.toString());
        Assert.assertTrue(query.getAtomList().contains(new Atom("arg+", "x1", "x0")));
        Assert.assertFalse(query.getAtomList().contains(new Atom("arg+", "x0", "x1")));
        boolean result = query.satisfy(interpretation);
        System.out.println(" -- Interpretation: " + interpretation.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
        query.normalizePass1();
        System.out.println(" -- Query (after normalizing pass 1): " + query.toString());
        query.normalizePass2();
        System.out.println(" -- Query (after normalizing pass 2): " + query.toString());
        Assert.assertTrue(query.getVariableIdList().containsAll(Arrays.asList(new String[]{"x0", "x1", "x2"})));
        Assert.assertTrue(query.getAtomList().contains(new Atom("arg+", "x0", "x1")));
        result = query.satisfy(interpretation);
        System.out.println(" -- Interpretation: " + interpretation.toString());
        System.out.println(" -- Satisfy Result: " + result);
        Assert.assertTrue(result);
    }


    // ----- Satisfaction Relation Tests

    @Test
    public void QuerySatTest1() {

        System.out.println();
        System.out.println(" **** Query Satisfaction Test 1 *** ");

        Query query = genQuery2();
        Interpretation interpretation = new Interpretation("I");

        boolean result = query.satisfy(interpretation);
        boolean satResult = query.isSatisfiable();
        List<Assignment> assignmentList = query.getAssignmentList();

        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Interpretation: " + interpretation.toString());
        System.out.println(" -- Satisfy Result: " + result);
        System.out.println(" -- Satisfiable? " + satResult);
        System.out.println(" -- Assignment List: " + assignmentList.toString());

        Assert.assertFalse(result);
        Assert.assertFalse(satResult);
        Assert.assertEquals("[]", assignmentList.toString());

    }

    @Test
    public void QuerySatTest2() {

        System.out.println();
        System.out.println(" **** Query Satisfaction Test 2 *** ");

        Query query = genQuery2();
        Interpretation interpretation = genInterpretation1();

        boolean result = query.satisfy(interpretation);
        boolean satResult = query.isSatisfiable();
        List<Assignment> assignmentList = query.getAssignmentList();

        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Interpretation: " + interpretation.toString());
        System.out.println(" -- Satisfy Result: " + result);
        System.out.println(" -- Satisfiable? " + satResult);
        System.out.println(" -- Assignment List: " + assignmentList.toString());

        Assert.assertTrue(result);
        Assert.assertTrue(satResult);
        Assert.assertEquals(
                "[Assignment{variableSet=[x1, x2], assignment={x1=y1, x2=y2}}]",
                assignmentList.toString());

    }

    @Test
    public void QuerySatTest3() {

        System.out.println();
        System.out.println(" **** Query Satisfaction Test 3 *** ");

        Query query = genQuery3();
        Interpretation interpretation = genInterpretation2();

        boolean result = query.satisfy(interpretation);
        boolean satResult = query.isSatisfiable();
        List<Assignment> assignmentList = query.getAssignmentList();

        System.out.println(" -- Query: " + query.toString());
        System.out.println(" -- Interpretation: " + interpretation.toString());
        System.out.println(" -- Satisfy Result: " + result);
        System.out.println(" -- Satisfiable? " + satResult);
        System.out.println(" -- Assignment List: " + assignmentList.toString());

        Assert.assertTrue(result);
        Assert.assertTrue(satResult);
        Assert.assertEquals(
                "[Assignment{variableSet=[a, b, c], assignment={a=prohibit, b=close, c=pass}}]",
                assignmentList.toString());

    }


}
