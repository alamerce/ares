import AbstractSemantic.*;
import AbstractSemantic.LearnerObject.InterpretationAnalyzer;
import AbstractSemantic.LearnerObject.TrainingData;
import AbstractSemantic.TransPatternObject.Template;
import Utility.Tokenizer;
import AbstractSemantic.TransPatternObject.Token;
import ComputationObject.WorkData;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static Parameters.Configuration.NORMALIZER;
import static Parameters.Configuration.OUTPUT_ABSTRACT_CONCEPT;

public class NewLearnerTest {

    //==================================================================
    // Attributes and useful method(s)
    //==================================================================

    // ---------- Constant attribute(s)

    private static final String TEST_RESOURCES_PATH = "src/test/resources/";
    private static final String LEARNING_TEST_DATA_1 = "learning_test_data_1.amrep";
    private static final String LEARNING_TEST_DATA_2 = "learning_test_data_2.amrep";
    private static final String LEARNING_TEST_DATA_3 = "learning_test_data_3.amrep";
    private static final String STATEMENT_1 = "<impossible, pass_vehicle, close_gate>";
    private static final String STATEMENT_2 = "<necessary, not close_exit-gate, after vehicle_down-02>";
    private static final String STATEMENT_3 = "<necessary, not_issue_enter-ticket, full_parking>";
    private static final String TRAINING_DATA_01 = "mpc_training_data_01.amrep";


    // ---------- Test Data Constructor(s)

    private void runComputation(String corpus) {
        String[] args = new String[1];
        args[0] = TEST_RESOURCES_PATH + corpus;
        Computation.main(args);
        Assert.assertTrue(Computation.isSemanticAnalyzeStepValid());
        Assert.assertTrue(Computation.hasIdentifiedRequirement());
    }

    private TrainingData genTrainingData(String corpus) {
        runComputation(corpus);
        List<WorkData> dataList = Computation.getEvalDataList();
        Assert.assertFalse(dataList.isEmpty());
        WorkData wd = dataList.get(0);
        Interpretation interpretation = wd.getInterpretation();
        String expectedOS = wd.getExpectedOutputStatementList().get(0);
        return new TrainingData(1, interpretation, expectedOS);
    }


    public Set<TrainingData> genTrainingDataSet(String corpus) {

        Set<TrainingData> trainingDataSet = new HashSet<>();

        runComputation(corpus);
        List<WorkData> dataList = Computation.getEvalDataList();
        Assert.assertFalse(dataList.isEmpty());

        int count = 0;
        for(WorkData wd: dataList) {
            Interpretation interpretation = wd.getInterpretation();
            for (String expOS: wd.getExpectedOutputStatementList()) {
                count++;
                TrainingData trainData;
                trainData = new TrainingData(count, interpretation, expOS);
                trainingDataSet.add(trainData);
            }
        }

        return trainingDataSet;

    }


    public void printPatternSet(NewLearner learner) {
        TransPatternSet pSet = learner.getTransductionPatternSet();
        System.out.println("-- Pattern Set (" + pSet.size() + ") " + pSet.toString());
        System.out.println();
        System.out.println("Precision / pattern: ");
        int i = 0;
        //int j = 0;
        for(TransPattern p: pSet.getSet()) {
            i++;
            //if (i > 1) System.out.print("  ");
            String rawQuery = p.getQuery().toString();
            String rawTemplate = p.getTemplate().getRawTemplateString();
            System.out.println("P-" + i + ": \n" +
                    "  -- " + rawQuery + ", \n" +
                    "  -- " + rawTemplate);
            System.out.println("  -- First Line Precision =" + p.getPrecisionFirstLine() +
                    "  /  Second Line Precision =" + p.getPrecisionSecondLine() +
                    "");
        }
    }

    public void printPreparationData(Set<TrainingData> tdSet, InterpretationAnalyzer intA) {
        System.out.println("-- Training Data Set (" + tdSet.size() + "):");
        for(TrainingData td: tdSet) System.out.println("     " + td.toString());
        System.out.println("-- Interpretation Analyzer: " + intA.getInterpretation().toString());
    }

    public void printTokenList(List<Token> tokenList) {
        System.out.println("-- Token Object List: " + tokenList.toString());
        System.out.print("-- Token String List: ");
        for(Token t: tokenList)
            System.out.print("(" + t.getTokenType() + ", " + t.getRawMarkString() + ")  ");
    }

    public void printTemplateSet(Set<AbstractSemantic.TransPatternObject.Template> templateSet) {
        System.out.println("-- Template Set: ");
        int i = 0;
        for(AbstractSemantic.TransPatternObject.Template t: templateSet) {
            i++;
            System.out.println("  T-" + i + ": " + t.getRawTemplateString());
        }
    }


    //==================================================================
    // Pattern Set Init Tests
    //==================================================================

    @Test
    public void TransPatternSetInitTest() {
        System.out.println(" *** Transduction Pattern Set Initialization Test 1 ***");
        TransPatternSet pSet = new TransPatternSet();
        System.out.println("Before loading: " + pSet.toString());
        pSet.oldLoad();
        System.out.println("After loading: " + pSet.toString());
        Assert.assertTrue(pSet.size() > 10);
    }


    //==================================================================
    // Learning Method Tests
    //==================================================================

    @Test
    public void PreparationTest1() {
        System.out.println(" *** Preparation Test 1 ***");
        TrainingData trainingData = genTrainingData(LEARNING_TEST_DATA_1);
        Interpretation interpretation =
                trainingData.getInterpretationRun().getLastInterpretation();
        Set<TrainingData> trainingDataSet = new HashSet<>();
        trainingDataSet.add(trainingData);
        NewLearner learner = new NewLearner(trainingDataSet);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        printPreparationData(learner.getTrainingDataSet(), interpretationAnalyzer);
    }

    @Test
    public void TokenizationTest1() {
        System.out.println(" *** Tokenization Test 1 ***");
        Tokenizer tokenizer = new Tokenizer(STATEMENT_1);
        List<Token> statementTokenList = tokenizer.getTokenList();
        printTokenList(statementTokenList);
    }

    @Test
    public void TokenizationTest2() {
        System.out.println(" *** Tokenization Test 2 ***");
        Tokenizer tokenizer = new Tokenizer(STATEMENT_2);
        List<Token> statementTokenList = tokenizer.getTokenList();
        printTokenList(statementTokenList);
    }

    @Test
    public void TokenizationTest3() {
        System.out.println(" *** Tokenization Test 3 ***");
        Tokenizer tokenizer = new Tokenizer(STATEMENT_3);
        List<Token> statementTokenList = tokenizer.getTokenList();
        printTokenList(statementTokenList);
    }

    @Test
    public void TemplateLearningTest1() {
        System.out.println(" *** Template Learning Test 1 ***");
        TrainingData trainingData = genTrainingData(LEARNING_TEST_DATA_1);
        Interpretation interpretation =
                trainingData.getInterpretationRun().getLastInterpretation();
        Set<TrainingData> trainingDataSet = new HashSet<>();
        trainingDataSet.add(trainingData);
        NewLearner learner = new NewLearner(trainingDataSet);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        System.out.println("-- Definition Analyzer: " + interpretationAnalyzer.getInterpretation().toString());
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;
        String normalizedStatement = NORMALIZER.normalize(STATEMENT_1);
        Tokenizer tokenizer = new Tokenizer(normalizedStatement); // tokenization
        List<Token> tokenList = tokenizer.getTokenList();
        printTokenList(tokenList);
        System.out.println("\n");
        Set<Template> templateSet =
                learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, normalizedStatement);
        printTemplateSet(templateSet);
        templateSet = learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, STATEMENT_1);
        printTemplateSet(templateSet);
        Assert.assertTrue(templateSet.size() > 5);
    }

    @Test
    public void TemplateLearningTest2() {
        System.out.println(" *** Template Learning Test 2 ***");
        TrainingData trainingData = genTrainingData(LEARNING_TEST_DATA_2);
        Interpretation interpretation =
                trainingData.getInterpretationRun().getLastInterpretation();
        Set<TrainingData> trainingDataSet = new HashSet<>();
        trainingDataSet.add(trainingData);
        NewLearner learner = new NewLearner(trainingDataSet);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        System.out.println("-- Definition Analyzer: " + interpretationAnalyzer.getInterpretation().toString());
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;
        String normalizedStatement = NORMALIZER.normalize(STATEMENT_2);
        Tokenizer tokenizer = new Tokenizer(normalizedStatement); // tokenization
        List<Token> tokenList = tokenizer.getTokenList();
        printTokenList(tokenList);
        System.out.println("\n");
        Set<Template> templateSet =
                learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, normalizedStatement);
        printTemplateSet(templateSet);
        templateSet = learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, STATEMENT_2);
        printTemplateSet(templateSet);
        Assert.assertTrue(templateSet.size() >= 0);
    }

    @Test
    public void TemplateLearningTest3() {
        System.out.println(" *** Template Learning Test 3 ***");
        TrainingData trainingData = genTrainingData(LEARNING_TEST_DATA_3);
        Interpretation interpretation =
                trainingData.getInterpretationRun().getLastInterpretation();
        Set<TrainingData> trainingDataSet = new HashSet<>();
        trainingDataSet.add(trainingData);
        NewLearner learner = new NewLearner(trainingDataSet);
        InterpretationAnalyzer interpretationAnalyzer = new InterpretationAnalyzer(interpretation);
        System.out.println("-- Definition Analyzer: " + interpretationAnalyzer.getInterpretation().toString());
        String templateConcept = OUTPUT_ABSTRACT_CONCEPT;
        String normalizedStatement = NORMALIZER.normalize(STATEMENT_3);
        Tokenizer tokenizer = new Tokenizer(normalizedStatement); // tokenization
        List<Token> tokenList = tokenizer.getTokenList();
        printTokenList(tokenList);
        System.out.println("\n");
        Set<Template> templateSet =
                learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, normalizedStatement);
        printTemplateSet(templateSet);
        templateSet = learner.learnTemplates(interpretationAnalyzer, templateConcept, tokenList, STATEMENT_3);
        printTemplateSet(templateSet);
        Assert.assertTrue(templateSet.size() > 0);
    }

    // TODO: specific test for queries infering

    @Test
    public void MainLearningTest1() {
        System.out.println(" *** Main Learning Test 1 ***");
        TrainingData trainingData = genTrainingData(LEARNING_TEST_DATA_1);
        Interpretation interpretation =
                trainingData.getInterpretationRun().getLastInterpretation();
        Set<TrainingData> trainingDataSet = new HashSet<>();
        trainingDataSet.add(trainingData);
        NewLearner learner = new NewLearner(trainingDataSet);
        learner.learnPatterns(interpretation, STATEMENT_1);
        printPatternSet(learner);
        int resultSize = learner.getTransductionPatternSet().getSet().size();
        Assert.assertTrue(resultSize > 0 && resultSize < 20 );
    }

    @Test
    public void MainLearningTest2() {
        System.out.println(" *** Main Learning Test 2 ***");
        TrainingData trainingData = genTrainingData(LEARNING_TEST_DATA_2);
        Interpretation interpretation =
                trainingData.getInterpretationRun().getLastInterpretation();
        Set<TrainingData> trainingDataSet = new HashSet<>();
        trainingDataSet.add(trainingData);
        NewLearner learner = new NewLearner(trainingDataSet);
        learner.learnPatterns(interpretation, STATEMENT_2);
        printPatternSet(learner);
        int resultSize = learner.getTransductionPatternSet().getSet().size();
        Assert.assertTrue(resultSize > 0 && resultSize < 20 );
    }

    @Test
    public void MainLearningTest3() {
        System.out.println(" *** Main Learning Test 3 ***");
        TrainingData trainingData = genTrainingData(LEARNING_TEST_DATA_3);
        Interpretation interpretation =
                trainingData.getInterpretationRun().getLastInterpretation();
        Set<TrainingData> trainingDataSet = new HashSet<>();
        trainingDataSet.add(trainingData);
        NewLearner learner = new NewLearner(trainingDataSet);
        learner.learnPatterns(interpretation, STATEMENT_3);
        printPatternSet(learner);
        int resultSize = learner.getTransductionPatternSet().getSet().size();
        Assert.assertTrue(resultSize > 0 && resultSize < 20 );
    }


    //==================================================================
    // Training Tests
    //==================================================================

    @Test
    public void TrainingTest1() {

        System.out.println(" *** Training Test 1 ***");

        Set<TrainingData> trainingDataSet = genTrainingDataSet(TRAINING_DATA_01);
        NewLearner learner = new NewLearner(trainingDataSet);

        for(TrainingData trainingData: trainingDataSet) {
            learner.learnPatterns(trainingData);
            TransPatternSet result = learner.getTransductionPatternSet();
            System.out.println();
            System.out.println("<Training Step> " + trainingData.getRefString());
            System.out.println(" -- Pattern Set Size: " + result.getSet().size());
            //for(TransPattern p: result.getSet()) System.out.println(p.toString());
        }

        TransPatternSet result = learner.getTransductionPatternSet();
        System.out.println();
        System.out.println("<<<Final Training Results>>>");
        System.out.println(" -- Pattern Set Size: " + result.getSet().size());
        System.out.println(" -- Pattern Set: " + result.getSet().size());
        for(TransPattern p: result.getSet())
            System.out.println(" --- " + p.getQuery().toString() + "  |  " +
                    p.getTemplate().getRawTemplateString() + "  |  " +
                    "PPI: " + p.getPrecisionFirstLine());
        //System.out.println(result.toString());
        //for(TransPattern p: result.getSet()) System.out.println(p.toString());
        Assert.assertTrue(result.getSet().size() > 0 );

    }

    @Test
    public void TrainingTest2() {

        System.out.println(" *** Training Test 2 ***");

        Set<TrainingData> trainingDataSet = genTrainingDataSet(TRAINING_DATA_01);
        NewLearner learner = new NewLearner(trainingDataSet);

        learner.learnPatternsOld();

        TransPatternSet result = learner.getTransductionPatternSet();
        System.out.println();
        System.out.println("<<<Final Training Results>>>");
        System.out.println(" -- Pattern Set Size: " + result.getSet().size());
        System.out.println(" -- Pattern Set: " + result.getSet().size());
        for(TransPattern p: result.getSet())
            System.out.println(" --- " + p.getQuery().toString() + "  |  " +
                    p.getTemplate().getRawTemplateString() + "  |  " +
                    "PPI: " + p.getPrecisionFirstLine());
        //System.out.println(result.toString());
        //for(TransPattern p: result.getSet()) System.out.println(p.toString());
        Assert.assertTrue(result.getSet().size() > 0 );

    }

    @Test
    public void TrainingTest3() {

        System.out.println(" *** Training Test 3 ***");

        Set<TrainingData> trainingDataSet = genTrainingDataSet(TRAINING_DATA_01);
        NewLearner learner = new NewLearner(trainingDataSet);

        learner.learnPatterns();

        TransPatternSet result = learner.getTransductionPatternSet();
        System.out.println();
        System.out.println("<<<Final Training Results>>>");
        System.out.println(" -- Pattern Set Size: " + result.getSet().size());
        System.out.println(" -- Pattern Set: " + result.getSet().size());
        for(TransPattern p: result.getSet())
            System.out.println(" --- " + p.getQuery().toString() + "  |  " +
                    p.getTemplate().getRawTemplateString() + "  |  " +
                    "PPI: " + p.getPrecisionFirstLine());
        //System.out.println(result.toString());
        //for(TransPattern p: result.getSet()) System.out.println(p.toString());
        Assert.assertTrue(result.getSet().size() > 0 );

    }

}
