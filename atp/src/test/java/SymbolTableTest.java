import LogicalQuery.Atom;
import Parameters.Category;
import SourceAnalyzer.Amr.Classifier;
import SourceAnalyzer.SymbolTable.Symbol;
import SourceAnalyzer.SymbolTable.Role;
import SourceAnalyzer.SymbolTable.Table;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SymbolTableTest {

    //==================================================================
    // Useful test data
    //==================================================================

    private Classifier classifier = new Classifier();


    //==================================================================
    // Tests: class Role
    //==================================================================

    @Test
    public void RoleTest() {

        // Test Data
        Role r1, r2;

        // Test Code
        r1 = new Role("A1", "x4");
        System.out.println("r1: " + r1.toString());
        r2 = new Role("polarity", "x6");
        System.out.println("r2: " + r2.toString());

        // Asserts
        Assert.assertEquals("(A1, x4)", r1.toString());
        Assert.assertEquals("(polarity, x6)", r2.toString());

    }

    //==================================================================
    // Tests: class Symbol
    //==================================================================

    @Test
    public void SymbolTest1() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Symbol v1, v2, v3, v4, v5, v6, v7, v8, v9;
        List<Role> rList;

        // Test Code
        v1 = new Symbol("iTest", Category.ENTITY);
        System.out.println("v1: " + v1.toString());
        rList = new ArrayList<>();
        rList.add(new Role("A1", "x2"));
        v2 = new Symbol("x1", Category.UNKNOWN, "possible", rList);
        System.out.println("v2: " + v2.toString());
        v3 = new Symbol("x1", Category.MODALITY, "possible", rList);
        System.out.println("v3: " + v3.toString());
        rList = new ArrayList<>();
        rList.add(new Role("A0", "x3"));
        rList.add(new Role("A1", "x4"));
        rList.add(new Role("condition", "x5"));
        v4 = new Symbol("x2", Category.PROPERTY, "focus", rList);
        System.out.println("v4: " + v4.toString());
        v5 = new Symbol("x3", Category.ENTITY, "reflector");
        System.out.println("v5: " + v5.toString());
        v6 = new Symbol("x4", Category.ENTITY, "sunlight");
        System.out.println("v6: " + v6.toString());
        rList = new ArrayList<>();
        rList.add(new Role("polarity", "x6"));
        rList.add(new Role("A0", "x7"));
        v7 = new Symbol("x5", Category.PROPERTY, "clear", rList);
        System.out.println("v7: " + v7.toString());
        v8 = new Symbol("x6", Category.LOGIC_CONNECTOR, "-");
        System.out.println("v8: " + v8.toString());
        v9 = new Symbol("x7", Category.ENTITY, "sky");
        System.out.println("v9: " + v9.toString());

        // Asserts
        Assert.assertEquals("(iTest, ***No Concept***, [], ENTITY)", v1.toString());
        Assert.assertEquals("(x1, possible, [(A1, x2)], UNKNOWN)", v2.toString());
        Assert.assertEquals("(x1, possible, [(A1, x2)], MODALITY)", v3.toString());
        Assert.assertEquals("(x2, focus, [(A0, x3), (A1, x4), (condition, x5)], PROPERTY)", v4.toString());
        Assert.assertEquals("(x3, reflector, [], ENTITY)", v5.toString());
        Assert.assertEquals("(x4, sunlight, [], ENTITY)", v6.toString());
        Assert.assertEquals("(x5, clear, [(polarity, x6), (A0, x7)], PROPERTY)", v7.toString());
        Assert.assertEquals("(x6, -, [], LOGIC_CONNECTOR)", v8.toString());
        Assert.assertEquals("(x7, sky, [], ENTITY)", v9.toString());

    }

    @Test
    public void SymbolTest2() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Symbol v1, v2;
        List<Role> rList;

        // Test Code
        rList = new ArrayList<>();
        rList.add(new Role("ARG1", "x2"));
        v1 = new Symbol("x1", Category.UNKNOWN, "possible");
        System.out.println("v1: " + v1.toString());
        v2 = new Symbol("x1", Category.UNKNOWN, "possible");
        v2.setCategory(Category.MODALITY);
        v2.setRoleList(rList);
        System.out.println("v2: " + v2.toString());

        // Asserts
        Assert.assertEquals("(x1, possible, [], UNKNOWN)", v1.toString());
        Assert.assertEquals("(x1, possible, [(ARG1, x2)], MODALITY)", v2.toString());

    }

    @Test
    public void SymbolTest3() {

        // Test Code
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("name", "Pierre"));
        rList0.add(new Role("type", "Ciseaux"));
        Symbol v1 = new Symbol("x", Category.UNKNOWN, "test-role", rList0);
        System.out.println("v1: " + v1.toString());
        boolean b0 = v1.hasPostCoreRole(0);
        boolean b1 = v1.hasPostCoreRole(1);
        boolean b2 = v1.hasPostCoreRole(2);
        Role r0 = v1.getPostCoreRole(0).get(0);
        Role r1 = v1.getPostCoreRole(1).get(0);
        List<Role> rList2 = v1.getPostCoreRole(2);
        System.out.println("r0: " + b0 + ", " + r0.toString() + ", " + r0.getRoleId() + ", " + r0.getInstanceId());
        System.out.println("r1: " + b1 + ", " + r1.toString() + ", " + r1.getRoleId() + ", " + r1.getInstanceId());
        System.out.println("r2: " + b2 + ", " + rList2.toString());

        // Asserts
        Assert.assertEquals("(x, test-role, [(ARG0, x0), (ARG1, x2), (name, Pierre), (type, Ciseaux)], UNKNOWN)", v1.toString());
        Assert.assertTrue(b0);
        Assert.assertTrue(b1);
        Assert.assertFalse(b2);
        Assert.assertEquals("(ARG0, x0)", r0.toString());
        Assert.assertEquals("(ARG1, x2)", r1.toString());
        Assert.assertTrue(rList2.isEmpty());
        Assert.assertEquals("ARG0", r0.getRoleId());
        Assert.assertEquals("x0", r0.getInstanceId());

    }

    @Test
    public void SymbolTest4() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Symbol v1;
        List<Role> rList0, rList1, rList2, rList3;
        Role r0, r1, r2;

        // Test Code
        rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("name", "Pierre"));
        rList0.add(new Role("type", "Ciseaux"));
        v1 = new Symbol("x", Category.UNKNOWN, "test-role", rList0);
        System.out.println("v1: " + v1.toString());
        rList1 = v1.getRoleList();
        System.out.println("rList1: " + rList1.toString());
        rList2 = v1.getCoreRoleList();
        System.out.println("rList2: " + rList2.toString());
        rList3 = v1.getSpecificRoleList();
        System.out.println("rList3: " + rList3.toString());

        // Asserts
        Assert.assertEquals("(x, test-role, [(ARG0, x0), (ARG1, x2), (name, Pierre), (type, Ciseaux)], UNKNOWN)", v1.toString());
        Assert.assertEquals("[(ARG0, x0), (ARG1, x2), (name, Pierre), (type, Ciseaux)]", rList1.toString());
        Assert.assertEquals("[(ARG0, x0), (ARG1, x2)]", rList2.toString());
        Assert.assertEquals("[(name, Pierre), (type, Ciseaux)]", rList3.toString());

    }

    @Test
    public void SymbolTest5() {

        // Test Data
        Symbol v1;
        List<Role> rList0, rList1, rList2, rList3;

        // Test Code
        rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("ARG1", "x3"));
        rList0.add(new Role("name", "Pierre"));
        rList0.add(new Role("type", "Ciseaux"));
        v1 = new Symbol("x", Category.UNKNOWN, "test-role", rList0);
        System.out.println("v1: " + v1.toString());
        rList1 = v1.getCoreRoleList();
        System.out.println("rList1: " + rList1.toString());
        rList2 = v1.getPreCoreRoleList();
        System.out.println("rList2: " + rList2.toString());
        rList3 = v1.getPostCoreRoleList();
        System.out.println("rList3: " + rList3.toString());

        // Asserts
        Assert.assertEquals("(x, test-role, [(ARG0, x0), (ARG1, x2), (ARG1, x3), (name, Pierre), (type, Ciseaux)], UNKNOWN)", v1.toString());
        Assert.assertEquals("[(ARG0, x0), (ARG1, x2), (ARG1, x3)]", rList1.toString());
        Assert.assertEquals("[]", rList2.toString());
        Assert.assertEquals("[(ARG0, x0), (ARG1, x2), (ARG1, x3)]", rList3.toString());

    }

    @Test
    public void SymbolTest6() {

        // Test Code
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("ARG1-of", "y2"));
        rList0.add(new Role("ARG1", "x3"));
        rList0.add(new Role("name", "Jean"));
        rList0.add(new Role("name", "Pierre"));
        rList0.add(new Role("type", "Ciseaux"));
        Symbol v1 = new Symbol("x", Category.UNKNOWN, "test-role", rList0);
        System.out.println("v1: " + v1.toString());
        List<Role> rList1 = v1.getCoreRoleList();
        System.out.println("rList1: " + rList1.toString());
        List<Role> rList2 = v1.getPreCoreRoleList();
        System.out.println("rList2: " + rList2.toString());
        List<Role> rList3 = v1.getPostCoreRoleList();
        System.out.println("rList3: " + rList3.toString());

        // Asserts
        Assert.assertEquals("[(ARG0, x0), (ARG1, x2), (ARG1-of, y2), (ARG1, x3)]", rList1.toString());
        Assert.assertEquals("[(ARG1-of, y2)]", rList2.toString());
        Assert.assertEquals("[(ARG0, x0), (ARG1, x2), (ARG1, x3)]", rList3.toString());

    }

    @Test
    public void SymbolTest7() {

        // Test Code
        List<Role> rList0 = new ArrayList<>();
        rList0.add(new Role("ARG0", "x0"));
        rList0.add(new Role("ARG1", "x2"));
        rList0.add(new Role("ARG1-of", "y2"));
        rList0.add(new Role("ARG1", "x3"));
        rList0.add(new Role("name", "Jean"));
        rList0.add(new Role("name", "Pierre"));
        rList0.add(new Role("type", "Ciseaux"));
        Symbol v1 = new Symbol("x", Category.UNKNOWN, "test-role", rList0);
        System.out.println("v1: " + v1.toString());
        List<Role> rList1 = v1.getSpecificRoleList();
        System.out.println("rList1: " + rList1.toString());;
        List<Role> rList2 = v1.getSpecificRole("name");
        System.out.println("rList2: " + rList2.toString());

        // Asserts
        Assert.assertEquals("[(name, Jean), (name, Pierre), (type, Ciseaux)]", rList1.toString());
        Assert.assertEquals("[(name, Jean), (name, Pierre)]", rList2.toString());

    }


    //==================================================================
    // Tests: class Classifier
    //==================================================================

    @Test
    public void ClassifierTest1() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        List<Role> rList;
        Symbol v1, v2, v3, v4, v5, v6, v7, v8;
        Table t1;
        Category c1, c2, c3, c4, c5, c6, c7, c8;

        // Construction of Symbol Table
        rList = new ArrayList<>();
        rList.add(new Role("ARG1", "x2"));
        v1 = new Symbol("x1", Category.MODALITY, "possible-01", rList);
        rList = new ArrayList<>();
        rList.add(new Role("ARG0", "x3"));
        rList.add(new Role("ARG1", "x4"));
        rList.add(new Role("condition", "x5"));
        v2 = new Symbol("x2", Category.PROPERTY, "focus", rList);
        v3 = new Symbol("x3", Category.ENTITY, "reflector");
        v4 = new Symbol("x4", Category.ENTITY, "sunlight");
        rList = new ArrayList<>();
        rList.add(new Role("polarity", "x6"));
        rList.add(new Role("ARG0", "x7"));
        v5 = new Symbol("x5", Category.PROPERTY, "clear", rList);
        v6 = new Symbol("x6", Category.LOGIC_CONNECTOR, "-");
        v7 = new Symbol("x7", Category.ENTITY, "sky");
        v8 = new Symbol("x8", Category.TEMPORALITY, "after");
        t1 = new Table("test");
        t1.add(v1);
        t1.add(v2);
        t1.add(v3);
        t1.add(v4);
        t1.add(v5);
        t1.add(v6);
        t1.add(v7);
        t1.add(v8);
        System.out.print(t1.toString());

        // Test Code
        c1 = classifier.evalCategory(t1.lookup("x1"));
        c2 = classifier.evalCategory(t1.lookup("x2"));
        c3 = classifier.evalCategory(t1.lookup("x3"));
        c4 = classifier.evalCategory(t1.lookup("x4"));
        c5 = classifier.evalCategory(t1.lookup("x5"));
        c6 = classifier.evalCategory(t1.lookup("x6"));
        c7 = classifier.evalCategory(t1.lookup("x7"));
        c8 = classifier.evalCategory(t1.lookup("x8"));

        // Asserts
        Assert.assertEquals("MODALITY", c1.toString());
        Assert.assertEquals("PROPERTY", c2.toString());
        Assert.assertEquals("ENTITY", c3.toString());
        Assert.assertEquals("ENTITY", c4.toString());
        Assert.assertEquals("PROPERTY", c5.toString());
        Assert.assertEquals("LOGIC_CONNECTOR", c6.toString());
        Assert.assertEquals("ENTITY", c7.toString());
        Assert.assertEquals("TEMPORALITY", c8.toString());

    }

    @Test
    public void ClassifierTest2() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        List<Role> rList;
        Symbol v1, v2, v3, v4, v5, v6, v7, v8;
        Table t1;
        Category c1, c2, c3, c4, c5, c6, c7, c8;

        // Construction of Symbol Table
        rList = new ArrayList<>();
        rList.add(new Role("ARG1", "x2"));
        v1 = new Symbol("x1", Category.UNKNOWN, "possible-01", rList);
        rList = new ArrayList<>();
        rList.add(new Role("ARG0", "x3"));
        rList.add(new Role("ARG1", "x4"));
        rList.add(new Role("condition", "x5"));
        v2 = new Symbol("x2", Category.UNKNOWN, "focus", rList);
        v3 = new Symbol("x3", Category.UNKNOWN, "reflector");
        v4 = new Symbol("x4", Category.UNKNOWN, "sunlight");
        rList = new ArrayList<>();
        rList.add(new Role("polarity", "x6"));
        rList.add(new Role("ARG0", "x7"));
        v5 = new Symbol("x5", Category.UNKNOWN, "clear", rList);
        v6 = new Symbol("x6", Category.UNKNOWN, "-");
        v7 = new Symbol("x7", Category.UNKNOWN, "sky");
        v8 = new Symbol("x8", Category.UNKNOWN, "after");
        t1 = new Table("test");
        t1.add(v1);
        t1.add(v2);
        t1.add(v3);
        t1.add(v4);
        t1.add(v5);
        t1.add(v6);
        t1.add(v7);
        t1.add(v8);
        System.out.print(t1.toString());

        // Test Code
        c1 = classifier.evalCategory(t1.lookup("x1"));
        c2 = classifier.evalCategory(t1.lookup("x2"));
        c3 = classifier.evalCategory(t1.lookup("x3"));
        c4 = classifier.evalCategory(t1.lookup("x4"));
        c5 = classifier.evalCategory(t1.lookup("x5"));
        c6 = classifier.evalCategory(t1.lookup("x6"));
        c7 = classifier.evalCategory(t1.lookup("x7"));
        c8 = classifier.evalCategory(t1.lookup("x8"));

        // Asserts
        Assert.assertEquals("MODALITY", c1.toString());
        Assert.assertEquals("PROPERTY", c2.toString());
        Assert.assertEquals("ENTITY", c3.toString());
        Assert.assertEquals("ENTITY", c4.toString());
        Assert.assertEquals("PROPERTY", c5.toString());
        Assert.assertEquals("LOGIC_CONNECTOR", c6.toString());
        Assert.assertEquals("ENTITY", c7.toString());
        Assert.assertEquals("TEMPORALITY", c8.toString());

    }


    //==================================================================
    // Tests: class Table
    //==================================================================

    @Test
    public void TableTest() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        List<Role> rList;
        Symbol v1, v2, v3, v4, v5, v6, v7;
        Table t1;

        // Test Code
        rList = new ArrayList<>();
        rList.add(new Role("A1", "x2"));
        v1 = new Symbol("x1", Category.MODALITY, "possible", rList);
        rList = new ArrayList<>();
        rList.add(new Role("A0", "x3"));
        rList.add(new Role("A1", "x4"));
        rList.add(new Role("condition", "x5"));
        v2 = new Symbol("x2", Category.PROPERTY, "focus", rList);
        v3 = new Symbol("x3", Category.ENTITY, "reflector");
        v4 = new Symbol("x4", Category.ENTITY, "sunlight");
        rList = new ArrayList<>();
        rList.add(new Role("polarity", "x6"));
        rList.add(new Role("A0", "x7"));
        v5 = new Symbol("x5", Category.PROPERTY, "clear", rList);
        v6 = new Symbol("x6", Category.LOGIC_CONNECTOR, "-");
        v7 = new Symbol("x7", Category.ENTITY, "sky");
        t1 = new Table("test");
        t1.add(v1);
        t1.add(v2);
        t1.add(v3);
        t1.add(v4);
        t1.add(v5);
        t1.add(v6);
        t1.add(v7);
        System.out.print(t1.toString());

        // Asserts
        Assert.assertEquals("<test> Symbol Table:\n" +
                "  (x1, possible, [(A1, x2)], MODALITY)\n" +
                "  (x2, focus, [(A0, x3), (A1, x4), (condition, x5)], PROPERTY)\n" +
                "  (x3, reflector, [], ENTITY)\n" +
                "  (x4, sunlight, [], ENTITY)\n" +
                "  (x5, clear, [(polarity, x6), (A0, x7)], PROPERTY)\n" +
                "  (x6, -, [], LOGIC_CONNECTOR)\n" +
                "  (x7, sky, [], ENTITY)\n", t1.toString());

    }

    @Test
    public void CategorizeTableTest() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        List<Role> rList;
        Symbol v1, v2, v3, v4, v5, v6, v7, v8;
        Table t1;

        // Construction of Symbol Table
        rList = new ArrayList<>();
        rList.add(new Role("ARG1", "x2"));
        v1 = new Symbol("x1", Category.UNKNOWN, "possible-01", rList);
        rList = new ArrayList<>();
        rList.add(new Role("ARG0", "x3"));
        rList.add(new Role("ARG1", "x4"));
        rList.add(new Role("condition", "x5"));
        v2 = new Symbol("x2", Category.UNKNOWN, "focus", rList);
        v3 = new Symbol("x3", Category.UNKNOWN, "reflector");
        v4 = new Symbol("x4", Category.UNKNOWN, "sunlight");
        rList = new ArrayList<>();
        rList.add(new Role("polarity", "x6"));
        rList.add(new Role("ARG0", "x7"));
        v5 = new Symbol("x5", Category.UNKNOWN, "clear", rList);
        v6 = new Symbol("x6", Category.UNKNOWN, "-");
        v7 = new Symbol("x7", Category.UNKNOWN, "sky");
        v8 = new Symbol("x8", Category.UNKNOWN, "after");
        t1 = new Table("test");
        t1.add(v1);
        t1.add(v2);
        t1.add(v3);
        t1.add(v4);
        t1.add(v5);
        t1.add(v6);
        t1.add(v7);
        t1.add(v8);
        System.out.print("< Table before categorizing >\n" + t1.toString());

        // Construction of Symbol Table
        t1.categorize(classifier);
        System.out.print("< Table after categorizing >\n" + t1.toString());

        // Asserts
        Assert.assertEquals("<test> Symbol Table:\n" +
                "  (x8, after, [], TEMPORALITY)\n" +
                "  (x1, possible-01, [(ARG1, x2)], MODALITY)\n" +
                "  (x2, focus, [(ARG0, x3), (ARG1, x4), (condition, x5)], PROPERTY)\n" +
                "  (x3, reflector, [], ENTITY)\n" +
                "  (x4, sunlight, [], ENTITY)\n" +
                "  (x5, clear, [(polarity, x6), (ARG0, x7)], PROPERTY)\n" +
                "  (x6, -, [], LOGIC_CONNECTOR)\n" +
                "  (x7, sky, [], ENTITY)\n", t1.toString());

    }

    @Test
    public void AtomListFromSymbolTableTest() {

        // Test Data
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        List<Role> rList;
        Symbol v1, v2, v3, v4, v5, v6, v7, v8, v9;
        Table t1;
        List<Atom> atomList;

        // Construction of Symbol Table
        rList = new ArrayList<>();
        rList.add(new Role("ARG1", "x2"));
        v1 = new Symbol("x1", Category.UNKNOWN, "possible-01", rList);
        rList = new ArrayList<>();
        rList.add(new Role("ARG0", "x3"));
        rList.add(new Role("ARG1", "x4"));
        rList.add(new Role("condition", "x5"));
        v2 = new Symbol("x2", Category.UNKNOWN, "focus", rList);
        v3 = new Symbol("x3", Category.UNKNOWN, "reflector");
        v4 = new Symbol("x4", Category.UNKNOWN, "sunlight");
        rList = new ArrayList<>();
        rList.add(new Role("polarity", "x6"));
        rList.add(new Role("ARG0", "x7"));
        v5 = new Symbol("x5", Category.UNKNOWN, "clear", rList);
        v6 = new Symbol("x6", Category.UNKNOWN, "-");
        v7 = new Symbol("x7", Category.UNKNOWN, "sky");
        v8 = new Symbol("x8", Category.UNKNOWN, "after");
        v9 = new Symbol("x9", Category.UNKNOWN, "before");
        t1 = new Table("test");
        t1.add(v1);
        t1.add(v2);
        t1.add(v3);
        t1.add(v4);
        t1.add(v5);
        t1.add(v6);
        t1.add(v7);
        t1.add(v8);
        t1.add(v9);
        t1.categorize(classifier);
        System.out.print("< Table >\n" + t1.toString());

        // List of atoms from symbol table
        atomList = t1.getAtomList(classifier);
        System.out.print("< Atom List >\n" + atomList.toString());

        // Asserts
        Assert.assertEquals("[" +
                        "TEMPORALITY(x8), " +
                        "TEMPORALITY(x9), " +
                        "ARG1(x1, x2), " +
                        "MODALITY(x1), " +
                        "ARG0(x2, x3), " +
                        "ARG1(x2, x4), " +
                        "condition(x2, x5), " +
                        "PROPERTY(x2), " +
                        "ENTITY(x3), " +
                        "ENTITY(x4), " +
                        "polarity(x5, x6), " +
                        "ARG0(x5, x7), " +
                        "PROPERTY(x5), " +
                        "LOGIC_CONNECTOR(x6), " +
                        "ENTITY(x7)]",
                atomList.toString());

    }


}
